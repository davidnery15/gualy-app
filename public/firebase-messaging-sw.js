// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
// import '@firebase/messaging'
// import '@firebase/init'
// import firebase from 'firebase/app'
const PROD = false
// import image from './app_icon1024pt.png'
// let messaging = firebase.messaging()
// console.log("PROD:: ", PROD)
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
const config = {
  apiKey: "AIzaSyCt5mxGdtaMEzPxHIzs675RvuUQaJs4mvo",
  authDomain: "gualy-b39cb.firebaseapp.com",
  databaseURL: "https://gualy-b39cb.firebaseio.com",
  projectId: "gualy-b39cb",
  storageBucket: "gualy-b39cb.appspot.com",
  messagingSenderId: "824355529309"
}

const devConfig = {
  apiKey: "AIzaSyB3dYWITHYydWTk-OLBvGNj_qOr5JFztqE",
  authDomain: "gualy-dev.firebaseapp.com",
  databaseURL: "https://gualy-dev.firebaseio.com",
  projectId: "gualy-dev",
  storageBucket: "gualy-dev.appspot.com",
  messagingSenderId: "990374764736"
}
firebase.initializeApp( PROD ? config : devConfig )

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.

 // [START initialize_firebase_in_sw]
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here, other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

 // Initialize the Firebase app in the service worker by passing in the
 // messagingSenderId.
 firebase.initializeApp({
   'messagingSenderId': 'YOUR-SENDER-ID'
 });

 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 // [END initialize_firebase_in_sw]
 **/


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler( payload => {
  console.log('[firebase-messaging-sw.js] Received background message ', payload)
  // Customize notification here
  const notificationTitle = payload.notification.title
  const notificationOptions = {
    body: payload.notification.body,
    badge: "http://gualy.com/wp-content/uploads/2018/07/app_icon-300x300.png",
    icon: "http://gualy.com/wp-content/uploads/2018/07/app_icon-300x300.png",
    // Star Wars shamelessly taken from the awesome Peter Beverloo
    // https://tests.peter.sh/notification-generator/
    //prove when its on dev and on an divice
    // vibrate: [500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500]
  };
  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
// [END background_handler]