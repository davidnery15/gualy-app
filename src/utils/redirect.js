import React from 'react'
import { Redirect } from 'react-router'

const redirect = props => {
  const { location, userInCollectionData, isLoggedIn } = props
  // console.log('props at redirect', props)
  if (isLoggedIn && userInCollectionData) {
    const { type, isPasswordSettedByUser } = userInCollectionData
    // console.log('logged as ' + type)
    switch (type) {
      case 'admin':
        return <Redirect to='/dashboard-admin' />
      case 'support':
        return <Redirect to='/incidencias' />
      case 'cashier':
        return <Redirect to='/transacciones' />
      case 'marketing':
        return <Redirect to='/notificaciones' />
      case 'commerce':
        // console.log('isPasswordSettedByUser ', isPasswordSettedByUser)
        if (isPasswordSettedByUser) {
          return <Redirect to='/dashboard' />
        } else {
          return <Redirect to='/nueva-contrasena' />
        }
      case 'client':
        return <Redirect to='/dashboard' />
      default:
        // console.log('redirect null')
      return null
    }
  } else if (location.pathname  === '/') {
    // console.log('location.pathname ', location.pathname)
    return null
  } else {
    // console.log('redirected to login')
    return <Redirect to='/' />
  }
}

export default redirect