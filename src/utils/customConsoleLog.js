import {CONSOLE_LOGS} from '../config/index'
export const customConsoleLog = (string, value) => {
  if (CONSOLE_LOGS) {
    console.log(`${string}`, value)
  } 
}