import { banks } from './../constants/bankLogos'
const iconReject = require('./../assets/help-icons/icon-bank-blue.svg')

const getBankLogo = ( bankAccountNumber ) => {
  const bankId = bankAccountNumber ? bankAccountNumber.slice(0, 4) : ""
  const filteredLogo = banks.filter(item => item.id === bankId)[0]
  const filteredLogoPhoto = filteredLogo ? filteredLogo.photo : iconReject
  return filteredLogoPhoto
}

export default getBankLogo