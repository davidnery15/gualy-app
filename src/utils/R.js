
import values from 'ramda/src/values'
import filter from 'ramda/src/filter'
import compose from 'ramda/src/compose'



export default {
    values,
    filter,
    compose
}