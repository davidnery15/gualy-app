export const timestampParser = (date) => new Date(date).getTime()
