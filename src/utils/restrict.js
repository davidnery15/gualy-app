export const restrictTo = (value) => (lenght) => value.slice(0, lenght)

export const removeNonNumbers = (number) => {
    const amountString = number
      .toString()
      .split('')
      .filter(chr => !chr.match(/^([^0-9]*)$/))
      .join('')
    return amountString === "" ? "" : Number(amountString) / 100
  }