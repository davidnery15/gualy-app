import { CLOUD_FUNCTIONS_URL } from '../config/urls'
import axios from 'axios'
import store from '../redux/store'

const createAxiosRequest = method => {
  return async (name, data) => await axios[method](
    `${CLOUD_FUNCTIONS_URL}/${name}`,
    data,
    {
      headers: {
        Authorization: `Bearer ${store.getState().general.accessToken}`
      }
    }
  )
}
  
export const postRequest = createAxiosRequest('post')
export const getRequest = createAxiosRequest('get')

export default createAxiosRequest
