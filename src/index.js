import React from 'react'
import ReactDOM from 'react-dom'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import './theme/theme.css'
import { postRequest } from './utils/createAxiosRequest'
import store, { history } from './redux/store'

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
window.addEventListener('beforeinstallprompt', event => {
  // beforeinstallprompt Event fired

  // e.userChoice will return a Promise. 
  // For more details read: https://developers.google.com/web/fundamentals/getting-started/primers/promises
  try {
    event.userChoice.then( async choiceResult => {
      console.log('choiceResult.outcome: ', choiceResult.outcome);
      const userDownloadPWARes = await postRequest('reports/userDownloadPWA', {
        userChoice: choiceResult.outcome
      })
        // .then(res => res.toJson())
      console.log('Registered on before install prompt res: ', userDownloadPWARes)
    })
  }
  catch (error) {
    console.error('Error installing pwa', error)
  } 
})
if (module.hot) {
  module.hot.accept();
}
registerServiceWorker()

