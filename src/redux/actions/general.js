import { auth, db } from '../../firebase'
import firebase from 'firebase/app'
import R from '../../utils/R'
// import { deleteToken } from '../../push-notification'
import { customConsoleLog } from '../../utils/customConsoleLog'
const filterSnap = R.compose(
  R.filter(data => data.type === 'commerce' && !data.blocked && !data.hidden),
  R.values,
)
// REGISTER
export const USER_REGISTER_STARTED = 'USER_REGISTER_STARTED'
export const USER_REGISTER_DONE = 'USER_REGISTER_DONE'
export const USER_REGISTER_ERROR = 'USER_REGISTER_ERROR'
//GET USER INFO
export const GET_USER_INFO = 'GET_USER_INFO'
// LOGIN
export const USER_LOGIN_STARTED = 'USER_LOGIN_STARTED'
export const USER_LOGIN_DONE = 'USER_LOGIN_DONE'
export const USER_LOGIN_ERROR = 'USER_LOGIN_ERROR'
//LOGOUT
export const USER_LOGOUT = 'USER_LOGOUT'
// GET COMMERCES
export const GET_COMMERCES_STARTED = 'GET_COMMERCES_STARTED'
export const GET_COMMERCES_DONE = 'GET_COMMERCES_DONE'
export const GET_COMMERCES_ERROR = 'GET_COMMERCES_ERROR'
// APPROVED HISTORY TRANSACTIONS
export const GET_APPROVED_TRANSACTION_HISTORY_STARTED = 'GET_APPROVED_TRANSACTION_HISTORY_STARTED'
export const GET_APPROVED_TRANSACTION_HISTORY_DONE = 'GET_APPROVED_TRANSACTION_HISTORY_DONE'
export const GET_APPROVED_TRANSACTION_HISTORY_ERROR = 'GET_APPROVED_TRANSACTION_HISTORY_ERROR'
// REJECTED HISTORY TRANSACTIONS
export const GET_REJECTED_TRANSACTION_HISTORY_STARTED = 'GET_REJECTED_TRANSACTION_HISTORY_STARTED'
export const GET_REJECTED_TRANSACTION_HISTORY_DONE = 'GET_REJECTED_TRANSACTION_HISTORY_DONE'
export const GET_REJECTED_TRANSACTION_HISTORY_ERROR = 'GET_REJECTED_TRANSACTION_HISTORY_ERROR'
// PENDING HISTORY TRANSACTIONS
export const GET_PENDING_TRANSACTION_HISTORY_STARTED = 'GET_PENDING_TRANSACTION_HISTORY_STARTED'
export const GET_PENDING_TRANSACTION_HISTORY_DONE = 'GET_PENDING_TRANSACTION_HISTORY_DONE'
export const GET_PENDING_TRANSACTION_HISTORY_ERROR = 'GET_PENDING_TRANSACTION_HISTORY_ERROR'
// MONEY REQUEST TRANSACTIONS
export const GET_MONEY_REQUEST_TRANSACTIONS_STARTED = 'GET_MONEY_REQUEST_TRANSACTIONS_STARTED'
export const GET_MONEY_REQUEST_TRANSACTIONS_DONE = 'GET_MONEY_REQUEST_TRANSACTIONS_DONE'
export const GET_MONEY_REQUEST_TRANSACTIONS_ERROR = 'GET_MONEY_REQUEST_TRANSACTIONS_ERROR'
// REALIZED TRANSACTIONS
export const GET_REALIZED_TRANSACTIONS_STARTED = 'GET_REALIZED_TRANSACTIONS_STARTED'
export const GET_REALIZED_TRANSACTIONS_DONE = 'GET_REALIZED_TRANSACTIONS_DONE'
export const GET_REALIZED_TRANSACTIONS_ERROR = 'GET_REALIZED_TRANSACTIONS_ERROR'
// GET WITHDRAWS
export const GET_WITHDRAWS_STARTED = 'GET_WITHDRAWS_STARTED'
export const GET_WITHDRAWS_DONE = 'GET_WITHDRAWS_DONE'
export const GET_WITHDRAWS_ERROR = 'GET_WITHDRAWS_ERROR'
// GET WITHDRAWS
export const GET_DEPOSITS_STARTED = 'GET_DEPOSITS_STARTED'
export const GET_DEPOSITS_DONE = 'GET_DEPOSITS_DONE'
export const GET_DEPOSITS_ERROR = 'GET_DEPOSITS_ERROR'
// GET NOTIFICATIONS HISTORY
export const GET_NOTIFICATIONS_HISTORY_STARTED = 'GET_NOTIFICATIONS_HISTORY_STARTED'
export const GET_NOTIFICATIONS_HISTORY_DONE = 'GET_NOTIFICATIONS_HISTORY_DONE'
export const GET_NOTIFICATIONS_HISTORY_ERROR = 'GET_NOTIFICATIONS_HISTORY_ERROR'
//----GENERAL ACTIONS----
//REGISTER USER
export const registerUserEmailPass = ({ email, password, ...userInfo }) => (dispatch) => {
  dispatch({ type: USER_REGISTER_STARTED })
  auth.createUserWithEmailAndPassword(email, password)
    .then((user) => {
      user.sendEmailVerification().then(() => {
        dispatch({
          type: USER_REGISTER_DONE,
          payload: { email, authCheck: true }
        })
      }).catch((error) => {
        // customConsoleLog("error sending email verification", error)
      })
    })
    .then(() => {
      const uid = auth.currentUser.uid
      db.ref(`users/${uid}`).set({
        email,
        password,
        ...userInfo
      }).catch(error => {
        // customConsoleLog("Database: ", error)
      })
    })
    .catch(error => dispatch({
      type: USER_REGISTER_ERROR,
      error
    }))
}
//GET USER INFO
export const getUserInfo = (uid) => {
  return async (dispatch) => {
    await db.ref('users').child(uid).on('value', (snap) => {
      if (snap.val()) {
        dispatch({
          type: GET_USER_INFO,
          payload: snap.val()
          ,
          response: true
        })
      } else {
        dispatch({ type: GET_USER_INFO, response: false })
      }
    })
  }
}
//LOGIN
export const loginUserEmailPass = ({ email, password }) => {
  return async (dispatch) => {
    dispatch({ type: USER_LOGIN_STARTED })
    try {
      await auth.setPersistence(firebase.auth.Auth.Persistence.SESSION)
      const response = await auth.signInWithEmailAndPassword(email.trim(), password.trim())
      if (response.user.emailVerified) {
        auth.currentUser.getIdToken(true).then(idToken => {
          dispatch({
            type: USER_LOGIN_DONE,
            payload: {
              user: response.user,
              accessToken: idToken
            }
          })
        })
      } else {
        dispatch({
          type: USER_LOGIN_ERROR,
          payload: {
            code: 'email-not-verified',
            email: 'The email has not been verified'
          },
          error: true,
        })
      }
    } catch (error) {
      // customConsoleLog(error)
      dispatch({
        type: USER_LOGIN_ERROR,
        payload: error,
        error: true,
      })
    }
  }
}
export const userInitialData = ({ email, password }) => (dispatch) => {
  dispatch({ type: USER_LOGIN_STARTED })
  auth.signInWithEmailAndPassword(email, password)
    .then((user) => {
      customConsoleLog("USER: ", user)
      dispatch({
        type: USER_LOGIN_DONE,
        payload: { user }
      })
    })
    .catch(error => dispatch({
      type: USER_LOGIN_ERROR,
      error
    }))
}
//LOGOUT
export const userLogout = () => (dispatch) => {
  // deleteToken()
  dispatch({
    type: USER_LOGOUT,
    payload: {
      isFetching: false,
    }
  })
  auth.signOut()
  // auth.signOut().then((e)=>{ customConsoleLog('Sign Out correctly', e)}, (e)=> { customConsoleLog(e, 'Sign Out Error')})
}
//GET COMMERCES
export const getCommerces = ({ commercesIndex }) => (dispatch) => {
  dispatch({ type: GET_COMMERCES_STARTED })
  const commercesRef = db
    .ref(`users`)
    .orderByChild("type")
    .equalTo("commerce")
    .limitToLast(commercesIndex)
  commercesRef.once('value', snapshot => {
    const commerces = filterSnap(snapshot.val())
    dispatch({
      type: GET_COMMERCES_DONE,
      payload: {
        commerces: commerces,
        loadingCommerces: false,
      }
    })
  }).catch(error => {
    customConsoleLog("error on get commerces: ", error)
    dispatch({
      type: GET_COMMERCES_ERROR,
      payload: { loadingCommerces: false, }
    })
  })
}
//----TRANSACTIONS----
// GET HISTORY TRANSACTIONS  
export const getApprovedHistoryTransactions = (//APPROVED
  {
    userKey,
    transactionHistorylimitIndex,
  }) => (dispatch) => {
    const transactionHistoryRef = db
      .ref(`transactionHistory/${userKey}`)
      .orderByChild("status")
      .equalTo("Approved")
      .limitToLast(transactionHistorylimitIndex)
    dispatch({ type: GET_APPROVED_TRANSACTION_HISTORY_STARTED })
    transactionHistoryRef.once('value', snapshot => {
      const transactionsHistory = R.values(snapshot.val())
      dispatch({
        type: GET_APPROVED_TRANSACTION_HISTORY_DONE,
        payload: {
          approvedTransactionsHistory: transactionsHistory.sort((a, b) => b.timestamp - a.timestamp),
          loadingApprovedTransactions: false,
        }
      })
    }).catch(error => {
      customConsoleLog("error on get transaction history: ", error)
      dispatch({
        type: GET_APPROVED_TRANSACTION_HISTORY_ERROR,
        payload: { loadingApprovedTransactions: false, }
      })
    })
  }
export const getRejectedHistoryTransactions = (//REJECTED
  {
    userKey,
    transactionHistorylimitIndex,
  }) => (dispatch) => {
    const transactionHistoryRef = db
      .ref(`transactionHistory/${userKey}`)
      .orderByChild("status")
      .equalTo("Rejected")
      .limitToLast(transactionHistorylimitIndex)
    dispatch({ type: GET_REJECTED_TRANSACTION_HISTORY_STARTED })
    transactionHistoryRef.once('value', snapshot => {
      const transactionsHistory = R.values(snapshot.val())
      dispatch({
        type: GET_REJECTED_TRANSACTION_HISTORY_DONE,
        payload: {
          rejectedTransactionsHistory: transactionsHistory.sort((a, b) => b.timestamp - a.timestamp),
          loadingRejectedTransactions: false,
        }
      })
    }).catch(error => {
      customConsoleLog("error on get transaction history: ", error)
      dispatch({
        type: GET_REJECTED_TRANSACTION_HISTORY_ERROR,
        payload: { loadingRejectedTransactions: false, }
      })
    })
  }
export const getPendingHistoryTransactions = (//PENDING
  {
    userKey,
    transactionHistorylimitIndex,
  }) => (dispatch) => {
    const transactionHistoryRef = db
      .ref(`transactionHistory/${userKey}`)
      .orderByChild("status")
      .equalTo("Pending")
      .limitToLast(transactionHistorylimitIndex)
    dispatch({ type: GET_PENDING_TRANSACTION_HISTORY_STARTED })
    transactionHistoryRef.once('value', snapshot => {
      const transactionsHistory = R.values(snapshot.val())
      dispatch({
        type: GET_PENDING_TRANSACTION_HISTORY_DONE,
        payload: {
          pendingTransactionsHistory: transactionsHistory.sort((a, b) => b.timestamp - a.timestamp),
          loadingPendingTransactions: false,
        }
      })
    }).catch(error => {
      customConsoleLog("error on get transaction history: ", error)
      dispatch({
        type: GET_PENDING_TRANSACTION_HISTORY_ERROR,
        payload: { loadingTransactions: false, }
      })
    })
  }

//GET MONEY REQUEST TRANSACTIONS
export const getMoneyRequestTransactions = (
  { userKey,
    moneyRequestTransactionslimitIndex,
  }) => (dispatch) => {
    const moneyRequestsRef = db
      .ref(`moneyRequest/${userKey}`)
      .limitToLast(moneyRequestTransactionslimitIndex)
    dispatch({ type: GET_MONEY_REQUEST_TRANSACTIONS_STARTED })
    moneyRequestsRef.once('value', snapshot => {
      const moneyRequests = R.values(snapshot.val())
      dispatch({
        type: GET_MONEY_REQUEST_TRANSACTIONS_DONE,
        payload: {
          moneyRequests: moneyRequests.sort((a, b) => b.timestamp - a.timestamp),
          loadingMoneyRequest: false,
        }
      })
    }).catch(error => {
      customConsoleLog("error on get transaction history: ", error)
      dispatch({
        type: GET_MONEY_REQUEST_TRANSACTIONS_ERROR,
        payload: { loadingMoneyRequest: false, }
      })
    })
  }
//GET REALIZED TRANSACTIONS
export const getRealizedTransactions = ({ realizedTransactionsIndex }) => (dispatch) => {
  dispatch({ type: GET_REALIZED_TRANSACTIONS_STARTED })
  db.ref('transactionLogs')
    .orderByChild('mode')
    .equalTo('Send')
    .limitToLast(realizedTransactionsIndex)
    .once('value', (snapshot) => {
      const data = R.values(snapshot.val())
      dispatch({
        type: GET_REALIZED_TRANSACTIONS_DONE,
        payload: {
          realizedTransactions: data,
          loadingRealizedTransactions: false,
        }
      })
    })
    .catch(error => {
      customConsoleLog("error on get realized transactions: ", error)
      dispatch({
        type: GET_REALIZED_TRANSACTIONS_ERROR,
        payload: { loadingRealizedTransactions: false, }
      })
    })
}
//GET WITHDRAWS
export const getWithdraws = ({ withdrawsIndex }) => (dispatch) => {
  dispatch({ type: GET_WITHDRAWS_STARTED })
  db.ref('transactionLogs')
    .orderByChild('mode')
    .equalTo('Withdraw')
    .limitToLast(withdrawsIndex)
    .once('value', (snapshot) => {
      const data = R.values(snapshot.val())
      dispatch({
        type: GET_WITHDRAWS_DONE,
        payload: {
          withdraws: data,
          loadingWithdraws: false,
        }
      })
    })
    .catch(error => {
      customConsoleLog("error on get withdraws: ", error)
      dispatch({
        type: GET_WITHDRAWS_ERROR,
        payload: { loadingWithdraws: false, }
      })
    })
}
//GET DEPOSITS
export const getDeposits = ({ depositsIndex }) => (dispatch) => {
  dispatch({ type: GET_DEPOSITS_STARTED })
  db.ref('transactionLogs')
    .orderByChild('mode')
    .equalTo('Deposit')
    .limitToLast(depositsIndex)
    .once('value', (snapshot) => {
      const data = R.values(snapshot.val())
      dispatch({
        type: GET_DEPOSITS_DONE,
        payload: {
          deposits: data,
          loadingDeposits: false,
        }
      })
    })
    .catch(error => {
      customConsoleLog("error on get deposits: ", error)
      dispatch({
        type: GET_WITHDRAWS_ERROR,
        payload: { loadingDeposits: false, }
      })
    })
}
//GET NOTIFICATIONS HISTORY
export const getNotificationsHistory = ({ notificationsHistorylimitIndex }) => (dispatch) => {
  dispatch({ type: GET_NOTIFICATIONS_HISTORY_STARTED })
  db.ref('notificationLog')
    .limitToLast(notificationsHistorylimitIndex)
    .once('value', (snapshot) => {
      const data = R.values(snapshot.val())
      dispatch({
        type: GET_NOTIFICATIONS_HISTORY_DONE,
        payload: {
          notificatonsHistory: data,
          loadingNotificationsHistory: false,
        }
      })
    })
    .catch(error => {
      customConsoleLog("error on get deposits: ", error)
      dispatch({
        type: GET_NOTIFICATIONS_HISTORY_ERROR,
        payload: { loadingNotificationsHistory: false, }
      })
    })
}