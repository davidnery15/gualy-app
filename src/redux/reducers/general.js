import {
  //USER REGISTER
  USER_REGISTER_STARTED,
  USER_REGISTER_DONE,
  USER_REGISTER_ERROR,
  //USER LOGIN
  USER_LOGIN_STARTED,
  USER_LOGIN_DONE,
  USER_LOGIN_ERROR,
  //LOGOUT
  USER_LOGOUT,
  //GET USER INFO
  GET_USER_INFO,
  //HISTORY TRANSACTIONS
  //APPROVED
  GET_APPROVED_TRANSACTION_HISTORY_STARTED,
  GET_APPROVED_TRANSACTION_HISTORY_DONE,
  GET_APPROVED_TRANSACTION_HISTORY_ERROR,
  //REJECTED
  GET_REJECTED_TRANSACTION_HISTORY_STARTED,
  GET_REJECTED_TRANSACTION_HISTORY_DONE,
  GET_REJECTED_TRANSACTION_HISTORY_ERROR,
  //PENDING
  GET_PENDING_TRANSACTION_HISTORY_STARTED,
  GET_PENDING_TRANSACTION_HISTORY_DONE,
  GET_PENDING_TRANSACTION_HISTORY_ERROR,
  //MONEY REQUESTS TRANSACTIONS
  GET_MONEY_REQUEST_TRANSACTIONS_STARTED,
  GET_MONEY_REQUEST_TRANSACTIONS_DONE,
  GET_MONEY_REQUEST_TRANSACTIONS_ERROR,
  //GET COMMERCES
  GET_COMMERCES_STARTED,
  GET_COMMERCES_DONE,
  GET_COMMERCES_ERROR,
  //GET REALIZED TRANSACTIONS
  //APPROVED
  GET_REALIZED_TRANSACTIONS_STARTED,
  GET_REALIZED_TRANSACTIONS_DONE,
  GET_REALIZED_TRANSACTIONS_ERROR,
  //GET WITHDRAWS 
  GET_WITHDRAWS_STARTED,
  GET_WITHDRAWS_DONE,
  GET_WITHDRAWS_ERROR,
  //GET DEPOSITS 
  GET_DEPOSITS_STARTED,
  GET_DEPOSITS_DONE,
  GET_DEPOSITS_ERROR,
  //GET NOTIFICATIONS HISTORY 
  GET_NOTIFICATIONS_HISTORY_STARTED,
  GET_NOTIFICATIONS_HISTORY_DONE,
  GET_NOTIFICATIONS_HISTORY_ERROR,
} from '../actions/general'
const initialAuthState = {
  //GENERAL
  isLoggedIn: null,
  token: '',
  isFetching: null,
  error: null,
  errorCode: null,
  errorMessage: null,
  success: null,
  authCheck: false,
  userFirebaseData: null,
  userInCollectionData: null,
  accessToken: null,
  //HISTORY TRANSACTIONS
  //APPROVED
  approvedTransactionsHistory: null,
  loadingApprovedTransactions: false,
  //REJECTED
  rejectedTransactionsHistory: null,
  loadingRejectedTransactions: false,
  //PENDING
  pendingTransactionsHistory: null,
  loadingPendingTransactions: false,
  //MONEY REQUEST TRANSACTIONS 
  moneyRequests: null,
  loadingMoneyRequest: false,
  //GET COMMERCES
  commerces: null,
  loadingCommerces: false,
  //GET REALIZED TRANSACTIONS
  realizedTransactions: null,
  loadingRealizedTransactions: false,
  //GET WITHDRAWS
  withdraws: null,
  loadingWithdraws: false,
  //GET DEPOSITS
  deposits: null,
  loadingDeposits: false,
  //GET NOTIFICATIONS HISTORY
  notificatonsHistory: null,
  loadingNotificationsHistory: false,
}
const auth = (state = initialAuthState, action) => {
  //----GENERAL REDUCERS----
  switch (action.type) {
    //GET USER INFO
    case GET_USER_INFO:
      return {
        ...state,
        isFetching: false,
        userInCollectionData: action.payload,
      }
    //USER LOGIN
    case USER_LOGIN_STARTED:
      return { ...state, loginStarted: true, isFetching: true }
    case USER_LOGIN_DONE:
      return {
        ...state,
        isLoggedIn: true,
        userFirebaseData: action.payload.user,
        accessToken: action.payload.accessToken,
        error: null,
        errorCode: null,
        errorMessage: null,
      }
    case USER_LOGIN_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        error: action.error,
        errorCode: action.payload.code,
        errorMessage: action.payload.message,
        isFetching: false
      }
    //USER LOGOUT
    case USER_LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        userFirebaseData: {},
        userInCollectionData: {},
        error: null,
        errorCode: null,
        errorMessage: null,
        accessToken: null,
        isFetching: action.payload.isFetching
      }
    //USER REGISTER
    case USER_REGISTER_STARTED:
      return { ...state, registerStarted: true }
    case USER_REGISTER_DONE:
      return {
        ...state,
        authCheck: true,
        registerMessage: `${action.payload.email} registered!!`
      }
    case USER_REGISTER_ERROR:
      return {
        ...state,
        registerMessage: `USER_REGISTER_ERROR: ${action.error}`,
        errorMessage: action.error.message
      }
    //HISTORY TRANSACTIONS
    //APPROVED
    case GET_APPROVED_TRANSACTION_HISTORY_STARTED:
      return {
        ...state,
        loadingApprovedTransactions: true
      }
    case GET_APPROVED_TRANSACTION_HISTORY_DONE:
      return {
        ...state,
        loadingApprovedTransactions: action.payload.loadingApprovedTransactions,
        approvedTransactionsHistory: action.payload.approvedTransactionsHistory,
      }
    case GET_APPROVED_TRANSACTION_HISTORY_ERROR:
      return {
        ...state,
        loadingApprovedTransactions: action.payload.loadingApprovedTransactions,
      }
    //REJECTED
    case GET_REJECTED_TRANSACTION_HISTORY_STARTED:
      return {
        ...state,
        loadingRejectedTransactions: true
      }
    case GET_REJECTED_TRANSACTION_HISTORY_DONE:
      return {
        ...state,
        loadingRejectedTransactions: action.payload.loadingRejectedTransactions,
        rejectedTransactionsHistory: action.payload.rejectedTransactionsHistory,
      }
    case GET_REJECTED_TRANSACTION_HISTORY_ERROR:
      return {
        ...state,
        loadingRejectedTransactions: action.payload.loadingRejectedTransactions,
      }
    //PENDING
    case GET_PENDING_TRANSACTION_HISTORY_STARTED:
      return {
        ...state,
        loadingPendingTransactions: true
      }
    case GET_PENDING_TRANSACTION_HISTORY_DONE:
      return {
        ...state,
        loadingPendingTransactions: action.payload.loadingPendingTransactions,
        pendingTransactionsHistory: action.payload.pendingTransactionsHistory,
      }
    case GET_PENDING_TRANSACTION_HISTORY_ERROR:
      return {
        ...state,
        loadingPendingTransactions: action.payload.loadingPendingTransactions,
      }
    //MONEY REQUEST TRANSACTIONS
    case GET_MONEY_REQUEST_TRANSACTIONS_STARTED:
      return {
        ...state,
        loadingMoneyRequest: true
      }
    case GET_MONEY_REQUEST_TRANSACTIONS_DONE:
      return {
        ...state,
        loadingMoneyRequest: action.payload.loadingMoneyRequest,
        moneyRequests: action.payload.moneyRequests,
      }
    case GET_MONEY_REQUEST_TRANSACTIONS_ERROR:
      return {
        ...state,
        loadingMoneyRequest: action.payload.loadingMoneyRequest,
      }
    //GET COMMERCES
    case GET_COMMERCES_STARTED:
      return {
        ...state,
        loadingCommerces: true
      }
    case GET_COMMERCES_DONE:
      return {
        ...state,
        loadingCommerces: action.payload.loadingCommerces,
        commerces: action.payload.commerces,
      }
    case GET_COMMERCES_ERROR:
      return {
        ...state,
        loadingCommerces: action.payload.loadingCommerces,
      }
    //GET REALIZED TRANSACTIONS
    case GET_REALIZED_TRANSACTIONS_STARTED:
      return {
        ...state,
        loadingRealizedTransactions: true
      }
    case GET_REALIZED_TRANSACTIONS_DONE:
      return {
        ...state,
        loadingRealizedTransactions: action.payload.loadingRealizedTransactions,
        realizedTransactions: action.payload.realizedTransactions,
      }
    case GET_REALIZED_TRANSACTIONS_ERROR:
      return {
        ...state,
        loadingRealizedTransactions: action.payload.loadingRealizedTransactions,
      }
    //GET WITHDRAWS
    case GET_WITHDRAWS_STARTED:
      return {
        ...state,
        loadingRealizedTransactions: true
      }
    case GET_WITHDRAWS_DONE:
      return {
        ...state,
        loadingWithdraws: action.payload.loadingWithdraws,
        withdraws: action.payload.withdraws,
      }
    case GET_WITHDRAWS_ERROR:
      return {
        ...state,
        loadingWithdraws: action.payload.loadingWithdraws,
      }
    //GET DEPOSITS
    case GET_DEPOSITS_STARTED:
      return {
        ...state,
        loadingDeposits: true
      }
    case GET_DEPOSITS_DONE:
      return {
        ...state,
        loadingDeposits: action.payload.loadingDeposits,
        deposits: action.payload.deposits,
      }
    case GET_DEPOSITS_ERROR:
      return {
        ...state,
        loadingDeposits: action.payload.loadingDeposits,
      }
    //GET NOTIFICATIONS HISTORY
    case GET_NOTIFICATIONS_HISTORY_STARTED:
      return {
        ...state,
        loadingDeposits: true
      }
    case GET_NOTIFICATIONS_HISTORY_DONE:
      return {
        ...state,
        loadingNotificationsHistory: action.payload.loadingNotificationsHistory,
        notificatonsHistory: action.payload.notificatonsHistory,
      }
    case GET_NOTIFICATIONS_HISTORY_ERROR:
      return {
        ...state,
        loadingNotificationsHistory: action.payload.loadingNotificationsHistory,
      }
    default:
      return state
  }
}
export default auth
