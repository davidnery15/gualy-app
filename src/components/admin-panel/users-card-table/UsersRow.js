import React, { Component } from 'react'
import { css } from 'emotion'
// import { NavLink } from 'react-router-dom'
import MoreModal from './../../general/modals/MoreModal'
import { NavLink } from 'react-router-dom'
// import infoIcon from './../../../assets/ic-info.svg'
import lockIconRed from './../../../assets/ic-lock-copy.svg'
import lockIconGreen from './../../../assets/ic-lock-copy-2.svg'
import iconEdit from './../../../assets/icon-edit.svg'
// import iconNotiYellow from './../../../assets/ic-notification-yellow.svg'
import moreIcon from './../../../assets/ic-more-vert.svg';
import { toast } from 'react-toastify'
import OptionsModalWrapper from '../../general/OptionsModalWrapper'
import AreYouSureModal from '../../general/modals/areYouSureModal'
import { postRequest } from '../../../utils/createAxiosRequest'
import moment from 'moment'
import userImage from './../../../assets/accountCircle.svg'
const approvedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #95fad8;
    display: flex;
    @media (max-width: 1300px) {
      display: none;
    }
  `
const rejectedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ff6061;
    display: flex;
    @media (max-width: 1300px) {
      display: none;
    }
  `
const actionsContainer = css`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
    margin-right: 12px;
    @media (max-width: 1300px) {
      display: none;
    }
  `
const userImg = css`
    width: 44.2px;
    height: 44.2px;
    border-radius: 50%;
    object-fit: cover;
    margin: 0 24px;
    @media (max-width: 1300px) {
      margin: 0;
    }
    @media (max-width: 990px) {
      display:none;
    }
  `
const tableRow = css`
    td{
      padding: 0 10px 24px 10px;
      font-size: 11.8px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #f6f7fa;
      div{
        white-space: nowrap;
        text-overflow: ellipsis;
        // overflow: hidden;
      }
    }
    @media (max-width: 1300px) {
      td{
        // div{
        //   width: 180px;
        // }
      }
    }
    @media (max-width: 910px) {
      td{
        // div{
        //   width: 80px;
        // }
      }
    }
  `
const emailContainer = css`
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    @media (max-width: 1300px) {
      width: 140px;
    }
  `
const moreIconImg = css`
    display: none;
    @media (max-width: 1300px) {
      display: initial;
    }
  `
const statusContainer = css`
    display: none;
    width: 100%;
    @media (max-width: 1300px) {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      width: 70px !important;
    }
  `
const tdStatus = css`
    @media (max-width: 1300px) {
      width: 70px;
    }
    @media (max-width: 910px){
      display: none;
    }
  `
const tdIndex = css`
    padding-left: 0 !important;
    padding-right: 0 !important;
  `
const tdMoreIcon = css`
    position: relative;
    cursor: pointer;
    img{
      width: 18px;
      height: 18px;
      object-fit: contain;
    }
  `
const userContainer = `
    display:flex;
    justify-content: center;
    align-items:center;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    @media (max-width: 1300px) {
      width: 500px !important;
    }
  `

export default class UsersRow extends Component {
  state = ({
    visible: false,
    areYouSureModalIsOpenBlock: false,
    areYouSureModalIsOpenUnBlock: false,
    isLoading: false
  })
  handleModal = e => {
    this.setState({
      visible: !this.state.visible
    })
  }
  openAreYouSureModalBlock = () => {
    this.setState({ areYouSureModalIsOpenBlock: !this.state.areYouSureModalIsOpenBlock })
  }
  openAreYouSureModalUnBlock = () => {
    this.setState({ areYouSureModalIsOpenUnBlock: !this.state.areYouSureModalIsOpenUnBlock })
  }
  blockUser = async () => {
    this.setState({ isLoading: true })
    try {
      const uid = this.props.info.userKey
      const adminID = this.props.adminID
      const reason = 'Usuario Bloqueado'
      const action = 'block'
      const data = {
        uid,
        adminID,
        reason,
        action
      }
      // console.log("SEND: ", data)
      let response = await postRequest('users/closeSession', data)
      // console.log("RESPONSE: ", response)
      if (response.data.success) {
        this.props.databaseRequest()
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario bloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      // console.log("error blocking user", error)
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
    }
  }
  unBlockUser = async () => {
    this.setState({ isLoading: true })
    try {
      const uid = this.props.info.userKey
      const adminID = this.props.adminID
      const data = {
        uid,
        adminID,
      }
      // console.log("SENDED: ", data)
      let response = await postRequest('users/unBlockUser', data)
      // console.log("RESPONSE: ", response)
      if (response.data.success) {
        this.props.databaseRequest()
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario desbloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      // console.log("error unblocking user", error)
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
    }
  }
  render() {
    const { info, index, commerceType, userType } = this.props
    return (
      <tr className={tableRow}>
        <td className={tdIndex}>{index}</td>
        <td>
          <div className={userContainer}>
            <img
              className={userImg}
              src={info.profilePicture}
              onError={(e) => { e.target.onerror = null; e.target.src = userImage }}
              alt=""
            />
            {
              commerceType ?
                <span>{info.name}</span>
                :
                <span>{info.firstName}</span>
            }
          </div>
        </td>
        <td>
          {
            commerceType
              ?
              <div>{info.dni.type}{' '}{info.dni.id}</div>
              :
              <div>{info.lastName}</div>
          }
        </td>
        <td>
          {
            commerceType
              ?
              <div
                css={`
              width: 200px;
              white-space: normal !important;
              `}
              >{info.address}</div>
              :
              <div>{info.dni.type}{' '}{info.dni.id}</div>
          }
        </td>
        <td>
          <div className={emailContainer}>
            {info.email}
          </div>
        </td>
        <td>
          {info.phone}
        </td>
        <td>{moment(info.createdAt).format('DD-MM-YYYY')}</td>
        <td className={tdStatus}>
          {
            info.blocked
              ?
              <span className={rejectedText}>Bloqueado</span>
              :
              <span className={approvedText}>Desbloqueado</span>
          }
          <div className={statusContainer}>
            {
              info.blocked
                ?
                <img src={lockIconRed} alt="unlocked" />
                :
                <img src={lockIconGreen} alt="locked" />
            }
          </div>
        </td>
        <td className={tdMoreIcon} onClick={this.handleModal}>
          <div className={actionsContainer}>
            {/* <button className='btn-transparent'><img src={infoIcon} alt="info" /></button> */}
            {
              userType === 'admin'
                ? <NavLink to={`/editar-usuario?uid=${info.userKey}`}>
                  <button className='btn-transparent'><img src={iconEdit} alt="edit" /></button>
                </NavLink>
                : null
            }
            {
              info.blocked
                ?
                <button
                  className='btn-transparent'
                  onClick={this.openAreYouSureModalUnBlock}
                >
                  <img
                    src={lockIconGreen}
                    alt="lock"
                  />
                </button>
                :
                <button
                  className='btn-transparent'
                  onClick={this.openAreYouSureModalBlock}
                >
                  <img
                    src={lockIconRed}
                    alt="unlock"
                  />
                </button>
            }
            {/* <button className='btn-transparent'><img src={iconNotiYellow} alt="noti" /></button> */}
          </div>
          <img
            src={moreIcon}
            alt="more"
            className={moreIconImg}
          />
          <MoreModal
            visible={this.state.visible}
            responsive={true}
          >
            {/* <button className='btn-transparent'><img src={infoIcon} alt="info" /></button> */}
            {
              userType === 'admin'
                ? <NavLink to={`/editar-usuario?uid=${info.userKey}`}>
                  <button className='btn-transparent'><img src={iconEdit} alt="edit" /></button>
                </NavLink>
                : null
            }
            {
              info.blocked
                ?
                <button
                  className='btn-transparent'
                  onClick={this.openAreYouSureModalUnBlock}
                >
                  <img
                    src={lockIconGreen}
                    alt="lock"
                  />
                </button>
                :
                <button
                  className='btn-transparent'
                  onClick={this.openAreYouSureModalBlock}>
                  <img src={lockIconRed}
                    alt="unlock"
                  />
                </button>
            }
            {/* <button className='btn-transparent'><img src={iconNotiYellow} alt="noti" /></button> */}
          </MoreModal>
          <OptionsModalWrapper
            show={this.state.areYouSureModalIsOpenBlock}
            onClose={this.openAreYouSureModalBlock}
            width="310px"
          >
            <AreYouSureModal
              modalShow={this.openAreYouSureModalBlock}
              onSubmit={this.blockUser}
              message="Esta seguro que desea bloquear este usuario?"
              cancelText="CANCELAR"
              actionText="BLOQUEAR"
              isLoading={this.state.isLoading}
            />
          </OptionsModalWrapper>
          <OptionsModalWrapper
            show={this.state.areYouSureModalIsOpenUnBlock}
            onClose={this.openAreYouSureModalUnBlock}
            width="310px"
          >
            <AreYouSureModal
              modalShow={this.openAreYouSureModalUnBlock}
              onSubmit={this.unBlockUser}
              message="Esta seguro que desea desbloquear este usuario?"
              cancelText="CANCELAR"
              actionText="DESBLOQUEAR"
              isLoading={this.state.isLoading}
            />
          </OptionsModalWrapper>
        </td>
      </tr>
    )
  }
} 