import React, { Component } from 'react'
import { css } from 'emotion'
// import infoIcon from './../../../assets/ic-info.svg'
import lockIconRed from './../../../assets/ic-lock-copy.svg'
import lockIconGreen from './../../../assets/ic-lock-copy-2.svg'
import iconEdit from './../../../assets/icon-edit.svg'
// import iconNotiYellow from './../../../assets/ic-notification-yellow.svg'
import { toast } from 'react-toastify'
import OptionsModalWrapper from '../../general/OptionsModalWrapper'
import AreYouSureModal from '../../general/modals/areYouSureModal'
import { postRequest } from '../../../utils/createAxiosRequest'
import { NavLink } from 'react-router-dom'
import moment from 'moment'
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    margin-top: 15px;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const mobileTable = css`
    td{
      padding: 0 22px;
      padding-top: 15px;
      width: 100%
    }
  `
const tdTitle = css`
    font-size: 11px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.09;
    letter-spacing: 0.9px;
    text-align: left;
    color: #f6f7fa;
  `
const tdData = css`
    font-size: 10px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1;
    letter-spacing: normal;
    text-align: right;
    color: #f6f7fa;
  `
const approvedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #95fad8;
    display: flex;
    justify-content: flex-end;
    text-align: right;
  `
const rejectedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ff6061;
    // display: flex;
  `
const actionsContainer = css`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
  `
const emailLabel = css`
text-overflow: ellipsis;
word-break: break-all;
  `
export default class MobileUsersTable extends Component {
  state = ({
    visible: false,
    areYouSureModalIsOpenBlock: false,
    areYouSureModalIsOpenUnBlock: false,
    isLoading: false
  })
  openAreYouSureModalBlock = () => {
    this.setState({ areYouSureModalIsOpenBlock: !this.state.areYouSureModalIsOpenBlock })
  }
  openAreYouSureModalUnBlock = () => {
    this.setState({ areYouSureModalIsOpenUnBlock: !this.state.areYouSureModalIsOpenUnBlock })
  }
  blockUser = async () => {
    this.setState({ isLoading: true })
    try {
      const uid = this.props.user.userKey
      const adminID = this.props.adminID
      const reason = 'Usuario Bloqueado'
      const action = 'block'
      const data = {
        uid,
        adminID,
        reason,
        action
      }
      // console.log("SEND: ", data)
      let response = await postRequest('users/closeSession', data)
      // console.log("RESPONSE: ", response)
      if (response.data.success) {
        this.props.databaseRequest()
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario bloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      // console.log("error blocking user", error)
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
    }
  }
  unBlockUser = async () => {
    this.setState({ isLoading: true })
    try {
      const uid = this.props.user.userKey
      const adminID = this.props.adminID
      const data = {
        uid,
        adminID,
      }
      // console.log("SENDED: ", data)
      let response = await postRequest('users/unBlockUser', data)
      // console.log("RESPONSE: ", response)
      if (response.data.success) {
        this.props.databaseRequest()
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario desbloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      // console.log("error unblocking user", error)
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
    }
  }
  render() {
    const { user, index, userType } = this.props
    return (
      <div>
        <table className={mobileTable}>
          <tbody>
            <tr>
              <td className={tdTitle}>ID</td>
              <td className={tdData}>
                {index}
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Nombre</td>
              <td className={tdData}>
                {user.firstName}
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Apellido</td>
              <td className={tdData}>
                {user.lastName}
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Documento de&nbsp;Identidad</td>
              <td className={tdData}>{user.dni.id}</td>
            </tr>
            <tr>
              <td className={tdTitle}>Email</td>
              <td className={tdData}>
                <p
                className={emailLabel}
                >
                  {user.email}
                </p>
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Teléfono</td>
              <td className={tdData}>{user.phone}</td>
            </tr>
            <tr>
              <td className={tdTitle}>Fecha&nbsp;de Registro</td>
              <td className={tdData}>{moment(user.createdAt).format('DD-MM-YYYY')}</td>
            </tr>
            <tr>
              <td className={tdTitle}>Estado</td>
              <td className={tdData}>
                {
                  user.blocked ?
                    <span
                      onClick={this.openAreYouSureModalUnBlock}
                      className={rejectedText}>
                      Bloqueado
                  </span>
                    :
                    <span
                      onClick={this.openAreYouSureModalBlock}
                      className={approvedText}>
                      Desbloqueado
                    </span>
                }
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Acciones</td>
              <td className={tdData}>
                <div className={actionsContainer}>
                  {/* <img src={infoIcon} alt="info" /> */}
                  {
                    userType === 'admin'
                      ? <NavLink to={`/editar-usuario?uid=${user.userKey}`}>
                        <img src={iconEdit} alt="edit" />
                      </NavLink>
                      : null
                  }
                  {
                    user.blocked ?
                      <img
                        onClick={this.openAreYouSureModalUnBlock}
                        src={lockIconGreen} alt="lock"
                      />
                      :
                      <img
                        onClick={this.openAreYouSureModalBlock}
                        src={lockIconRed}
                        alt="unlock"
                      />
                  }
                  {/* <img src={iconNotiYellow} alt="noti" /> */}
                  <OptionsModalWrapper
                    show={this.state.areYouSureModalIsOpenBlock}
                    onClose={this.openAreYouSureModalBlock}
                    width="310px">
                    <AreYouSureModal
                      modalShow={this.openAreYouSureModalBlock}
                      onSubmit={this.blockUser}
                      message="Esta seguro que desea bloquear este usuario?"
                      cancelText="CANCELAR"
                      actionText="BLOQUEAR"
                      isLoading={this.state.isLoading}
                    />
                  </OptionsModalWrapper>
                  <OptionsModalWrapper
                    show={this.state.areYouSureModalIsOpenUnBlock}
                    onClose={this.openAreYouSureModalUnBlock}
                    width="310px">
                    <AreYouSureModal
                      modalShow={this.openAreYouSureModalUnBlock}
                      onSubmit={this.unBlockUser}
                      message="Esta seguro que desea desbloquear este usuario?"
                      cancelText="CANCELAR"
                      actionText="DESBLOQUEAR"
                      isLoading={this.state.isLoading}
                    />
                  </OptionsModalWrapper>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className={tableDivisor}></div>
      </div>
    )
  }
}
