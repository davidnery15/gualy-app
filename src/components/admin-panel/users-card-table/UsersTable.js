import React from 'react'
import { css } from 'emotion'
import UsersRow from './UsersRow'
const cardTable = css`
    width: 100%;
    padding: 0 15px;
    border-collapse: collapse;
    overflow: auto !important;
  `
const tHead = css`
    tr{
      th{
        text-align: left; 
        padding: 40px 10px;
        font-size: 11px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: 0.9px;
        color: #f6f7fa;
        span{
          padding-left: 93.19px;
        }
      }
    }
    @media (max-width: 990px) {
      tr{
        th{
          span{
            padding-left: 0;
          }
        }
      }
    }
  `
const thActions = css`
    padding-rigth: 0;
    padding-left: 0;
    // @media (max-width: 1270px) {
    //   display: none;
    // }
  `
const thStatus = css`
    @media (max-width: 1270px) {
      width: 70px;
    }
    @media(max-width: 910px) {
      display: none;
    }
  `
const UsersTable = ({ 
  users, 
  commerceType, 
  adminID, 
  userType, 
  databaseRequest 
}) => {
  return (
    <table className={cardTable}>
      <thead className={tHead}>
        <tr>
          <th css={`padding-left:0 !important;`}>ID</th>
          <th> <span> Nombre </span> </th>
          <th> {commerceType ? 'RIF' : 'Apellido'}</th>
          {
            commerceType ? <th> Dirección </th> :
              <th>Documento <br /> de&nbsp;Identidad</th>
          }
          <th>Email</th>
          <th>Teléfono</th>
          <th>Fecha&nbsp;de registro</th>
          <th className={thStatus}>Estado</th>
          <th className={thActions}>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {
          users.map((user, index) =>
            <UsersRow
              info={user}
              key={user.userKey}
              index={(users.length + 1) - (index + 1)}
              commerceType={(commerceType ? true : false)}
              adminID={adminID}
              userType={userType}
              databaseRequest={databaseRequest}
            />
          )
        }
      </tbody>
    </table>
  )
}
export default UsersTable