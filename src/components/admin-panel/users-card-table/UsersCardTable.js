import React from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import { ToastContainer } from 'react-toastify'
import TableHeader from './../../general/tables/TableHeader'
import UsersTable from './UsersTable'
import MobileUsersTable from './MobileUsersTable'
import Workbook from 'react-excel-workbook'
import moment from 'moment'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { postRequest } from '../../../utils/createAxiosRequest'
import { toast } from 'react-toastify'
import throttle from 'lodash.throttle'
/* CSS */
const component = css`
    margin: 50px;
    border-radius: 5px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    background-color: #2a2c6a;
    max-width: 1350px;
    align-self: center;
    width: 100%;
    @media (max-width: 1230px) {
      margin: 0 25px;
      margin-top: 40px;
      margin-bottom: 40px;
    }
  `
const responsive = css`
    display: none;
    @media (max-width: 750px) {
      display: flex !important;
      flex-direction: column;
      overflow: auto !important;
    }
  `
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const cardTableComponent = css`
    height: 70vh;
    padding-left: 32px;
    padding-right: 32px;
    position: relative;
    overflow: auto !important;
    min-height: 400px;
    /* Track */
      ::-webkit-scrollbar-track {
        background: #f6f7fa;
        border: 3px solid transparent;
        background-clip: content-box;
      }
    /* Handle */
      ::-webkit-scrollbar-thumb {
        border: 1px;
        background-color: #95fad8;
        border-radius: 2px;
      }
    ::-webkit-scrollbar {
        width: 7px;
    }
    @media (max-width: 750px) {
      display: none;
      padding-left: 0;
      padding-right: 0;
      height: 100%;
      max-height: 383px;
    }
  `
const cardTableBottom = css`
    position: sticky;
    bottom: -1px;
    width: 100%;
    height: 100px;;
    background-image: linear-gradient(to bottom, rgba(41, 43, 105, 0), rgba(41, 43, 105, 0.5) 49%, #2a2c6a);
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
const noRegistersText = css`
  opacity: 0.5;
  text-align: center;
`

export default class UsersCardTable extends React.Component {
  state = ({
    search: '',
    filteredUsers: "",
    isSearchingData: false,
  })
  throtled = throttle(async () => {
    if (this.state.search) {
      this.setState({ isSearchingData: true })
      const data = {
        value: this.state.search,
      }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest('users/filter', data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success) {
          this.setState({ isSearchingData: false })
          if (Object.values(resp.data.data.message).length === 0) {
            // toast.error("No se encontraron resultados.")
            this.setState({
              filteredUsers: Object.values(resp.data.data.message)
            })
          } else {
            this.setState({
              filteredUsers: Object.values(resp.data.data.message)
            })
          }
        } else {
          this.setState({ isSearchingData: false })
        }
      } catch (error) {
        this.setState({ isSearchingData: false })
        customConsoleLog(error)
        toast.error("Verifique su conexión y vuelva a intentarlo.")
      }
    }
  }, 1000)
  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.throtled()
    }
  }
  updateSearch = (event) => {
    this.setState({
      search: event.target.value.substr(0, 120),
    }, () => {
      this.throtled()
    })
  }
  onScrollMobile = () => {
    customConsoleLog("mobileScroll: ")
    if (
      (this.mobileTable.scrollHeight - this.mobileTable.scrollTop === this.mobileTable.clientHeight)
      && this.props.databaseRequest
    ) {
      this.props.databaseRequest()
    }
  }
  onScrollDesktop = () => {
    customConsoleLog("desktopScroll: ")
    if (
      (this.desktopTable.scrollHeight - this.desktopTable.scrollTop === this.desktopTable.clientHeight)
      && this.props.databaseRequest
    ) {
      this.props.databaseRequest()
    }
  }
  componentDidMount = () => {
    if (this.desktopTable) {
      if (this.desktopTable.clientHeight || this.mobileTable.clientHeight === this.desktopTable.scrollHeight || this.mobileTable.scrollHeight) {
        this.props.databaseRequest()
      }
    }
  }
  goBackButtonClick = () => {
    this.setState({
      filteredUsers: "",
      search: "",
    })
  }
  render() {
    const {
      users,
      title,
      loading,
      store,
      commerceType,
      adminID,
      showOrderByButton,
      showFilterByButton,
      icon,
      userType,
      databaseRequest,
      idAdmin,
      isDownloadSelectOpen,
      openDownloadBySelect,
      allUsers,
      allUsersLoading,
    } = this.props
    const {
      search,
      isSearchingData,
    } = this.state
    const usersFiltered = !this.state.filteredUsers || !search ? users : this.state.filteredUsers
    return (
      <div className={component}>
        <TableHeader
          onKeyPress={this.onKeyPress}
          isSearchingData={isSearchingData}
          title={title}
          updateSearch={this.updateSearch}
          search={this.state.search}
          placeholder='Buscar...'
          filter='true'
          store={(store ? true : false)}
          isDownload={true}
          downloadTitle={title}
          showOrderByButton={showOrderByButton}
          showFilterByButton={showFilterByButton}
          icon={icon}
          isBackButtonShow={this.state.filteredUsers && search}
          goBackButtonClick={this.goBackButtonClick}
          downloadColumns={
            commerceType ?
              <Workbook.Sheet
                data={usersFiltered}
                name={`${moment().utc(-264).format('YYYY-MM-DD h:mm A')}`}
              >
                <Workbook.Column label="ID" value="userKey" />
                <Workbook.Column label="Nombre" value="commerceName" />
                <Workbook.Column label="Rif" value={row => `${row.dni.type} ${row.dni.id}`} />
                <Workbook.Column label="Correo" value="email" />
                <Workbook.Column label="Dirección" value="address" />
                <Workbook.Column label="email" value="email" />
                <Workbook.Column label="Teléfono" value="phone" />
                <Workbook.Column label="Fecha de registro" value={row => moment(row.createdAt).format('YYYY-MM-DD')} />
                <Workbook.Column label="Estado" value={row => row.blocked ? "Bloqueado" : "Desbloqueado"}
                />
              </Workbook.Sheet>
              : <Workbook.Sheet
                data={usersFiltered}
                name={`${moment().utc(-264).format('YYYY-MM-DD h:mm A')}`}
              >
                <Workbook.Column label="ID" value="userKey" />
                <Workbook.Column label="Nombre" value="firstName" />
                <Workbook.Column label="Apellido" value="lastName" />
                <Workbook.Column label="Documento de identidad" value={row => `${row.dni.type} ${row.dni.id}`} />
                <Workbook.Column label="Correo" value="email" />
                <Workbook.Column label="Teléfono" value="phone" />
                <Workbook.Column label="Fecha de registro" value={row => moment(row.createdAt).format('YYYY-MM-DD')} />
                <Workbook.Column label="Estado" value={row => row.blocked ? "Bloqueado" : "Desbloqueado"} />
              </Workbook.Sheet>
          }
          downloadColumnsAll={
            commerceType ?
              <Workbook.Sheet
                data={allUsers}
                name={`${moment().utc(-264).format('YYYY-MM-DD h:mm A')}`}
              >
                <Workbook.Column label="ID" value="userKey" />
                <Workbook.Column label="Nombre" value="commerceName" />
                <Workbook.Column label="Rif" value={row => `${row.dni.type} ${row.dni.id}`} />
                <Workbook.Column label="Correo" value="email" />
                <Workbook.Column label="Dirección" value="address" />
                <Workbook.Column label="email" value="email" />
                <Workbook.Column label="Teléfono" value="phone" />
                <Workbook.Column label="Fecha de registro" value={row => moment(row.createdAt).format('YYYY-MM-DD')} />
                <Workbook.Column label="Estado" value={row => row.blocked ? "Bloqueado" : "Desbloqueado"}
                />
              </Workbook.Sheet>
              : <Workbook.Sheet
                data={allUsers}
                name={`${moment().utc(-264).format('YYYY-MM-DD h:mm A')}`}
              >
                <Workbook.Column label="ID" value="userKey" />
                <Workbook.Column label="Nombre" value="firstName" />
                <Workbook.Column label="Apellido" value="lastName" />
                <Workbook.Column label="Documento de identidad" value={row => `${row.dni.type} ${row.dni.id}`} />
                <Workbook.Column label="Correo" value="email" />
                <Workbook.Column label="Teléfono" value="phone" />
                <Workbook.Column label="Fecha de registro" value={row => moment(row.createdAt).format('YYYY-MM-DD')} />
                <Workbook.Column label="Estado" value={row => row.blocked ? "Bloqueado" : "Desbloqueado"} />
              </Workbook.Sheet>
          }
          idAdmin={idAdmin}
          isDownloadSelectOpen={isDownloadSelectOpen}
          openDownloadBySelect={openDownloadBySelect}
          allUsersLoading={allUsersLoading}
        />

        <div className={tableDivisor}></div>
        <div
          ref={ref => this.desktopTable = ref}
          onScroll={this.onScrollDesktop}
          className={cardTableComponent}>
          {
            loading || isSearchingData ?
              (<div className={spinnerContainer}> <Spinner color='white' /> </div>)
              : usersFiltered && usersFiltered.length > 0
                ? <UsersTable
                  users={usersFiltered}
                  commerceType={(commerceType ? true : false)}
                  adminID={adminID}
                  userType={userType}
                  databaseRequest={databaseRequest}
                />
                : <div className={spinnerContainer}>
                  <h1 className={noRegistersText}>No hay registros</h1>
                </div>
          }
          <div className={cardTableBottom}></div>
        </div>
        <div
          ref={ref => this.mobileTable = ref}
          onScroll={this.onScrollMobile}
          className={`${cardTableComponent} ${responsive}`}>
          {
            loading || isSearchingData ?
              (<div className={spinnerContainer}>
                <Spinner color='white' /> </div>) :
              (
                usersFiltered && usersFiltered.length > 0
                  ? usersFiltered.map((user, index) =>
                    <MobileUsersTable
                      user={user}
                      key={user.userKey}
                      userType={userType}
                      index={(
                        (!this.state.filteredUsers
                          ? users
                          : this.state.filteredUsers)
                          .length + 1) - (index + 1)}
                      commerceType={(commerceType ? true : false)}
                      adminID={adminID}
                      databaseRequest={databaseRequest}
                    />
                  )
                  : <div className={spinnerContainer}>
                    <h1 className={noRegistersText}>No hay registros</h1>
                  </div>
              )
          }
        </div>
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    )
  }
}