import React, { Component } from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'

import KpiIndicator from './KpiIndicator'

/*CSS*/
  const header = css`
    padding: 3.5px 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  `
  const titleContainer = css`
    width: 75%;
    h1{
      font-family: Montserrat;
      font-size: 24px;
      font-weight: 900;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;
      text-align: left;
      color: #2a2d6a;
      margin: 0;
    }
    @media(max-width: 380px) {
      h1{
        font-size: 16px !important;
        width: 65px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    }
  `
  const dropdownContainer = css`
    select{
      background-color: transparent;
      border: none;
      font-family: Montserrat;
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.36;
      letter-spacing: normal;
      text-align: left;
      color: #242657;
      cursor: pointer;
      option{
        text-align: right;
      }
    }
  `
  const divisor = css`
    width: 100%;
    border: solid 1px #242657;
    opacity: 0.5;
  `
  const body = css`
    display: flex;
    height: 144px;
    flex-direction: row;
    justify-content: space-between;
    /* margin: 24px 32px;
    margin-bottom: 0px; */
    position: relative;
    @media (max-width: 1740px) {
      height: 82%;
    }
    @media(max-width: 1440px) {
      height: 144px;
    }
    @media (max-width: 425px) {
      height: 115px
    }
  `   
  const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `

export default class KpiIndicatorContainer extends Component{
  state={
    allData: this.props.data,
    data: [0, 0],
    selectedDate: ''
  }

  handleDate = (event) => {
    const selectedOption = event.target.value
    const allData = this.props.data
    let data = []
    switch (selectedOption) {
      case 'today':
        data = allData ? Object.values(allData.today) : []
        break;
      case 'yesterday':
        data = allData ? Object.values(allData.yesterday) : []
        break;
      case 'lastWeek':
        data = allData ? Object.values(allData.lastWeek) : []
        break;
      case 'lastMonth':
        data = allData ? Object.values(allData.lastMonth) : []
        break;
      case 'currentWeek':
        data = allData ? Object.values(allData.currentWeek) : []
        break;
      case 'currentMonth':
        data = allData ? Object.values(allData.currentMonth) : []
        break;
      default:
        break;
    }
    this.setState({
      selectedDate: selectedOption,
      data
    })
  }
  
  render(){
    const { title, loading, color, data } = this.props
    const component = css`
      width: 100%;
      max-width: 450px;
      min-width: 350px;
      height: 180px;
      border-radius: 5.7px;
      background-color: ${color};
      box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
      margin: 24px 20px;
      @media (max-width: 2138px) {
        width: 350px;
      }
      @media (max-width: 1740px) {
        width: 100%;
        max-width: 100%;
        min-width: 350px;
        height: 210px;
      }
      @media(max-width: 1440px) {
        height: 180px;
      }
      @media (max-width: 425px) {
        width: 100%;
        min-width: 100%;
        max-width: 100%;
        margin: 10px 5px;
        min-height: 150px;
        height: 150px
      }
    `
    return(
      <div className={component}>
        <div className={header}>
          <div className={titleContainer}>
            <h1>{title}</h1>
          </div>
          <div className={dropdownContainer}>
            <select disabled={loading} onChange={this.handleDate} value={this.state.selectedDate}>
              <option value="today">Hoy</option>
              <option value="yesterday">Ayer</option>
              <option value="currentWeek">Semana actual</option>
              <option value="currentMonth">Mes actual</option>
              <option value="lastWeek">Semana anterior</option>
              <option value="lastMonth">Mes anterior</option>
            </select>
          </div>
        </div>
        <div className={divisor}></div>
        <div className={body}>
          {
            loading ? (<div className={spinnerContainer}> <Spinner color='#2a2d6a' /> </div>) :
              (<KpiIndicator title={title} data={(this.state.selectedDate === '' ? Object.values(data.today) : this.state.data)}/>)
          }
          
        </div>
      </div>
    )
  }
}