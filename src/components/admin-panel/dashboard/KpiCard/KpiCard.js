import React, { Component } from 'react'
import { css } from 'emotion'
import moment from 'moment'
import KpiIndicatorContainer from './KpiIndicatorContainer'
import { postRequest } from '../../../../utils/createAxiosRequest'

/*CSS*/
  const component = css`
    margin: 50px;
    margin-bottom: 0px;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    min-height: 298px;
    height: 100%;
    @media(max-width: 1230px) {
      margin: 0 25px;
      margin-top: 40px;
    }
    @media(max-width: 400px) {
      margin-bottom: 40px;
    }
  `
  const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
  const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
  const body = css`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    padding: 0 32px;
    @media (max-width: 1740px) {
      padding: 24px 32px;
    }
    @media(max-width: 830px) {
      flex-direction: column;
    }
    @media(max-width: 425px) {
      padding: 5px 24px;
    }
  `
  const kpiContainer = css`
    width: 50%;
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    margin:  0;
    @media (max-width: 830px) {
      width: 100%;
    }
  `

export default class KpiCard extends Component {
  state = {
    kpiFee: {},
    kpiWithdraw: {},
    kpiDeposit: {},
    kpiTransaction: {},
    loadingFee: true,
    loadingWithdraw: true,
    loadingDeposit: true,
    loadingTransaction: true,
  }

  componentDidMount() {
    this.getKpiFee()
    this.getKpiWithdraw()
    this.getKpiDeposit()
    this.getKpiTransaction()
  }

  getKpiFee = async() => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = { date }
    const respKpiFee = await postRequest('kpi/kpiFee', data) 
    this.setState({
      kpiFee: respKpiFee.data,
      loadingFee: false
    })
  }

  getKpiWithdraw = async () => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = { date }
    const respKpiWithdraw = await postRequest('kpi/kpiWithdraw', data) 
    this.setState({
      kpiWithdraw: respKpiWithdraw.data,
      loadingWithdraw: false
    })
  }

  getKpiDeposit = async () => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = { date }
    const respKpiDeposit = await postRequest('kpi/kpiDeposit', data) 
    this.setState({
      kpiDeposit: respKpiDeposit.data,
      loadingDeposit: false
    })
  }

  getKpiTransaction = async () => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = { date }
    const respKpiTransaction = await postRequest('kpi/kpiNumberTransaction', data)
    this.setState({
      kpiTransaction: respKpiTransaction.data,
      loadingTransaction: false
    })
  }

  render() {
    const {kpiFee, kpiWithdraw, kpiDeposit, kpiTransaction} = this.state
    return( 
      <div className={component}>
        <div className={header}>
          <h1>KPI's</h1>
        </div>
        <div className={divisor}></div>
        <div className={body}>
          <div className={kpiContainer}>
            <KpiIndicatorContainer title='Fee' data={kpiFee} loading={this.state.loadingFee} color='#14a0c0'/>
            <KpiIndicatorContainer title='Depósitos' data={kpiDeposit} loading={this.state.loadingDeposit} color='#95fad8'/>
          </div>
          <div className={kpiContainer}>
          <KpiIndicatorContainer title='Retiros' data={kpiWithdraw} loading={this.state.loadingWithdraw} color='#01cc9b'/>
          <KpiIndicatorContainer title='Transacciones' data={kpiTransaction} loading={this.state.loadingTransaction} color='#8078ff'/>
          </div>
        </div>
      </div>
    )
  }
}