import React from 'react'
import {css} from 'emotion'
import CountUp from 'react-countup'
import { BS } from '../../../../config/currencies.js'

/*CSS */
  const component = css`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    @media (max-width: 1740px) {
      justify-content: space-evenly;
      padding: 15px;
    }
    @media(max-width: 1440px) {
      justify-content: center;
      padding: 0;
    }
  `
  const countContainer = css`
    width: 100%;
    display: flex;
    padding: 0 10px;
    justify-content: space-between;
    align-items: center;
    h3{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.36;
      letter-spacing: normal;
      text-align: left;
      color: #242657;
    }
    @media (max-width: 1740px) {
      h3{
        font-size: 20px;
      }
    }
    @media(max-width: 1440px) {
      h3{
        font-size: 14px;
      }
    }
  `
  const divisor = css`
    width: 80%;
    border-top: 1px solid #242657;
    margin: 0 10px 10px 10px;
  `
  const amountRowTitle = css`
    width: 100%;
    display: flex; 
    justify-content: flex-start;
    align-items: center;
    padding-left: 10px;
    h3{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.36;
      letter-spacing: normal;
      text-align: left;
      color: #242657;
    }
    @media (max-width: 1740px) {
      h3{
        font-size: 20px;
      }
    }
    @media(max-width: 1440px) {
      h3{
        font-size: 14px;
      }
    }
  `
  const amountRowNumber = css`
    width: 100%;
    display: flex; 
    justify-content: flex-start;
    align-items: center;
    padding: 0 10px;
    div{
      display: flex; 
      justify-content: space-between;
      width: 100%;
      h2{
        font-size: 42px;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.36;
        letter-spacing: normal;
        text-align: right;
        color: #242657;
        margin:0;
      }
    }
    @media (max-width: 425px) {
      div{
        h2{
          font-size: 16px;
          font-weight: bold;
          font-style: normal;
          font-stretch: normal;
          line-height: 1.36;
          letter-spacing: normal;
          text-align: left;
          color: #242657;
        }
      }
    }
  `
  const amountRowNumberTransaction = css`
    width: 100%;
    display: flex; 
    justify-content: flex-end;
    align-items: center;
    padding: 0 10px;
    h2{
      font-size: 32px;
      font-weight: bold
      font-style: normal;
      font-stretch: normal;
      line-height: 1.36;
      letter-spacing: normal;
      text-align: right;
      color: #242657;
      margin:0;
    }
    @media (max-width: 1740px) {
      h2{
        font-size: 38px;
      }
    }
    @media(max-width: 1440px) {
      h2{
        font-size: 32px;
      }
    }
  `
  const transactionsComponent = css`
    justify-content: space-between;
    padding-top: 15px;
    h2{
      font-size: 63px !important;
      font-weight: normal;
    }
    @media (max-width: 1740px) {
      padding: 15px;
    }
    @media(max-width: 1440px) {
      padding: 0;
    }
    @media (max-width: 425px) {
      h2{
        font-size: 43px !important;
      }
    }
  `

const KpiIndicator = (props) =>{
  return(
    props.title === 'Transacciones' ?
      (
        <div className={`${component} ${transactionsComponent}`}> 
          <div className={countContainer}>
            <h3>CANTIDAD</h3>
          </div>
          <div className={amountRowNumberTransaction}>
            {
              props.data &&
            <h2>
              <CountUp
                start={0}
                end={props.data[0]}
                duration={0.5}
                separator="."
              />
            </h2>
            }
          </div>
        </div> 
      ) :
      (
        <div className={component}>
          <div className={countContainer}>
            <h3>CANTIDAD</h3>
            <div className={divisor}></div>
            {
            //  props.data ? <h3>{props.data[0]}</h3> : null
              props.data ?
                <h3>
                  <CountUp
                  start={0}
                  end={props.data[0]}
                  duration={0.5}
                  separator="."
                  />
                </h3> : null
            }
          </div>
          <div className={amountRowTitle}>
            <h3>VOLUMEN</h3>
          </div>
          <div className={amountRowNumber}>
            {
              props.data &&
            <div>
              <h2>{BS}</h2>
              <h2>
                <CountUp
                  start = {0}
                  end={props.data[1]}
                  separator= '.'
                  decimal= ','
                  decimals={2}
                  duration={1}
                />
             </h2>
            </div>
          }
        </div>
      </div>
      )
  )
}

export default KpiIndicator