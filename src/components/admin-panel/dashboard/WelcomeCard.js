import React from 'react'
import { NavLink } from 'react-router-dom'
import { css } from 'emotion'
import styled from 'react-emotion'

/*CSS*/
  const component = css`
    margin: 50px;
    margin-bottom: 0px;
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    padding: 23px 25px;
    display: flex;
    flex-direction: row;
    @media(max-width: 1230px) {
      margin: 0 25px;
      margin-top: 40px;
    }
    @media(max-width: 600px) {
      flex-direction: column
    }
  `
  const welcomeBody = css`
    display: flex;
    flex-direction: column;
    width: 100%;
    h1{
      margin: 0;
      font-size: 24px;
      text-align: left;
    }
  `
  const divisor = css`
    width: 55px;
    border: 1px solid;
    margin-top: 10px;
  `
  const cardData = css`
    width: 100%;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    margin-top: 12px;
    height: 100%;
    h3{
      font-family: Montserrat;
      font-size: 16px;
      text-align: left;
      margin: 0;
    }
    p{
      margin: 0;
      margin-top: 5px;
      opacity: 0.65;
      font-family: Montserrat;
      font-size: 11px;
      text-align: left;
      color: #f6f7fa;
    }
  `
  const buttonBody = css`
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-end;
    align-items: center;
    width: 100%;
    @media(max-width: 600px) {
      margin-top: 24px;
      flex-direction: column
      justify-content: center;
    }
  `
  const AcceptButton = styled('button') `
    /* height: 100%; */
    padding: 8px 18px;
    border-radius: 5px;
    border-radius: 100px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: center;
    color: #242656;
    cursor: pointer;
    outline: none !important;
  `

  const SendButton = styled('button') `
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #95fad8;
    border: none;
    background-color: #2a2d6a;
    border-radius: 2px;
    cursor:pointer;
    outline:none !important;
    margin-left: 3%;
    @media(max-width: 600px) {
      margin-top: 24px;
    }
  `

const WelcomeCard = ({ displayName, lastSignInTime }) =>
  <div className={component}>
    <div className={welcomeBody}>
      <h1>¡Bienvenido a <b>Gualy</b>!</h1>
      <div className={divisor}></div>
      <div className={cardData}>
        <h3>Has iniciado sesión como <b>{displayName ? displayName : '...'}</b></h3>
        <p>Tú último inicio de sesión fue el {lastSignInTime ? lastSignInTime : '...'}</p>
      </div>
    </div>
    <div className={buttonBody}>
      <NavLink to='/comercios'>
        <AcceptButton>AGREGAR COMERCIO</AcceptButton>
      </NavLink>
      <NavLink to='/notificaciones'>
        <SendButton>ENVIAR&nbsp;NOTIFICACIÓN</SendButton>
      </NavLink>
    </div>
  </div>

export default WelcomeCard