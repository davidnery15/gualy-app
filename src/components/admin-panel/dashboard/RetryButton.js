import React from 'react'
import {css} from 'emotion'

/* CSS */
  const component = css`
    width: 50%;
    height: 300px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(11, 11, 29, 0.50);
  `
  const retryButton = css`
    padding: 8px 18px;
    border-radius: 5px;
    border-radius: 100px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: center;
    color: #242656;
    cursor: pointer;
    outline: none !important;
  `

const RetryButton = ({onClick, text}) => 
  <div className={component}>
    <button className={retryButton} onClick={onClick}>
      {text}
    </button>
  </div>

export default RetryButton