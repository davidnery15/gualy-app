import React from 'react'
import { Doughnut } from 'react-chartjs-2'
import { css } from 'emotion'
import Spinner from 'react-spinkit'

import RetryButton from './../RetryButton'
import arrowIcon from './../../../../assets/drop-down-arrow.svg'

/*CSS*/
  const component = css`
    width: 100%;
    height: 100%;
    min-height: 300px;
    padding: 15px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    @media(max-width: 600px) {
      min-height: 100px;
    }
  `
  const spinnerContainer = css`
    width: 100%;
    height: 100%;
    min-height: 300px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
  const componentChart = css`
    width: 100%;
    height: 100%;
  `
  const dropdownContainer = css`
    width: 50%;
    padding: 0 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0;
    position: relative;
    select{
      top: 0px;
      width: 100%
      border-bottom: 1px solid 
      padding: 0;
      z-index: 1000;
    }
  `
  const customSelect = css`
    height: 47.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    outline:none !important;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    option{
      background-color: #242657;
    }
  `
  const arrowContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    pointer-events: none;
  `
  const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
  const dropdownDiv = css`
    width: 100%;
    display: flex;
    justify-content: center;
  `

const ActivityChart = ({dataChart, loading, onChange, value, activityError, retry, labels, titleText}) => 
  <div className={component}>
    {
      loading ? (<div className={spinnerContainer}> <Spinner color='white'/> </div>) :
        (activityError ? <RetryButton onClick={(e) => retry} text='Reintentar'/> :
          <div className={componentChart}>
            <div className={dropdownDiv}>
              <div className={dropdownContainer}>
                <select className={`${customSelect}`} onChange={onChange} value={value}>
                  <option value="today">Hoy</option>
                  <option value="yesterday">Ayer</option>
                  <option value="lastWeek">La semana pasada</option>
                  <option value="lastMonth">Mes anterior</option>
                  <option value="lastYear">Año anterior</option>
                  <option value="currentWeek">Semana actual</option>
                  <option value="currentMonth">Mes actual</option>
                  <option value="currentYear">Año actual</option>
                </select>
                <div className={arrowContainer}>
                  <img src={arrowIcon} className={arrow} alt="dropdown" />
                </div>
              </div>
            </div>
            <Doughnut
            data={dataChart.reduce((prevValue, currentValue) => currentValue + prevValue) === 0 ?
            {
              labels: ['No hay actividad'],
              datasets: [
                {
                  data: [1],
                  backgroundColor: ['#95fad8'],
                  hoverBackgroundColor: ["#95fad8"]
                }
              ]
            } 
            : 
            {
              labels: labels,
              datasets: [
                {
                  data: dataChart,
                  backgroundColor: [
                    "#09909f",
                    "#8078ff",
                  ],
                  hoverBackgroundColor: [
                    "#076c77",
                    "#665fd4",
                  ]
                }
              ]
            }
            } 
            options={{
              title: {
                display: true,
                text: titleText,
                fontSize: window.innerWidth < 600 ? 10 : 20,
                fontColor: '#fff',
                padding: window.innerWidth < 980 ? 10 : 40,
              },
              labels: {
                fontColor: '#fff', position:' '
              },
              legend: {
                display: true,
                position: 'bottom',
                labels: {
                  padding: window.innerWidth < 980 ? 10 : 40,
                  fontColor: '#fff'
                }
              }, elements: {
                arc: {
                  borderWidth: 0
                }
              },
            }}
            />
          </div>
        )
    }
</div>

export default ActivityChart