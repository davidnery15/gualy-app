import React, { Component } from 'react'
import { css } from 'emotion'
import moment from 'moment';
import ActivityChart from './ActivityChart'
import { postRequest } from '../../../../utils/createAxiosRequest'
/*CSS*/
  const component = css`
    margin: 50px;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    padding: 0 0 23px 0;
    min-height: 300px;
    height: 100%;
    @media(max-width: 1230px) {
      margin: 40px 25px !important;
    }
    @media(max-width: 400px) {
      display: none;
    }
  `
  const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
  const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
  const upperComponent = css`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    margin-top: 23px;
    @media(max-width: 1300px) {
      flex-direction: column;
    }
    @media(max-width: 600px) {
      margin-top: 0;
    }
  }
  `
  const chartContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100%;
    min-height: 300px;
    width:50%;
    @media(max-width: 1300px) {
      width: 100%;
    }
  `
  const verticalDivisor = css`
    height: 100%;
    padding: 0 20px;
    @media(max-width: 1000px) {
      height: 10px; 
    }
  `

export default class VersusCard extends Component {
  state = {
    dataChart: [1],
    allDataChart: {},
    activityLoading: true,
    activityError: false,
    selectedDate: '',
    dataChartUsers: [1],
    allDataChartUsers: {},
    loadingUsers: true,
    errorUsers: false,
    selectedDateUsers: ''
  }

  componentDidMount() {
    this.getActivityData()
    this.getUsersData()
  }

  getUsersData = async () => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = {
      date
    }
    let allDataChartUsers = {}
    let dataChartUsers = []
    try {
      let resp = await postRequest('reports/userReport', data) 
      allDataChartUsers = resp.data
      dataChartUsers = Object.values(allDataChartUsers.today)
      this.setState({
        dataChartUsers,
        allDataChartUsers,
        loadingUsers: false,
        selectedDateUsers: 'today'
      })
    } catch (resp) {
      // console.log(resp);
      this.setState({
        errorUsers: true,
      })
    }
  }

  getActivityData = async () => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = {
      date
    }
    let allDataChart = {}
    let dataChart = []
    try {
      let resp = await postRequest('reports/activityUserCommerce', data)
      allDataChart = resp.data
      dataChart = Object.values(allDataChart.today.transactionAmount)
      this.setState({
        dataChart,
        allDataChart,
        activityLoading: false,
        selectedDate: 'today'
      })
    } catch (resp) {
      // console.log(resp);
      this.setState({
        activityError: true,
      })
    }
  }

  handleDate = (event) => {
    const selectedOption = event.target.value
    let { allDataChart } = this.state
    let dataChart = []

    switch (selectedOption) {
      case 'today':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.today.transactionAmount || {}) : []
        break;
      case 'yesterday':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.yesterday.transactionAmount || {}) : []
        break;
      case 'lastWeek':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.lastWeek.transactionAmount || {}) : []
        break;
      case 'lastMonth':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.lastMonth.transactionAmount || {}) : []
        break;
      case 'lastYear':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.lastYear.transactionAmount || {}) : []
        break;
      case 'currentWeek':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.currentWeek.transactionAmount || {}) : []
        break;
      case 'currentMonth':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.currentMonth.transactionAmount || {}) : []
        break;
      case 'currentYear':
        dataChart = typeof allDataChart === 'object' ? Object.values(allDataChart.currentYear.transactionAmount || {}) : []
        break;
      default:
        break;
    }
    this.setState({
      selectedDate: event.target.value,
      dataChart
    })
  }

  handleDateUsers = (event) => {
    const selectedOption = event.target.value
    let { allDataChartUsers } = this.state
    let dataChartUsers = []

    switch (selectedOption) {
      case 'today':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.today || {}) : []
        break;
      case 'yesterday':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.yesterday || {}) : []
        break;
      case 'lastWeek':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.lastWeek || {}) : []
        break;
      case 'lastMonth':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.lastMonth || {}) : []
        break;
      case 'lastYear':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.lastYear || {}) : []
        break;
      case 'currentWeek':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.currentWeek || {}) : []
        break;
      case 'currentMonth':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.currentMonth || {}) : []
        break;
      case 'currentYear':
        dataChartUsers = typeof allDataChartUsers === 'object' ? Object.values(allDataChartUsers.currentYear || {}) : []
        break;
      default:
        break;
    }
    this.setState({
      selectedDateUsers: event.target.value,
      dataChartUsers
    })
  }

  render() {
    return (
      <div className={component}>
        <div className={header}>
          <h1> VERSUS </h1>
        </div>
        <div className={divisor}></div>
        <div className={`${upperComponent} upperComponent`}>
          <div className={`${chartContainer} chartContainer`}>
            <ActivityChart
              dataChart={this.state.dataChart}
              loading={this.state.activityLoading}
              onChange={this.handleDate}
              value={this.state.selectedDate}
              activityError={this.state.activityError}
              retry={this.getActivityData}
              labels={[ 'Comercio a Comercio', 'Usuario a Usuario']}
              titleText='Actividad Comercial vs Usuarios'
            />
          </div>
          <div className={verticalDivisor}></div>
          <div className={`${chartContainer} chartContainer`}>
            <ActivityChart
              dataChart={this.state.dataChartUsers}
              loading={this.state.loadingUsers}
              onChange={this.handleDateUsers}
              value={this.state.selectedDateUsers}
              activityError={this.state.errorUsers}
              labels={['Comercios', 'Usuarios']}
              retry={this.getUsersData}
              titleText='Nuevos Usuarios'
            />
          </div>
        </div>
      </div>
    )
  }
}