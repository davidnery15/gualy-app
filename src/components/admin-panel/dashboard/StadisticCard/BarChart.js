import React from 'react'
import { Bar } from 'react-chartjs-2'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import moment from 'moment'

/*CSS*/
  const component = css`
    width: 100%;
    height: 100%;
    max-width: 1400px;
    min-height: 300px;
    padding: 15px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #242656;
  `
  const spinnerContainer = css`
    width: 100%;
    height: 100%;
    min-height: 300px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `

const BarChart = ({data, loading}) => {
  const days = []
  for (let i = 0; i < 10; i++) {
    days[i] = moment().utc().subtract(i, 'days').format('YYYY-MM-DD').toString(); 
  }  
  return(
    <div className={component}>
      {
        loading ? (<div className={spinnerContainer}> <Spinner color='white' /> </div>) :
          (
            <Bar data={{
              labels: days.reverse(),
              datasets: [
                { label: 'Balance Diario',
                  data: data.reverse(),
                  backgroundColor: [
                    "#8078ff", "#8078ff", "#8078ff", "#8078ff", "#8078ff", "#8078ff", "#8078ff", "#8078ff", "#8078ff", "#8078ff" 
                  ],
                  hoverBackgroundColor: [
                    "#95fad8", "#95fad8", "#95fad8", "#95fad8", "#95fad8", "#95fad8", "#95fad8", "#95fad8", "#95fad8", "#95fad8"
                  ]
                }
              ]
            }}
            options={{
              title: {
                display: true,
                text: 'Evolución del Balance',
                fontSize: 20,
                fontColor: '#fff'
              },
              legend: {
                display: true,
                position: 'bottom',
                labels: {
                  fontColor: '#fff'
                }
              },
              scales: {
                yAxes: [{
                  ticks: {
                    fontColor: "white",
                    beginAtZero: true
                  }
                }],
                xAxes: [{
                  ticks: {
                    fontColor: "white",
                  }
                }]
              }, elements: {
                arc: {
                  borderWidth: 0
                }
              },
            }}/>
          )
    }
  </div>
  )
}

export default BarChart