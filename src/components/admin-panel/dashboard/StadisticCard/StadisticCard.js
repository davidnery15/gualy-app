import React, { Component } from 'react'
import { css } from 'emotion'
import   moment from 'moment';
import BarChart from './BarChart'
import { postRequest } from '../../../../utils/createAxiosRequest'
/*CSS*/
  const component = css`
    margin: 50px;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    padding: 0 0 34px 0;
    @media(max-width: 1230px) {
      margin: 40px 25px !important;
    }
    @media(max-width: 540px) {
      display: none;
    }
  `
  const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
  const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
    margin-bottom: 34px;
  `
  const upperComponent = css`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    margin-top: 23px;
    @media(max-width: 1000px) {
      flex-direction: column;
    }
    @media(max-width: 600px) {
      margin-top: 0;
    }
  }
  `
  const chartContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100%;
    min-height: 300px;
    width:50%;
    @media(max-width: 1000px) {
      width: 100%;
    }
  `
  const responsive = css`
    width: 100% !important;
    @media(max-width: 540px) {
      display: none;
    }
  `

export default class StadisticCard extends Component{
  state = { 
    evolutionData: {},
    barChartData: [],
    evolutionLoading: true,
    evolutionError: false,
  }

  interval = null

  componentDidMount() {
    this.getCommerceEvolutionData()
    this.interval = setInterval(() => { 
      this.getCommerceEvolutionData()
    }, 60000);
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  getCommerceEvolutionData = async() => {
    const date = moment().utc().format('YYYY-MM-DD')
    const data = {
      date
    }
    let evolutionData = {}
    let barChartData = []
    try {
      let resp = await postRequest('reports/commerceEvolution', data) 
      evolutionData = Object.values(resp.data)
      evolutionData.map( data => {
        return barChartData.push(data.amount)
      })
      this.setState({
        evolutionData,
        barChartData,
        evolutionLoading: false
      })
    } catch (error) {
      // console.log('StadisticCard error', error)
    }
    
  }

  render() {
    return(
      <div className={component}>
        <div className={header}>
          <h1> ESTADÍSTICAS </h1>
            </div>
          <div className={divisor}></div>
        <div className={`${upperComponent} upperComponent`} css={`margin-top: 0 !important;`}>
          <div className={`${chartContainer} ${responsive} chartContainer`}>
            <BarChart data={this.state.barChartData === undefined ? [0] : this.state.barChartData} loading={this.state.evolutionLoading}/>
          </div>
        </div>
      </div>
    )
  }
}