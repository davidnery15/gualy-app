import React from 'react'
import moment from 'moment'
// eslint-disable-next-line
import es from 'moment/locale/es'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

import { auth, db } from './../../../firebase'
import WelcomeCard from './WelcomeCard'
import StadisticCard from './StadisticCard/StadisticCard'
import VersusCard from './VersusCard/VersusCard'
import KpiCard from './KpiCard/KpiCard'
import Header from '../AdminHeader'

class Dashboard extends React.Component {
  state = { 
    userLogged: true,
    loading: true,
    userData: {}
  }

  componentDidMount = () => {
    auth.onAuthStateChanged((user) => {
      user ?
        this.setState({
          userLogged: true
        })
        :
        this.setState({
          userLogged: false
        })
      if (user) {
        const adminUid = user.uid ? user.uid : null
        const displayName = user.displayName ? user.displayName : null
        const lastSignInTime = moment(user.metadata.lastSignInTime).utc(-264).format('DD-MM-YYYY h:mm a')
        const userEmail = user.email ? user.email : null
        const usersRef = db.ref('users').orderByChild('userKey').equalTo(adminUid.toString())
        let userData = {}
        usersRef.on('value', async (snapshot) => {
          const data = Object.values(snapshot.val())
          userData = {
            profilePicture: data[0].profilePicture,
            displayName: displayName,
            email: userEmail,
            lastSignInTime: lastSignInTime,
            type: data[0].type,
            userId: adminUid,
            userLogged: true
          }
          this.setState({
            loading: false,
            userData
          })
        })
      }
    })
  }

  redirectAdmin = () => {
    if (this.state.userLogged) {
      if (this.state.userData.type) {
        const redirect = (this.state.userData.type === 'admin' || this.state.userData.type === 'marketing') ? null : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }

  render() {
    const { displayName, lastSignInTime } = this.state.userData
    
    return (
      <div>
        <Header />
        <WelcomeCard displayName={displayName} lastSignInTime={lastSignInTime}/>
        <KpiCard />
        <StadisticCard />
        <VersusCard />
        {
          this.redirectAdmin() 
        }
      </div>
    );
  }
}

export default connect(() => ({}))(Dashboard)