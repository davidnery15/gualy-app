import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import Header from '../AdminHeader'
import { css } from 'emotion'
import ic_account_balance_wallet from '../../../assets/ic_account_balance_wallet.svg'
import ic_top_users from '../../../assets/ic_top_users.svg'
import ic_top_business from '../../../assets/ic_top_business.svg'
// import ic_receipt from '../../../assets/ic_receipt.svg'
import ic_black_list_users from '../../../assets/ic_black_list_users.svg'
import ic_group_add_users from '../../../assets/ic_group_add_24px.svg'
import ic_black_list_transactions from '../../../assets/ic_black_list_transactions.svg'
import ic_per_users_transactions from '../../../assets/ic_per_users_transactions.svg'
import ReportButton from './ReportButton'



const reportesContainer = css`
  display: flex;
  justify-content: center;
  align-items: center;
`

const buttonsContainer = css`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin: 50px;
  max-width: 900px;
  @media(max-width: 346px) {
    margin: 20px;
  }
`
class Reportes extends Component {
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.props.userInCollectionData.type) {
        const redirect = this.props.userInCollectionData.type === 'admin'? null : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  render() {
    return(
      <div>
        <Header/>
        <div className={reportesContainer}>
          <div className={buttonsContainer}>
            <ReportButton
              iconUrl={ic_account_balance_wallet}
              text='BALANCE'
              to='/balance'
            />
            <ReportButton
              iconUrl={ic_top_users}
              text='TOP USUARIOS'
              to='/top-usuarios'
            />
            <ReportButton
              iconUrl={ic_top_business}
              text='TOP COMERCIOS'
              to='/top-comercios'
            />
            {/* <ReportButton
              iconUrl={ic_receipt}
              text='RESUMEN FEES'
              /> */}
            <ReportButton
              iconUrl={ic_black_list_users}
              text='LISTA NEGRA DE USUARIOS'
              to='/lista-negra-usuarios'
            />
            <ReportButton
              iconUrl={ic_black_list_transactions}
              text='LISTA NEGRA DE TRANSFERENCIAS'
              to='/lista-negra-transferencias'
            />
            <ReportButton
              iconUrl={ic_per_users_transactions}
              text='TRANSFERENCIA POR USUARIO'
            />
            <ReportButton
              iconUrl={ic_group_add_users}
              text='REFERIDOS'
            />
          </div>
        </div>
        {
          this.redirect()
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn
  }
}

export default connect(mapStateToProps)(Reportes)