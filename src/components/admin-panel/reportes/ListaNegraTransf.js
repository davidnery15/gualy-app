import React, { Component } from 'react'
import Header from '../AdminHeader'
import { css } from 'emotion'
import Table from './Table'
import { postRequest } from '../../../utils/createAxiosRequest'
// CSS
  const container = css`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: center;
    /* border: 1px solid red; */
  `
  const card = css`
    margin: 50px;
    box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    border-radius: 5px;
    background-color: #2a2c6a;
  `
  const amountFont = css`
    font-size: 18px;
    font-weight: bold;
    text-align: right;
    color: #8078ff;
  `

class ListaNegraTransf extends Component {

  state = {
    data: []
  }

  componentDidMount = () => {
     postRequest('reports/blacklistReport', {}) 
      .then(response => {
        // console.log('transferencias blacklist', response.data.data)
        this.setState({
          data: response.data.data.blacklist,
        })
      }).catch(error => {
        // console.error('Error transferencias blacklist: ', error)
      })
  }

  render() {
    return (
      <div>
        <Header />
        <div className={container}>
          <div className={card}>
            <Table
              title='Lista negra de transferencias'
              columns={[
                {
                  name: 'ID',
                  propName: 'id'
                },
                {
                  name: 'Nombre de usuario',
                  propName: 'username'
                },
                {
                  name: 'Fecha',
                  propName: 'date'
                },
                {
                  name: 'Uid',
                  propName: 'uid'
                },
                {
                  name: 'Numero de transaccion',
                  propName: 'transactionNumber',
                  customClassName: css`
                    text-align: right;
                  `
                },
                {
                  name: 'Volumen de la transaccion',
                  propName: 'amount',
                  customClassName: amountFont,
                },
              ]}
              data={this.state.data}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default ListaNegraTransf