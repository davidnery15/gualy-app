import React, { Component } from 'react'
import Header from '../AdminHeader'
import { css } from 'emotion'
import Table from './Table'
import { postRequest } from '../../../utils/createAxiosRequest'
// CSS
const container = css`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: center;
    /* border: 1px solid red; */
  `
const card = css`
    margin: 50px;
    box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    border-radius: 5px;
    background-color: #2a2c6a;
  `

class ListaNegraUsuarios extends Component {

  state = {
    data: []
  }

  componentDidMount = () => {
    postRequest('userBlacklistReport', {})
      .then(response => {
        //console.log('usuarios blacklist', response.data.data)
        this.setState({
          data: response.data.data.usersBlacklist,
        })
      }).catch(error => {
        //console.error('Error listanegra usuarios: ', error)
      })
  }

  render() {
    return (
      <div>
        <Header />
        <div className={container}>
          <div className={card}>
            <Table
              title='Lista negra de usuarios'
              columns={[
                {
                  name: 'ID',
                  propName: 'id'
                },
                {
                  name: 'Nombre de usuario',
                  propName: 'username'
                },
                {
                  name: 'Correo electronico',
                  propName: 'email'
                },
                {
                  name: 'Telefono',
                  propName: 'uid'
                },
                {
                  name: 'Fecha',
                  propName: 'date'
                },
              ]}
              data={this.state.data}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default ListaNegraUsuarios