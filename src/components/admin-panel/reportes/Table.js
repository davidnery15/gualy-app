import React from 'react'
import { css } from 'emotion'
import TableHeader from '../../general/tables/TableHeader'
import CountUp from 'react-countup'
import Spinner from 'react-spinkit'
import { BS } from '../../../config/currencies.js'
import AcceptButton from '../../general/AcceptButton'
import { CSVLink } from 'react-csv'
import moment from 'moment'
import GoBackButton from '../../general/goBackButton'
// CSS
const tableContainer = css`
    display: flex;
    flex-direction: column;
    padding: 20px;
    position: relative;
    @media(max-width: 850px) {
      display: none;
    }
  `
const separator = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const table = css`
    td {
      /* border: 1px solid red; */
      padding: 20px;
    }
    th {
      padding: 20px;
    }
  `
const tableContainerMobile = css`
    display: none;
    flex-direction: column;
    min-height: 400px;
    position: relative;
    @media(max-width: 850px) {
      display: flex;
    }
  `
const tableMobileRow = css`
    display: flex;
    flex-direction: column;
    padding: 5px;
    border-bottom: 1px solid #1d1f4a;
  `
const tableMobileColumn = css`
    display: flex;
    justify-content: space-between;
    span {
      @media(max-width: 505px) {
        font-size: 12px;
      }
    }
  `
const tableMobileColumnTitle = css`
    font-weight: bold;
  `
const nameCss = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    text-align: right;
  `
const imgRound = css`
    border-radius: 1000px;
    width: 35px;
    height: 35px;
    margin: 5px;
  `
const spinnerContainer = css`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 300px;
    width: 100%;
  `
const reportButtonContainer = css`
    display: flex;
    margin: auto
  `
const reportButtonContainerMobile = css`
    display: flex;
    margin: auto;
    margin-bottom: 20px
  `
const tableHeaderContainer = css`
    display: flex;
  `
const goBackButton = css`
  position: absolute;
  top: 21px;
  left: 21px;
  border: none;
  background: transparent;
  height: 21px;
  width: 21px;
  cursor: pointer;
    `

const DesktopTable = ({
  columns = [],
  data = [],
  title = 'Inserte un titulo aqui',
  exportToExcel,
  topUsers,
  headers
}) =>
  <div className={tableContainer}>
    <GoBackButton
      height="20px"
      width="20px"
      style={goBackButton}
      route="/reportes"
    />
    <div className={tableHeaderContainer}>
      <TableHeader
        title={title}
        filter={true}
        icon={true}
      />
      <div className={reportButtonContainer}>
        {topUsers
          ? topUsers.length > 0
            ? <CSVLink filename={`${title}-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.csv`} css={`margin: auto`} data={topUsers} headers={headers}>
              <AcceptButton
                width="auto"
                height="38px"
                content="DESCARGAR CSV"
                onClick={exportToExcel}
              />
            </CSVLink>
            : null
          : null
        }
      </div>
    </div>
    <div className={separator}></div>
    {data.length > 0 ?
      <table className={table}>
        <thead>
          <tr>
            {columns.length > 0
              ? columns.map(({ name, propName }) =>
                <th
                  className={`${propName === 'amount' || propName === 'count' ? css`text-align: right;` : ''}`}
                  key={propName}
                >
                  {name}
                </th>
              )
              : null
            }
          </tr>
        </thead>
        <tbody>
          {columns.length > 0
            ? data.map((item, index) =>
              <tr key={index}>
                {columns.map(({ propName, customClassName = '' }) =>
                  <td
                    key={propName}
                    className={`${propName === 'name' || propName === 'username' ? nameCss : ''} ${customClassName}`}
                  >
                    {(propName === 'name' || propName === 'username') && (item.thumbnail || item.profilePicture)
                      ? <img className={imgRound} src={item.thumbnail || item.profilePicture} alt='thumbnail' />
                      : null
                    }
                    {propName === 'amount'
                      ? <CountUp
                        start={item[propName]}
                        end={item[propName]}
                        duration={0}
                        separator="."
                        decimals={2}
                        decimal=","
                        prefix={`${BS}`}
                      />
                      : <span>
                        {item[propName]}
                      </span>
                    }
                  </td>
                )}
              </tr>
            )
            : <div>Please define the column descriptor</div>
          }
        </tbody>
      </table>
      : <div className={spinnerContainer}> <Spinner color='white' /> </div>
    }
  </div>

const MobileTable = ({
  data = [],
  columns = [],
  title = 'Inserte un titulo aqui',
  exportToExcel,
  topUsers,
  headers
}) =>
  <div className={tableContainerMobile}>
    <GoBackButton
      height="20px"
      width="20px"
      style={goBackButton}
      route="/reportes"
    />
    <TableHeader
      title={title}
      filter={true}
      icon={true}
    />
    <div className={reportButtonContainerMobile}>
      {topUsers
        ? topUsers.length > 0
          ? <CSVLink filename={`${title}-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.csv`} css={`margin: auto`} data={topUsers} headers={headers}>
            <AcceptButton
              width="auto"
              height="38px"
              content="DESCARGAR CSV"
              onClick={exportToExcel}
            />
          </CSVLink>
          : null
        : null
      }
    </div>
    <div className={separator}></div>
    {columns.length > 0 && data.length > 0
      ? data.map((item, index) =>
        <div className={tableMobileRow} key={index}>
          {columns.map(({ propName, name, customClassName }) =>
            <div className={tableMobileColumn} key={propName}>
              <span className={tableMobileColumnTitle}>{name}</span>
              <span className={customClassName}>
                {propName === 'amount'
                  ? <CountUp
                    start={item[propName]}
                    end={item[propName]}
                    duration={0}
                    separator="."
                    decimals={2}
                    decimal=","
                    prefix={`${BS}`}
                  />
                  : item[propName]
                }
              </span>
            </div>
          )}
        </div>
      )
      : <div className={spinnerContainer}> <Spinner color='white' /> </div>
    }
  </div>

export default props =>
  <div>
    <DesktopTable {...props} />
    <MobileTable {...props} />
  </div>

