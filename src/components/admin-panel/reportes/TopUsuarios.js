import React, { Component } from 'react'
import Header from '../AdminHeader'
import { css } from 'emotion'
import Table from './Table'
import moment from 'moment'
import { postRequest } from '../../../utils/createAxiosRequest'
// CSS
const container = css`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: center;
    /* border: 1px solid red; */
  `
const card = css`
    margin: 50px;
    box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    border-radius: 5px;
    background-color: #2a2c6a;
  `
const amountFont = css`
    font-size: 18px;
    font-weight: bold;
    text-align: right;
    color: #8078ff;
  `

class TopUsuarios extends Component {

  state = {
    data: [],
    startDate: '2018-05-05',
    endDate: moment().format('YYYY-MM-DD'),
    textFile: null,
  }

  componentDidMount = () => {
    const { startDate, endDate } = this.state
    postRequest('reports/topUsers', {
      startDate,
      endDate,
    })
      .then(response => {
        //console.log('users', response.data)
        this.setState({
          data: response.data.data
        })
      }).catch(error => {
        //console.error('error api call', error)
      })
  }

  headers = [
    { label: 'ID', key: 'uid' },
    { label: 'Nombre', key: 'name' },
    { label: 'Email', key: 'email' },
    { label: 'Cantidad de transacciones', key: 'count' },
    { label: 'Volumen de transacciones', key: 'amount' },
  ];

  render() {
    // console.log("data: ", this.state.data)
    return (
      <div>
        <Header />
        <div className={container}>
          <div className={card}>
            <Table
              headers={this.headers}
              title='Top de usuarios'
              textFile={this.state.textFile}
              topUsers={this.state.data}
              filename='report_top_usuarios.xlsx'
              columns={[
                {
                  name: 'ID',
                  propName: 'id'
                },
                {
                  name: 'Nombre',
                  propName: 'name'
                },
                // {
                //   name: 'Documento de identidad',
                //   propName: 'dni'
                // },
                {
                  name: 'Email',
                  propName: 'email'
                },
                {
                  name: 'Cantidad de transacciones',
                  propName: 'count',
                  customClassName: css`
                    text-align: right;
                  `,
                },
                {
                  name: 'Volumen de transacciones',
                  propName: 'amount',
                  customClassName: amountFont,
                },
              ]}
              data={this.state.data.map((us, index) => ({ ...us, id: index + 1 }))}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default TopUsuarios