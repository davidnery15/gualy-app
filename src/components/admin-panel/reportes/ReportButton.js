import React from 'react'
import { css } from 'emotion'
import { Link } from 'react-router-dom'

const buttonContainer = css`
  display: flex;
  width: 33%;
  align-items: center;
  flex-direction: column;
  text-decoration: none !important;
  cursor: pointer;
  h3 {
    margin-top: 15px;
    font-size: 20px;
    letter-spacing: 1.5px;
    text-align: center;
    line-height: 30px;
    color: white;
  }
  @media (max-width: 768px) {
    width: 50%;
  }
  @media(max-width: 590px) {
    h3 {
      font-size: 10px;
      margin-top: 5px;
      line-height: 13px;
    }
  }
`

const reportButton = css`
  width: 182.7px;
  height: 182.7px;
  border: none;
  border-radius: 121px;
  box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.16);
  background-color: #95fad8;
  cursor: pointer;
  img {
    width: 50%;
  }
  @media(max-width: 590px) {
    width: 98px;
    height: 98px;
  }
`

export default ({to='', iconUrl='', text='', ...props}) =>
  <Link to={to} className={buttonContainer}>
    <button className={reportButton} {...props}>
      <img src={iconUrl} alt='icon'/>
    </button>
    <h3>
      {text}
    </h3>
  </Link>
