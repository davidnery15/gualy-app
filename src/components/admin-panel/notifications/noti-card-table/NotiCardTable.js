import React from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import OptionsModalWrapper from './../../../general/OptionsModalWrapper'
import DateRangeModal from './../../../general/modals/DateRangeModal'
import TableHeader from './../../../general/tables/TableHeader'
import NotiTable from './NotiTable'
import MobileNotiTable from './MobileNotiTable'
import { customConsoleLog } from '../../../../utils/customConsoleLog'
import { toast } from 'react-toastify'
import { postRequest } from '../../../../utils/createAxiosRequest'
import throttle from 'lodash.throttle'
import moment from 'moment'
/* CSS */
const component = css`
    margin: 50px;
    border-radius: 5px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    background-color: #2a2c6a;
    @media (max-width: 1230px) {
      margin: 0 12px;
      margin-top: 40px;
      margin-bottom: 40px;
    }
  `
const responsive = css`
    display: none;
    @media (max-width: 620px) {
      display: flex !important;
      flex-direction: column;
    }
  `
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const cardTableComponent = css`
    overflow-x: hidden;
    height: 450px;
    padding-left: 32px;
    padding-right: 32px;
    position: relative;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 3px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 2px;
    }
    ::-webkit-scrollbar {
        width: 7px;
    }
    @media (max-width: 620px) {
      display: none;
      padding-left: 0;
      padding-right: 0;
      height: 383px;
    }
  `
const cardTableBottom = css`
    position: sticky;
    bottom: -1px;
    width: 100%;
    height: 100px;;
    background-image: linear-gradient(to bottom, rgba(41, 43, 105, 0), rgba(41, 43, 105, 0.5) 49%, #2a2c6a);
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
const noRegistersText = css`
opacity: 0.5;
text-align: center;
`
export default class NotiCardTable extends React.Component {
  state = ({
    search: '',
    selection: {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection',
    },
    isDateFilter: false,
    filteredTransactions: "",
    isSearchingData: false,
    dateRangePickerIsOpen: false,
  })

  updateSearch = (event) => {
    this.setState({
      search: event.target.value.substr(0, 120),
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  throtled = throttle(async () => {
    if (this.state.search || this.state.isDateFilter) {
      this.setState({ isSearchingData: true })
      const data = this.state.isDateFilter
        ? {
          adminID: this.props.userKey,
          startDate: moment(this.state.selection.startDate).format('YYYY-MM-DD'),
          finishDate: moment(this.state.selection.endDate).format('YYYY-MM-DD'),
        }
        : {
          adminID: this.props.userKey,
          query: this.state.search,
        }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest(`pushNotification/adminFilter?mode=${this.state.isDateFilter ? "date" : "fields"}`,
          data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success && resp.data.data.message.success) {
          this.setState({
            filteredTransactions: Object.values(resp.data.data.message.message),
            isSearchingData: false
          })
        } else {
          this.setState({
            filteredTransactions: "",
            isSearchingData: false
          })
        }
      } catch (error) {
        this.setState({ isSearchingData: false })
        customConsoleLog(error)
        toast.error("Verifique su conexión y vuelva a intentarlo.")
      }
    }
  }, 1000)
  onScrollMobile = () => {
    customConsoleLog("mobileScroll: ")
    if (
      (this.mobileTable.scrollHeight - this.mobileTable.scrollTop === this.mobileTable.clientHeight)
      && this.props.notificationsTransactionsRequest
    ) {
      this.props.notificationsTransactionsRequest()
    }
  }
  onScrollDesktop = () => {
    customConsoleLog("desktopScroll: ")
    if (
      (this.desktopTable.scrollHeight - this.desktopTable.scrollTop === this.desktopTable.clientHeight)
      && this.props.notificationsTransactionsRequest
    ) {
      this.props.notificationsTransactionsRequest()
    }
  }
  closeDateFilter = () => {
    this.setState({
      isDateFilter: false,
      filteredTransactions: "",
    })
  }
  openDateRangeModal = () => {
    this.setState({
      dateRangePickerIsOpen: !this.state.dateRangePickerIsOpen,
      isDateFilter: false,
      selection: {
        ...this.state.selection,
        startDate: new Date(),
        endDate: new Date(),
      }
    })
  }
  searchDateRange = () => {
    this.setState({
      isDateFilter: true,
      dateRangePickerIsOpen: false,
      search: "",
    }, () => {
      this.throtled()
    })
  }
  handleSelect = (ranges) => {
    this.setState({
      selection: {
        ...this.state.selection,
        startDate: ranges.selection.startDate,
        endDate: ranges.selection.endDate,
      }
    })
  }
  goBackButtonClick = () => {
    this.setState({
      isDateFilter: false,
      search: "",
      filteredTransactions: "",
    })
  }
  render() {
    const {
      notifications,
      title,
      loading
    } = this.props
    const {
      search,
      filteredTransactions,
      isDateFilter,
      isSearchingData,
      selection,
      dateRangePickerIsOpen
    } = this.state
    const notificationsFiltered = search || isDateFilter
      ? filteredTransactions
      : notifications
    return (
      <div className={component}>
        <TableHeader
          title={title}
          updateSearch={this.updateSearch}
          search={search}
          placeholder='Buscar...'
          icon={true}
          isSearchingData={isSearchingData}
          openDateRangeModal={this.openDateRangeModal}
          dateRangePickerIsOpen={dateRangePickerIsOpen}
          dateRangeFilterHandleSelect={this.handleSelect}
          ranges={selection}
          searchDateRange={this.searchDateRange}
          isBackButtonShow={search || isDateFilter}
          goBackButtonClick={this.goBackButtonClick}
        />
        <div className={tableDivisor}></div>
        <div
          ref={ref => this.desktopTable = ref}
          onScroll={this.onScrollDesktop}
          className={cardTableComponent}
        >
          {
            isSearchingData || loading
              ? (<div className={spinnerContainer}> <Spinner color='white' /> </div>)
              : notificationsFiltered && notificationsFiltered.length > 0
                ? <NotiTable notifications={(notificationsFiltered)} />
                : <div className={spinnerContainer}>
                  <h1 className={noRegistersText}>No hay registros</h1>
                </div>
          }
          <div className={cardTableBottom}></div>
        </div>
        <div
          ref={ref => this.mobileTable = ref}
          onScroll={this.onScrollMobile}
          className={`${cardTableComponent} ${responsive}`}
        >
          {
            isSearchingData || loading
              ? (<div className={spinnerContainer}> <Spinner color='white' /> </div>)
              : notificationsFiltered && notificationsFiltered.length > 0
                ? (notificationsFiltered.map(noti =>
                  <MobileNotiTable
                    notifications={noti}
                    key={noti.notificationUID}
                  />
                ))
                : <div className={spinnerContainer}>
                  <h1 className={noRegistersText}>No hay registros</h1>
                </div>
          }
        </div>
        <OptionsModalWrapper
          show={this.state.dateRangePickerIsOpen}
          onClose={this.openDateRangeModal}
          width={310}
        >
          <DateRangeModal
            onChange={this.handleSelect}
            moveRangeOnFirstSelection={false}
            ranges={this.state.selection}
            onSearchClick={this.searchDateRange}
          />
        </OptionsModalWrapper>
      </div>
    )
  }
}