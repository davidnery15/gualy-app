import React from 'react'
import { css } from 'emotion'

/* CSS */
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    margin-top: 15px;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const mobileTable = css`
    td{
      padding: 0 22px;
      padding-top: 15px;
      width: 100%
    }
  `
const tdTitle = css`
    font-size: 11px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.09;
    letter-spacing: 0.9px;
    text-align: left;
    color: #f6f7fa;
  `
const tdData = css`
    font-size: 10px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1;
    letter-spacing: normal;
    text-align: right;
    color: #f6f7fa;
  `
const textContainer = css`
    span{
      font-size: 11px;
      font-weight: 900;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.42;
      letter-spacing: normal;
    }
    @media (max-width: 360px) {
      width: 120px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
const usersContainer = css`
  position: relative;
  height: 50px;
  width: 100%;
  overflow: auto;
 `
const scrollContainer = css`
  position: absolute;
 `
const MobileNotiTable = ({ notifications }) =>
  <div>
    <table className={mobileTable}>
      <tbody>
        <tr>
          <td className={tdTitle}>
            <span> Fecha <br /> y&nbsp;hora</span>
          </td>
          <td className={tdData}>
            <span>
              {notifications.date}<br />{notifications.time}
            </span>
          </td>
        </tr>
        <tr>
          <td className={tdTitle}>Enviado por</td>
          <td className={tdData}>
            <div className={textContainer}>
              <span>{notifications.senderUsername}</span>
              <br />
              {notifications.senderEmail}
            </div>
          </td>
        </tr>
        <tr>
          <td className={tdTitle}>Receptor</td>
          <td className={tdData}>
            <div className={textContainer}>
              <div className={usersContainer}>
                <div className={scrollContainer}>
                  {notifications.users.map((user, i) => <p key={i}>{user.email}</p>)}
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td className={tdTitle}>Contenido</td>
          <td className={tdData}>{notifications.body}</td>
        </tr>
      </tbody>
    </table>
    <div className={tableDivisor}></div>
  </div>


export default MobileNotiTable