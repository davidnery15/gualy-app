import React from 'react'
import { css } from 'emotion'

import NotiRow from './NotiRow'

/* CSS */
  const cardTable = css`
    width: 100%;
    padding: 0 15px;
    border-collapse: collapse;
  `
  const tHead = css`
    text-align: center; 
  `
  const tHeadTitle = css`
    text-align: left; 
    @media (max-width: 1000px) {
      text-align: center; 
    }
  `
  const tHeadUsers = css`
    text-align: left; 
    padding: 40px 43px;
    @media (max-width: 1000px) {
      padding: 40px 6px;
    }
  `
  const tHeadTitleDate = css`
    text-align: left; 
    padding-right: 10px;
    @media (max-width: 1000px){
      span{
        display: none;
      }
    }

  `

const NotiTable = ({ notifications }) => {
  return (
    <table className={cardTable}>
      <thead className={tHead}>
        <tr>
          <th className={tHeadTitleDate}>Fecha y&nbsp;hora <span>de envío</span></th>
          <th className={tHeadUsers}>Enviado por</th>
          <th className={tHeadUsers}>Receptor</th>
          <th className={tHeadTitle}>Contenido</th>
        </tr>
      </thead>
      <tbody>
        {
          notifications.map(noti =>
            <NotiRow
              info={noti}
              key={noti.notificationUID}
            />
          )
        }
      </tbody>
    </table>
  )
}


export default NotiTable