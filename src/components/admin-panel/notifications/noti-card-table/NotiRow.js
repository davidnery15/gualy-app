import React from 'react'
import { css } from 'emotion'
import User from './../../../general/User'
/* CSS */
const tData = css`
    text-align: left; 
  `
const tDataDate = css`
    width: 14%;
    padding-right: 30px;
    min-width: 104px;
  `
const tDataUsers = css`
    width: 23%;
  `
const tDataDescription = css`
    text-align: left; 
    width: 20%;
    @media (max-width: 1070px) {
      width: 140px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
const dateData = css`
    font-size: 11.8px;
    font-weight: 300;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #f6f7fa;
  `
const descriptionData = css`
    @media (max-width: 1070px) {
      width: 140px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    @media (max-width: 800px) {
      width: 80px;
    }
  `
const usersContainer = css`
   position: relative;
   height: 100px;
   width: 80%;
   overflow: auto;
  `
const scrollContainer = css`
   position: absolute;
  `
const NotiRow = ({ info }) => {
  // const receiverUser = {
  //   profileUri: info.users[0].profilePicture,
  //   uid: null,
  //   name: info.users ? (info.users.length <= 1 ? info.users[0].name : (`${info.users[0].name} y ${(info.users.length ? info.users.length : '') - 1} más`)) : '',
  //   email: info.usersType ? (info.usersType.length > 1 ? 'Todos' : (info.usersType[0] === 'client' ? 'Cliente' : 'Commercio')) : ''
  // }
  const senderUser = {
    profileUri: info.senderProfilePicture,
    uid: info.senderUid,
    name: info.senderUsername,
    email: 'Administrador(HD)'
  }
  return (
    <tr>
      <td className={`${tData} ${tDataDate}`}><span className={dateData}>{info.date}{' '}{info.time}</span></td>
      <td className={tDataUsers}><User textAlign="left" info={senderUser} /></td>
      <td className={tDataUsers}>
        <div className={usersContainer}>
          <div className={scrollContainer}>
            {info.users.map((user, i) => <p key={i}>{user.email}</p>)}
          </div>
        </div>
      </td>
      <td className={tDataDescription}>
        <div className={dateData}>
          <p className={descriptionData}>{info.body}</p>
        </div>
      </td>
    </tr>
  )
}
export default NotiRow