import React from 'react'
import { css } from 'emotion'

import gualyIcon from './../../../../assets/logos/logo-gualy-03.png'

/* CSS */
  const component = css`
    display: none;  
    @media(max-width: 608px){
      display: flex;
      flex-direction: column
      h4{
        font-size: 13px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: 1.1px;
        color: #f6f7fa;
      }
    }
  `
  const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
  const body = css`
    display: flex;
    justify-content: space-between;
    margin-bottom: 0px;
    flex-direction: column;
    margin: 24px 25px;
  `
  const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
  const switchContainer = css`
    display: flex;
    justify-content: space-around;
  `
  const activeSwitch = css`
    padding: 14px 37px
    border-radius: 34.4px;
    background-color: rgba(255, 255, 255, 0);
    border: solid 1.4px #95fad8;
    cursor: pointer;
    outline: none !important;
    font-size: 12px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.03;
    letter-spacing: normal;
    text-align: center;
    color: #95fad8;
  `
  const inactiveSwitch = css`
    background: transparent;
    border:none;
    outline: none !important;
    font-size: 12px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.03;
    letter-spacing: normal;
    text-align: center;
    color: #f6f7fa;
    cursor: pointer;
  `
  const mobilePhone = css`
    display: flex;
    justify-content: center;
    align-items: flex-end;
    height: 262px;
    width: 100%;
    position: relative;
    bottom: -15px;
  `
  const mobileCase = css`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    height: 240px;
    width: 222px;
    border: 1px solid;
    border-bottom: none;
    border-radius: 19.2px;
  `
  const speaker = css`
    margin-top: 24px;
    width: 36px;
    height: 7px;
    border: 1px solid;
    border-radius: 19.2px;
  `
  const screen = css`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    flex-direction: column;
    margin-top: 24px;
    height: 198px;
    width: 193px;
    border: 1px solid;
    border-bottom: none;
    h1{
      margin-top: 45px;
      font-size: 16px;
      font-weight: 800;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
    }
  `
  const iOSnotiContainer = css`
    margin-top: 15px;
    width: 250px;
    border-radius: 8.4px;
    background-color: rgb(188, 188, 199);
  `
  const iOSnotiHeader = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 22.4px;
    border-radius: 8.4px 8.4px 0 0;
    background-color: rgba(240, 240, 240, 0.25);
    padding: 0 5.6px;
    span{
      opacity: 0.9;
      mix-blend-mode: difference;
      font-family: .SF NS Text;
      font-size: 9.1px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #9b9b9b;
    }
    div{
      span{
        margin-left: 5.6px;
      }
    }
  `
  const iOStimeText = css`
    mix-blend-mode: difference;
    font-family: .SF NS Text;
    font-size: 8.4px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: right;
    color: #808080;
  `
  const gualyIconLogo = css`
    width: 13.9px;
    height: 13.9px;
    border-radius: 3.4px;
  `
  const iOSnotiBody = css`
    padding: 5.4px 11px;
    display; flex;
    align-items: flex-start;
    white-space: normal !important;
    min-height: 12px;
    h5{
      font-family: .SF NS Text;
      font-size: 10.4px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #000000;
      margin: 0;
      overflow: hidden;
      min-height: 13px;
    }
    h6{
      margin-top: 4.4px;
      margin-bottom: 0;
      height: 10px;
      mix-blend-mode: difference;
      font-family: .SF NS Text;
      font-size: 8.4px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #808080;
    }
  `
  const AndroidNotiContainer = css`
    display: flex;
    padding: 9px;
    width: 250px;
    margin-top: 15px;
    margin-bottom: 10px;
    border-radius: 1.5px;
    background-color: #ffffff;
    img{
      width: 28.9px;
      height: 27.9px;
      border-radius: 3.4px;
    }
  `
  const AndroidNotiHeader = css`
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    width: 100%;
    margin-left: 9.7px;
    div{
      display: flex;
      justify-content: space-between;
      width: 100%;
      span{
        opacity: 0.9;
        mix-blend-mode: difference;
        font-family: .SF NS Text;
        font-size: 9.1px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #9b9b9b;
      }
    }
    h5{
      font-family: Roboto;
      font-size: 10.2px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.43;
      letter-spacing: normal;
      text-align: left;
      color: rgba(0, 0, 0, 0.54);
      margin: 0px;
      margin-top: 2.9px;
      margin-bottom: 0;
      overflow: hidden;
    }
  `
  const footer = css`
    z-index: 100;
    margin: 0px;
    padding: 10px;
    background-color: #242656;
    width: 100%;
    h2{
      margin: 0;
      font-size: 16px;
      font-weight: 800;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
    }
  `
  const mobileContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center; 
    margin-top: 20px;
    background-color: rgba(0, 0, 0, 0.25);
  `
export default ({ message, mobile, handleMobile }) => {
  return (
    <div className={component}>
      <div className={header}>
        <h1> VISTA PREVIA</h1>
      </div>
      <div className={divisor}></div>
      <div className={body}>
        <div className={switchContainer}>
          <button onClick={handleMobile} className={(mobile === true ? activeSwitch : inactiveSwitch)}> iOS </button>
          <button onClick={handleMobile} className={(mobile === true ? inactiveSwitch : activeSwitch)}>Android</button>
        </div>
        <div className={mobileContainer}>
          <div className={mobilePhone}>
            <div className={mobileCase}>
              <div className={speaker}></div>
              {
                mobile ? (
                  <div className={screen}>
                    <div className={iOSnotiContainer}>
                      <div className={iOSnotiHeader} >
                        <div>
                          <img className={gualyIconLogo} src={gualyIcon} alt="gualy icon" />
                          <span>GUALY</span>
                        </div>
                        <span className={iOStimeText}>ahora</span>
                      </div>
                      <div className={iOSnotiBody}>
                        <h5>{message.length === 0 ? 'Ingresa el contenido de la notificación push' : message}</h5>
                        <h6>Abrir</h6>
                      </div>
                    </div>
                    <h1>iOS</h1>
                  </div>
                ) : 
                (
                  <div className={screen}>
                    <div className={AndroidNotiContainer}>
                      <img src={gualyIcon} alt="gualy icon" />
                      <div className={AndroidNotiHeader} >
                        <div>
                          <span>GUALY</span>
                          <span className={iOStimeText}>ahora</span>
                        </div>
                        <h5>{message.length === 0 ? 'Ingresa el contenido de la notificación push' : message}</h5>
                      </div>
                    </div>
                    <h1>Android </h1>
                  </div>
                )
              }
              

            </div>
          </div>
          <div className={footer}>
            <h2>Vista previa de las notificaciones</h2>
          </div>
        </div>
      </div>
    </div>
  )
}