import React from 'react'
import { css } from 'emotion'
import moment from 'moment'
// eslint-disable-next-line
// import es from 'moment/locale/es'
import { ToastContainer, toast } from 'react-toastify'
import NotiControl from './NotiControl'
import NotiPreview from './NotiPreview'
import MobileNotiPreview from './MobileNotiPreview'
import { postRequest } from '../../../../utils/createAxiosRequest'
import { customConsoleLog } from '../../../../utils/customConsoleLog'
import R from '../../../../utils/R'
/* CSS */
const component = css`
    margin: 50px;
    margin-bottom: 0px;
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    @media (max-width: 1230px) {
      margin: 0 25px;
      margin-top: 40px;
    }
  `
const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const body = css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 24px 32px;
    margin-bottom: 0px;
    @media (max-width: 970px) {
      flex-direction: column;
      margin: 24px 25px;
    }
  `
export default class NotiCard extends React.Component {
  state = ({
    message: '',
    select: 'bulk',
    mobile: true,
    uids: '',
    users: [],
    emailInput: '',
    filteredEmails: [],
    displayError: false,
    errorMessage: '',
    notiLoading: false
  })

  componentWillReceiveProps() {
    this.setState({
      users: this.props.users
    })
  }

  handleEmailInput = (event) => {
    this.setState({
      emailInput: event.target.value,
    })
  }

  handleSelect = (event) => {
    const selectedOption = event.target.value
    if (selectedOption !== '') {
      this.setState({ filteredEmails: [], emailInput: '' })
    }
    this.setState({ select: selectedOption })
  }

  handleMessage = (event) => {
    this.setState({
      message: event.target.value.substr(0, 120)
    })
  }

  handleMobile = () => {
    this.setState({
      mobile: !this.state.mobile
    })
  }

  onSaveUidByEmail = async () => {
    this.toastAddingUserId = toast('Buscando usuario...', { autoClose: false })
    let { emailInput, filteredEmails } = this.state
    let user = ""
    const data = {
      attrib: "email",
      value: emailInput
    }
    if (emailInput) {
      customConsoleLog('SENDED: ', data)
      let resp = await postRequest('users/searchUsersByAttrib', data)
      customConsoleLog('RESPONSE: ', resp)
      if (resp.data.success) {
        user = R.values(resp.data.data.data)[0]
      }
      if (user && (user.FCMToken || user.expoPushToken)) {
        this.setState({ displayError: false, errorMessage: '' })
        toast.update(this.toastAddingUserId, {
          render: 'Usuario encontrado.',
          autoClose: 5000,
        })
        filteredEmails.push({
          email: user.email,
          uid: user.userKey
        })
        this.setState({
          filteredEmails,
          emailInput: ''
        })
      } else {
        if (!user) {
          this.setState({ displayError: true, errorMessage: 'No se encontraron usuarios con este correo.' })
          return toast.update(this.toastAddingUserId, {
            render: 'No se encontraron usuarios con este correo.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
        if (user && !user.FCMToken && !user.expoPushToken) {
          this.setState({ displayError: true, errorMessage: 'El usuario no se ha loguiado desde su dispositivo, por lo tanto no puede recibir la notificación.' })
          return toast.update(this.toastAddingUserId, {
            render: 'El usuario no se ha loguiado desde su dispositivo, por lo tanto no puede recibir la notificación.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
    } else {
      this.setState({ displayError: true, errorMessage: 'Por favor ingrese un correo.' })
      return toast.update(this.toastAddingUserId, {
        render: 'Por favor ingrese un correo.',
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
    }
  }

  onRemoveUidByEmail = (uid) => {
    let { filteredEmails } = this.state
    let newFilteredEmails = filteredEmails.filter(email => {
      return email.uid !== uid
    })
    this.setState({
      filteredEmails: newFilteredEmails
    })
  }
  toastRegisterId = null
  sendPush = async (event) => {
    event.preventDefault();
    this.toastRegisterId = toast('Enviando notificación...', { autoClose: false })
    this.setState({ notiLoading: true })
    const adminID = this.props.adminUid
    const date = moment().format('DD-MM-YYYY')
    const time = moment().format('h:mm a')
    const { filteredEmails } = this.state
    let uids = []
    filteredEmails.map(filteredEmail => uids.push(filteredEmail.uid))
    uids = uids.join(',')
    const data = {
      adminID,
      time,
      date,
      uids,
      message: this.state.message.trim(),
      pushType: this.state.select
    }
    if (
      data.message.length > 5 &&
      (this.state.select === '' ?
        this.state.filteredEmails.length > 0
          ? true
          : false
        : true)
    ) {
      try {
        // console.log('SENDED: ', data)
        let resp = await postRequest('pushNotification/sendPushNotification', data)
        // console.log('RESPONSE: ', resp)
        if (resp.data.success) {
          this.setState({ notiLoading: false, displayError: false, errorMessage: '' })
          toast.update(this.toastRegisterId, {
            render: 'Notificacion enviada.',
            autoClose: 5000,
          })
        } else {
          this.setState({ notiLoading: false, displayError: true, errorMessage: resp.data.error.message })
          return toast.update(this.toastRegisterId, {
            render: resp.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        // console.log("sendPushNotification error: ", error)
        this.setState({ notiLoading: false, displayError: true, errorMessage: 'Verifique su conexión y vuelva a intentarlo.' })
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        return toast.update(this.toastRegisterId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } else {
      this.setState({ notiLoading: false, displayError: true })
      if (this.state.select === '' && this.state.filteredEmails.length === 0) {
        this.setState({ errorMessage: 'Debe colocar al menos un destinatario.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar al menos un destinatario.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (data.message.length < 5) {
        this.setState({ errorMessage: 'El mensaje debe tener más de 5 caracteres' })
        return toast.update(this.toastRegisterId, {
          render: 'El mensaje debe tener más de 5 caracteres.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }

  render() {
    return (
      <div className={component}>
        <div className={header}>
          <h1> ENVIAR NUEVA NOTIFICACIÓN PUSH </h1>
        </div>
        <div className={divisor}></div>
        <div className={body}>
          <NotiControl
            handleMessage={this.handleMessage}
            handleSelect={this.handleSelect}
            handleEmailInput={this.handleEmailInput}
            onSaveUidByEmail={this.onSaveUidByEmail}
            onRemoveUidByEmail={this.onRemoveUidByEmail}
            filteredEmails={this.state.filteredEmails}
            select={this.state.select}
            message={this.state.message}
            emailInput={this.state.emailInput}
            sendPush={this.sendPush}
            displayError={this.state.displayError}
            errorMessage={this.state.errorMessage}
            notiLoading={this.state.notiLoading}
          />
          <NotiPreview message={this.state.message} />
        </div>
        <MobileNotiPreview handleMobile={this.handleMobile} message={this.state.message} mobile={this.state.mobile} />
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    )
  }
}