import React from 'react'
import { css } from 'emotion'

import iconReject from './../../../../assets/icon-close.svg'

/* CSS */
const component = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    overflow-x: hidden;
    height: 100px;
    position: relative;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 3px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 2px;
    }
    ::-webkit-scrollbar {
        width: 7px;
    }
  `
const email = css`
    width: 100%;
    margin-top: 2px;
    display: flex;
    justify-content: space-between;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    span {
      cursor: pointer;
    }
  `

const NotiControlUserList = ({ filteredEmails, onRemoveUidByEmail }) =>
  <div className={component}>
    <div css={`padding-top: 20px;width: 100%`}>
      {
        filteredEmails.map((filteredEmail) => (
          <div key={filteredEmail.uid} className={email}>
            {filteredEmail.email}
            <span onClick={e => onRemoveUidByEmail(filteredEmail.uid)}>
              <img src={iconReject} alt="icon reject" />
            </span>
          </div>
        ))
      }
    </div>
  </div>

export default NotiControlUserList