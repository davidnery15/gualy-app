import React from 'react'
import { css } from 'emotion'
import styled from 'react-emotion'
import errorSVG from './../../../../assets/ic-info-red.svg'
import Input from './../../../general/Input'
import NotiControlUserList from './NotiControlUserList'
import arrowIcon from './../../../../assets/drop-down-arrow.svg'
import checkIcon from './../../../../assets/check-24px.svg'
import AcceptButton from '../../../general/AcceptButton'
/* CSS */
const component = css`  
    margin: 0px;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    width: 50%;
    @media (max-width: 1230px) {
      width:40%;
      flex-direction: column;
      justify-content: flex-start;
    }
    @media (max-width: 970px) {
      width: 100%;
      flex-direction: row;
      justify-content: space-between;
    }
    @media (max-width: 608px) {
      width:100%;
      flex-direction: column;
      justify-content: center;
      align-items: flex-start;
    }
  `
const senderPanel = css`
    display: flex;
    width: 40%;
    flex-direction: column;
    @media (max-width: 1230px) {
      width: 100%;
    }
    @media (max-width: 970px) {
      width: 50%;
    }
    @media (max-width: 608px) {
      width:100%;
      align-items: center;
    }
  `
const dropdown = css`
    display: flex;
    width: 100%;
    border-bottom: 1px solid;
  `
const MobileSelect = styled('select')`
    height: 47.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    outline:none !important;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    option{
      background-color: #242657;
    }
  `
const inputPanel = css`
    display: flex;
    flex-direction: column
    width: 60%;
    margin-left: 7%;
    h2{
      font-size: 14px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.43;
      letter-spacing: normal;
      text-align: left;
      margin: 0px;
    }
    @media (max-width: 1230px) {
      width: 100%;
      margin: 0;
      margin-top: 27.7px;
    }
    @media (max-width: 970px) {
      width: 60%;
      margin-left: 3%;
      margin-top: 0px;
    }
    @media (max-width: 608px) {
      width:100%;
      margin: 0;
      h2{
        margin-top: 28px;
      }
    }
  `
const inputContainer = css`
    margin-top: 13px;
    width: 100%;
    padding: 0 5px;
    h2{
      margin-top: 7px;
      text-align: right;
    }
  `
const buttonContainer = css`
    margin-top: 36px;
    margin-bottom: 36px;
    display: flex;
    justify-content: flex-end
    width: 100%;
    @media (max-width: 608px) {
      width:100%;
      margin: 0;
      margin-top: 28px;
      justify-content: center;
      h2{
        margin-top: 28px;
      }
    }
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const arrowContainer = css`
    pointer-events: none;
    width: 5%;
    display: flex;
    justify-content: center;
    align-items: center;
  `
const emailInputContainer = css`
    width: 100%
    margin-top: 40px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-bottom: 15px;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const group = css`
    display: flex;
    position: relative;
    width: 100%;
  `
const emailInputClass = css`
    width: 90%;
  `
const inputButtonContainer = css`
    width: 100%;
    display: flex;
    flex-direction: row;
  `
const checkIconClass = css`
    width: 100%;
    height: 100%;
  `
const checkButtonClass = css`
    width: 50px;
    height: 50px;
    background: transparent;
    border: none;
    cursor: pointer;
    button:focus {outline:0;}
  `
export default class NotiControl extends React.Component {
  render() {
    const {
      message,
      select,
      emailInput,
      handleMessage,
      handleSelect,
      handleEmailInput,
      sendPush,
      onSaveUidByEmail,
      onRemoveUidByEmail,
      filteredEmails,
      displayError,
      errorMessage,
      notiLoading
    } = this.props
    return (
      <form onSubmit={sendPush} className={component}>
        <div className={`${senderPanel} senderPanel`}>
          <div className={dropdown}>
            <MobileSelect name='mobileSelect' id='mobileSelect' placeholder='Enviar a...' onChange={handleSelect} value={select}>
              <option value="bulk">Todas las cuentas</option>
              <option value="client">Usuarios</option>
              <option value="commerce">Comercios</option>
              <option value="">Cuenta específica</option>
            </MobileSelect>
            <div className={arrowContainer}>
              <img src={arrowIcon} className={arrow} alt='' />
            </div>
          </div>
          {
            select === '' ?
              <div className={emailInputContainer}>
                <div className={`${group}`}>
                  <div className={`${inputContainer}`}>
                    <div className={inputButtonContainer}>
                      <input
                        className={emailInputClass}
                        placeholder='Email'
                        onChange={handleEmailInput}
                        type="text"
                        value={emailInput}
                        onKeyPress={event => {
                          if (event.key === 'Enter') {
                            event.preventDefault()
                            onSaveUidByEmail()
                          }
                        }}
                      />
                      <button
                        type="button"
                        onClick={onSaveUidByEmail}
                        className={checkButtonClass}
                      >
                      <img 
                      src={checkIcon}
                      alt=""
                      className={checkIconClass}
                      />
                      </button>
                    </div>
                    <span className='highlight'></span>
                    <span className='bar'></span>
                  </div>
                </div>
                <NotiControlUserList filteredEmails={filteredEmails} onRemoveUidByEmail={onRemoveUidByEmail} />
              </div> : null
          }
        </div>
        <div className={inputPanel}>
          <h2>Contenido de la notificación</h2>
          <div className={inputContainer}>
            <Input updateSearch={handleMessage} placeholder="Ingresa el contenido de la notificación push..." search={message} />
            <h2>{message.length} / 120</h2>
          </div>
          {
            displayError ?
              <div className={errorContainer}>
                {errorMessage}
                <img
                  className={errorSvgCss}
                  src={errorSVG}
                  alt="error Icon"
                />
              </div>
              : null
          }
          <div className={buttonContainer}>
            <AcceptButton
              type="submit"
              loading={notiLoading}
              content="ENVIAR NOTIFICACIÓN"
              height="38px"
              width="218px"
            />
          </div>
        </div>
      </form>
    )
  }
}