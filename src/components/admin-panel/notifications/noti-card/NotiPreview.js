import React from 'react'
import { css } from 'emotion'
import gualyIcon from './../../../../assets/logos/logo-gualy-03.png'

/* CSS */
  const component = css`  
    margin: 0px;
    margin-left: 40px;
    margin-bottom: 28px;
    display: flex;
    background-color: rgba(0, 0, 0, 0.25);
    flex-direction: column;
    width: 50%;
    height: 100%;
    @media(max-width: 1230px) {
      width: 60%;
      margin-left: 15px;
    }
    @media(max-width: 970px) {
      width: 100%
      margin-top: 20px;
      margin-left: 0px;
    }
    @media(max-width: 608px){
      display: none;
    }
  `
  const body = css`
    display: flex;
    width: 100%;
    height: 100%;
  `
  const mobile = css`
    display: flex;
    justify-content: center;
    align-items: flex-end;
    height: 262px;
    width: 100%;
    position: relative;
    bottom: -15px;
  `
  const footer = css`
    z-index: 100;
    margin: 0px;
    padding: 10px;
    background-color: #242656;
    h2{
      margin: 0;
      font-size: 16px;
      font-weight: 800;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
    }
  `
  const mobileCase = css`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    height: 240px;
    width: 222px;
    border: 1px solid;
    border-bottom: none;
    border-radius: 19.2px;
  `
  const speaker = css`
    margin-top: 24px;
    width: 36px;
    height: 7px;
    border: 1px solid;
    border-radius: 19.2px;
  `
  const screen = css`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    flex-direction: column;
    margin-top: 24px;
    height: 198px;
    width: 193px;
    border: 1px solid;
    border-bottom: none;
    h1{
      margin-top: 45px;
      font-size: 16px;
      font-weight: 800;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
    }
  `
  const iOSnotiContainer = css`
    margin-top: 15px;
    width: 250px;
    border-radius: 8.4px;
    background-color: rgb(188, 188, 199);
  `
  const iOSnotiHeader = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 22.4px;
    border-radius: 8.4px 8.4px 0 0;
    background-color: rgba(240, 240, 240, 0.25);
    padding: 0 5.6px;
    span{
      opacity: 0.9;
      mix-blend-mode: difference;
      font-family: .SF NS Text;
      font-size: 9.1px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #9b9b9b;
    }
    div{
      span{
        margin-left: 5.6px;
      }
    }
  `
  const iOStimeText = css`
    mix-blend-mode: difference;
    font-family: .SF NS Text;
    font-size: 8.4px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: right;
    color: #808080;
  `
  const gualyIconLogo = css`
    width: 13.9px;
    height: 13.9px;
    border-radius: 3.4px;
  `
  const iOSnotiBody = css`
    padding: 5.4px 11px;
    display; flex;
    align-items: flex-start;
    white-space: normal !important;
    min-height: 12px;
    h5{
      font-family: .SF NS Text;
      font-size: 10.4px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #000000;
      margin: 0;
      overflow: hidden;
      min-height: 13px;
    }
    h6{
      margin-top: 4.4px;
      margin-bottom: 0;
      height: 10px;
      mix-blend-mode: difference;
      font-family: .SF NS Text;
      font-size: 8.4px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #808080;
    }
  `
  const AndroidNotiContainer = css`
    display: flex;
    padding: 9px;
    width: 250px;
    margin-top: 15px;
    margin-bottom: 10px;
    border-radius: 1.5px;
    background-color: #ffffff;
    img{
      width: 28.9px;
      height: 27.9px;
      border-radius: 3.4px;
    }
  `
  const AndroidNotiHeader = css`
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    width: 100%;
    margin-left: 9.7px;
    div{
      display: flex;
      justify-content: space-between;
      width: 100%;
      span{
        opacity: 0.9;
        mix-blend-mode: difference;
        font-family: Roboto;
        font-size: 11.6px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #9b9b9b;
      }
    }
    h5{
      font-family: Roboto;
      font-size: 10.2px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.43;
      letter-spacing: normal;
      text-align: left;
      color: rgba(0, 0, 0, 0.54);
      margin: 0px;
      margin-top: 2.9px;
      margin-bottom: 0;
      overflow: hidden;
    }
  `

export default ({ message }) =>{
  return(
    <div className={component}>
      <div className={body}>
        <div className={mobile}>
          <div className={mobileCase}>
            <div className={speaker}></div>
            <div className={screen}>
              <div className={iOSnotiContainer}>
                <div className={iOSnotiHeader} >
                  <div>
                    <img className={gualyIconLogo} src={gualyIcon} alt="gualy icon" />
                    <span>GUALY</span>
                  </div>
                  <span className={iOStimeText}>ahora</span>
                </div>
                <div className={iOSnotiBody}>
                  <h5>{ message.length === 0 ? 'Ingresa el contenido de la notificación push' : message}</h5>
                  <h6>Abrir</h6>
                </div>
              </div>
              <h1>iOS</h1>
            </div>
          </div>
        </div>
        <div className={mobile}>
          <div className={mobileCase}>
            <div className={speaker}></div>
            <div className={screen}>
              <div className={AndroidNotiContainer}>
                <img src={gualyIcon} alt="gualy icon" />
                <div className={AndroidNotiHeader} >
                  <div>
                    <span>GUALY</span>
                    <span className={iOStimeText}>ahora</span>
                  </div>
                  <h5>{message.length === 0 ? 'Ingresa el contenido de la notificación push' : message}</h5>
                </div>
              </div>
              <h1>Android </h1>
            </div>
          </div>
        </div>
      </div>
      <div className={footer}>
        <h2>Vista previa de las notificaciones</h2>
      </div>
    </div>
  )
}