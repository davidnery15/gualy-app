import React, { Component } from 'react'
import { css } from 'emotion'
import Header from '../AdminHeader'
import NotiCard from './noti-card/NotiCard'
import NotiCardTable from './noti-card-table/NotiCardTable'
import { db } from './../../../firebase'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import { userLogout, getNotificationsHistory } from '../../../redux/actions/general'
import { customConsoleLog } from '../../../utils/customConsoleLog'
/* CSS */
const notificationsHistorylimitIndex = 10
const container = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
  `
class Notifications extends Component {
  state = {
    notifications: [],
    itemsIndex: 10
  }
  componentDidMount() {
    const userKey = this.props.userKey
    const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
    isUserBlockRef.once('value', snapshot => {
      this.setState({
        isUserBlocked: snapshot.val(),
      })
    })
    this.props.dispatch(getNotificationsHistory({ userKey, notificationsHistorylimitIndex }))
     const notificationLogsRef = db.ref('notificationLog')
     notificationLogsRef.on('value', (snapshot) => {
       let notifications = [];
       snapshot.forEach(data => {
         notifications.push(data.val())
       })
       this.setState({
         notifications: notifications.sort((a, b) => b.timestamp - a.timestamp),
         loading: false
       })
     })
  }
  redirect = () => {
    if (this.props.isLoggedIn && this.props.userInCollectionData) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
      if (this.props.userInCollectionData.type) {
        const redirect =
          this.props.userInCollectionData.type === 'admin' ||
            this.props.userInCollectionData.type === 'support' ||
            this.props.userInCollectionData.type === 'marketing' ||
            this.props.userInCollectionData.type === 'cashier'
            ? null
            : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  notificationsTransactionsRequest = () => {
    customConsoleLog("----get new notifications----")
    this.setState({ itemsIndex: this.state.itemsIndex + notificationsHistorylimitIndex }, () => {
      const notificationsHistorylimitIndex = this.state.itemsIndex
      const userKey = this.props.userKey
      this.props.dispatch(getNotificationsHistory({ userKey, notificationsHistorylimitIndex }))
    })
  }
  render() {
    return (
      <div>
        <Header />
        <div className={container}>
          <NotiCard
            adminUid={this.props.userInCollectionData ? this.props.userInCollectionData.userKey : ''}
          />
          <NotiCardTable
            notifications={Object.values(this.props.notificatonsHistory || {})}
            loading={this.props.loadingNotificationsHistory}
            title='HISTORIAL DE NOTIFICACIONES'
            userKey={this.props.userInCollectionData.userKey}
            notificationsTransactionsRequest={this.notificationsTransactionsRequest}
          />
        </div>
        {
          this.redirect()
        }
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn,
    //NOTIFICATIONS HISTORY
    notificatonsHistory: state.general.notificatonsHistory,
    loadingNotificationsHistory: state.general.loadingNotificationsHistory,
  }
}
export default connect(mapStateToProps)(Notifications)