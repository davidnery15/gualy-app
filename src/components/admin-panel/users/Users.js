import React, { Component } from 'react'
import { css } from 'emotion'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import Header from '../AdminHeader'
import UsersCardTable from '../users-card-table/UsersCardTable'
import { db } from './../../../firebase'
import AddUser from './AddUser'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { timestampParser } from '../../../utils/timestampParser'
import R from '../../../utils/R'
const container = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
  `
const scrollDataRequestedRange = 10
class Users extends Component {
  state = {
    users: [],
    loading: true,
    itemsIndex: 10,
    isDownloadSelectOpen: false,
    allUsers: "",
    allUsersLoading: false,
  }
  componentDidMount() {
    const usersRef = db
      .ref('users')
      .orderByChild('type')
      .equalTo('client')
      .limitToLast(this.state.itemsIndex)
    usersRef.once('value', (snapshot) => {
      const users = R.values(snapshot.val())
      this.setState({
        users: users.sort((a, b) => (b.createdTimestamp || new Date(b.createdAt).getTime()) - (a.createdTimestamp || new Date(a.createdAt).getTime())),
        loading: false
      })
    })
  }
  databaseRequest = () => {
    customConsoleLog("databaseRequest users")
    this.setState({ itemsIndex: this.state.itemsIndex + scrollDataRequestedRange }, () => {
      const usersRef = db
        .ref('users')
        .orderByChild('type')
        .equalTo('client')
        .limitToLast(this.state.itemsIndex)
      usersRef.once('value', (snapshot) => {
        const users = R.values(snapshot.val())
        this.setState({
          users: users.sort((a, b) => (b.createdTimestamp || timestampParser(b.createdAt)) - (a.createdTimestamp || timestampParser(b.createdAt))),
          loading: false
        })
      })
    })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.props.userInCollectionData.type) {
        const redirect =
          this.props.userInCollectionData.type === 'admin' ||
            this.props.userInCollectionData.type === 'support' ||
            this.props.userInCollectionData.type === 'marketing' ||
            this.props.userInCollectionData.type === 'cashier'
            ? null
            : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  openDownloadBySelect = () => {
    if(this.state.isDownloadSelectOpen){
      this.setState({ isDownloadSelectOpen: false })
    } else {
      this.setState({ allUsersLoading: true }, () => {
        const usersRef = db
          .ref('users')
          .orderByChild('type')
          .equalTo('client')
        usersRef.once('value', (snapshot) => {
          const users = R.values(snapshot.val())
          this.setState({
            allUsers: users.sort((a, b) => (b.createdTimestamp || new Date(b.createdAt).getTime()) - (a.createdTimestamp || new Date(a.createdAt).getTime())),
            allUsersLoading: false,
            isDownloadSelectOpen: true,
          })
        })
      })
    }
  }
  render() {
    customConsoleLog("itemsIndex users: ", this.state.itemsIndex)
    customConsoleLog("users: ", this.state.users)
    return (
      <div className={container}>
        <Header />
        <UsersCardTable
          users={Object.values(this.state.users)}
          title='USUARIOS REGISTRADOS'
          loading={this.state.loading}
          adminID={this.props.userInCollectionData.userKey}
          userType={this.props.userInCollectionData.type}
          showOrderByButton={false}
          showFilterByButton={false}
          icon={false}
          databaseRequest={this.databaseRequest}
          idAdmin={true}
          isDownloadSelectOpen={this.state.isDownloadSelectOpen}
          openDownloadBySelect={this.openDownloadBySelect}
          allUsers={this.state.allUsers}
          allUsersLoading={this.state.allUsersLoading}
        />
        {
          this.props.userInCollectionData.type === 'admin'
            ? <AddUser adminID={this.props.userInCollectionData.userKey} />
            : null
        }
        {
          this.redirect()
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(Users)