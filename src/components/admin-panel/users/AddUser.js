import React, { Component } from 'react'
import { css } from 'emotion'
import styled from 'react-emotion'
import { toast } from 'react-toastify'
import { style } from 'react-toastify'
import arrowIcon from './../../../assets/drop-down-arrow.svg'
import iconCamera from './../../../assets/icon-camera.svg'
import { securityQuestionKeys } from './../../../constants/securityQuestionKeys'
import iconVisibilityOn from './../../../assets/baseline-visibility-on.svg'
import iconVisibilityOff from './../../../assets/baseline-visibility-off.svg'
import moment from 'moment'
import AcceptButton from '../../general/AcceptButton'
import { postRequest } from '../../../utils/createAxiosRequest'
import { emailsRegex, numbers, letters } from '../../../constants/regex'
import { customConsoleLog } from '../../../utils/customConsoleLog'
// Changing default tostify css 
style({
  colorProgressDefault: "#95fad8",
  fontFamily: "Montserrat",
});

/* CSS */
const component = css`
    align-self: center;
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    @media (max-width: 1230px) {
      margin: 40px 25px;
    }
    max-width: 1200px;
    margin-bottom: 50px;
    width: 100%;
  `
const firstComponent = css`
    margin-top: 0 !important;
  `
const firstTitle = css`
    margin-bottom: 15px;
  `
const lastComponent = css`
      display: flex;
      justify-content: flex-start;
      align-items: center;
      flex-direction: column;
      width: 100%;
      padding: 0 15%;
      position: relative;
      @media( max-width: 1024px) {
        padding: 0 8%;
      }
      margin-top: 40px;
      margin-bottom: 70px;
      @media( max-width: 930px) {
      margin-bottom: 50px !important;
      }
      @media( max-width: 630px) {
      padding: 0;
      margin-bottom: 50px !important;
      }
  `
const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const body = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 25px 32px;
    margin-bottom: 0px;
    input{
      font-size: 15.7px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.5;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      border-bottom: 1px solid #ffffff;
    }
    @media (max-width: 970px) {
      flex-direction: column;
      margin: 24px 25px;
      margin-bottom: 0px;
    }
  `
const commerceDataDiv = css`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    width: 100%;
    padding: 0 15%;
    position: relative;
    @media( max-width: 1024px) {
      padding: 0 8%;
    }
    @media( max-width: 930px) {
      div{
        margin-top: 15px;
      }
    }
     @media( max-width: 630px) {
      padding: 0;
      div{
        margin-top: 15px;
      }
    }
  `
const commerceDataTitle = css`
    position: absolute;
    width: 100%;
    margin-top: 0 !important;
    padding-left: 5%;
    h2{
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      text-align: left;
      color: #f6f7fa;
    }
    @media(max-width: 930px) {
      position: initial;
      padding-left: 0;
    }
  `
const commerceMainDataDiv = css`
    display:flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin-top: 0 !important;
    div {
      margin-top: 15px;
    }
  `
const UploadBtn = styled('button')`
    width: 139px;
    height: 139px;
    color: #242656;
    display: flex;
    justify-content: center;
    align-items: center;
    outline: none !important;
    cursor: pointer;
    background-color: #95fad8;
    border: none;
    border-radius: 72px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    position: relative;
    @media(max-width: 630px) {
      width: 72px;
      height: 72px;
    }
  `
const iconCameraClass = css`
    width: 53px;
    height: 47px;
    object-fit: contain;
    @media(max-width: 630px) {
      width: 35px;
      height: 32px;
    }
  `
const uploadBtnWrapper = css`
    margin-top: 0 !important;
  `
const fullWidthInput = css`
    width: 100%;
    margin-top: 40px;
    @media(max-width: 1024px) {
      margin-top: 45px !important;
    }
  `
const inputDescription = css`
    text-align: center !important;
    @media( max-width: 630px) {
      text-align: left !important;
    }
  `
// const hiddenInputDiv = css`
//   visibility: hidden;
//   @media( max-width: 670px) {
//     display: none;
//   }
// `
const tripleInputDiv = css`
    width: 100%;
    display: flex;
    justify-content: space-between;
    div{
      width: 30%;
      margin-top: 40px;
      input{
        width: 100%;
      }
    }
    @media( max-width: 670px) {
      margin-top: 0 !important;
      display: flex;
      flex-direction: column;
      div{
        width: 100%;
        margin-top: 15px;
      }
    }
  `
const tripleInputDivSecond = css`
    width: 100%;
    position: relative;
    display: flex;
    justify-content: space-between;
    div{
      margin-top: 40px
      input{
        width: 100%;
      }
    }
    @media( max-width: 670px) {
      margin-top: 0 !important;
      display: flex;
      flex-direction: column;
      div{
        width: 100%;
        margin-top: 15px;
      }
    }
  `
const DivDNI = css`
    width: 40px !important;
    border-bottom: 1px solid white;
    margin-right: 15px;
    position: relative;
    min-width: 55px;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const imagePreview = css`
    width: 100%;
    height: 100%;
    position: absolute;
    cursor: pointer;
    border-radius: 72px;
    object-fit: cover;
    border: 2px solid white;
  `
const displayErrors = css`
    input:invalid {
      border-color:red;
    }
    input:invalid specficError 
  `
const specficError = css`
    border: 3px solid red;
  `
// const helpText = css`
//   margin-top: 20px;
//   margin-bottom: 5px;
//   justify-content: center;
//   align-self: center;
//   width: 196px;
//   max-width: 196px;
//   height: 56px;
//   font-family: Montserrat;
//   font-size: 11px;
//   font-weight: 300;
//   text-align: center;
//   color: #f6f7fa;
// `

// const anchorLink = css`
//     text-decoration: none;
//     color: #95fad8 !important;

//     &:active {
//       color: #95fad8;
//       text-decoration: none;
//     }

//     &:visited {
//       color: #95fad8;
//       text-decoration: none;
//     }

//     &:hover {
//       color: #95fad8;
//       text-decoration: none;
//     }
//   `

const passwordContainer = css`
    position: relative;
    width: 30%;
  `
const visibilityOnCSS = css` 
    width: 25px; 
    height: 25px; 
    object-fit: contain; 
    cursor: pointer;
  `
const btnVisibility = css`
    background: transparent;
    border: none;
    position: absolute;
    right: 5px;
    top: 10px;
  `
const marginTop = css`
  margin-top: 20px !important;
`
const dropDownDivContainer = css`
border-bottom: 1px solid white;
width: 100%;
div{
  margin-top: 0;
}
`
const dropDownDiv = css`
position: relative;
display: flex;
align-items:center;
`
const CustomDropdown = styled('select')`
height: 43.3px;
width:100%;
padding: 7px 4px;
background: transparent;
border: none;
cursor: pointer;
font-size: 15px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.68;
letter-spacing: normal;
text-align: left;
color: #ffffff;
appearance: none; 
outline: none !important;
option{
  background-color: #242657;
}
`
const dropdownQuestionPadding = css`
padding-right: 15px;
@media(max-width: 500px){
  option{
    font-size: 8px;
  }
}
`
const arrowContainer = css`
width: 15px !important;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
position: absolute;
right: 0;
top: 0;
pointer-events: none;
`
const arrow = css`
width: 10.4px;
height: 6.6px;
object-fit: contain;
`
const beforeTermsAndConditions = css`
font-size: 10px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: 1.4;
letter-spacing: normal;
text-align: center;
margin-right: 3px;
  `
const displayRedBorders = css`
    border-color:red;
`
const individualChild = css`
    width: 30%;
    margin-top: 15px !important;
`
const nameInputDiv = css`
width: 252.4px;
height: 100%;
input{
  text-align: center;
  font-size: 20px;
  font-weight: 900;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.18;
  letter-spacing: normal;
  text-align: center;
  color: #ffffff;
}
@media( max-width: 630px) {
  width: 100%;
}
`
class AddUser extends Component {
  state = ({
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    securityAnswer: '',
    address: '',
    contactIdType: '',
    contactIdNumber: '',
    profileImgURI: '',
    imageError: false,
    image: '',
    phone: '',
    bornDate: '',
    questionKey: '',
    loading: false,
    passwordVisibility: false,
    displayErrors: false,
    modalIsOpen: false,
    redirectAfterRegister: false,
    registered: false,
    errorMessage: '',
    userType: '',
  })
  handleSubmit = async (event) => {
    event.preventDefault()
    this.setState({ loading: true })
    this.toastRegisterId = toast('Agregando usuario...', { autoClose: false })
    let { firstName,
      lastName,
      contactIdType,
      password,
      contactIdNumber,
      email,
      phone,
      questionKey,
      securityAnswer,
      bornDate,
      image,
      userType
    } = this.state
    const phoneNumber = `+58${phone.trim()}`
    const data = {
      firstName: firstName.trim(),
      lastName: lastName.trim(),
      name: `${firstName.trim()} ${lastName.trim()}`,
      idType: contactIdType,
      idNumber: contactIdNumber.trim(),
      email: email.trim(),
      phone: phoneNumber,
      bornDate: moment(bornDate).format('DD-MM-YYYY'),
      publicKey: '-L-CGPS6Iuge1PFk755b',
      idCountryName: 'Cédula',
      password: password.trim(),
      profileImgURI: image, //base 64 image
      type: userType,
      questionKey,
      securityAnswer: securityAnswer.trim(),
      deviceUniqueToken: '',
      adminID: this.props.adminID,
      address: this.state.address,
    }
    customConsoleLog("SENDED: ", data)
    if (this.state.firstName.trim() !== '' &&
      this.state.lastName.trim() !== '' &&
      this.state.userType !== '' &&
      this.state.contactIdType !== '' &&
      this.state.password.trim() !== '' &&
      this.state.address.trim() !== '' &&
      this.state.contactIdNumber.trim() !== '' &&
      this.state.email.trim() !== '' &&
      this.state.questionKey !== '' &&
      this.state.securityAnswer.trim() !== '' &&
      this.state.bornDate !== '' &&
      this.validate18years(this.state.bornDate) &&
      this.state.image !== '' &&
      phone.trim().length === 10 &&
      this.state.password.trim().length > 7 &&
      this.state.email.trim().match(emailsRegex) &&
      this.state.password.trim().match(numbers) &&
      this.state.password.trim().match(letters)
    ) {
      try {
        let response = await postRequest('addUserByAdmin', data)
        customConsoleLog("RESPONSE: ", response)
        if (response.data.success) {
          this.setState({
            loading: false,
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            securityAnswer: '',
            contactIdType: '',
            contactIdNumber: '',
            userType: '',
            profileImgURI: '',
            imageError: false,
            image: '',
            phone: '',
            address: '',
            bornDate: '',
            questionKey: '',
            passwordVisibility: false,
            acceptTerms: false,
            displayErrors: false,
            redirectAfterRegister: true,
            registered: true
          })
          toast.update(this.toastRegisterId, {
            render: 'Su cuenta ha sido registrada exitosamente.',
            autoClose: 5000,
          })
        } else {
          this.setState({
            loading: false,
            errorMessage: response.data.error.message
          })
          return toast.update(this.toastRegisterId, {
            render: response.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        this.setState({
          loading: false,
          errorMessage: 'Verifique su conexión y vuelva a intentarlo.'
        })
        console.error(error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        return toast.update(this.toastRegisterId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        loading: false
      })
      if (this.state.image === '') {
        this.setState({ errorMessage: 'Debe subir una foto de perfil.', imageError: true })
        return toast.update(this.toastRegisterId, {
          render: 'Debe subir una foto de perfil.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.userType.value === '') {
        event.target.userType.focus()
        this.setState({ errorMessage: 'Debe colocar un tipo de usuario.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un tipo de usuario.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.firstName.value.trim() === '') {
        event.target.firstName.focus()
        this.setState({ errorMessage: 'Debe colocar un nombre de contacto.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un nombre de contacto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.lastName.value.trim() === '') {
        event.target.lastName.focus()
        this.setState({ errorMessage: 'Debe colocar un apellido de contacto.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un apellido de contacto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.bornDate.value === '') {
        event.target.bornDate.focus()
        this.setState({ errorMessage: 'Debe colocar una fecha de nacimiento.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una fecha de nacimiento.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.validate18years(this.state.bornDate)) {
        event.target.bornDate.focus()
        this.setState({ errorMessage: 'Debes ser mayor de edad para crear una cuenta Gualy.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debes ser mayor de edad para crear una cuenta Gualy.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (this.state.contactIdType === '') {
        event.target.contactIdType.focus()
        this.setState({ errorMessage: 'Debe colocar colocar tipo de documento.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar colocar tipo de documento.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.contactIdNumber.value.trim() === '') {
        event.target.contactIdNumber.focus()
        this.setState({ errorMessage: 'Debe colocar un número de cédula.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un número de cédula.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.email.value.trim() === '') {
        event.target.email.focus()
        this.setState({ errorMessage: 'Debe colocar un email.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un email.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }

      if (!this.state.email.trim().match(emailsRegex)) {
        event.target.email.focus()
        this.setState({ errorMessage: 'Debe colocar un email valido.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un email valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (phone.trim().value === '') {
        event.target.phone.focus()
        this.setState({ errorMessage: 'Debe colocar un número de telefóno.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un número de telefóno.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (phone.trim().length !== 10) {
        event.target.phone.focus()
        this.setState({ errorMessage: 'Su número de telefóno debe contener 10 digitos, ej: 4164543456' })
        return toast.update(this.toastRegisterId, {
          render: 'Su número de telefóno debe contener 10 digitos, ej: 4164543456',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.address.value.trim() === '') {
        event.target.address.focus()
        this.setState({ errorMessage: 'Debe colocar una dirección.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una dirección.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.password.value.trim() === '') {
        event.target.password.focus()
        this.setState({ errorMessage: 'Debe colocar una contraseña.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una contraseña.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.password.value.trim().length <= 7) {
        event.target.password.focus()
        this.setState({ errorMessage: 'Debe colocar una contraseña entre 8 y 15 caracteres.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una contraseña entre 8 y 15 caracteres.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.password.trim().match(numbers)) {
        event.target.password.focus()
        this.setState({ errorMessage: 'La contraseña debe contener un número entre (0-9).' })
        return toast.update(this.toastRegisterId, {
          render: 'La contraseña debe contener un número entre (0-9).',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.password.trim().match(letters)) {
        event.target.password.focus()
        this.setState({ errorMessage: 'La contraseña debe contener una letra mayúscula.' })
        return toast.update(this.toastRegisterId, {
          render: 'La contraseña debe contener una letra mayúscula.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.questionKey.value === '') {
        event.target.questionKey.focus()
        this.setState({ errorMessage: 'Debe colocar una pregunta de seguridad.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una pregunta de seguridad.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.securityAnswer.value.trim() === '') {
        event.target.securityAnswer.focus()
        this.setState({ errorMessage: 'Debe colocar una respuesta de seguridad.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una respuesta de seguridad.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  validate18years = (bornDate) => {
    const bornDateArray = bornDate.split('-')
    const eighteenYearsAgo = new Date().setFullYear(new Date().getFullYear() - 18)
    const minBornTimeStamp = new Date(eighteenYearsAgo).setHours(0, 0, 0, 0)// La fecha minima de nacimiento para un mayor de edad
    const bornTimeStamp = new Date(`${bornDateArray[0]}-${bornDateArray[1]}-${bornDateArray[2]}`).setHours(24, 0, 0, 0)
    if (bornTimeStamp > minBornTimeStamp) {
      return false //is not an adult
    } else {
      return bornDate ? true : false//is an adult
    }
  }
  handleVisibility = (e) => {
    e.preventDefault()
    this.setState({ passwordVisibility: !this.state.passwordVisibility })
  }

  handlePasswordVisibility = (e) => {
    e.preventDefault()
    this.setState({
      passwordVisibility: !this.state.passwordVisibility
    })
  }

  onChangeQuestionError = () => {
    this.setState({
      questionKeyError: false
    })
  }

  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0];
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            image: reader.result,
            imageError: false
          })
        }
        reader.readAsDataURL(file);
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }

  handleInputChange = ({ target }) => {
    const value =
      target.name === 'phone'
        ? target.value.length < 11
          ? target.value !== '0'
            ? target.value
            : target.value.slice(0, -1)
          : target.value.slice(0, -1)
        : target.name === 'contactIdNumber'
          ? target.value.length < 10 ? target.value : target.value.slice(0, -1)
          : target.name === 'password'
            ? target.value.length < 16 ? target.value : target.value.slice(0, -1)
            : target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [target.name]: value
    })
  }
  getOptions = items => items.map(item => {
    return <option key={item.customId} value={item.questionKey}>{item.text}</option>
  })
  handleQuestionSelect = ({ target: { value }, target }) => {
    this.setState({
      [target.name]: value
    })
  }
  render() {
    const { passwordVisibility } = this.state
    return (
      <div className={component}>
        <div className={header}>
          <h1> AÑADIR USUARIOS </h1>
        </div>
        <div className={divisor}></div>
        <form onSubmit={this.handleSubmit} noValidate autoComplete='off'>
          <div className={`${body} ${(this.state.displayErrors === true ? displayErrors : '')}`}>
            <div className={`${commerceDataDiv} ${firstComponent}`}>
              <div className={commerceMainDataDiv}>
                <div className={uploadBtnWrapper}>
                  <UploadBtn className={this.state.imageError ? specficError : ''}>
                    <img
                      src={this.state.image === '' ? iconCamera : this.state.image}
                      className={this.state.image === '' ? iconCameraClass : imagePreview}
                      alt="uploadPicture"
                    />
                    <input
                      className={inputFileHidden}
                      value={this.state.profileImgURI}
                      onChange={this.encodeImageFileAsURL}
                      name='profileImgURI'
                      type='file'
                      accept="image/*"
                      required
                    />
                  </UploadBtn>
                </div>
                <div className={nameInputDiv}>
                  <div className={`group mb-1`}>
                    <div css={`padding-bottom:5px;`} className={`${marginTop} ${dropDownDivContainer} ${this.state.displayErrors && this.state.questionKey === '' ? displayRedBorders : ''}`}>
                      <div className={dropDownDiv}>
                        <CustomDropdown className={dropdownQuestionPadding} value={this.state.userType} name='userType' onChange={this.handleQuestionSelect} required>
                          <option value="" disabled defaultValue>Tipo de usuario</option>
                          <option value="admin">Administrador</option>
                          <option value="client">Cliente</option>
                          <option value="cashier">Cajero</option>
                          <option value="support">Soporte</option>
                          <option value="marketing">marketing</option>
                          <option value="operator">Operador</option>
                        </CustomDropdown>
                        <div className={arrowContainer}>
                          <img src={arrowIcon} className={arrow} alt="dropdown" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={commerceDataDiv} css={`margin-top:0 !important;`}>
              <div css={`position: relative;width:100%;margin-top: 40px`}>
                <div className={`${commerceDataTitle} ${firstTitle}`}>
                  <h2>DATOS PERSONALES</h2>
                </div>
              </div>
              <div className={tripleInputDiv} css={`margin-top:0 !important;`}>
                <div>
                  <input
                    placeholder="Nombre"
                    onChange={this.handleInputChange}
                    value={this.state.firstName}
                    name='firstName'
                    pattern='(^[A-Za-záéíóúñ]+\s*([A-Za-záéíóúñ]+)?)$'
                    maxLength="40"
                    required
                  />
                </div>
                <div>
                  <input
                    placeholder="Apellido"
                    onChange={this.handleInputChange}
                    value={this.state.lastName}
                    name='lastName'
                    pattern='(^[A-Za-záéíóúñ]+\s*([A-Za-záéíóúñ]+)?)$'
                    maxLength="40"
                    required
                  />
                </div>
                <div css={`padding-bottom:5px;position: relative;width: 30%; margin-top: 14px !important`} className='group mb-1'>
                  <span className={beforeTermsAndConditions} >Fecha de nacimiento</span>
                  <input
                    name='bornDate'
                    type="date"
                    onChange={this.handleInputChange}
                    value={this.state.bornDate}
                    style={{ textAlign: "center" }}
                    required
                  />
                  <span className='highlight'></span>
                  <span className='bar'></span>
                </div>
              </div>
              <div className={tripleInputDiv}>
                <div className='d-flex'>
                  <div
                    className={`${dropDownDiv} 
                  ${firstComponent} 
                  ${DivDNI}
                  ${this.state.displayErrors && this.state.contactIdType === '' ? displayRedBorders : ''}
                  `}>
                    {/* <span>C.I.&nbsp;</span> */}
                    <CustomDropdown name='contactIdType' value={this.state.contactIdType} onChange={this.handleQuestionSelect}>
                      <option value="" defaultValue></option>
                      <option value="V-">V-</option>
                      <option value="E-">E-</option>
                      <option value="J-">J-</option>
                      <option value="G-">G-</option>
                    </CustomDropdown>
                    <div className={`${arrowContainer} ${firstComponent}`}>
                      <img src={arrowIcon} className={arrow} alt='arrow' />
                    </div>
                  </div>
                  <input
                    type="number"
                    placeholder="Cédula"
                    onChange={this.handleInputChange}
                    value={this.state.contactIdNumber}
                    name='contactIdNumber'
                    maxLength="8"
                    pattern='^\d{7,8}$'
                    required
                  />
                </div>
                <div>
                  <input
                    placeholder="Email"
                    type='email'
                    onChange={this.handleInputChange}
                    value={this.state.email}
                    name='email'
                    required
                  />
                </div>
                <div css={`position: relative`}>
                  <p
                    css={`
                      position: absolute;
                      font-weight: bold;
                      color: #8078ff;
                      left: -3px;
                      top: 10px;
                      `}
                  >+58</p>
                  <input
                    css={`
                     padding-left: 40px;
                     `}
                    placeholder="Telefono"
                    type='tel'
                    value={this.state.phone}
                    onChange={this.handleInputChange}
                    name='phone'
                    required
                    pattern='^([(]?\d{3,4}[)]?)\s?-?\s?\d{3}\s?-?\s?\d{4}$'
                    maxLength="11"
                  />
                </div>
                {/* <div className={hiddenInputDiv}><input/></div> */}
              </div>
              <div className={`${fullWidthInput}`}>
                <input
                  className={inputDescription}
                  placeholder='Dirección *'
                  name='address'
                  onChange={this.handleInputChange}
                  value={this.state.address}
                  required
                />
              </div>
              <div css={`position: relative; width:100%; margin-top: 40px`}>
                <div className={`${commerceDataTitle} ${firstTitle}`}>
                  <h2>SEGURIDAD</h2>
                </div>
              </div>
              <div className={tripleInputDivSecond}>
                <div css={`padding-bottom:5px;`} className={` ${passwordContainer}`}>
                  <input
                    type={passwordVisibility ? 'text' : 'password'}
                    placeholder='Contraseña *'
                    name='password'
                    onChange={this.handleInputChange}
                    value={this.state.password}
                    required
                  />
                  <button
                    className={btnVisibility}
                    onClick={this.handleVisibility}>
                    <img
                      className={visibilityOnCSS}
                      src={passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
                      alt="visibilidad"
                    />
                  </button>
                  <span className='highlight'></span>
                  <span className='bar'></span>
                </div>
                <div className={`group mb-1 ${individualChild}`}>
                  <div css={`padding-bottom:5px;`} className={`${marginTop} ${dropDownDivContainer} ${this.state.displayErrors && this.state.questionKey === '' ? displayRedBorders : ''}`}>
                    <div className={dropDownDiv}>
                      <CustomDropdown className={dropdownQuestionPadding} value={this.state.questionKey} name='questionKey' onChange={this.handleQuestionSelect} required>
                        <option value="" disabled defaultValue>Pregunta de seguridad</option>
                        {
                          this.getOptions(securityQuestionKeys)
                        }
                      </CustomDropdown>
                      <div className={arrowContainer}>
                        <img src={arrowIcon} className={arrow} alt="dropdown" />
                      </div>
                    </div>
                  </div>
                </div>
                <div css={`padding-bottom:5px;width: 30%`} className='group mb-1'>
                  <input
                    type='text'
                    placeholder='Respuesta secreta *'
                    name='securityAnswer'
                    onChange={this.handleInputChange}
                    value={this.state.securityAnswer}
                    required
                  />
                  <span className='highlight'></span>
                  <span className='bar'></span>
                </div>
                {/* <div className={hiddenInputDiv}><input/></div> */}
              </div>
            </div>
            <div className={`${lastComponent}`}>
              <AcceptButton
                width="216px"
                height="36px"
                loading={this.state.loading}
                content="AGREGAR USUARIO"
                type="submit"
              />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default AddUser