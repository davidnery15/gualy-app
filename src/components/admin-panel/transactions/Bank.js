import React from 'react'
import { css } from 'emotion'
import { getBankName } from './../../../constants/bankLogos'
/* CSS */
  const userTextContainer = css`
    text-align: middle;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 1px;
    padding: 15px 6px 3px 6px;
    @media (max-width: 1070px) {
      width: 148px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
  const userContainer = css`
    display: flex;
    margin: 1px;
    padding: 1px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items:center;
  `
  const bankText = css`
    font-size: 12px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #f6f7fa;
    @media (max-width: 1070px) {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
  const bankNumber = css`
    font-size: 11px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.9px;
    color: #7e848c;
    @media (max-width: 1070px) {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
const Bank = ({ info }) => {
  // console.log("NUMERO: ", (info.gualyBankAccount || info.userBankAccount).slice(0,4))
  // console.log("PRUEBA DE BANCO: ", getBankName(info.gualyBankAccount || info.userBankAccount).slice(0,4))
  return (
    <div className={userContainer}>
      <div className={userTextContainer}>
        <h2 className={bankText}>{getBankName((info.gualyBankAccount || info.userBankAccount).slice(0,4))}</h2>
        <p className={bankNumber}>{(info.gualyBankAccount || info.userBankAccount)}</p>
      </div>
    </div>
  );
}
export default Bank;