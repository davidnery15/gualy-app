import React from 'react'
import { css } from 'emotion'
import { getBankName } from './../../../constants/bankLogos'
import AcceptButton from '../../general/AcceptButton'
import RejectButton from '../../general/RejectButton'
import OptionsModalWrapper from '../../general/OptionsModalWrapper'
import errorSVG from '../../../assets/ic-info-red.svg'
const modalHeader = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 32px;
    width: 100%;
    height: 80px;
    margin-top: 10px;
  `
const modalBody = css`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    height: 100%;
    padding: 5px 32px;
    padding-bottom: 20px;
  `
const userImg = css`
    width: 34.2px;
    height: 34.2px;
    border-radius: 50%;
    object-fit: cover;
  `
const userName = css`
    font-size: 16px;
    font-weight: 900;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.4;
    letter-spacing: normal;
    line-height: 1.5;
  `
const userEmail = css`
    font-family: Montserrat;
    font-size: 12px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
  `
const userTextContainer = css`
    text-align: middle;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 1px;
    padding: 25px 6px 3px 6px;
  `
const userContainer = css`
    display: flex;
    margin: 1px;
    padding: 1px;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: left;
    align-items:center;
  `
const bankContainer = css`
    width:100%;
    height:100%;
    display: flex;
    flex-direction: column;
    align-items:flex-end;
  `
const bankText = css`
    font-size: 12px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    color: #f6f7fa;
    word-break: break-all;
    line-height: 1;
  `
const bankNumber = css`
    font-size: 11px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.9px;
    color: #7e848c;
  `
const modalTable = css`
    width: 100%;
    padding: 0;
    td {
      padding: 5px !important;
      padding-top: 17px;
    }
  `
const modalDataTitle = css`
    font-size: 11px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.9px;
    text-align: left;
    color: #f6f7fa;
  `
const modalData = css`
    font-size: 12px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: right;
    color: #f6f7fa;
  `
const statusText = css`
    font-size: 12px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: right;
    color: #f2ff4e;
  `
const tableDataBankTitle = css`
    display: flex;
  `
// const displayRedBorders = css`
//   border-color:red !important;
//   border-bottom: 1px solid #757575;
// `
const button = css`
  margin-top:13px 
`
const placeholder = css`
 ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 11px
}
::-moz-placeholder { /* Firefox 19+ */
  font-size: 11px
}
:-ms-input-placeholder { /* IE 10+ */
  font-size: 11px
}
:-moz-placeholder { /* Firefox 18- */
  font-size: 11px
}
`
const errorSvgCss = css`
width: 14.7px;
height: 13px;
object-fit: contain;
margin: 5px;
`
const errorContainer = css`
color: red;
display: flex;
justify-content: center;
align-items: center; 
font-size: 12px;
margin-top: 15px;
`
export default class Modal extends React.Component {
  render() {
    const {
      info,
      showModal,
      onClose,
      displayErrors,
      errorMessage,
      loading,
      bankReference,
      onChange,
      mode,
      updateStatusButtonWithdraw,
      updateStatusButtonOrder,
      reason,
    } = this.props
    return (
      <OptionsModalWrapper show={showModal} onClose={onClose} height="auto" width="310px">
        <div className={modalHeader}>
          <div className={userContainer}>
            <img className={userImg} src={info.senderProfilePicture} alt="search icon" />
            <div className={userTextContainer}>
              <h2 className={userName}>{info.senderUsername}</h2>
              <p className={userEmail}>{info.senderEmail}</p>
            </div>
          </div>
        </div>
        <div className={modalBody}>
          <table className={modalTable}>
            <tbody>
              <tr>
                <td><div className={modalDataTitle}>Fecha y hora</div></td>
                <td><div className={modalData}>{info.date}{' '}{info.time}</div></td>
              </tr>
              <tr>
                <td className={tableDataBankTitle}><div className={modalDataTitle}>Banco de origen</div></td>
                <td>
                  <div className={modalData}>
                    <div className={bankContainer}>
                      <h2 className={bankText}>{info.userBankAccount}</h2>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td className={tableDataBankTitle}><div className={modalDataTitle}>Banco destino</div></td>
                <td>
                  <div className={modalData}>
                    <div className={bankContainer}>
                      <h2 className={bankText}>{getBankName((info.gualyBankAccount || '0000').slice(0, 4))}</h2>
                      <span className={bankNumber}>{info.gualyBankAccount}</span>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td><div className={modalDataTitle}>Monto</div></td>
                <td><div className={modalData}>{mode === 'Withdraw' ? info.withdrawAmount || info.amount : info.amount}</div></td>
              </tr>
              <tr>
                <td><div className={modalDataTitle}>Estado</div></td>
                <td><div className={modalData}> <span className={statusText}> {info.status === 'Pending' ? 'Pendiente' : null} </span> </div></td>
              </tr>
              <tr>
                <td><div className={modalDataTitle}>Cedula</div></td>
                <td><div className={modalData}> {info.senderDni || "Transacción sin cedula"} </div></td>
              </tr>
              <tr>
                <td><div className={modalDataTitle}>Referencia bancaria</div></td>
                {
                  mode === 'Withdraw'
                    ? <td>
                      <input
                        value={bankReference}
                        name='bankReference'
                        disabled={loading}
                        required
                        onChange={onChange}
                        className={`${placeholder}`}
                        placeholder="Referencia bancaria *"
                        type="text"
                      />
                    </td>
                    : <td>
                      <div className={modalData}> <span className={statusText}> {info.bankReference} </span> </div>
                    </td>
                }
              </tr>
              {
                mode === 'Deposit'
                  ? <tr>
                    <td><div className={modalDataTitle}>Referencia bancaria interna</div></td>
                    <td>
                      <input
                        value={bankReference}
                        name='bankReference'
                        disabled={loading}
                        required
                        onChange={onChange}
                        className={`${placeholder}`}
                        placeholder="Referencia bancaria *"
                        type="text"
                      />
                    </td>
                  </tr>
                  : null
              }
              <tr>
                    <td><div className={modalDataTitle}>Razón de rechazo</div></td>
                    <td>
                      <input
                        value={reason}
                        name='reason'
                        disabled={loading}
                        required
                        onChange={onChange}
                        className={`${placeholder}`}
                        placeholder="Razón de rechazo *"
                        type="text"
                      />
                    </td>
                  </tr>
            </tbody>
          </table>
          {
            displayErrors ?
              <div className={errorContainer}>
                {errorMessage}
                <img
                  className={errorSvgCss}
                  src={errorSVG}
                  alt="error Icon"
                />
              </div>
              : null
          }
          <AcceptButton
            width="100%"
            height="34.2px"
            className={button}
            content="APROBAR TRANSACCIÓN"
            loading={loading}
            onClick={(e) => info.mode === 'Withdraw' 
            ? updateStatusButtonWithdraw('Approved') 
            : updateStatusButtonOrder('Approved')}
          />
          <RejectButton
            width="100%"
            height="47.2px"
            className={button}
            loading={loading}
            content="RECHAZAR TRANSACCIÓN"
            onClick={(e) => info.mode === 'Withdraw' 
            ? updateStatusButtonWithdraw('Rejected') 
            : updateStatusButtonOrder('Rejected')}
          />
        </div>
      </OptionsModalWrapper>
    );
  }
}