import React, { Component } from 'react'
import { css } from 'emotion'
import { Redirect } from 'react-router'
import Header from '../AdminHeader'
import MobileCardTable from './MobileCardTable'
import { connect } from 'react-redux'
import { postRequest } from '../../../utils/createAxiosRequest'
import { toast, ToastContainer } from 'react-toastify'
import Modal from './Modal'
import Table from '../../general/tables/Table'
import TransactionDetailsModal from '../../general/modals/transactionDetailsModal'
import {
  getRealizedTransactions,
  getWithdraws,
  getDeposits,
} from '../../../redux/actions/general'
import { customConsoleLog } from '../../../utils/customConsoleLog';
// import { database } from 'firebase/app';
/* CSS */
const isShow = css`
    @media(max-width: 1100px){
      display: none !important
    }
  `
const transactionsColumns = [
  { label: 'Fecha', key: 'adminDate' },
  { label: 'Cuenta origen', key: 'originAccount' },
  { label: 'Cuenta destino', key: 'destinyAccount' },
  { label: 'Concepto', key: 'concept' },
  { label: 'Estatus', key: 'transactionStatus' },
  { label: 'Monto', key: 'transactionsAmount' },
]
const withdrawsColumns = [
  { label: 'F. Solicitado', key: 'adminDate' },
  { label: 'F. Procesado', key: 'processedDate' },
  { label: 'Usuario', key: 'user' },
  { label: 'Banco destino', key: 'destinyBank' },
  { label: 'Estado', key: 'transactionStatus' },
  { label: 'Monto', key: 'transactionsAmount' },
]
const depositsColumns = [
  { label: 'F. Solicitado', key: 'adminDate' },
  { label: 'F. Procesado', key: 'processedDate' },
  { label: 'Usuario', key: 'user' },
  { label: 'Banco destino', key: 'destinyBank' },
  { label: 'Referencia bancaria', key: 'bankReference' },
  { label: 'Estado', key: 'transactionStatus' },
  { label: 'Monto', key: 'transactionsAmount' },
]
const scrollDataRequestedRange = 10
class Transactions extends Component {
  state = {
    //ORDER BY
    loadingMobile: true,
    bankReference: '',
    displayErrors: false,
    processWithdrawLoading: false,
    showProcessWithdrawModal: false,
    errorMessage: '',
    currentTransaction: [],
    mobileTransactionType: 'transactions',
    reason: '',
    //SCROLLING
    itemsIndexTransactions: 10,
    itemsIndexWithdraws: 10,
    itemsIndexDeposits: 10,
    transactionDetailModalIsOpen: false,
    selectedTransaction: "",
  }
  componentDidMount = () => {
    //REALIZED TRANSACTIONS 
    const realizedTransactionsIndex = this.state.itemsIndexTransactions
    this.props.dispatch(getRealizedTransactions({ realizedTransactionsIndex }))
    //DEPOSITS REQUEST
    const depositsIndex = this.state.itemsIndexWithdraws
    this.props.dispatch(getDeposits({ depositsIndex }))
    //WITHDRAWS REQUEST
    const withdrawsIndex = this.state.itemsIndexWithdraws
    this.props.dispatch(getWithdraws({ withdrawsIndex }))
  }
  handleMobile = (event) => {
    this.setState({ mobileTransactionType: event.target.value })
  }
  updateRealizedTransactions = () => {
    this.setState({ itemsIndexTransactions: this.state.itemsIndexTransactions + scrollDataRequestedRange }, () => {
      const realizedTransactionsIndex = this.state.itemsIndexTransactions
      this.props.dispatch(getRealizedTransactions({ realizedTransactionsIndex }))
    })
  }
  updateWithdraws = () => {
    this.setState({ itemsIndexWithdraws: this.state.itemsIndexWithdraws + scrollDataRequestedRange }, () => {
      const withdrawsIndex = this.state.itemsIndexWithdraws
      this.props.dispatch(getWithdraws({ withdrawsIndex }))
    })
  }
  updateDeposits = () => {
    this.setState({ itemsIndexDeposits: this.state.itemsIndexDeposits + scrollDataRequestedRange }, () => {
      const depositsIndex = this.state.itemsIndexDeposits
      this.props.dispatch(getDeposits({ depositsIndex }))
    })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.props.userInCollectionData.type) {
        const redirect =
          this.props.userInCollectionData.type === 'admin' ||
            this.props.userInCollectionData.type === 'cashier'
            ? null
            : < Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }

  //deposits
  handleChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  onCloseShowProcessWithdrawModal = config => () => {
    this.setState({
      showProcessWithdrawModal: !this.state.showProcessWithdrawModal,
      displayErrors: false,
      bankReference: '',
      currentTransaction: config.info
    })
  }
  onCloseShowProcessWithdrawFromModal = () => {
    this.setState({
      showProcessWithdrawModal: !this.state.showProcessWithdrawModal,
      displayErrors: false,
      bankReference: '',
    })
  }
  updateStatusButtonOrder = async (status) => {
    const idTransaction = this.state.currentTransaction.idTransaction
    this.setState({ processWithdrawLoading: true })
    this.toastLoaderId = toast('Cargando...', { autoClose: false })
    if (status === 'Approved' && this.state.bankReference.trim() === ''
      ? false : true &&
        status === 'Rejected' && !this.state.reason ? false : true) {
      const data = {
        adminID: this.props.userInCollectionData.userKey,
        orderID: idTransaction,
        action: status,
        secondBankReference: this.state.bankReference.trim(),
        reason: this.state.reason,
      }
      // console.log("SENDED: ", data)
      try {
        let resp = await postRequest('transactions/processPendingOrder', data)
        // console.log("RESPONSE: ", resp)
        if (resp.data.success) {
          this.updateDeposits()
          toast.update(this.toastLoaderId, {
            render: status === 'Approved'
              ? 'Transacción aprobada con exito.'
              : 'Transacción rechazada con exito.',
            autoClose: 5000
          })
          this.setState({
            processWithdrawLoading: false,
            bankReference: '',
            displayErrors: false,
            showProcessWithdrawModal: false,
            errorMessage: '',
            reason: '',
          })
        } else {
          this.setState({
            processWithdrawLoading: false,
            displayErrors: true,
            errorMessage: resp.data.error.message
          })
          toast.update(this.toastLoaderId, {
            render: resp.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        // console.log(error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        this.setState({
          processWithdrawLoading: false,
          errorMessage: errorMessage,
          displayErrors: true,
        })
        toast.update(this.toastLoaderId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        processWithdrawLoading: false,
      })
      if (status === 'Approved' && this.state.bankReference.trim() === '') {
        this.setState({ errorMessage: 'Ingrese referencia bancaria.' })
        toast.update(this.toastLoaderId, {
          render: 'Ingrese referencia bancaria.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (status === 'Rejected' && !this.state.reason) {
        this.setState({ errorMessage: 'Ingrese razón re rechazo.' })
        toast.update(this.toastLoaderId, {
          render: 'Ingrese razón re rechazo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    }
  }
  updateStatusButtonWithdraw = async (status) => {
    this.setState({ processWithdrawLoading: true })
    const idTransaction = this.state.currentTransaction.idTransaction
    this.toastLoaderId = toast('Cargando...', { autoClose: false })
    if (status === 'Approved' && this.state.bankReference.trim() === '' ? false : true &&
      status === 'Rejected' && !this.state.reason ? false : true) {
      const data = {
        adminID: this.props.userInCollectionData.userKey,
        transactionID: idTransaction,
        action: status,
        bankReference: this.state.bankReference.trim(),
        reason: this.state.reason,
      }
      // console.log("SENDED: ", data)
      try {
        let resp = await postRequest('transactions/processWithdraw', data)
        // console.log("RESPONSE: ", resp)
        if (resp.data.success) {
          this.updateWithdraws()
          toast.update(this.toastLoaderId, {
            render: status === 'Approved'
              ? 'Transacción aprobada con exito.'
              : 'Transacción rechazada con exito.',
            autoClose: 5000
          })
          this.setState({
            processWithdrawLoading: false,
            bankReference: '',
            displayErrors: false,
            showProcessWithdrawModal: false,
            errorMessage: '',
            reason: '',
          })
        } else {
          this.setState({
            processWithdrawLoading: false,
            errorMessage: resp.data.error.message,
            displayErrors: true
          })
          toast.update(this.toastLoaderId, {
            render: resp.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        // console.log(error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        this.setState({
          processWithdrawLoading: false,
          errorMessage: errorMessage,
          displayErrors: true
        })
        toast.update(this.toastLoaderId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        processWithdrawLoading: false,
      })
      if (status === 'Approved' && this.state.bankReference.trim() === '') {
        this.setState({ errorMessage: 'Ingrese referencia bancaria.' })
        toast.update(this.toastLoaderId, {
          render: 'Ingrese referencia bancaria.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (status === 'Rejected' && !this.state.reason) {
        this.setState({ errorMessage: 'Ingrese razón re rechazo.' })
        toast.update(this.toastLoaderId, {
          render: 'Ingrese razón re rechazo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    }
  }
  //HISTORY MODAL
  closeTransactionDetailsModal = () => {
    this.setState({
      transactionDetailModalIsOpen: false,
      selectedTransaction: ""
    })
  }
  onHistoryTransactionClick = ({ transaction }) => () => {
    this.setState({
      selectedTransaction: transaction,
      transactionDetailModalIsOpen: true
    })
  }
  render() {
    customConsoleLog("========================================>")
    customConsoleLog("transactions: ", Object.values(this.props.realizedTransactions || {}))
    customConsoleLog("deposits: ", this.props.deposits)
    customConsoleLog("withdraws: ", this.props.withdraws)
    customConsoleLog("========================================")
    customConsoleLog("itemsIndexTransactions: ", this.state.itemsIndexTransactions)
    customConsoleLog("itemsIndexWithdraws: ", this.state.itemsIndexWithdraws)
    customConsoleLog("itemsIndexDeposits: ", this.state.itemsIndexDeposits)
    customConsoleLog("========================================>")
    const mobileTransactions =
      this.state.mobileTransactionType === 'transactions'
        ? this.props.realizedTransactions
        : this.state.mobileTransactionType === 'deposits'
          ? this.props.deposits
          : this.state.mobileTransactionType === 'withdraws'
            ? this.props.withdraws
            : {}

    const mobileColumns =
      this.state.mobileTransactionType === 'transactions'
        ? transactionsColumns
        : this.state.mobileTransactionType === 'deposits'
          ? depositsColumns
          : this.state.mobileTransactionType === 'withdraws'
            ? withdrawsColumns
            : {}
    const onRechargeClickMobile = this.state.mobileTransactionType === 'transactions'
      ? this.updateRealizedTransactions
      : this.state.mobileTransactionType === 'deposits'
        ? this.updateDeposits
        : this.state.mobileTransactionType === 'withdraws'
          ? this.updateWithdraws
          : null
    return (
      <div>
        <Header />
        <div>
          <Table
            height={450}
            mobileMediaWidth="1150px"
            data={Object.values(this.props.realizedTransactions || {})}
            title="TRANSACCIONES REALIZADAS CON GUALY"
            type="transactions"
            loading={this.props.loadingRealizedTransactions}
            columns={transactionsColumns}
            maxWidth="1500px"
            className={isShow}
            disableHeader={false}
            //table header
            icon={true}
            filter={true}
            placeholder='Buscar...'
            //scrolling
            databaseRequest={this.updateRealizedTransactions}
            onRechargeClick={this.updateRealizedTransactions}
            userKey={this.props.userInCollectionData.userKey}
            onHistoryTransactionClick={this.onHistoryTransactionClick}
          />
          <Table
            data={Object.values(this.props.withdraws || {})}
            title="RETIROS DE GUALY"
            type="withdraws"
            loading={this.props.loadingWithdraws}
            height={450}
            mobileMediaWidth="1150px"
            columns={withdrawsColumns}
            maxWidth="1500px"
            className={isShow}
            disableHeader={false}
            //table header
            icon={true}
            filter={true}
            placeholder='Buscar...'
            onCloseShowProcessWithdrawModal={this.onCloseShowProcessWithdrawModal}
            //scrolling
            databaseRequest={this.updateWithdraws}
            onRechargeClick={this.updateWithdraws}
            userKey={this.props.userInCollectionData.userKey}
            onHistoryTransactionClick={this.onHistoryTransactionClick}
          />
          <Table
            data={Object.values(this.props.deposits || {})}
            title="INGRESOS GUALY"
            type="deposits"
            loading={this.props.loadingDeposits}
            height={450}
            mobileMediaWidth="1150px"
            columns={depositsColumns}
            maxWidth="1500px"
            className={isShow}
            disableHeader={false}
            //table header
            icon={true}
            filter={true}
            placeholder='Buscar...'
            onCloseShowProcessWithdrawModal={this.onCloseShowProcessWithdrawModal}
            //scrolling
            databaseRequest={this.updateDeposits}
            onRechargeClick={this.updateDeposits}
            userKey={this.props.userInCollectionData.userKey}
            onHistoryTransactionClick={this.onHistoryTransactionClick}
          />
          <MobileCardTable
            loadingMobile={this.props.transactionsMobileLoading}
            icon={true}
            transactions={mobileTransactions}
            handleMobile={this.handleMobile}
            mobileTransactionType={this.state.mobileTransactionType}
            onCloseShowProcessWithdrawModal={this.onCloseShowProcessWithdrawModal}
            columns={mobileColumns}
            databaseRequest={onRechargeClickMobile}
            onRechargeClick={onRechargeClickMobile}
            userKey={this.props.userInCollectionData.userKey}
            onHistoryTransactionClick={this.onHistoryTransactionClick}
          />
          <Modal
            onChange={this.handleChange}
            showModal={this.state.showProcessWithdrawModal}
            onClose={this.onCloseShowProcessWithdrawFromModal}
            bankReference={this.state.bankReference}
            loading={this.state.processWithdrawLoading}
            errorMessage={this.state.errorMessage}
            displayErrors={this.state.displayErrors}
            info={this.state.currentTransaction}
            mode={this.state.currentTransaction.mode}
            reason={this.state.reason}
            updateStatusButtonWithdraw={this.updateStatusButtonWithdraw}
            updateStatusButtonOrder={this.updateStatusButtonOrder}
          />
          <TransactionDetailsModal
            onClose={this.closeTransactionDetailsModal}
            showModal={this.state.transactionDetailModalIsOpen}
            transaction={this.state.selectedTransaction}
            isAdmin={true}
          />
        </div>
        {
          this.redirect()
        }
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn,
    //GET COMMERCES
    realizedTransactions: state.general.realizedTransactions,
    loadingRealizedTransactions: state.loadingRealizedTransactions,
    //GET WITHDRAWS
    withdraws: state.general.withdraws,
    loadingWithdraws: state.loadingWithdraws,
    //GET DEPOSITS
    deposits: state.general.deposits,
    loadingDeposits: state.loadingDeposits,

    transactionsMobileLoading: state.transactionsMobileLoading,
  }
}
export default connect(mapStateToProps)(Transactions)
