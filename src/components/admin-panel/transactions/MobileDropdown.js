import React from 'react'
import { css } from 'emotion'
import styled from 'react-emotion'
import arrowIcon from './../../../assets/drop-down-arrow.svg'
/* CSS */
const component = css`
    background-color: transparent;
    height: 100%;
    margin-left: 20px;
    margin-right: 20px;
  `
const dropdown = css`
    display: flex;
    width:100%;
    border-bottom: 1px solid;
  `
const MobileSelect = styled('select')`
    height: 47.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    outline:none !important;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    option{
      background-color: #242657;
    }
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const arrowContainer = css`
    pointer-events: none;
    width: 5%;
    display: flex;
    justify-content: center;
    align-items: center;
  `

export default class mobibleDropdown extends React.Component {
  render() {
    return (
      <div className={component}>
        <div className={`${dropdown}`}>
          <MobileSelect name='mobileSelect' onChange={this.props.handleMobile} value={this.props.mobileTransactionType}>
            <option value="transactions" defaultValue>TRANSACCIONES CON GUALY</option>
            <option value="withdraws">RETIROS DE GUALY</option>
            <option value="deposits">INGRESOS A GUALY</option>
          </MobileSelect>
          <div className={arrowContainer}>
            <img src={arrowIcon} className={arrow} alt='' />
          </div>
        </div>
      </div>
    )
  }
}