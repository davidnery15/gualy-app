import React from 'react'
import { css } from 'emotion'
import MobileDropdown from './MobileDropdown'
import Table from '../../general/tables/Table'
/* CSS */
const component = css`
    margin-top: 40px;
    margin-bottom: 40px;
    background-color: transparent;
    height: 100%;
    @media (min-width: 1100px) {
      display: none;
    }
  `
const MobileCardTable = ({
  transactions,
  handleMobile,
  mobileTransactionType,
  icon,
  loadingMobile,
  onCloseShowProcessWithdrawModal,
  maxWidth,
  columns,
  databaseRequest,
  recharge,
  onRechargeClick,
  userKey,
  onHistoryTransactionClick,
}) => <div className={component}>
    <MobileDropdown
      handleMobile={handleMobile}
      mobileTransactionType={mobileTransactionType}
    />
    <Table
      loading={loadingMobile}
      title={
        mobileTransactionType === 'transactions'
          ? 'TRANSACCIONES REALIZADAS CON GUALY'
          : mobileTransactionType === 'deposits'
            ? "INGRESOS GUALY"
            : mobileTransactionType === 'withdraws'
              ? "RETIROS DE GUALY"
              : ''
      }
      type={mobileTransactionType}
      data={transactions}
      columns={columns}
      mobileMediaWidth="1150px"
      height={450}
      maxWidth={maxWidth}
      //table header
      filter={true}
      placeholder='Buscar...'
      icon={icon}
      //transactions
      onCloseShowProcessWithdrawModal={onCloseShowProcessWithdrawModal}
      databaseRequest={databaseRequest}
      recharge={recharge}
      onRechargeClick={onRechargeClick}
      userKey={userKey}
      onHistoryTransactionClick={onHistoryTransactionClick}
    />
  </div>
export default MobileCardTable


