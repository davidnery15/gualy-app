import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css } from 'emotion'
import styled from 'react-emotion'
import { toast } from 'react-toastify'
import { style } from 'react-toastify'
import arrowIcon from './../../../assets/drop-down-arrow.svg'
import iconCamera from './../../../assets/icon-camera.svg'
import { postRequest } from '../../../utils/createAxiosRequest'
import AcceptButton from '../../general/AcceptButton'
import { restrictTo } from '../../../utils/restrict'
import { cities } from './venezuelan-cities.json'
import { categories } from './categories.json'
import { emailsRegex } from '../../../constants/regex'
import AddBankAccountForm from '../../general/AddBankAccountForm'
import { banks } from '../../../constants/bankLogos'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import R from '../../../utils/R'
import { googleApiKey } from '../../../config/index'
import GoogleMapReact from 'google-map-react'
// Changing default tostify css 
style({
  colorProgressDefault: "#95fad8",
  fontFamily: "Montserrat",
});

/* CSS */
const component = css`
    margin: 50px;
    margin-bottom: 50px;
    margin-top: 0;
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    max-width: 1200px;
    align-self: center;
    width: 100%;
    @media (max-width: 1230px) {
      margin: 40px 25px;
    }
  `
const firstComponent = css`
    margin-top: 0 !important;
  `
const firstTitle = css`
    margin-bottom: 15px;
  `
const lastComponent = css`
    margin-bottom: 70px;
    @media( max-width: 930px) {
      margin-bottom: 50px !important;
    }
    @media( max-width: 630px) {
      margin-bottom: 50px !important;
    }
  `
const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const body = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 25px 10px;
    margin-bottom: 0px;
    input{
      font-size: 15.7px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.5;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      border-bottom: 1px solid #ffffff;
    }
    // div{
    //   margin-top: 70px;
    // }
    @media (max-width: 970px) {
      flex-direction: column;
      margin: 24px 25px;
      margin-bottom: 0px;
      div{
        margin-top: 50px;
      }
    }
    @media (max-width: 360px) {
      margin: 25px 10px;
    }
  `
const commerceDataDiv = css`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    width: 100%;
    padding: 0 15%;
    position: relative;
    margin-top: 20px;
    div{
      margin-top: 45px;
    }
    @media( max-width: 1024px) {
      padding: 0 8%;
    }
    @media( max-width: 930px) {
      div{

        margin-top: 15px;
      }
    }
     @media( max-width: 630px) {
      padding: 0;
      div{
        margin-top: 15px;
      }
    }
  `
const commerceDataTitle = css`
    position: absolute;
    width: 100%;
    margin-top: 0 !important;
    padding-left: 5%;
    h2{
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      text-align: left;
      color: #f6f7fa;
    }
    @media(max-width: 930px) {
      position: initial;
      padding-left: 0;
    }
  `
const commerceMainDataDiv = css`
    display:flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin-top: 0 !important;
    div {
      margin-top: 15px;
    }
  `
const UploadBtn = styled('button')`
    width: 139px;
    height: 139px;
    color: #242656;
    display: flex;
    justify-content: center;
    align-items: center;
    outline: none !important;
    cursor: pointer;
    background-color: #95fad8;
    border: none;
    border-radius: 72px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    position: relative;
    @media(max-width: 630px) {
      width: 72px;
      height: 72px;
    }
  `
const iconCameraClass = css`
    width: 53px;
    height: 47px;
    object-fit: contain;
    @media(max-width: 630px) {
      width: 35px;
      height: 32px;
    }
  `
const uploadBtnWrapper = css`
    margin-top: 0 !important;
  `

const nameInputDiv = css`
    width: 252.4px;
    height: 100%;
    input{
      text-align: center;
      font-size: 20px;
      font-weight: 900;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.18;
      letter-spacing: normal;
      text-align: center;
      color: #ffffff;
    }
    @media( max-width: 630px) {
      width: 100%;
    }
  `
const rifInputDiv = css`
    width: 160px;
    height: 100%;
    input{
      text-align: center;
    }
  `
const fullWidthInput = css`
    width: 100%;
    @media(max-width: 1024px) {
      margin-top: 45px !important;
    }
  `
const inputDescription = css`
    text-align: center !important;
    @media( max-width: 630px) {
      text-align: left !important;
    }
  `
// const hiddenInputDiv = css`
// visibility: hidden;
// @media( max-width: 670px) {
// display: none;
// }
// `
const doubleInput = css`
    width: 100%;
    display: flex;
    justify-content: center;
    input{
      width: 46%;
    }
    @media( max-width: 530px) {
      margin-top: 0 !important;
      display: flex;
      flex-direction: column;
      align-items: center;
      input{
        width: 100%;
        margin-top: 15px;
      }
    }
  `
const tripleInputDiv = css`
    width: 100%;
    display: flex;
    justify-content: space-between;
    div{
      width: 30%;
      input{
        width: 100%;
      }
    }
    @media( max-width: 670px) {
      margin-top: 0 !important;
      display: flex;
      flex-direction: column;
      div{
        width: 100%;
        margin-top: 15px;
      }
    }
  `
const dropDownDiv = css`
    position: relative;
    display: flex;
    align-items:center;
    
  `
const CustomDropdown = styled('select')`
    height: 43.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    outline: none !important;
    option{
      background-color: #242657;
    }
    
  `
const CustomDropdown2 = styled('select')`
    height: 43.3px;
    width:90%;
    padding: 10px 0;
    background: transparent;
    border: none;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    outline: none !important;
    option{
      background-color: #242657;
    }
    
  `
const arrowContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    pointer-events: none;
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const DivDNI = css`
    width: 40px !important;
    border-bottom: 1px solid white;
    margin-right: 15px;
    position: relative;
    min-width: 55px;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const DivDNI2 = css`
    border-bottom: 1px solid white;
    margin-right: 15px;
    position: relative;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const imagePreview = css`
    width: 100%;
    height: 100%;
    position: absolute;
    cursor: pointer;
    border-radius: 72px;
    object-fit: cover;
    border: 2px solid white;
  `
const displayErrors = css`
    input:invalid {
      border-color:red;
    }
    input:invalid specficError 
  `
const specficError = css`
    border: 3px solid red;
  `
const displayRedBorders = css`
border-bottom-color:red;
`
const addBankAccountFormContainer = css`
div {
  margin-top: 0 !important;
}
`
const commerceTypeContainer = css`
width: 200px;
padding-top: 20px;
margin-right: 0;
`
const mapContainer = css`
margin-top: 30px; 
width: 90%;
@media( max-width: 360px) {
  width: 100%;
}
`
const hideClass = css`
display: none;
`
class AddCommerce extends Component {
  state = ({
    email: '',
    firstName: '',
    lastName: '',
    address: '',
    contactIdType: '',
    contactIdNumber: '',
    profileImgURI: '',
    imageError: false,
    image: '',
    phone: '',
    loading: false,
    displayErrors: false,
    errorMessage: '',
    idNumber: '',
    name: '',
    corporateName: '',
    description: '',
    bankAccount: '',
    latitude: '',
    longitude: '',
    adminID: '',
    city: '',
    idType: '',
    category: '',
    bankAccountArray: [],
    addAccountLoading: false,
    deleteModalIsOpen: false,
    modalIsOpen: false,
    accountNumber: "",
    selectedBankAccount: "",
    commerceType: 'physicalCommerce',
    //GOOGLE MAPS
    defaultCenter: { lat: 10.653860, lng: -71.645966 },
    zoom: 11,
  })

  handleSubmit = async (event) => {
    event.preventDefault();
    this.toastLoaderId = toast('Registrando comercio', { autoClose: false })
    if (
      this.state.email.trim() !== '' &&
      this.state.email.trim().match(emailsRegex) &&
      this.state.firstName.trim() !== '' &&
      this.state.lastName.trim() !== '' &&
      (this.state.address.trim() === '' && this.state.commerceType === 'physicalCommerce' ? false : true) &&
      this.state.contactIdType !== '' &&
      this.state.contactIdNumber.trim() !== '' &&
      this.state.image !== '' &&
      this.state.phone.trim() !== '' &&
      (this.state.idNumber.trim() === '' && this.state.commerceType === 'physicalCommerce' ? false : true) &&
      this.state.name.trim() !== '' &&
      this.state.category !== '' &&
      this.state.corporateName.trim() !== '' &&
      this.state.description.trim() !== '' &&
      (this.state.idType === '' && this.state.commerceType === 'physicalCommerce' ? false : true) &&
      this.state.city.trim() !== '' &&
      this.state.bankAccountArray.length > 0
    ) {
      this.setState({
        displayErrors: false, errorMessage: '', loading: true
      })
      // form is valid! We can parse and submit data
      const vePhoneNumber = `+58${this.state.phone.trim()}`
      const data = {
        firstName: this.state.firstName.trim(), // Dixon 
        lastName: this.state.lastName.trim(), // Carrizo
        category: this.state.category, // Alimentacion
        idType: this.state.commerceType === 'physicalCommerce'
          ? this.state.idType : "", // J-
        idNumber: this.state.commerceType === 'physicalCommerce'
          ? this.state.idNumber.trim()
          : "", // 12345678-1
        email: this.state.email.trim(), //dcarrizo@lctis.com.ve
        phone: vePhoneNumber, // 04141234567
        publicKey: '-L-CGPS6Iuge1PFk755b',
        idCountryName: this.state.idType === 'V-'
          || this.state.idType === 'E-'
          ? 'Cedula' : this.state.idType === 'J-'
            ? 'RIF' : 'Pasaporte', // this.state.idType, // J-

        profileImgURI: this.state.image, // base64
        name: this.state.name.trim(), // Lctis
        deviceUniqueToken: '',
        corporateName: this.state.corporateName, //Humbee Partners
        type: 'commerce',
        description: this.state.description.trim(),
        address: this.state.address.trim(),
        bankAccount: this.state.bankAccountArray,
        latitude: this.state.commerceType === 'physicalCommerce'
          ? String(this.state.latitude).trim()
          : "",
        longitude: this.state.commerceType === 'physicalCommerce'
          ? String(this.state.longitude).trim() : "",
        adminID: this.props.adminID,
        city: this.state.city,
        contactIdType: this.state.contactIdType, // V- 
        contactIdNumber: this.state.contactIdNumber.trim(), // 23853928
        commerceType: this.state.commerceType,
      }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest('users/addUserByAdmin', data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success) {
          this.setState({
            email: '',
            firstName: '',
            lastName: '',
            address: '',
            contactIdType: '',
            contactIdNumber: '',
            profileImgURI: '',
            imageError: false,
            image: '',
            phone: '',
            loading: false,
            displayErrors: false,
            errorMessage: '',
            idNumber: '',
            name: '',
            corporateName: '',
            description: '',
            bankAccountArray: [],
            latitude: '',
            longitude: '',
            adminID: '',
            city: '',
          })
          toast.update(this.toastLoaderId, {
            render: 'Comercio Registrado Exitosamente.',
            autoClose: 5000
          })
        } else {
          this.setState({
            displayErrors: true, errorMessage: resp.data.error.message, loading: false
          })
          toast.update(this.toastLoaderId, {
            render: resp.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        customConsoleLog("add commerce error: ", error)
        const errorMessage = ({
          '401': 'Su sesión ha caducado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    } else {
      this.setState({
        displayErrors: true
      })
      if (this.state.image === '') {
        this.setState({
          imageError: true
        })
        return toast.update(this.toastLoaderId, {
          render: 'Debe subir una foto de perfil.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.name.trim() === '') {
        event.target.name.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar el nombre del comercio.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.commerceType === '') {
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar tipo de comercio.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.idNumber.trim() === '' && this.state.commerceType === 'physicalCommerce') {
        event.target.idNumber.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar el RIF del comercio.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.idType === '' && this.state.commerceType === 'physicalCommerce') {
        event.target.idType.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar colocar tipo de documento del rif del comercio.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.corporateName.trim() === '') {
        event.target.corporateName.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar la razón social del comercio.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.description.trim() === '') {
        event.target.description.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar una descripción del comercio.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.address.trim() === '' && this.state.commerceType === 'physicalCommerce') {
        event.target.address.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar una dirección fiscal.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.firstName.trim() === '') {
        event.target.firstName.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar un nombre de contacto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.lastName.trim() === '') {
        event.target.lastName.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar un apellido de contacto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.contactIdNumber.trim() === '') {
        event.target.contactIdNumber.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar un número de cédula.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.contactIdType === '') {
        event.target.contactIdType.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar colocar tipo de documento.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.email.trim() === '') {
        event.target.email.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar un email.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (!this.state.email.trim().match(emailsRegex)) {
        event.target.email.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar un email valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.phone.trim() === '') {
        event.target.phone.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar un número de telefóno.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.category === '') {
        event.target.category.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar categoria.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (this.state.city === '') {
        event.target.city.focus()
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar uan ciudad.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if (!this.state.bankAccountArray.length > 0) {
        return toast.update(this.toastLoaderId, {
          render: 'Debe colocar una cuenta bancaria.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    }
  }
  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0];
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            image: reader.result,
            imageError: false
          })
        }
        reader.readAsDataURL(file);
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }

  handleInputChange = ({ target }) => {
    const restrictedValue = restrictTo(target.value)
    let value
    // customConsoleLog("target.name: ", target.name)
    // customConsoleLog("restrictedValue: ", restrictedValue(10))

    if (target.type === 'checkbox') {
      value = target.checked
    } else {
      // customConsoleLog("target.value: ", target.value)
      value = ({
        'bankAccount': restrictedValue(20),
        'phone': target.value === '0' ? '' : restrictedValue(10),
        'contactIdNumber': restrictedValue(10),
        'password': restrictedValue(16)
      })[target.name] || target.value
    }
    this.setState({
      [target.name]: value
    })
  }
  onInputChange = ({ target }) => {
    customConsoleLog("target: ", target.name, "value: ", target.value)
    const value = target.name === 'accountNumber' &&
      target.value.length > 20
      ? target.value.slice(0, -1)
      : target.value
    this.setState({
      [target.name]: value
    })
  }
  //ADD BANK ACCOUNT
  addBankAccount = async (event) => {
    event.preventDefault()
    const bankId = this.state.accountNumber.slice(0, 4)
    const filteredLogo = banks.filter(item => item.id === bankId)[0]
    this.toastAddBankId = toast('Agregando cuenta...', { autoClose: false })
    this.setState({ addAccountLoading: true, displayErrors: false, errorMessage: '' })
    if (
      this.state.accountNumber.length === 20 &&
      filteredLogo
    ) {
      try {
        let array = this.state.bankAccountArray
        let boolean = R.filter(element => element.bankAccount === this.state.accountNumber, array)
        if (!boolean.length > 0) {
          array.push({
            bankAccount: this.state.accountNumber,
            bankAccountNumber: this.state.accountNumber,
            bankAccountID: `BA--${this.state.accountNumber}`,
          })
          this.setState({
            bankAccountArray: array,
            addAccountLoading: false,
            accountNumber: '',
            modalIsOpen: false,
            displayErrors: false
          })
          toast.update(this.toastAddBankId, {
            render: 'Cuenta bancaria ha sido agregada exitosamente.',
            autoClose: 5000,
          })
        } else {
          this.setState({
            addAccountLoading: false,
            errorMessage: 'Cuenta bancaria ya se encuentra agregada.'
          })
          toast.update(this.toastAddBankId, {
            render: 'Cuenta bancaria ya se encuentra agregada.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        this.setState({
          addAccountLoading: false,
          errorMessage: 'Verifique su conexión y vuelva a intentarlo.'
        })
        toast.update(this.toastAddBankId, {
          render: 'Verifique su conexión y vuelva a intentarlo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        customConsoleLog(error)
      }
    } else {
      this.setState({
        displayErrors: true,
        addAccountLoading: false
      })
      if (this.state.accountNumber.length !== 20) {
        this.setState({ errorMessage: 'Nro de cuenta debe ser de 20 dijitos' })
        return toast.update(this.toastAddBankId, {
          render: 'Nro de cuenta debe ser de 20 dijitos',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!filteredLogo) {
        this.setState({ errorMessage: 'Nro de cuenta invalida.' })
        return toast.update(this.toastAddBankId, {
          render: 'Nro de cuenta invalida.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  deleteBankAccount = (event) => {
    event.preventDefault()
    this.setState({ addAccountLoading: true }, () => {
      try {
        const bankAccountArray = R.filter(element => element.bankAccount !== this.state.selectedBankAccount, this.state.bankAccountArray)
        this.setState({
          bankAccountArray,
          addAccountLoading: false,
          deleteModalIsOpen: false,
        })
      } catch (error) {
        this.setState({
          addAccountLoading: false,
          errorMessage: 'Verifique su conexión y vuelva a intentarlo.'
        })
        toast.update(this.toastAddBankIdDelete, {
          render: 'Verifique su conexión y vuelva a intentarlo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        customConsoleLog("deleteBankAccount error:", error)
      }
    })
  }
  //SHOW ADD BANK ACCOUNTMODAL
  modalShow = () => {
    this.setState({
      accountNumber: '',
      displayErrors: false,
      modalIsOpen: !this.state.modalIsOpen
    })
  }
  //SHOW EDIT BANK ACCOUNT MODAL
  openDeleteModal = config => (event) => {
    event.preventDefault()
    this.setState({
      deleteModalIsOpen: !this.state.deleteModalIsOpen,
      selectedBankAccount: config.bankKey
    })
  }
  closeDeleteModal = (event) => {
    event.preventDefault()
    this.setState({
      deleteModalIsOpen: !this.state.deleteModalIsOpen,
    })
  }
  addMarker = (location) => {
    // console.log("location: ", location)
    this.setState({ latitude: location.lat, longitude: location.lng })
  }
  handleApiLoaded = (map, maps) => {
    // use map and maps objects
    // Create the search box and link it to the UI element.
    let input = document.getElementById('pac-input');
    let searchBox = maps && maps.places ? new maps.places.SearchBox(input) : null
    map.controls[maps.ControlPosition.TOP_LEFT].push(input);
    let marker = new maps.Marker(
      {
        draggable: true,
        position: { lat: this.state.latitude, lng: this.state.longitude },
        animation: maps.Animation.DROP,
      }
    );
    marker.setMap(map)
    // Zoom to 9 when clicking on marker
    // maps.event.addListener(marker, 'click', () => {
    //   map.setZoom(15)
    // })
    map.addListener('click', (e) => {
        marker.setPosition(e.latLng)
    })
    if (searchBox) {
      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', () => {
        searchBox.setBounds(map.getBounds())
      })
      let markers = []
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', () => {
        let places = searchBox.getPlaces()
        if (places.length === 0) {
          return;
        }
        // Clear out the old markers.
        markers.forEach(marker => {
          marker.setMap(null);
        });
        markers = [];
        // For each place, get the icon, name and location.
        let bounds = new maps.LatLngBounds()
        places.forEach(place => {
          if (!place.geometry) {
            // console.log("Returned place contains no geometry")
            return;
          }
          let icon = {
            url: place.icon,
            size: new maps.Size(71, 71),
            origin: new maps.Point(0, 0),
            anchor: new maps.Point(17, 34),
            scaledSize: new maps.Size(25, 25)
          }
          // Create a marker for each place.
          markers.push(new maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }))
          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport)
          } else {
            bounds.extend(place.geometry.location)
          }
        });
        map.fitBounds(bounds)
      })
    }
  }
  render() {
    customConsoleLog("state: ", this.state)
    return (
      <div className={component}>
        <div className={header}>
          <h1> AGREGAR COMERCIO </h1>
        </div>
        <div className={divisor}></div>
        <form onSubmit={this.handleSubmit} noValidate autoComplete='off'>
          <div className={`${body} ${(this.state.displayErrors === true ? displayErrors : '')}`}>
            <div className={`${commerceDataDiv} ${firstComponent}`}>
              <div className={`${commerceDataTitle} ${firstTitle}`}>
                <h2>DATOS DEL COMERCIO</h2>
              </div>
              <div className={commerceMainDataDiv}>
                <div className={uploadBtnWrapper}>
                  <UploadBtn className={this.state.imageError ? specficError : ''}>
                    <img
                      src={this.state.image === '' ? iconCamera : this.state.image}
                      className={this.state.image === '' ? iconCameraClass : imagePreview}
                      alt="uploadPicture"
                    />
                    <input
                      className={inputFileHidden}
                      value={this.state.profileImgURI}
                      onChange={this.encodeImageFileAsURL}
                      name='profileImgURI'
                      type='file'
                      accept="image/*"
                      required
                    />
                  </UploadBtn>
                </div>
                <div className={nameInputDiv}>
                  <input
                    placeholder="Nombre del Comercio"
                    value={this.state.name}
                    onChange={this.handleInputChange}
                    name='name'
                    required
                  />
                </div>
                <div
                  className={`
                  ${commerceTypeContainer} 
                  ${dropDownDiv} 
                  ${firstComponent} 
                  ${DivDNI2}
                  ${this.state.displayErrors && this.state.commerceType === '' ? displayRedBorders : ''}
               `}>
                  <CustomDropdown2
                    name='commerceType'
                    value={this.state.commerceType}
                    onChange={this.handleInputChange}
                    required
                  >
                    <option value="physicalCommerce">Comercio físico</option>
                    <option value="digitalCommerce">Comercio digital</option>
                  </CustomDropdown2>
                  <div className={`${arrowContainer} ${firstComponent}`}>
                    <img src={arrowIcon} className={arrow} alt='arrow' />
                  </div>
                </div>
                {
                  this.state.commerceType === 'physicalCommerce'
                    ? <div className='d-flex'>
                      <div className={`
                  ${dropDownDiv} 
                  ${firstComponent} 
                  ${DivDNI}
                  ${this.state.displayErrors && this.state.idType === '' ? displayRedBorders : ''}
                  `}
                      >
                        {/* <span>C.I.&nbsp;</span> */}
                        <CustomDropdown
                          name='idType'
                          onChange={this.handleInputChange}
                          value={this.state.idType}
                        >
                          <option value="" defaultValue></option>
                          <option value="V-">V-</option>
                          <option value="E-">E-</option>
                          <option value="P-">P-</option>
                          <option value="G-">G-</option>
                          <option value="J-">J-</option>
                          <option value="C-">C-</option>
                        </CustomDropdown>
                        <div className={`${arrowContainer} ${firstComponent}`}>
                          <img src={arrowIcon} className={arrow} alt='arrow' />
                        </div>
                      </div>
                      <div className={rifInputDiv}>
                        <input
                          type="text"
                          placeholder="R.I.F"
                          value={this.state.idNumber}
                          onChange={this.handleInputChange}
                          name='idNumber'
                          required
                        />
                      </div>
                    </div>
                    : null
                }
                <div className={nameInputDiv}>
                  <input
                    placeholder="Razón Social"
                    value={this.state.corporateName}
                    onChange={this.handleInputChange}
                    name='corporateName'
                    required
                  />
                </div>
              </div>
              <div className={`${fullWidthInput}`}>
                <input
                  className={inputDescription}
                  placeholder="Descripción"
                  value={this.state.description}
                  onChange={this.handleInputChange}
                  name='description'
                  required
                />
              </div>
              {
                this.state.commerceType === 'physicalCommerce'
                  ? <div className={fullWidthInput}>
                    <input
                      className={inputDescription}
                      placeholder='Dirección Fiscal'
                      value={this.state.address}
                      onChange={this.handleInputChange}
                      name='address'
                      required
                    />
                  </div>
                  : null
              }
            </div>
            <input
              id="pac-input"
              type="text"
              placeholder="Buscar lugar"
              className={this.state.commerceType === 'physicalCommerce'
                ? "" : hideClass}
            />
            <div className={this.state.commerceType === 'physicalCommerce'
              ? mapContainer : hideClass}>
              <div style={{ height: '500px', width: '100%' }}>
                <GoogleMapReact
                  bootstrapURLKeys={{ key: googleApiKey }}
                  defaultCenter={this.state.defaultCenter}
                  defaultZoom={this.state.zoom}
                  onClick={this.addMarker}
                  yesIWantToUseGoogleMapApiInternals
                  onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
                  mapTypeId="roadmap"
                />
              </div>
              <div className={doubleInput}>
                <input
                  placeholder='Longitud del comercio'
                  type="text"
                  style={{ maxWidth: "200px" }}
                  value={this.state.longitude}
                  onChange={this.handleInputChange}
                  name='longitude'
                  readOnly={true}
                />
                <input
                  placeholder='Latitud del comercio'
                  type="text"
                  style={{ marginLeft: "4px", maxWidth: "200px" }}
                  value={this.state.latitude}
                  onChange={this.handleInputChange}
                  name='latitude'
                  readOnly={true}
                />
              </div>
            </div>
            <div className={commerceDataDiv}>
              <div className={commerceDataTitle}>
                <h2>DATOS DE CONTACTO</h2>
              </div>
              <div className={tripleInputDiv}>
                <div>
                  <input
                    placeholder="Nombre"
                    value={this.state.firstName}
                    onChange={this.handleInputChange}
                    name='firstName'
                    required
                  />
                </div>
                <div>
                  <input
                    placeholder="Apellido"
                    value={this.state.lastName}
                    onChange={this.handleInputChange}
                    name='lastName'
                    required
                  />
                </div>
                <div className='d-flex'>
                  <div className={`
                  ${dropDownDiv} 
                  ${firstComponent} 
                  ${DivDNI}
                  ${this.state.displayErrors && this.state.contactIdType === '' ? displayRedBorders : ''}
                  `}>
                    {/* <span>C.I.&nbsp;</span> */}
                    <CustomDropdown
                      name='contactIdType'
                      value={this.state.contactIdType}
                      onChange={this.handleInputChange}
                      required>
                      <option value="" defaultValue></option>
                      <option value="V-">V-</option>
                      <option value="E-">E-</option>
                      <option value="J-">J-</option>
                      <option value="G-">G-</option>
                    </CustomDropdown>
                    <div className={`${arrowContainer} ${firstComponent}`}>
                      <img src={arrowIcon} className={arrow} alt='arrow' />
                    </div>
                  </div>
                  <input
                    type="number"
                    placeholder="Cédula"
                    value={this.state.contactIdNumber}
                    onChange={this.handleInputChange}
                    name='contactIdNumber'
                    required
                  />
                </div>
              </div>
              <div className={tripleInputDiv}>
                <div>
                  <input
                    placeholder="Email"
                    type='email'
                    value={this.state.email}
                    onChange={this.handleInputChange}
                    name='email'
                    required
                  />
                </div>
                <div css={`position: relative`}>
                  <p
                    css={`
                      position: absolute;
                      font-weight: bold;
                      color: #8078ff;
                      left: -3px;
                      top: 10px;
                      `}
                  >+58</p>
                  <input
                    css={`
                    padding-left: 40px;
                    `}
                    placeholder="Telefono"
                    type='number'
                    value={this.state.phone}
                    onChange={this.handleInputChange}
                    name='phone'
                    required
                  />
                </div>
                <div
                  className={`${dropDownDiv} 
                ${firstComponent} 
                ${DivDNI2}
                ${this.state.displayErrors && this.state.category === '' ? displayRedBorders : ''}
                `}>
                  <CustomDropdown2
                    name='category'
                    value={this.state.category}
                    onChange={this.handleInputChange}
                    required
                  >
                    <option value="" defaultValue>Categoria</option>
                    {
                      categories.map(({ value, name }, i) => {
                        return <option key={i} value={value}>{name}</option>
                      })
                    }
                  </CustomDropdown2>
                  <div className={`${arrowContainer} ${firstComponent}`}>
                    <img src={arrowIcon} className={arrow} alt='arrow' />
                  </div>
                </div>
                {/* <div className={hiddenInputDiv}><input/></div> */}
              </div>
              <div className={tripleInputDiv}>
                <div>

                </div>

                <div
                  className={`${dropDownDiv} 
                ${firstComponent} 
                ${DivDNI2}
                ${this.state.displayErrors && this.state.city === '' ? displayRedBorders : ''}
                `}>
                  <CustomDropdown2
                    name='city'
                    value={this.state.city}
                    onChange={this.handleInputChange}
                    required
                  >
                    <option value="" defaultValue>Ciudad</option>
                    {
                      cities.map(({ value, name }, index) => {
                        return <option key={index} value={value}>{name}</option>
                      })
                    }
                  </CustomDropdown2>
                  <div className={`${arrowContainer} ${firstComponent}`}>
                    <img src={arrowIcon} className={arrow} alt='arrow' />
                  </div>
                </div>
                <div>

                </div>
              </div>
              <div className={addBankAccountFormContainer}>
                <AddBankAccountForm
                  bankAccount={this.state.bankAccountArray}
                  deleteBankAccount={this.deleteBankAccount}
                  addBankAccount={this.addBankAccount}
                  addCommerceAccountNumber={this.state.accountNumber}
                  errorMessage={this.state.errorMessage}
                  displayErrors={this.state.displayErrors}
                  loading={this.state.addAccountLoading}
                  addBankAccountModalIsOpen={this.state.modalIsOpen}
                  addBankAccountModalShow={this.modalShow}
                  deleteBankAccountModalIsOpen={this.state.deleteModalIsOpen}
                  deleteBankAccountModalShow={this.openDeleteModal}
                  deleteBankAccountModalClose={this.closeDeleteModal}
                  isAddCommerce={true}
                  onChange={this.onInputChange}
                  minWidth="280px"
                />
              </div>
            </div>
            <div className={`${commerceDataDiv} ${lastComponent}`}>
              <AcceptButton
                width="216px"
                height="36px"
                loading={this.state.loading}
                content="AGREGAR COMERCIO"
                type="submit"
              />
            </div>
          </div>
        </form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    accessToken: state.general.accessToken
  }
}
export default connect(mapStateToProps)(AddCommerce)