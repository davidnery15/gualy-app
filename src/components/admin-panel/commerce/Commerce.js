import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css } from 'emotion'
import { Redirect } from 'react-router'
import AddCommerce from './AddCommerce'
import UsersCardTable from '../users-card-table/UsersCardTable'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import Header from '../AdminHeader'
import { db } from './../../../firebase'
import R from '../../../utils/R'
import { timestampParser } from '../../../utils/timestampParser'
/* CSS */
const container = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
  `
const scrollDataRequestedRange = 10
class Commerce extends Component {
  state = ({
    commerces: [],
    loading: true,
    itemsIndex: 10,
    isDownloadSelectOpen: false,
    allUsers: "",
    allUsersLoading: false,
  })
  componentDidMount() {
    const usersRef = db.ref('users')
    usersRef
      .orderByChild('type')
      .equalTo('commerce')
      .limitToLast(this.state.itemsIndex)
      .once('value', (snapshot) => {
        const commerces = R.values(snapshot.val())
        this.setState({
          commerces: commerces.sort((a, b) =>
            (b.createdTimestamp || timestampParser(b.createdAt)) - (a.createdTimestamp || timestampParser(a.createdAt))),
          loading: false
        })
      })
  }
  databaseRequest = () => {
    customConsoleLog("databaseRequest commerces")
    this.setState({ itemsIndex: this.state.itemsIndex + scrollDataRequestedRange }, () => {
      const usersRef = db
        .ref('users')
        .orderByChild('type')
        .equalTo('commerce')
        .limitToLast(this.state.itemsIndex)
      usersRef.once('value', (snapshot) => {
        const commerces = R.values(snapshot.val())
        this.setState({
          commerces: commerces.sort((a, b) =>
            (b.createdTimestamp || timestampParser(b.createdAt)) - (a.createdTimestamp || timestampParser(a.createdAt))),
          loading: false
        })
      })
    })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      const { type } = this.props.userInCollectionData
      if (
        type === 'admin' ||
        type === 'support' ||
        type === 'marketing' ||
        type === 'cashier'
      ) {
        return null
      } else {
        return <Redirect to="/" />
      }
    } else {
      return <Redirect to="/" />
    }
  }
  openDownloadBySelect = () => {
    if (this.state.isDownloadSelectOpen) {
      this.setState({ isDownloadSelectOpen: false })
    } else {
      this.setState({ allUsersLoading: true }, () => {
        const commercesRef = db
          .ref('users')
          .orderByChild('type')
          .equalTo('commerce')
        commercesRef.once('value', (snapshot) => {
          const commerces = R.values(snapshot.val())
          this.setState({
            allUsers: commerces.sort((a, b) => (b.createdTimestamp || new Date(b.createdAt).getTime()) - (a.createdTimestamp || new Date(a.createdAt).getTime())),
            allUsersLoading: false,
            isDownloadSelectOpen: true,
          })
        })
      })
    }
  }
  render() {
    customConsoleLog("itemsIndex commerces: ", this.state.itemsIndex)
    customConsoleLog("commerces: ", this.state.commerces)
    return (
      <div>
        <Header />
        <div className={container}>
          <UsersCardTable
            users={Object.values(this.state.commerces)}
            title='COMERCIOS ACTIVOS'
            loading={this.state.loading}
            adminID={this.props.userInCollectionData.userKey}
            commerceType={true}
            showOrderByButton={false}
            showFilterByButton={false}
            store={false}
            userType={this.props.userInCollectionData.type}
            databaseRequest={this.databaseRequest}
            idAdmin={true}
            isDownloadSelectOpen={this.state.isDownloadSelectOpen}
            openDownloadBySelect={this.openDownloadBySelect}
            allUsers={this.state.allUsers}
            allUsersLoading={this.state.allUsersLoading}
          />
          {
            this.props.userInCollectionData.type !== 'cashier'
              ? <AddCommerce adminID={this.props.userInCollectionData.userKey} />
              : null
          }
          {/* <CommerceCardTable
            commerces={Object.values(this.state.commerces)}
            title='COMERCIOS POR APROBAR'
            loading={this.state.loading} /> */}
        </div>
        {
          this.redirect()
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(Commerce)