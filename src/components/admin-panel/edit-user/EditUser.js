import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css } from 'emotion'
import { Redirect } from 'react-router'
import EditUserForm from './EditUserForm'
// import UsersCardTable from './../../general/users-card-table/UsersCardTable'
// import CommerceCardTable from './commerce-card-table/CommerceCardTable'
import Header from '../AdminHeader'
// import { auth } from './../../../firebase'
import { db } from './../../../firebase'

/* CSS */
const container = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
  `
class EditUser extends Component {
    state = ({
        userInfo: [],
        commerceInfo: [],
        queryUserId: null,
        loading: true,
    })
    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const queryUserId = url.searchParams.get('uid')
        if (queryUserId) {
            const usersRef = db.ref('users')
            const commerceRef = db.ref('commerce')
            usersRef.child(queryUserId).on('value', (snapshot) => {
                this.setState({
                    userInfo: snapshot.val(),
                    queryUserId,
                    loading: false,
                })
            })
            commerceRef.child(queryUserId).on('value', (snapshot) => {
                this.setState({
                    commerceInfo: snapshot.val(),
                    queryUserId,
                    loading: false,
                })
            })
        }
    }
    redirect = () => {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const queryUserId = url.searchParams.get('uid')
        if (this.props.userInCollectionData && queryUserId) {
            const { type } = this.props.userInCollectionData
            if (type === 'admin' || type === 'security') {
                return null
            } else {
                return <Redirect to="/" />
            }
        } else {
            return <Redirect to="/" />
        }
    }
    render() {
        return (
            <div>
                <Header />
                <div className={container}>
                    <EditUserForm
                        userInfo={this.state.userInfo}
                        bankAccount={this.state.userInfo ? Object.values(this.state.userInfo.bankAccount || {}) : ''}
                        commerceInfo={this.state.commerceInfo}
                        loading={this.state.loading}
                        adminID={this.props.userInCollectionData ? this.props.userInCollectionData.userKey : ''} />
                    {/* <CommerceCardTable
            commerces={Object.values(this.state.commerces)}
            title='COMERCIOS POR APROBAR'
            loading={this.state.loading} /> */}
                </div>
                {
                    this.redirect()
                }
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        userInCollectionData: state.general.userInCollectionData,
        isLoggedIn: state.general.isLoggedIn
    }
}
export default connect(mapStateToProps)(EditUser)