import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css } from 'emotion'
// import axios from 'axios'
import styled from 'react-emotion'
import { ToastContainer, toast } from 'react-toastify'
import { style } from 'react-toastify'
import arrowIcon from './../../../assets/drop-down-arrow.svg'
import Spinner from 'react-spinkit'
import AcceptButton from '../../general/AcceptButton'
import GoBackButton from '../../general/goBackButton'
import { postRequest } from '../../../utils/createAxiosRequest'
import { cities } from '../../admin-panel/commerce/venezuelan-cities.json'
import { categories } from '../commerce/categories.json'
import { securityQuestionKeys } from './../../../constants/securityQuestionKeys'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import AddBankAccountForm from '../../general/AddBankAccountForm'
import { googleApiKey } from '../../../config/index'
import GoogleMapReact from 'google-map-react'
// Changing default tostify css 
style({
  colorProgressDefault: "#95fad8",
  fontFamily: "Montserrat",
});
/* CSS */
const component = css`
    margin: 50px;
    margin-bottom: 50px;
    position: relative;
    min-height: 500px;
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    @media (max-width: 1230px) {
      margin: 40px 25px;
    }
  `
const firstComponent = css`
    margin-top: 0 !important;
  `
const firstTitle = css`
    margin-bottom: 15px;
  `
const lastComponent = css`
    margin-top: 70px;
    margin-bottom: 70px;
    @media( max-width: 930px) {
      margin-bottom: 50px !important;
    }
    @media( max-width: 630px) {
      margin-bottom: 50px !important;
    }
  `
const header = css`
    margin: 0px;
    padding: 34px 25px;
    h1{
      margin: 0px;
      margin-left: 60px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const body = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 25px 32px;
    margin-bottom: 0px;
    input{
      font-size: 15.7px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.5;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      border-bottom: 1px solid #ffffff;
    }
    @media (max-width: 970px) {
      flex-direction: column;
      margin: 24px 25px;
      margin-bottom: 0px;
    }
  `

const commerceDataDiv = css`
    display: flex;
    justify-content: flex-start;
    margin-top: 70px;
    align-items: center;
    flex-direction: column;
    width: 100%;
    padding: 0 15%;
    position: relative;
    div{
      margin-top: 45px;
    }
    @media( max-width: 1024px) {
      padding: 0 8%;
    }
    @media( max-width: 930px) {
      div{

        margin-top: 15px;
      }
    }
     @media( max-width: 630px) {
      padding: 0;
      div{
        margin-top: 15px;
      }
    }
    @media (max-width: 970px) {
      margin-top: 50px;
    }
  `
const commerceDataTitle = css`
    position: absolute;
    width: 100%;
    margin-top: 0 !important;
    padding-left: 5%;
    h2{
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      text-align: left;
      color: #f6f7fa;
    }
    @media(max-width: 930px) {
      position: initial;
      padding-left: 0;
    }
  `
const commerceMainDataDiv = css`
    display:flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin-top: 0 !important;
    div {
      margin-top: 15px;
    }
  `
const UploadBtn = styled('button')`
    width: 139px;
    height: 139px;
    color: #242656;
    display: flex;
    justify-content: center;
    align-items: center;
    outline: none !important;
    cursor: pointer;
    background-color: #95fad8;
    border: none;
    border-radius: 72px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    position: relative;
    @media(max-width: 630px) {
      width: 72px;
      height: 72px;
    }
  `
const uploadBtnWrapper = css`
    margin-top: 0 !important;
  `
const nameInputDiv = css`
    width: 252.4px;
    height: 100%;
    input{
      text-align: center;
      font-size: 20px;
      font-weight: 900;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.18;
      letter-spacing: normal;
      text-align: center;
      color: #ffffff;
    }
    @media( max-width: 630px) {
      width: 100%;
    }
  `
const rifInputDiv = css`
    width: 160px;
    height: 100%;
    input{
      text-align: center;
    }
  `
const fullWidthInput = css`
    width: 100%;
    @media(max-width: 1024px) {
      margin-top: 45px !important;
    }
  `
const inputDescription = css`
    text-align: center !important;
    @media( max-width: 630px) {
      text-align: left !important;
    }
  `
// const hiddenInputDiv = css`
//   visibility: hidden;
//   @media( max-width: 670px) {
//     display: none;
//   }
// `
const doubleInputMaps = css`
    width: 100%;
    display: flex;
    justify-content: center;
    input{
      width: 46%;
    }
    @media( max-width: 530px) {
      margin-top: 0 !important;
      display: flex;
      flex-direction: column;
      align-items: center;
      input{
        width: 100%;
        margin-top: 15px;
      }
    }
  `
const tripleInputDiv = css`
    width: 100%;
    display: flex;
    justify-content: space-between;
    div{
      width: 30%;
      input{
        width: 100%;
      }
    }
    @media( max-width: 670px) {
      margin-top: 0 !important;
      display: flex;
      flex-direction: column;
      div{
        width: 100%;
        margin-top: 15px;
      }
    }
  `
const dropDownDiv = css`
    position: relative;
    display: flex;
    align-items:center;
  `
const CustomDropdown = styled('select')`
    height: 43.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    outline: none !important;
    option{
      background-color: #242657;
    }
  `
const CustomDropdown2 = styled('select')`
height: 43.3px;
width:100%;
padding: 7px 4px;
background: transparent;
border: none;
cursor: pointer;
font-size: 15px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.68;
letter-spacing: normal;
text-align: left;
color: #ffffff;
appearance: none; 
outline: none !important;
option{
  background-color: #242657;
  color: #ffffff;
}
  `
const arrowContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    pointer-events: none;
    margin-top: 0 !important;
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const DivDNI = css`
    width: 40px !important;
    border-bottom: 1px solid white;
    // margin-right: 15px;
    position: relative;
    min-width: 55px;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const DivDNI2 = css`
    border-bottom: 1px solid white;
    // margin-right: 15px;
    position: relative;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const imagePreview = css`
    width: 100%;
    height: 100%;
    position: absolute;
    cursor: pointer;
    border-radius: 72px;
    object-fit: cover;
    border: 2px solid white;
  `
const displayErrors = css`
    input:invalid {
      border-color:red;
    }
    input:invalid specficError 
  `
const specficError = css`
    border: 3px solid red;
  `
const spinnerContainer = css`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
`
const goBackButtonClass = css`
  top: 34px;
  left: 34px;
  position: absolute
  `
const dropdownQuestionPadding = css`
padding-right: 15px;
@media(max-width: 500px){
  option{
    font-size: 8px;
  }
}
`
const clientDirectionContainer = css`
width: 100%;
input{
  margin: auto;
  width: 100%;
}
`
const mapContainer = css`
margin-top: 30px; 
width: 90%;
div {
  margin-top: 0 !important
}
@media( max-width: 360px) {
  width: 100%;
}
`
const hideClass = css`
display: none;
`
class EditUserForm extends Component {
  state = ({
    image: '',
    profileImgURI: '',
    visibility: false,
    displayErrors: false,
    imageError: false,
    questionKeyError: false,
    loading: false,
    modalShow: false,
    modalLoading: false,
    //form
    //commerce and client
    phone: '',
    address: '',
    category: '',
    idType: '',
    idNumber: '',
    name: '',
    profilePicture: '',
    firstName: '',
    lastName: '',
    email: '',
    questionKey: '',
    securityAnswer: '',
    //commerce
    corporateName: '',
    contactIdType: '',
    contactIdNumber: '',
    city: '',
    commerceLongitude: '',
    commerceLatitude: '',
    description: '',
    //accountNumbers
    modalIsOpen: false,
    deleteModalIsOpen: false,
    accountNumber: '',
    isLoading: false,
    errorMessage: '',
    selectedBankAccount: null,
    //GOOGLE MAPS
    defaultCenter: {
      lat: 10.653860,
      lng: -71.645966
    },
    zoom: 11,
  })

  handleSubmit = async (event) => {
    event.preventDefault();
    // const vePhoneNumber = `+58${this.state.phone}`
    this.setState({
      displayErrors: false,
      loading: true
    })
    this.toastLoaderId =
      toast(this.props.userInfo.type === 'commerce'
        ? 'Guardando comercio'
        : 'Guardando cliente',
        { autoClose: false })
    const data = {
      description: this.state.description ? this.state.description : "",
      email: this.state.email,
      firstName: this.state.firstName ? this.state.firstName : "",
      lastName: this.state.lastName ? this.state.lastName : "",
      dni: {
        type: this.state.idType ? this.state.idType : "",
        id: this.state.idNumber ? this.state.idNumber : "",
      },
      phone: this.state.phone ? `+58${this.state.phone}` : "",
      // type: string(opcional),
      address: this.state.address ? this.state.address : "",
      userKey: this.props.userInfo.userKey, //required
      contactIdType: this.state.contactIdType ? this.state.contactIdType : "",
      contactIdNumber: this.state.contactIdNumber ? this.state.contactIdNumber : "",
      corporateName: this.state.corporateName ? this.state.corporateName : "",
      name: this.state.name ? this.state.name : "",
      profilePicture: this.state.image ? this.state.image : "",
      category: this.state.category ? this.state.category : "",
      city: this.state.city ? this.state.city : "",
      commerceLatitude: this.state.commerceLatitude ? this.state.commerceLatitude : "",
      commerceLongitude: this.state.commerceLongitude ? this.state.commerceLongitude : "",
      adminID: this.props.adminID, //required
      questionKey: this.state.questionKey ? this.state.questionKey : "",
      securityAnswer: this.state.securityAnswer ? this.state.securityAnswer : ""
    }
    customConsoleLog("SENDED: ", data)
    try {
      this.setState({
        loading: false,
        image: ''
      })
      let resp = await postRequest('users/updateUserInfoByAdmin', data)
      customConsoleLog("RESPONSE: ", resp)
      if (resp.data.success) {
        this.setState({
          description: '',
          email: '',
          firstName: '',
          lastName: '',
          idType: '',
          idNumber: '',
          phone: '',
          address: '',
          contactIdType: '',
          contactIdNumber: '',
          corporateName: '',
          profilePicture: '',
          category: '',
          city: '',
          commerceLatitude: '',
          commerceLongitude: '',
          securityAnswer: '',
          questionKey: '',
          name: '',
        })
        toast.update(this.toastLoaderId, {
          render: this.props.userInfo.type === 'commerce'
            ? 'Comercio guardado Exitosamente.'
            : 'Cliente guardado Exitosamente.',
          autoClose: 5000
        })
      } else {
        return toast.update(this.toastLoaderId, {
          render: resp.data.error.message,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } catch (error) {
      this.setState({
        loading: false
      })
      toast.update(this.toastLoaderId, {
        render: "Verifique su conexión y vuelva a intentarlo.",
        type: toast.TYPE.ERROR,
        autoClose: 5000
      })
    }
  }
  getOptions = items => {
    const questions = Object.values(this.props.userInfo.questions || {})[0]
    return items.map(item =>
      questions
        ? item.questionKey !== questions.questionKey
          ? <option key={item.customId} value={item.questionKey}>{item.text}</option>
          : ""
        : <option key={item.customId} value={item.questionKey}>{item.text}</option>
    )
  }
  handlePasswordVisibility = (e) => {
    this.setState({
      passwordVisibility: !this.state.passwordVisibility
    })
  }
  onChangeQuestionError = () => {
    this.setState({
      questionKeyError: false
    })
  }
  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0];
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            image: reader.result,
            imageError: false
          })
        }
        reader.readAsDataURL(file);
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }
  //HANDLE INPUTS
  onInputChange = ({ target }) => {
    const value = target.name === 'accountNumber' &&
      target.value.length > 20
      ? target.value.slice(0, -1)
      : target.name === 'phone'
        ? target.value.length < 11
          ? target.value !== '0'
            ? target.value
            : target.value.slice(0, -1)
          : target.value.slice(0, -1)
        : target.value
    this.setState({
      [target.name]: value
    })
  }
  addMarker = (location) => {
    // console.log("location: ", location)
    this.setState({ commerceLatitude: location.lat, commerceLongitude: location.lng })
  }
  handleApiLoaded = (map, maps) => {
    // use map and maps objects
    // Create the search box and link it to the UI element.
    let input = document.getElementById('pac-input');
    let searchBox = maps && maps.places ? new maps.places.SearchBox(input) : null
    map.controls[maps.ControlPosition.TOP_LEFT].push(input);
    let marker = new maps.Marker(
      {
        draggable: true,
        position: {
          lat: this.props.userInfo.commerceLatitude
            ? this.props.userInfo.commerceLatitude
            : this.state.commerceLatitude,
          lng: this.props.userInfo.commerceLongitude
            ? this.props.userInfo.commerceLongitude
            : this.state.commerSceLongitude
        },
        animation: maps.Animation.DROP,
      }
    );
    marker.setMap(map)
    // Zoom to 9 when clicking on marker
    // maps.event.addListener(marker, 'click', () => {
    //   map.setZoom(15)
    // })
    map.addListener('click', function (e) {
      marker.setPosition(e.latLng)
    })
    if (searchBox) {
      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds())
      })
      let markers = []
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', function () {
        let places = searchBox.getPlaces()
        if (places.length === 0) {
          return;
        }
        // Clear out the old markers.
        markers.forEach(function (marker) {
          marker.setMap(null);
        });
        markers = [];
        // For each place, get the icon, name and location.
        let bounds = new maps.LatLngBounds()
        places.forEach(function (place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry")
            return;
          }
          let icon = {
            url: place.icon,
            size: new maps.Size(71, 71),
            origin: new maps.Point(0, 0),
            anchor: new maps.Point(17, 34),
            scaledSize: new maps.Size(25, 25)
          }
          // Create a marker for each place.
          markers.push(new maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }))
          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport)
          } else {
            bounds.extend(place.geometry.location)
          }
        });
        map.fitBounds(bounds)
      })
    }
  }
  render() {
    customConsoleLog("defaultCenter: ", this.state.defaultCenter)
    customConsoleLog("this.props.userInfo: ", this.props.userInfo)
    const questions = this.props.userInfo ? Object.values(this.props.userInfo.questions || {})[0] : ''
    const defaultCenter = this.props.userInfo.commerceLatitude &&
      this.props.userInfo.commerceLongitude
      ? {
        lat: this.props.userInfo.commerceLatitude,
        lng: this.props.userInfo.commerceLongitude
      }
      : this.state.defaultCenter
    return (
      <div className={component}>
        {
          this.props.userInfo
            ? <GoBackButton
              height="21px"
              width="21px"
              route={this.props.userInfo.type === 'commerce' ? '/comercios' : '/usuarios'}
              style={goBackButtonClass}
            />
            : null
        }
        <div className={header}>
          <h1>{this.props.userInfo ? `EDITAR ${this.props.userInfo.type === 'commerce' ? "COMERCIO" : "CLIENTE"}` : 'USUARIO NO ENCONTRADO'} </h1>
        </div>
        <div className={divisor}></div>
        {
          this.props.userInfo
            ? this.props.loading
              ? <div className={spinnerContainer}> <Spinner color='white' /> </div>
              : <form onSubmit={this.handleSubmit} noValidate autoComplete='off'>
                <div className={`${body} ${(this.state.displayErrors === true ? displayErrors : '')}`}>
                  {
                    this.props.userInfo.type === 'commerce' && this.props.userInfo.userKey && this.props.commerceInfo.commerceKey ?
                      <div>
                        <div className={`${commerceDataDiv} ${firstComponent}`}>
                          <div className={`${commerceDataTitle} ${firstTitle}`}>
                            <h2>DATOS DEL COMERCIO</h2>
                          </div>
                          <div className={commerceMainDataDiv}>
                            <div className={uploadBtnWrapper}>
                              <UploadBtn className={this.state.imageError ? specficError : ''}>
                                <img
                                  src={this.state.image === ''
                                    ? this.props.userInfo.profilePicture
                                    : this.state.image}
                                  className={imagePreview}
                                  alt="uploadPicture"
                                />
                                <input
                                  className={inputFileHidden}
                                  value={this.state.profileImgURI}
                                  name='profileImgURI'
                                  type='file'
                                  accept="image/*"
                                  onChange={this.encodeImageFileAsURL}
                                />
                              </UploadBtn>
                            </div>
                            <div className={nameInputDiv}>
                              Nombre:
                      <input
                                placeholder={this.props.userInfo.name}
                                value={this.state.name}
                                onChange={this.onInputChange}
                                name='name'
                              />
                            </div>
                            {
                              (this.props.userInfo.commerceType === 'physicalCommerce' ||
                                !this.props.userInfo.commerceType)
                                ? <div className='d-flex'>
                                  <div className={`${dropDownDiv} ${firstComponent} ${DivDNI}`}>
                                    <CustomDropdown
                                      name='idType'
                                      value={this.state.idType}
                                      onChange={this.onInputChange}
                                    >
                                      <option value={this.props.userInfo.dni.type}
                                        defaultValue>{this.props.userInfo.dni.type}</option>
                                      {this.props.userInfo.dni.type !== "E-" ? <option value="E-">E-</option> : null}
                                      {this.props.userInfo.dni.type !== "V-" ? <option value="V-">V-</option> : null}
                                      {this.props.userInfo.dni.type !== "J-" ? <option value="J-">J-</option> : null}
                                      {this.props.userInfo.dni.type !== "G-" ? <option value="G-">G-</option> : null}
                                    </CustomDropdown>
                                    <div className={`${arrowContainer} ${firstComponent}`}>
                                      <img src={arrowIcon} className={arrow} alt='arrow' />
                                    </div>
                                  </div>
                                  <div className={rifInputDiv}>
                                    Rif:
                    <input
                                      type="text"
                                      placeholder={this.props.userInfo.dni.id}
                                      value={this.state.idNumber}
                                      onChange={this.onInputChange}
                                      name='idNumber'
                                    />
                                  </div>
                                </div>
                                : null
                            }
                            <div className={nameInputDiv}>
                              Razon social:
                  <input
                                placeholder={this.props.commerceInfo.corporateName}
                                value={this.state.corporateName}
                                onChange={this.onInputChange}
                                name='corporateName'
                              />
                            </div>
                          </div>
                          <div className={`${fullWidthInput}`}>
                            Descripcion:
                    <input
                              className={inputDescription}
                              placeholder={this.props.userInfo.description}
                              value={this.state.description}
                              onChange={this.onInputChange}
                              name='description'
                            />
                          </div>
                          <div className={`${fullWidthInput}`}>
                            Dirección Fiscal:
                    <input
                              placeholder={this.props.userInfo.address}
                              value={this.state.address}
                              onChange={this.onInputChange}
                              name='address'
                            />
                          </div>
                          <input
                            id="pac-input"
                            type="text"
                            placeholder="Buscar lugar"
                            className={(this.props.userInfo.commerceType === 'physicalCommerce' ||
                              !this.props.userInfo.commerceType)
                              ? ""
                              : hideClass}
                          />
                          <div className={(this.props.userInfo.commerceType === 'physicalCommerce' ||
                            !this.props.userInfo.commerceType)
                            ? mapContainer
                            : hideClass}>
                            <div style={{ height: '500px', width: '100%' }}>
                              <GoogleMapReact
                                bootstrapURLKeys={{ key: googleApiKey }}
                                defaultCenter={defaultCenter}
                                defaultZoom={this.state.zoom}
                                onClick={this.addMarker}
                                yesIWantToUseGoogleMapApiInternals
                                onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
                                mapTypeId="roadmap"
                              />
                            </div>
                            <div className={doubleInputMaps}>
                              <input
                                placeholder={this.props.userInfo.commerceLongitude
                                  ? this.props.userInfo.commerceLongitude
                                  : 'Longitud del comercio'}
                                type="text"
                                style={{ maxWidth: "200px" }}
                                value={this.state.commerceLongitude}
                                onChange={this.onInputChange}
                                name='commerceLongitude'
                                readOnly={true}
                              />
                              <input
                                placeholder={this.props.userInfo.commerceLatitude
                                  ? this.props.userInfo.commerceLatitude
                                  : 'Latitud del comercio'}
                                type="text"
                                style={{ marginLeft: "4px", maxWidth: "200px" }}
                                value={this.state.commerceLatitude}
                                onChange={this.onInputChange}
                                name='commerceLatitude'
                                readOnly={true}
                              />
                            </div>
                          </div>
                        </div>
                        <div className={commerceDataDiv}>
                          <div className={commerceDataTitle}>
                            <h2>DATOS DE CONTACTO</h2>
                          </div>
                          <div className={tripleInputDiv}>
                            <div>
                              Nombre:
                      <input
                                placeholder={this.props.userInfo.firstName}
                                value={this.state.firstName}
                                onChange={this.onInputChange}
                                name='firstName'

                              />
                            </div>
                            <div>
                              Apellido:
                      <input
                                placeholder={this.props.userInfo.lastName}
                                value={this.state.lastName}
                                onChange={this.onInputChange}
                                name='lastName'

                              />
                            </div>
                            <div className='d-flex'>
                              <div className={`${dropDownDiv} ${firstComponent} ${DivDNI}`}>
                                {/* <span>C.I.&nbsp;</span> */}
                                <CustomDropdown
                                  name='contactIdType'
                                  value={this.state.contactIdType}
                                  onChange={this.onInputChange}
                                >
                                  <option value={this.props.commerceInfo.contactData.contactId.contactIdType} defaultValue>{this.props.commerceInfo.contactData.contactId.contactIdType}</option>
                                  {this.props.commerceInfo.contactData.contactId.contactIdType !== "E-" ? <option value="E-">E-</option> : null}
                                  {this.props.commerceInfo.contactData.contactId.contactIdType !== "V-" ? <option value="V-">V-</option> : null}
                                  {this.props.commerceInfo.contactData.contactId.contactIdType !== "J-" ? <option value="J-">J-</option> : null}
                                  {this.props.commerceInfo.contactData.contactId.contactIdType !== "G-" ? <option value="G-">G-</option> : null}
                                </CustomDropdown>
                                <div className={`${arrowContainer} ${firstComponent}`}>
                                  <img src={arrowIcon} className={arrow} alt='arrow' />
                                </div>
                              </div>
                              <input
                                type="number"
                                placeholder={this.props.commerceInfo.contactData.contactId.contactIdNumber}
                                value={this.state.contactIdNumber}
                                onChange={this.onInputChange}
                                name='contactIdNumber'
                                pattern='^\d{7,8}$'
                              />
                            </div>
                          </div>
                          <div className={tripleInputDiv}>
                            <div>
                              Email:
                      <input
                                placeholder={this.props.userInfo.email}
                                value={this.state.email}
                                onChange={this.onInputChange}
                                type='email'
                                name='email'
                              />
                            </div>
                            <div css={`position: relative`}>
                              Telefono:
                            <p
                                css={`
                      position: absolute;
                      font-weight: bold;
                      color: #8078ff;
                      left: 0;
                      top: 33px;
                      `}
                              >+58</p>
                              <input
                                css={`
                              padding-left: 40px;
                              `}
                                placeholder={this.props.userInfo.phone.substring(3)}
                                type='tel'
                                value={this.state.phone}
                                onChange={this.onInputChange}
                                name='phone'
                              />
                            </div>
                            <div className={`${dropDownDiv} ${firstComponent} ${DivDNI2}`}>
                              <CustomDropdown2
                                name='category'
                                value={this.state.category}
                                onChange={this.onInputChange}>
                                <option value=""
                                  defaultValue>
                                  {this.props.userInfo.category
                                    ? `${categories.filter(category => category.value === this.props.userInfo.category)[0].name} (Actual)`
                                    : "Sin categoria"}
                                </option>
                                {
                                  categories.map((category, i) => <option key={i} value={category.value}>{category.name}</option>)
                                }
                              </CustomDropdown2>
                              <div className={`${arrowContainer} ${firstComponent}`}>
                                <img src={arrowIcon} className={arrow} alt='arrow' />
                              </div>
                            </div>
                          </div>
                          <div className={tripleInputDiv}>
                            <div className={`${dropDownDiv} ${firstComponent} ${DivDNI2}`}>
                              <CustomDropdown2
                                name='city'
                                value={this.state.city}
                                onChange={this.onInputChange}
                              >
                                <option
                                  value=""
                                  defaultValue>
                                  {`${this.props.userInfo.city} (Actual)` || "Sin ciudad de origen"}
                                </option>
                                {
                                  cities.map((city, i) => <option key={i} value={city.value}>{city.name}</option>)
                                }
                              </CustomDropdown2>
                              <div className={`${arrowContainer} ${firstComponent}`}>
                                <img src={arrowIcon} className={arrow} alt='arrow' />
                              </div>
                            </div>
                            <div className={`${dropDownDiv} ${firstComponent} ${DivDNI2}`}>
                              <CustomDropdown
                                className={dropdownQuestionPadding}
                                value={this.state.questionKey}
                                name='questionKey'
                                onChange={this.onInputChange}
                              >
                                <option value={
                                  questions
                                    ? questions.questionKey
                                    : ""}
                                  defaultValue
                                  selected
                                >
                                  {
                                    questions ?
                                      securityQuestionKeys.map(item =>
                                        item.questionKey === questions.questionKey
                                          ? `${item.text} (Actual)` : '')
                                      : ""
                                  }
                                </option>
                                {
                                  this.getOptions(securityQuestionKeys)
                                }
                              </CustomDropdown>
                              <div className={arrowContainer}>
                                <img src={arrowIcon} className={arrow} alt="dropdown" />
                              </div>
                            </div>
                            <div>
                              <input
                                placeholder={this.props.userInfo.questions &&
                                  questions.answer
                                  ? questions.answer
                                  : "Respuesta secreta"}
                                value={this.state.securityAnswer}
                                onChange={this.onInputChange}
                                name='securityAnswer'
                                type='text'
                              />
                            </div>
                          </div>
                        </div>
                        <AddBankAccountForm
                          bankAccount={this.props.bankAccount}
                          userInfo={this.props.userInfo}
                        />
                      </div>
                      : null
                  }
                  {
                    this.props.userInfo.type === 'client' ?
                      <div>
                        <div className={`${commerceDataDiv} ${firstComponent}`}>
                          <div className={`${commerceDataTitle} ${firstTitle}`}>
                            <h2>DATOS DEL CLIENTE</h2>
                          </div>
                          <div className={commerceMainDataDiv}>
                            <div className={uploadBtnWrapper}>
                              <UploadBtn className={this.state.imageError ? specficError : ''}>
                                <img
                                  src={this.state.image === '' ? this.props.userInfo.profilePicture : this.state.image}
                                  className={imagePreview}
                                  alt="uploadPicture"
                                />
                                <input
                                  className={inputFileHidden}
                                  value={this.state.profileImgURI}
                                  name='profileImgURI'
                                  type='file'
                                  accept="image/*"
                                  onChange={this.encodeImageFileAsURL}
                                />
                              </UploadBtn>
                            </div>
                          </div>
                          <div className={clientDirectionContainer}>
                            <input
                              placeholder={this.props.userInfo.address
                                ? this.props.userInfo.address
                                : "El cliente no tiene direccion"}
                              value={this.state.address}
                              onChange={this.onInputChange}
                              name='address'
                            />
                          </div>
                        </div>
                        <div className={commerceDataDiv}>
                          <div className={commerceDataTitle}>
                            <h2>DATOS DE CONTACTO</h2>
                          </div>
                          <div className={tripleInputDiv}>
                            <div>
                              <input
                                placeholder={this.props.userInfo.firstName}
                                value={this.state.firstName}
                                onChange={this.onInputChange}
                                name='firstName'
                                pattern='(^[A-Za-záéíóúñ]+\s*([A-Za-záéíóúñ]+)?)$'
                                type='text'
                              />
                            </div>
                            <div>
                              <input
                                placeholder={this.props.userInfo.lastName}
                                value={this.state.lastName}
                                onChange={this.onInputChange}
                                name='lastName'
                                pattern='(^[A-Za-záéíóúñ]+\s*([A-Za-záéíóúñ]+)?)$'
                                type='text'
                              />
                            </div>
                            <div className='d-flex'>
                              <div className={`${dropDownDiv} ${firstComponent} ${DivDNI}`}>
                                {/* <span>C.I.&nbsp;</span> */}
                                <CustomDropdown
                                  value={this.state.idType}
                                  onChange={this.onInputChange}
                                  name='idType'
                                >
                                  <option
                                    value={this.props.userInfo.dni.type}
                                    defaultValue>
                                    {this.props.userInfo.dni.type}
                                  </option>
                                  {this.props.userInfo.dni.type !== "E-" ? <option value="E-">E-</option> : null}
                                  {this.props.userInfo.dni.type !== "V-" ? <option value="V-">V-</option> : null}
                                  {this.props.userInfo.dni.type !== "J-" ? <option value="J-">J-</option> : null}
                                  {this.props.userInfo.dni.type !== "G-" ? <option value="G-">G-</option> : null}
                                </CustomDropdown>
                                <div className={`${arrowContainer} ${firstComponent}`}>
                                  <img src={arrowIcon} className={arrow} alt='arrow' />
                                </div>
                              </div>
                              <input
                                type="number"
                                placeholder={this.props.userInfo.dni.id}
                                value={this.state.idNumber}
                                onChange={this.onInputChange}
                                name='idNumber'
                                pattern='^\d{7,8}$'
                              />
                            </div>
                          </div>
                          <div className={tripleInputDiv}>
                            <div>
                              <input
                                placeholder={this.props.userInfo.email}
                                value={this.state.email}
                                onChange={this.onInputChange}
                                type='email'
                                name='email'
                              />
                            </div>
                            <div css={`position: relative`}>
                              <p
                                css={`
                      position: absolute;
                      font-weight: bold;
                      color: #8078ff;
                      left: -3px;
                      top: 10px;
                      `}
                              >+58</p>
                              <input
                                css={`
                        padding-left: 40px;
                        `}
                                placeholder={this.props.userInfo.phone.substring(3)}
                                name='phone'
                                type="number"
                                onChange={this.onInputChange}
                                value={this.state.phone}
                              />
                              <span className='highlight'></span>
                              <span className='bar'></span>
                            </div>
                            <div className={`${dropDownDiv} ${firstComponent} ${DivDNI2}`}>
                              <CustomDropdown
                                className={dropdownQuestionPadding}
                                value={this.state.questionKey}
                                name='questionKey'
                                onChange={this.onInputChange}
                              >
                                <option value="" defaultValue>
                                  {
                                    questions
                                      ? securityQuestionKeys.map(item =>
                                        item.questionKey === questions.questionKey
                                          ? `${item.text} (Actual)`
                                          : '')
                                      : ""
                                  }
                                </option>
                                {
                                  this.getOptions(securityQuestionKeys)
                                }
                              </CustomDropdown>
                              <div className={arrowContainer}>
                                <img src={arrowIcon} className={arrow} alt="dropdown" />
                              </div>
                            </div>
                          </div>
                          <div className={tripleInputDiv}>
                            <div />
                            <div>
                              <input
                                placeholder="Respuesta secreta"
                                value={this.state.securityAnswer}
                                onChange={this.onInputChange}
                                name='securityAnswer'
                                pattern='(^[A-Za-záéíóúñ]+\s*([A-Za-záéíóúñ]+)?)$'
                                type='text'
                              />
                            </div>
                            <div />
                          </div>
                        </div>
                        <AddBankAccountForm
                          bankAccount={this.props.bankAccount}
                          userInfo={this.props.userInfo}
                        />
                      </div>
                      : null
                  }
                </div>
                {
                  this.state.description ||
                    this.state.email ||
                    this.state.firstName ||
                    this.state.lastName ||
                    this.state.idType ||
                    this.state.idNumber ||
                    this.state.phone ||
                    this.state.address ||
                    this.state.contactIdType ||
                    this.state.contactIdNumber ||
                    this.state.corporateName ||
                    this.state.name ||
                    this.state.image ||
                    this.state.category ||
                    this.state.city ||
                    this.state.commerceLatitude ||
                    this.state.commerceLongitude ||
                    this.state.questionKey ||
                    this.state.securityAnswer ?
                    <div className={`${commerceDataDiv} ${lastComponent}`}>
                      <AcceptButton
                        height="36px"
                        width="300px"
                        loading={this.state.loading}
                        content={this.props.userInfo ? this.props.userInfo.type === 'commerce' ? "GUARDAR COMERCIO" : "GUARDAR" : ''}
                        type="submit"
                      />
                    </div>
                    : null
                }
              </form>
            : null
        }

        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    accessToken: state.general.accessToken
  }
}
export default connect(mapStateToProps)(EditUserForm)