import React, { Component } from 'react'
import { Card, Button, CardHeader, CardBody, Col, Row } from 'reactstrap'
import { css } from 'emotion'
import firebase from '../../firebase';

const confirmEmailCard = css`
  width:  400px;
  max-width: 400px;
  height: 400px;
  border-radius: 5px;
  margin-top: 72px;
`

const confirmEmailTitleCard = css`
  font-size: 45px;
  font-weight: 900;
  line-height: 1.2;
`

const confirmEmailBody = css`
  font-family: Montserrat;
  font-size: 16px;
  font-weight: 300;
  text-align: center;
  color: #ffffff;
`

const hSeparator = css`
  width: 270px;
  min-height: 1px;
  opacity: 0.5;
  background-color: #1d1f4a;
  align-self: center;
  margin-bottom: 0;
  margin-top: 10px;
`

export default class ConfirmEmail extends Component {

  itemsRef = firebase.database().ref('/users');

  goHome = () => {
    this.props.history.push('/')
    // this.itemsRef.on('value', (snapshot) => { console.log(snapshot.val()) } )
  }

  render() {
    return (
      <Row>
        <Col className='justify-content-center d-flex'>
          <Card className={`${confirmEmailCard} box-shadow`}>
            <CardHeader className={`p-4 text-center mb-2`} css={`border-bottom: 0px solid transparent !important; margin-top:65px;`}>
              <span className={confirmEmailTitleCard}>Bienvenido</span>
            </CardHeader>
            <div className={hSeparator} />
            <CardBody className={`pl-5 pr-5 pt-0 justify-content-center d-flex flex-column`}>
              <div className='ml-3 mr-3 justify-content-center d-flex flex-column mt-0 align-items-center'>
                <div className={confirmEmailBody}>
                  Tu correo electrónico ha sido verificado exitosamente.
                  <br/>
                  {this.props.match.params.id}
                </div>
              </div>
              <Button color='primary' size='lg' className='mt-4' onClick={this.goHome}>USAR GUALY</Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}
