import React from 'react'
import { NavLink, Route, Link } from 'react-router-dom'
import { Navbar, Nav, NavItem } from 'reactstrap'
import { css } from 'emotion'
import { userLogout } from '../../redux/actions/general'
import { connect } from 'react-redux'
// import ProfileIcon from '../general/ProfileIcon/ProfileIcon'
import logo from '../../assets/logos/logo-gualy-dark-bg.png'
// import assessment from '../../assets/ic_assessment.svg'

const gualyNavbar = css`
    height: 100%;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    color: white;
    transition: all 0.2s;
    padding: 0 !important;
  `
const navbarBrandImage = css`
    margin: 0;

    img {
      height: 50px;
      width: 140px;
      margin: 0 55px;
    }

    @media (max-width: 500px) {
      & {
        img {
          width: 106.7px;
          height: 37px;
          margin: 0 0 0 15px;
        }
      }
    }
  `
const anchorLink = css`
    padding: 35px 10px;
    color: white;
    font-weight: 300;
    font-size: 14px;
    letter-spacing: 1.6px;
    outline: transparent;
    cursor:pointer;
    &:active {
      color: white;
      text-decoration: none;
    }
    &:visited {
      color: white;
      text-decoration: none;
    }
    &:hover {
      color: white;
      text-decoration: none;
    }

    @media (max-width: 1253px) {
      & {
        padding: 35px 20px;
      }
    }

    @media (max-width: 835px) {
      & {
        padding: 21px;
      }
    }
  `
const activeLink = css`
    width: 100%;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold !important;
    letter-spacing: 1.2px;
    color: #8078ff !important;
    fill: #8078ff;
  `
const activeLinkText = css`
  display: none;
  @media (max-width: 1360px) {
    display: inline;
  }
  `
const flexLi = css`
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.1s ease-in-out;
    border-bottom: solid 4px transparent;
  `
const navItemLogoSVG = css`
    @media (max-width: 1284px) {
      transition: all 0.5s;
    }
  `
const navItemLogo = css`
    fill: currentColor; 
  `
// const imgIcon = css`
//   width: 18px;
//   height: 20px;
// `
const navItemText = css`
    @media (max-width: 1500px) {
      display: none;
    }
  `
const leftSeparator = css`
    width: 1.2px;
    height: 100px;
    opacity: 0.5;
    background-color: #1d1f4a;
    margin-right: 30px;
    padding: 0;

    @media (max-width: 1010px) {
      display: none;
      transition: all 0.5s;
    }
  `

const rightSeparator = css`
    width: 1.2px;
    height: 100%;
    opacity: 0.5;
    margin-right: 30px;
    @media (max-width: 386px) {
      margin-right: 10px;
      transition: all 0.5s;
    }
    @media (max-width: 330px) {
      margin-right: 2px;
      transition: all 0.5s;
    }
  `
const horizontalSeparator = css`
    display: none;
    
    @media (max-width: 1010px) {
      width: 100%;
      height: 1.2px;
      opacity: 0.5;
      display: inline
      transition: all 0.5s;
      border-top: solid 1px #292929;
    }
  `

const NavItemContent = css`
    display: flex;
    justify-content: center;
    align-items: center;
  `

const navItemLogoSVGMargin = css`
    margin-right: 5px;
  `


const ActiveNavItem = ({ children, to, title, ...props }) => (
  <Route
    path={to}
    {...props}
    children={({ match }) => (
      <NavItem className={`${flexLi} ${match ? css`border-bottom: solid 4px #8078ff;` : ""}`}>
        <Link className={`${anchorLink} ${match ? activeLink : ""}`} to={to}>
          <div className={NavItemContent}>
            {children}
            {match ? <span className={activeLinkText}>{title}</span> : null}
            <span className={navItemText}>{title}</span>
          </div>
        </Link>
      </NavItem>
    )}
  />
);

const NormalNavbar = ({ type }) => {
  switch (type) {
    case 'admin':
      return (
        <Nav className='mr-auto' css={`
          display: flex;
          padding-top: 10px !important;
          
          @media (max-width: 1010px) {display: none;}`} >
          <ActiveNavItem to='/dashboard-admin' title="INICIO">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M5.91 10.75h4.73v7.093h5.91V8.386L8.275 0 0 8.386v9.457h5.91z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios' title="COMERCIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios' title="USUARIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/transacciones' title="TRANSACCIONES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M0 10.8h3.6V7.2H0v3.6zM0 18h3.6v-3.6H0V18zM0 3.6h3.6V0H0v3.6zm7.2 7.2h15.53V7.2H7.2v3.6zm0 7.2h15.53v-3.6H7.2V18zm0-14.4h15.53V0H7.2v3.6z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/notificaciones' title="NOTIFICACIONES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/incidencias' title="INCIDENCIAS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M9 18a9 9 0 1 1 9-9 9.01 9.01 0 0 1-9 9zm-1-6v2h2v-2zm0-7v5h2V5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/reportes' title="REPORTES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" width="18" height="18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M19 3H5a2.006 2.006 0 0 0-2 2v14a2.006 2.006 0 0 0 2 2h14a2.006 2.006 0 0 0 2-2V5a2.006 2.006 0 0 0-2-2zM9 17H7v-7h2zm4 0h-2V7h2zm4 0h-2v-4h2z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    case 'support':
      return (
        <Nav className='mr-auto' css={`
          display: flex;
          padding-top: 10px !important;
          
          @media (max-width: 1010px) {display: none;}`} >
          <ActiveNavItem to='/incidencias' title="INCIDENCIAS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M9 18a9 9 0 1 1 9-9 9.01 9.01 0 0 1-9 9zm-1-6v2h2v-2zm0-7v5h2V5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios' title="COMERCIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios' title="USUARIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/notificaciones' title="NOTIFICACIONES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    case 'cashier':
      return (
        <Nav className='mr-auto' css={`
          display: flex;
          padding-top: 10px !important;
          
          @media (max-width: 1010px) {display: none;}`} >
          <ActiveNavItem to='/transacciones' title="TRANSACCIONES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M0 10.8h3.6V7.2H0v3.6zM0 18h3.6v-3.6H0V18zM0 3.6h3.6V0H0v3.6zm7.2 7.2h15.53V7.2H7.2v3.6zm0 7.2h15.53v-3.6H7.2V18zm0-14.4h15.53V0H7.2v3.6z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/incidencias' title="INCIDENCIAS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M9 18a9 9 0 1 1 9-9 9.01 9.01 0 0 1-9 9zm-1-6v2h2v-2zm0-7v5h2V5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios' title="USUARIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios' title="COMERCIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/notificaciones' title="NOTIFICACIONES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    case 'marketing':
      return (
        <Nav className='mr-auto' css={`
          display: flex;
          padding-top: 10px !important;
          
          @media (max-width: 1010px) {display: none;}`} >
          <ActiveNavItem to='/notificaciones' title="NOTIFICACIONES">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/dashboard-admin' title="INICIO">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M5.91 10.75h4.73v7.093h5.91V8.386L8.275 0 0 8.386v9.457h5.91z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios' title="COMERCIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios' title="USUARIOS">
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    default:
      return (
        <Nav
          className='mr-auto'
          css={`
          display: flex;
          padding-top: 10px !important;
          @media (max-width: 1010px) {display: none;}`}
        >
        </Nav>
      )
  }
}

const ResponsiveNavbar = ({ type }) => {
  switch (type) {
    case 'admin':
      return (
        <Nav >
          <ActiveNavItem to='/dashboard-admin'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M5.91 10.75h4.73v7.093h5.91V8.386L8.275 0 0 8.386v9.457h5.91z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/transacciones'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M0 10.8h3.6V7.2H0v3.6zM0 18h3.6v-3.6H0V18zM0 3.6h3.6V0H0v3.6zm7.2 7.2h15.53V7.2H7.2v3.6zm0 7.2h15.53v-3.6H7.2V18zm0-14.4h15.53V0H7.2v3.6z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/notificaciones'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/incidencias'>
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M9 18a9 9 0 1 1 9-9 9.01 9.01 0 0 1-9 9zm-1-6v2h2v-2zm0-7v5h2V5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/reportes'>
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" width="18" height="18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M19 3H5a2.006 2.006 0 0 0-2 2v14a2.006 2.006 0 0 0 2 2h14a2.006 2.006 0 0 0 2-2V5a2.006 2.006 0 0 0-2-2zM9 17H7v-7h2zm4 0h-2V7h2zm4 0h-2v-4h2z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    case 'support':
      return (
        <Nav>
          <ActiveNavItem to='/incidencias'>
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M9 18a9 9 0 1 1 9-9 9.01 9.01 0 0 1-9 9zm-1-6v2h2v-2zm0-7v5h2V5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/notificaciones'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    case 'cashier':
      return (
        <Nav >
          <ActiveNavItem to='/transacciones'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M0 10.8h3.6V7.2H0v3.6zM0 18h3.6v-3.6H0V18zM0 3.6h3.6V0H0v3.6zm7.2 7.2h15.53V7.2H7.2v3.6zm0 7.2h15.53v-3.6H7.2V18zm0-14.4h15.53V0H7.2v3.6z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/incidencias'>
            <svg className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 18">
              <path className={navItemLogo} id="Shape" fillRule="evenodd" fill='#f6f7fa' d="M9 18a9 9 0 1 1 9-9 9.01 9.01 0 0 1-9 9zm-1-6v2h2v-2zm0-7v5h2V5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/notificaciones'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    case 'marketing':
      return (
        <Nav >
          <ActiveNavItem to='/notificaciones'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M7.5 18c.97 0 1.765-.81 1.765-1.8h-3.53c0 .99.794 1.8 1.765 1.8zm5.735-5.4V7.65c0-2.763-1.88-5.076-4.411-5.688V1.35C8.824.603 8.232 0 7.5 0c-.732 0-1.324.603-1.324 1.35v.612c-2.532.612-4.411 2.925-4.411 5.688v4.95L0 14.4v.9h15v-.9l-1.765-1.8z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/dashboard-admin'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M5.91 10.75h4.73v7.093h5.91V8.386L8.275 0 0 8.386v9.457h5.91z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/comercios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 15 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M2.136 2.123c2.847-2.83 7.463-2.831 10.311 0a7.218 7.218 0 0 1 0 10.251L7.292 17.5l-5.156-5.126a7.218 7.218 0 0 1 0-10.25zm3.28 3.262a2.627 2.627 0 0 0 0 3.728 2.664 2.664 0 0 0 3.75 0 2.627 2.627 0 0 0 0-3.728 2.664 2.664 0 0 0-3.75 0z" />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/usuarios'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="29" height="18" viewBox="0 0 29 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M19.285 7.714a3.841 3.841 0 0 0 3.845-3.857A3.842 3.842 0 0 0 19.285 0a3.853 3.853 0 0 0-3.857 3.857 3.852 3.852 0 0 0 3.857 3.857zM9 7.714a3.841 3.841 0 0 0 3.844-3.857A3.841 3.841 0 0 0 9 0a3.852 3.852 0 0 0-3.857 3.857A3.852 3.852 0 0 0 9 7.714zm0 2.572c-2.996 0-9 1.504-9 4.5V18h18v-3.214c0-2.996-6.004-4.5-9-4.5zm10.285 0c-.372 0-.797.026-1.246.065 1.491 1.08 2.532 2.532 2.532 4.435V18h7.715v-3.214c0-2.996-6.005-4.5-9-4.5z" />
            </svg>
          </ActiveNavItem>
        </Nav>
      )
    default:
      return (
        <Nav></Nav>
      )
  }
}

const FullHeader = props => {
  return (
    <Navbar className={gualyNavbar}>
      <NavLink className={navbarBrandImage} to='/dashboard-admin'>
        <img src={logo} alt={'Gualy'} />
      </NavLink>
      <div className={leftSeparator}></div>
      <NormalNavbar type={props.userInCollectionData ? props.userInCollectionData.type : null} />
      <Nav className='ml-auto' css={`padding-top: 10px !important;`}>
        {/* Search icon */}
        {/* <NavItem className={flexLi}>
          <Link className={anchorLink} to='/'>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M12.497 11.314h-.8l-.3-.308c1-1.132 1.6-2.675 1.6-4.32 0-3.703-2.9-6.686-6.498-6.686C2.899 0 0 2.983 0 6.686s2.9 6.685 6.499 6.685c1.6 0 3.099-.617 4.199-1.645l.3.308v.823L15.995 18l1.5-1.543-4.999-5.143zm-5.998 0c-2.5 0-4.5-2.057-4.5-4.628 0-2.572 2-4.629 4.5-4.629 2.499 0 4.498 2.057 4.498 4.629 0 2.571-2 4.628-4.498 4.628z" />
            </svg>
          </Link>
        </NavItem> */}
        {/* Settings icon / Perfil icon */}
        {/* <ProfileIcon /> */}
        {/* signOut icon */}
        <NavItem className={flexLi}>
          <Link className={anchorLink} to='/' onClick={() => props.dispatch(userLogout())}>
            <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="22" height="18" viewBox="0 0 22 18">
              <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M13.542 11.836H4.378V5.66h9.164V.133l8.513 8.684-8.513 8.684v-5.665zm-3.466 4.025H2.335c-.145 0-.32-.195-.32-.493V2.476c0-.298.175-.493.32-.493h7.741V0H2.335C1.016 0 0 1.136 0 2.476v12.892c0 1.34 1.015 2.476 2.335 2.476h7.741v-1.983z" />
            </svg>
          </Link>
        </NavItem>
        <div className={rightSeparator}>
        </div>
      </Nav>
      <div className={horizontalSeparator}></div>
      <Navbar css={`
          display: none;
          @media (max-width: 1010px) {
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
          }
      `} >
        <ResponsiveNavbar type={props.userInCollectionData ? props.userInCollectionData.type : null} />
      </Navbar>
    </Navbar>
  )
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(FullHeader)