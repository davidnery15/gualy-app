import React from 'react'
import { css } from 'emotion'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import Header from '../AdminHeader'
import IssuesCardTable from './issues-card-table/IssuesCardTable'
import { db } from './../../../firebase'
import CreateIssueBox from './CreateIssueBox'
import { ToastContainer, toast } from 'react-toastify'
import moment from 'moment'
import CreateIssueForm from './CreateIssueForm'
import EditIssueForm from './EditIssueForm'
import { postRequest } from '../../../utils/createAxiosRequest'
import R from '../../../utils/R'
import { customConsoleLog } from '../../../utils/customConsoleLog';
const container = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
  `
const scrollDataRequestedRange = 10
class Commerce extends React.Component {
  state = ({
    issues: [],
    users: [],
    selectedUser: [],
    //ORDER BY
    isOrderBy: false,
    reverseNewers: true,
    selectedIssue: "",
    loadingIssues: true,
    openCreateIssue: false,
    openEditIssue: false,
    adminData: {
      adminID: this.props.userInCollectionData.userKey,
      displayName: this.props.userInCollectionData.name
    },
    createIssueLoading: false,
    editIssueLoading: false,
    displayErrors: false,
    imageError: false,
    sended: false,
    selectedName: "",
    selectedIdNumber: "",
    selectedPhone: "",
    selectedUid: "",
    ImgURI: "",
    email: "",
    category: "",
    departament: "",
    assigned: "",
    issueTitle: "",
    description: "",
    internNote: "",
    answer: "",
    image: "",
    status: "",
    message: "",
    isInternNote: true,
    response: "",
    errorMessage: '',
    itemsIndex: 10
  })
  databaseRequest = () => {
    this.setState({ itemsIndex: this.state.itemsIndex + scrollDataRequestedRange }, () => {
      const issuesRef = db.ref('issues')
        .limitToLast(this.state.itemsIndex)
      issuesRef.once('value', (snapshot) => {
        const issues = R.values(snapshot.val())
        this.setState({ issues })
      })
    })
  }
  componentDidMount() {
    const issuesRef = db.ref('issues')
      .limitToLast(this.state.itemsIndex)
    issuesRef.once('value', (snapshot) => {
      const issues = R.values(snapshot.val())
      this.setState({ issues })
    })
    let supportUsers = ""
    const supportUsersRef = db.ref('users')
      .orderByChild('type')
      .equalTo("support")
    supportUsersRef.once('value', (snapshot) => {
      supportUsers = R.values(snapshot.val())
    })
    let cashierUsers = ""
    const cashierUsersRef = db.ref('users')
      .orderByChild('type')
      .equalTo("cashier")
    cashierUsersRef.once('value', (snapshot) => {
      cashierUsers = R.values(snapshot.val())
    })
    const adminUsersRef = db.ref('users')
      .orderByChild('type')
      .equalTo("admin")
    adminUsersRef.once('value', (snapshot) => {
      const adminUsers = R.values(snapshot.val())
      this.setState({ users: adminUsers.concat(supportUsers, cashierUsers) })
    })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.props.userInCollectionData.type) {
        const redirect =
          this.props.userInCollectionData.type === 'admin' ||
            this.props.userInCollectionData.type === 'cashier' ||
            this.props.userInCollectionData.type === 'support'
            ? null
            : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  openCreateIssue = () => {
    return this.setState({
      openCreateIssue: !this.state.openCreateIssue,
      openEditIssue: false,
      selectedName: "",
      selectedIdNumber: "",
      selectedPhone: "",
      displayErrors: false,
      errorMessage: '',
      imageError: false,
      ImgURI: "",
      email: "",
      category: "",
      departament: "",
      assigned: "",
      issueTitle: "",
      description: "",
      internNote: "",
      answer: "",
      image: "",
      status: "",
      userEmail: "",
    })
  }
  openEditIssue = config => () => {
    return this.setState({
      openEditIssue: !this.state.openEditIssue,
      selectedUser: config.user,
      selectedIssueUid: config.issueUid,
      userEmail: config.userEmail,
    })
  }
  openEditIssueBox = () => {
    return this.setState({
      openEditIssue: !this.state.openEditIssue,
      openCreateIssue: false,
      displayErrors: false,
      selectedUser: [],
      selectedIssueUid: "",
      imageError: false,
      message: "",
      selectedName: "",
      selectedIdNumber: "",
      selectedPhone: "",
      ImgURI: "",
      email: "",
      category: "",
      departament: "",
      assigned: "",
      issueTitle: "",
      description: "",
      internNote: "",
      answer: "",
      image: "",
      status: ""
    })
  }
  handleInputChange = ({ target }) => {
    if (target.name === 'email') {
      const user = this.state.users.filter(user => user.email === target.value)
      if (user[0]) {
        return this.setState({
          selectedIdNumber: `${user[0].dni.type}${user[0].dni.id}`,
          selectedName: user[0].name,
          selectedPhone: user[0].phone,
          selectedUid: user[0].userKey,
          [target.name]: target.value
        })
      } else {
        return this.setState({
          selectedIdNumber: '',
          selectedName: '',
          selectedPhone: '',
          selectedUid: '',
          [target.name]: target.value
        })
      }
    }
    this.setState({
      [target.name]: target.value
    })
  }
  handleInputChangeEditIssue = ({ target }) => {
    this.setState({
      [target.name]: target.value,
      sended: false
    })
  }
  goInternNote = () => {
    this.setState({
      isInternNote: true,
    })
  }
  goPublicAsnwer = () => {
    this.setState({
      isInternNote: false,
    })
  }
  getOptions = items => items.map((item, i) => {
    if (item.type === 'admin' || item.type === 'support')
      return <option key={i} value={item.userKey}>{item.name}</option>
    else return null
  })

  toastLoaderId = null
  handleEditIssueChange = async ({ target }) => {
    if (target.name === 'status') {
      try {
        this.toastLoaderId = toast('Actualizando incidencia', { autoClose: false })
        this.setState({ editIssueLoading: true, sended: false })
        const data = {
          status: target.value,
          issueID: this.state.selectedIssueUid
        }
        customConsoleLog("SEND: ", data)
        let response = await postRequest('updateIssueStatus', data)
        customConsoleLog('RESPONSE: ', response)
        if (response.data.success) {
          this.databaseRequest()
          toast.update(this.toastLoaderId, {
            render: 'Estatus cambiado con exito.',
            autoClose: 5000
          })
          this.setState({ editIssueLoading: false, sended: false })
        } else {
          this.setState({ editIssueLoading: false, sended: false })
          toast.update(this.toastLoaderId, {
            render: "Error al cambiar estatus",
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        this.setState({ editIssueLoading: false })
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
        customConsoleLog("error: ", error)
      }
    }
    if (target.name === 'departament') {
      this.setState({ editIssueLoading: true })
      this.toastLoaderId = toast('Actualizando incidencia', { autoClose: false })
      db.ref(`issues/${this.state.selectedIssueUid}`).update({
        departament: target.value
      })
        .then((value) => {
          this.databaseRequest()
          toast.update(this.toastLoaderId, {
            render: 'Departamento cambiado con exito.',
            autoClose: 5000
          })
          this.setState({ sended: false, editIssueLoading: false })
        }, (error) => {
          this.setState({ sended: false, editIssueLoading: false })
          toast.update(this.toastLoaderId, {
            render: "Error al cambiar departamento",
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        })
    }
    if (target.name === 'assigned') {
      if (target.value !== '') {
        try {
          this.toastLoaderId = toast('Agregando agente', { autoClose: false })
          this.setState({ editIssueLoading: true })
          const data = {
            uid: target.value,
            issueID: this.state.selectedIssueUid
          }
          customConsoleLog("SEND: ", data)
          let response = await postRequest('assignAgent', data)
          customConsoleLog('RESPONSE: ', response)
          if (response.data.success) {
            this.databaseRequest()
            toast.update(this.toastLoaderId, {
              render: 'Agente asignado satisfactoriamente.',
              autoClose: 5000
            })
            this.setState({ editIssueLoading: false, sended: false })
          } else {
            this.setState({ editIssueLoading: false, sended: false })
            toast.update(this.toastLoaderId, {
              render: response.data.error.message,
              type: toast.TYPE.ERROR,
              autoClose: 5000
            })
          }
        } catch (error) {
          this.setState({ editIssueLoading: false, sended: false })
          const errorMessage = ({
            '401': 'Su sesión ha expirado',
            'default': 'Verifique su conexión y vuelva a intentarlo.'
          })[error.response ? error.response.status || 'default' : 'default']
          toast.update(this.toastLoaderId, {
            render: errorMessage,
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
          customConsoleLog("error: ", error)
        }
      }
    }
    if (target.name === 'assigned-users') {
      if (target.value !== '') {
        try {
          this.toastLoaderId = toast('Removiendo agente', { autoClose: false })
          this.setState({ editIssueLoading: true })
          const data = {
            uid: target.value,
            issueID: this.state.selectedIssueUid
          }
          customConsoleLog("SEND: ", data)
          let response = await postRequest('removeAgent', data)
          customConsoleLog('RESPONSE: ', response)
          if (response.data.success) {
            this.databaseRequest()
            toast.update(this.toastLoaderId, {
              render: response.data.data.message,
              autoClose: 5000
            })
            this.setState({ editIssueLoading: false, sended: false })
          } else {
            this.setState({ editIssueLoading: false, sended: false })
            toast.update(this.toastLoaderId, {
              render: "Error al remover agente",
              type: toast.TYPE.ERROR,
              autoClose: 5000
            })
          }
        } catch (error) {
          this.setState({ editIssueLoading: false, sended: false })
          toast.update(this.toastLoaderId, {
            render: "Verifique su conexión y vuelva a intentarlo.",
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
          customConsoleLog("error: ", error)
        }
      }
    }
  }
  sendMessage = async (e) => {
    e.preventDefault()
    const date = moment().utc(-264).format('YYYY-MM-DD')
    const dateTime = moment().utc(-264).format('YYYY-MM-DD h:mm A')
    const time = moment().utc(-264).format('h:mm A')
    const postData = {
      date: date,
      dateTime: dateTime,
      message: this.state.message,
      // timestamp: Date.now(),
      isPublic: false,
      time: time,
      type: "adminMessage",
      userData: {
        name: this.state.adminData.displayName,
        userKey: this.state.adminData.adminID
      }
    }
    if (postData.message.length > 5) {
      const newMessageKey = db.ref(`issues/${this.state.selectedIssueUid}`).child('messages').push().key
      postData['messageId'] = newMessageKey
      let updates = {}
      updates[`/messages/${newMessageKey}/`] = postData
      this.setState({
        message: '',
        sended: true
      })
      await db.ref(`issues/${this.state.selectedIssueUid}`).update(updates)
      await this.databaseRequest()
    } else {
      return toast.warn('El mensaje debe tener más de 5 caracteres.')
    }
  }
  toastLoaderId2 = null
  sendResponse = async e => {
    this.setState({ editIssueLoading: true })
    e.preventDefault()
    const data = {
      uid: this.props.userInCollectionData.userKey,
      issueID: this.state.selectedIssueUid,
      message: this.state.response.trim(),
    }
    customConsoleLog("SENDED: ", data)
    if (this.state.response.length > 5) {
      this.toastLoaderId2 = toast('Enviando respuesta', { autoClose: false })
      try {
        let resp = await postRequest('publicateIssueAnswer', data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success) {
          this.databaseRequest()
          this.setState({ editIssueLoading: false, response: "" })
          toast.update(this.toastLoaderId2, {
            render: 'Respuesta enviada exitosamente.',
            autoClose: 5000
          })
        } else {
          this.setState({ editIssueLoading: false })
          toast.update(this.toastLoaderId2, {
            render: "Error en la red, por favor intente mas tarde",
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        this.setState({ editIssueLoading: false })
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderId2, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
        customConsoleLog("error: ", error)
      }
    } else {
      this.setState({ editIssueLoading: false })
      return toast.warn('El mensaje debe tener más de 5 caracteres.')
    }
  }
  handleKeyPress = e => {
    if (e.key === 'Enter' && e.target.name === "message") {
      e.preventDefault()
      this.sendMessage(e)
    }
    if (e.key === 'Enter' && e.target.name === "response") {
      e.preventDefault()
      this.sendResponse(e)
    }
  }
  createIsssue = async (event) => {
    event.preventDefault()
    this.toastAddIssueId = toast('Creando incidencia...', { autoClose: false })
    this.setState({ createIssueLoading: true, errorMessage: '', displayErrors: false })
    if (this.state.email.trim() !== '' &&
      this.state.category !== '' &&
      this.state.departament !== '' &&
      this.state.status !== '' &&
      this.state.description.trim() !== ''
      // this.state.assigned !== '' &&
      // this.state.image !== '' &&
      // this.state.imageURI !== '' &&
      // this.state.selectedIdNumber !== '' &&
      // this.state.selectedName !== '' &&
      // this.state.selectedUid !== '' &&
      // this.state.issueTitle !== ''
      // this.state.internNote !== '' &&
      // this.state.answer !== '' &&
      // this.state.status !== ''
    ) {
      const data = {
        uid: this.state.selectedUid || this.props.userInCollectionData.userKey,
        category: this.state.category,
        departament: this.state.departament,
        description: this.state.description.trim(),
        userEmail: this.state.selectedUid ? '' : this.state.email.trim(),
        message: this.state.description.trim(),
        date: moment().format('YYYY-MM-DD'),
        time: moment().format('LTS'),
        attachments: this.state.image ? [{ base64Img: this.state.image }] : ''
      }
      customConsoleLog('SENDED: ', data)
      try {
        let response = await postRequest('makeIssue', data)
        customConsoleLog('RESPONSE: ', response)
        if (response.data.success) {
          this.databaseRequest()
          this.setState({
            createIssueLoading: false,
            displayErrors: false,
            imageError: false,
            selectedName: "",
            selectedIdNumber: "",
            selectedPhone: "",
            selectedUid: "",
            ImgURI: "",
            email: "",
            category: "",
            departament: "",
            assigned: "",
            issueTitle: "",
            description: "",
            internNote: "",
            answer: "",
            image: "",
            status: "",
            errorMessage: ''
          })
          toast.update(this.toastAddIssueId, {
            render: response.data.data.message,
            autoClose: 5000,
          })
        } else {
          this.setState({ createIssueLoading: false, errorMessage: response.data.error.message, displayErrors: true, })
          return toast.update(this.toastAddIssueId, {
            render: response.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        this.setState({ createIssueLoading: false })
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        this.setState({ createIssueLoading: false, errorMessage, displayErrors: true, })
        return toast.update(this.toastAddIssueId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        createIssueLoading: false
      })
      // if (this.state.image === '' || this.state.imageURI === '') {
      //   this.setState({
      //     imageError: true
      //   })
      // }
      if (this.state.email.trim() === '') {
        event.target.email.focus()
        this.setState({ errorMessage: 'Debe colocar un email.' })
        return toast.update(this.toastAddIssueId, {
          render: 'Debe colocar un email.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      // if (this.state.selectedIdNumber === '' ||
      //   this.state.selectedName === '' ||
      //   this.state.selectedUid === '') {
      //   event.target.email.focus()
      //   return toast.error('Debe colocar un email valido.')
      // }
      if (this.state.category === '') {
        event.target.category.focus()
        this.setState({ errorMessage: 'Debe colocar una categoria.' })
        return toast.update(this.toastAddIssueId, {
          render: 'Debe colocar una categoria.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (this.state.departament === '') {
        event.target.departament.focus()
        this.setState({ errorMessage: 'Debe colocar un departamento.' })
        return toast.update(this.toastAddIssueId, {
          render: 'Debe colocar un departamento.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      // if (this.state.assigned === '') {
      //   event.target.assigned.focus()
      //   return toast.error('Debe colocar un asignado')
      // }
      // if (this.state.issueTitle === '') {
      //   event.target.issueTitle.focus()
      //   this.setState({ errorMessage: 'Debe colocar un titulo para la incidencia.' })
      //   return toast.update(this.toastAddIssueId, {
      //     render: 'Debe colocar un titulo para la incidencia.',
      //     type: toast.TYPE.ERROR,
      //     autoClose: 5000,
      //   })
      // }
      if (this.state.description.trim() === '') {
        event.target.description.focus()
        this.setState({ errorMessage: 'Debe colocar una descripción para la incidencia.' })
        return toast.update(this.toastAddIssueId, {
          render: 'Debe colocar una descripción para la incidencia.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      // if (this.state.image === '' || this.state.imageURI === '') {
      //   return toast.error('Debe subir una foto de perfil.')
      // }
      if (this.state.status === '') {
        event.target.status.focus()
        this.setState({ errorMessage: 'Debe colocar estado de la incidencia.' })
        return toast.update(this.toastAddIssueId, {
          render: 'Debe colocar estado de la incidencia.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0];
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            image: reader.result,
            imageError: false
          })
        }
        reader.readAsDataURL(file);
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }
  //ORDER BY
  toggleOrderBy = () => {
    this.setState({
      isOrderBy: !this.state.isOrderBy,
    })
  }
  orderByNewers = () => {
    this.setState({
      reverseNewers: true,
      dateFilter: false,
      isOrderBy: false
    })
  }
  orderByOlders = () => {
    this.setState({
      reverseNewers: false,
      dateFilter: false,
      isOrderBy: false
    })
  }
  render() {
    customConsoleLog("itemsIndex: ", this.state.itemsIndex)
    customConsoleLog("issues: ", this.state.issues)
    customConsoleLog("users: ", this.state.users)
    const selectedIssue = this.state.issues.filter(issue => issue.issueUid === this.state.selectedIssueUid)
    return (
      <div>
        <Header />
        <div className={container}>
          {
            this.state.openCreateIssue ?
              <CreateIssueBox
                userDescription="Operador o Administrador:"
                title="AGREGAR INCIDENCIA"
                adminName={this.props.userInCollectionData.name}
                goBackFunction={this.openCreateIssue}
              >
                <CreateIssueForm
                  errorMessage={this.state.errorMessage}
                  displayErrors={this.state.displayErrors}
                  handleSubmit={this.createIsssue}
                  handleInputChange={this.handleInputChange}
                  users={this.state.users}
                  selectedIdNumber={this.state.selectedIdNumber}
                  selectedName={this.state.selectedName}
                  selectedPhone={this.state.selectedPhone}
                  email={this.state.email}
                  category={this.state.category}
                  departament={this.state.departament}
                  assigned={this.state.assigned}
                  issueTitle={this.state.issueTitle}
                  description={this.state.description}
                  internNote={this.state.internNote}
                  image={this.state.image}
                  imageError={this.state.imageError}
                  ImgURI={this.state.imageURI}
                  status={this.state.status}
                  createIssueLoading={this.state.createIssueLoading}
                  getOptions={this.getOptions}
                  encodeImageFileAsURL={this.encodeImageFileAsURL}
                />
              </CreateIssueBox>
              :
              this.state.openEditIssue ?
                <CreateIssueBox
                  userDescription="USUARIO:"
                  title="EDITAR INCIDENCIA"
                  adminName={
                    this.state.selectedUser
                      ? this.state.selectedUser.name
                      : `${this.state.userEmail} (No registrado)`
                  }
                  goBackFunction={this.openEditIssueBox}
                >
                  <EditIssueForm
                    displayErrors={this.state.displayErrors}
                    sended={this.state.sended}
                    handleEditIssueChange={this.handleEditIssueChange}
                    handleInputChange={this.handleInputChangeEditIssue}
                    selectedUser={this.state.selectedUser}
                    selectedIssue={selectedIssue[0]}
                    getOptions={this.getOptions}
                    editIssueLoading={this.state.editIssueLoading}
                    users={this.state.users}
                    message={this.state.message}
                    sendMessage={this.sendMessage}
                    handleKeyPress={this.handleKeyPress}
                    goPublicAsnwer={this.goPublicAsnwer}
                    goInternNote={this.goInternNote}
                    isInternNote={this.state.isInternNote}
                    response={this.state.response}
                    sendResponse={this.sendResponse}
                    userEmail={this.state.userEmail}
                  />
                </CreateIssueBox>
                :
                <IssuesCardTable
                  toggleOrderBy={this.toggleOrderBy}
                  orderByNewers={this.orderByNewers}
                  orderByOlders={this.orderByOlders}
                  reverseNewers={this.state.reverseNewers}
                  isOrderBy={this.state.isOrderBy}
                  handleDateChange={this.handleDateChange}
                  toggleCalendar={this.toggleCalendar}
                  openCreateIssue={this.openCreateIssue}
                  users={Object.values(this.state.users)}
                  rows={this.state.reverseNewers ?
                    Object.values(this.state.issues).sort((a, b) => b.timestamp - a.timestamp)
                    :
                    Object.values(this.state.issues)}
                  openEditIssue={this.openEditIssue}
                  title='INCIDENCIAS ACTIVAS'
                  loading={this.state.issues.length > 0 && this.state.users.length > 0 ? false : true}
                  adminData={this.state.adminData}
                  //scrolling
                  databaseRequest={this.databaseRequest}
                />
          }
        </div>
        {
          this.redirect()
        }
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(Commerce)
