import React, { Component } from 'react'
import { css } from 'emotion'
import OptionsModalWrapper from '../../../general/OptionsModalWrapper'
import AreYouSureModal from '../../../general/modals/areYouSureModal'
import MoreModal from './../../../general/modals/MoreModal'
import IssueStatus from './IssueStatus'
import IssueModal from './IssueModal/IssueModal'
// import infoIcon from './../../../../assets/ic-info.svg'
import lockIconRed from './../../../../assets/ic-lock-copy.svg'
import lockIconGreen from './../../../../assets/ic-lock-copy-2.svg'
// import iconNotiYellow from './../../../../assets/ic-notification-yellow.svg'
import moreIcon from './../../../../assets/ic-more-vert.svg';
import { toast } from 'react-toastify'
import iconEdit from './../../../../assets/icon-edit.svg'
import moment from "moment"
import userImage from '../../../../assets/accountCircle.svg'
import { postRequest } from '../../../../utils/createAxiosRequest'
const actionsContainer = css`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
    margin-right: 12px;
    @media (max-width: 1300px) {
      display: none;
    }
  `
const userImg = css`
    width: 44.2px;
    height: 44.2px;
    border-radius: 50%;
    object-fit: cover;
    margin: 0 24px;
    @media (max-width: 990px) {
      display:none;
    }
  `
const tableRow = css`
    td{
      padding: 0 10px 24px 10px;
      font-size: 11.8px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #f6f7fa;
      text-align: center
    }
  `
const emailContainer = css`
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    @media (max-width: 1300px) {
      width: 140px;
    }
    @media (max-width: 930px) {
      width: 100px;
    }
  `
const moreIconImg = css`
    display: none;
    @media (max-width: 1300px) {
      display: initial;
    }
  `
const tdIndex = css`
    padding-left: 0 !important;
    padding-right: 0 !important;
  `
const tdMoreIcon = css`
    position: relative;
    cursor: pointer;
    img{
      width: 18px;
      height: 18px;
      object-fit: contain;
    }
  `
const userContainer = css`
    display: flex;
    width: 100%;
    align-items: center;
  `
export default class IssuesRow extends Component {
  state = ({
    show: false,
    visibleMoreModal: false,
    areYouSureModalIsOpenBlock: false,
    areYouSureModalIsOpenUnBlock: false,
    isLoading: false
  })
  showModal = () => {
    this.setState({
      ...this.state,
      show: !this.state.show
    })
  }
  openAreYouSureModalBlock = () => {
    this.setState({ areYouSureModalIsOpenBlock: !this.state.areYouSureModalIsOpenBlock })
  }
  openAreYouSureModalUnBlock = () => {
    this.setState({ areYouSureModalIsOpenUnBlock: !this.state.areYouSureModalIsOpenUnBlock })
  }
  handleMoreModal = e => {
    this.setState({
      visibleMoreModal: !this.state.visibleMoreModal
    })
  }
  unBlockUser = async () => {
    this.setState({ isLoading: true })
    try {
      const uid = this.props.user.userKey
      const adminID = this.props.adminData.adminID
      const data = {
        uid,
        adminID,
      }
      let response = await postRequest('users/unBlockUser', data)
      if (response.data.success) {
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario desbloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
      // console.log("error unblocking user", error)
    }
  }
  blockUser = async () => {
    this.setState({ isLoading: true })
    try {
      const uid = this.props.user.userKey
      const adminID = this.props.adminData.adminID
      const reason = 'Usuario Bloqueado'
      const action = 'block'
      const data = {
        uid,
        adminID,
        reason,
        action
      }
      // console.log("SEND: ", data)
      let response = await postRequest('users/closeSession', data)
      // console.log("RESPONSE: ", response)
      if (response.data.success) {
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario bloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
      // console.log("error blocking user", error)
    }
  }
  render() {
    const { info, index, adminData, user, openEditIssue } = this.props
    return (
      <tr className={tableRow}>
        <td className={tdIndex}>{index}</td>
        <td>
          <div className={userContainer}>
            <img
              className={userImg}
              src={info.userData.profilePicture}
              onError={(e) => { e.target.onerror = null; e.target.src = userImage }}
              alt=""
            />
            <span> {info.userData.name || 'No registrado'}</span>
          </div>
        </td>
        <td> <div className={emailContainer}> {info.userData.email} </div> </td>
        <td> {info.userData.phone || 'No registrado'} </td>
        <td> {moment(info.createdDate).utc(-264).format('DD-MM-YYYY')}{' '}{info.createdTime} </td>
        <td>
          <div> {info.assigned === '' ? 'No asignado' : 'Asignado'} </div>
        </td>
        <td>
          <div>{info.category}</div>
        </td>
        <td>
          <IssueStatus info={info.status} />
        </td>
        <td className={tdMoreIcon}>
          <div className={actionsContainer}>
            {/* <img onClick={this.showModal} src={infoIcon} alt="info" /> */}
            <img src={iconEdit} onClick={openEditIssue({ user, issueUid: info.issueUid, userEmail: info.userData.email })} alt="edit" />
            {
              user ?
                user.blocked ?
                  <img onClick={this.openAreYouSureModalUnBlock} src={lockIconGreen} alt="lock" />
                  :
                  <img onClick={this.openAreYouSureModalBlock} src={lockIconRed} alt="unlock" />
                : null
            }
            {/* <img src={iconNotiYellow} alt="noti" /> */}
          </div>
          <button className='btn-transparent' onClick={this.handleMoreModal}>
            <img src={moreIcon} alt="more" className={moreIconImg} />
          </button>
          <MoreModal visible={this.state.visibleMoreModal} responsive={true}>
            {/* <img onClick={this.showModal} src={infoIcon} alt="info" /> */}
            <img onClick={openEditIssue({ user, issueUid: info.issueUid, userEmail: info.userData.email })} src={iconEdit} alt="edit" />
            {
              user ?
                user.blocked ?
                  <img onClick={this.openAreYouSureModalUnBlock} src={lockIconGreen} alt="lock" />
                  :
                  <img onClick={this.openAreYouSureModalBlock} src={lockIconRed} alt="unlock" />
                : null
            }
            {/* <img src={iconNotiYellow} alt="noti" /> */}
          </MoreModal>
          <IssueModal
            onClose={this.showModal}
            show={this.state.show}
            info={info}
            index={index}
            adminData={adminData}
          />
          <OptionsModalWrapper show={this.state.areYouSureModalIsOpenBlock} onClose={this.openAreYouSureModalBlock} width="310px">
            <AreYouSureModal
              modalShow={this.openAreYouSureModalBlock}
              onSubmit={this.blockUser}
              message="Esta seguro que desea bloquear este usuario?"
              cancelText="CANCELAR"
              actionText="BLOQUEAR"
              isLoading={this.state.isLoading}
            />
          </OptionsModalWrapper>
          <OptionsModalWrapper show={this.state.areYouSureModalIsOpenUnBlock} onClose={this.openAreYouSureModalUnBlock} width="310px">
            <AreYouSureModal
              modalShow={this.openAreYouSureModalUnBlock}
              onSubmit={this.unBlockUser}
              message="Esta seguro que desea desbloquear este usuario?"
              cancelText="CANCELAR"
              actionText="DESBLOQUEAR"
              isLoading={this.state.isLoading}
            />
          </OptionsModalWrapper>
        </td>
      </tr>
    )
  }
} 