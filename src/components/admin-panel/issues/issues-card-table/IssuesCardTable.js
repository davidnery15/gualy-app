import React, { Component } from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import TableHeaderIssue from './TableHeaderIssue'
import IssuesTable from './IssuesTable'
import MobileIssuesTable from './MobileIssuesTable'
import OptionsModalWrapper from '../../../general/OptionsModalWrapper'
import DateRangeModal from '../../../general/modals/DateRangeModal'
import { customConsoleLog } from '../../../../utils/customConsoleLog'
import { toast } from 'react-toastify'
import { postRequest } from '../../../../utils/createAxiosRequest'
import throttle from 'lodash.throttle'
import moment from 'moment'
const component = css`
    margin: 50px;
    border-radius: 5px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    background-color: #2a2c6a;
    @media (max-width: 1230px) {
      margin: 0 25px;
      margin-top: 40px;
      margin-bottom: 40px;
    }
  `
const responsive = css`
    display: none;
    min-height: 500px;
    @media (max-width: 870px) {
      display: flex !important;
      flex-direction: column;
    }
  `
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const cardTableComponent = css`
    overflow-x: hidden;
    height: 70vh;
    padding-left: 32px;
    padding-right: 32px;
    position: relative;
    /* Track */
      ::-webkit-scrollbar-track {
        background: #f6f7fa;
        border: 3px solid transparent;
        background-clip: content-box;
      }
    /* Handle */
      ::-webkit-scrollbar-thumb {
        border: 1px;
        background-color: #95fad8;
        border-radius: 2px;
      }
    ::-webkit-scrollbar {
        width: 7px;
    }
    @media (max-width: 870px) {
      display: none;
      padding-left: 0;
      padding-right: 0;
      height: 100%;
      max-height: 70vh;
    }
  `
const cardTableBottom = css`
    position: sticky;
    bottom: -1px;
    width: 100%;
    height: 100px;;
    background-image: linear-gradient(to bottom, rgba(41, 43, 105, 0), rgba(41, 43, 105, 0.5) 49%, #2a2c6a);
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
const noRegistersText = css`
  opacity: 0.5;
  text-align: center;
`
export default class IssuesCardTable extends Component {
  state = ({
    search: '',
    isFilterBY: false,
    isDateFilter: false,
    isSearchingData: false,
    dateRangePickerIsOpen: false,
    selection: {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection',
    },
    filteredTransactions: {}
  })
  componentDidMount = () => {
    if (this.desktopTable) {
      if (this.desktopTable.clientHeight || this.mobileTable.clientHeight === this.desktopTable.scrollHeight || this.mobileTable.scrollHeight) {
        this.props.databaseRequest()
      }
    }
  }
  updateSearch = (event) => {
    this.setState({
      search: event.target.value.substr(0, 120),
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  throtled = throttle(async () => {
    const {
      isFilterBySolves,
      isFilterByPending,
      isFilterAll,
      isDateFilter,
      search,
      selection
    } = this.state
    const requestStatus = isFilterBySolves ? "Resolved" : isFilterByPending ? "Pending" : isFilterAll ? "All" : "All"
    const requestMode = isDateFilter ? "date" : "fields"
    if (search || isDateFilter) {
      this.setState({ isSearchingData: true })
      const data = isDateFilter
        ? {
          startDate: moment(selection.startDate).format('YYYY-MM-DD'),
          finishDate: moment(selection.endDate).format('YYYY-MM-DD'),
        }
        : {
          query: search,
        }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest(`issues/filter?mode=${requestMode}&status=${requestStatus}`, data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success && resp.data.data.message.success) {
          this.setState({
            filteredTransactions: Object.values(resp.data.data.message.message),
            isSearchingData: false
          })
        } else {
          this.setState({
            isSearchingData: false,
            filteredTransactions: {},
          })
        }
      } catch (error) {
        this.setState({ isSearchingData: false })
        customConsoleLog(error)
        toast.error("Verifique su conexión y vuelva a intentarlo.")
      }
    }
  }, 1000)
  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.throtled()
    }
  }
  filterBySolves = () => {
    this.setState({
      isFilterBySolves: true,
      isFilterByPending: false,
      isFilterAll: false,
      isFilterBY: false,
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  filterByPending = () => {
    this.setState({
      isFilterBySolves: false,
      isFilterByPending: true,
      isFilterAll: false,
      isFilterBY: false,
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  filterByAll = () => {
    this.setState({
      isFilterBySolves: false,
      isFilterByPending: false,
      isFilterAll: true,
      isFilterBY: false,
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  toggleFilterBy = () => {
    this.setState({ isFilterBY: !this.state.isFilterBY })
  }
  onScrollDesktop = () => {
    if (
      (this.desktopTable.scrollHeight - this.desktopTable.scrollTop === this.desktopTable.clientHeight)
      && this.props.databaseRequest
    ) {
      this.props.databaseRequest()
    }
  }
  onScrollMobile = () => {
    if (
      (this.mobileTable.scrollHeight - this.mobileTable.scrollTop === this.mobileTable.clientHeight)
      && this.props.databaseRequest
    ) {
      this.props.databaseRequest()
    }
  }
  openDateRangeModal = () => {
    this.setState({
      dateRangePickerIsOpen: !this.state.dateRangePickerIsOpen,
      isDateFilter: false,
      selection: {
        ...this.state.selection,
        startDate: new Date(),
        endDate: new Date(),
      }
    })
  }
  searchDateRange = () => {
    this.setState({
      isDateFilter: true,
      dateRangePickerIsOpen: false,
      search: "",
    }, () => {
      this.throtled()
    })
  }
  handleSelect = (ranges) => {
    this.setState({
      selection: {
        ...this.state.selection,
        startDate: ranges.selection.startDate,
        endDate: ranges.selection.endDate,
      }
    })
  }
  goBackButtonClick = () => {
    this.setState({
      isDateFilter: false,
      search: "",
      filteredTransactions: "",
    })
  }
  render() {
    const {
      filteredTransactions,
      isDateFilter,
      search,
      isSearchingData,
    } = this.state
    const {
      rows,
      users,
      title,
      loading,
      adminData,
      openCreateIssue,
      openEditIssue,
      toggleCalendar,
      handleDateChange,
      closeDateFilter,
      dateFilter,
      isOrderBy,
      toggleOrderBy,
      orderByNewers,
      orderByOlders,
      reverseNewers,
      //scrolling
      databaseRequest
    } = this.props
    const transactionsFilteredByEndpoint = search ||
      isDateFilter ||
      (filteredTransactions.length > 0 && search) ||
      (filteredTransactions.length > 0 && isDateFilter)
      ? filteredTransactions
      : rows
    const finalFiltering = transactionsFilteredByEndpoint &&  transactionsFilteredByEndpoint.length > 0
      ? this.state.isFilterBySolves
        ? transactionsFilteredByEndpoint.filter(rows => rows.status === 'Resolved')
        : this.state.isFilterByPending
          ? transactionsFilteredByEndpoint.filter(rows => rows.status === 'Pending')
          : transactionsFilteredByEndpoint
      : transactionsFilteredByEndpoint
    return (
      <div className={component}>
        <TableHeaderIssue
          title={title}
          isOrderBy={isOrderBy}
          reverseNewers={reverseNewers}
          handleDateChange={handleDateChange}
          toggleCalendar={toggleCalendar}
          closeDateFilter={closeDateFilter}
          dateFilter={dateFilter}
          toggleOrderBy={toggleOrderBy}
          orderByNewers={orderByNewers}
          orderByOlders={orderByOlders}
          openCreateIssue={openCreateIssue}
          updateSearch={this.updateSearch}
          search={this.state.search}
          placeholder='Buscar...'
          filter='true'
          icon={false}
          isFilterBy={this.state.isFilterBY}
          filterBySolves={this.filterBySolves}
          filterByPending={this.filterByPending}
          filterByAll={this.filterByAll}
          toggleFilterBy={this.toggleFilterBy}
          onKeyPress={this.onKeyPress}
          isSearchingData={isSearchingData}
          //date range
          dateRangePickerIsOpen={this.state.dateRangePickerIsOpen}
          dateRangeFilterHandleSelect={this.handleSelect}
          ranges={this.state.selection}
          searchDateRange={this.searchDateRange}
          openDateRangeModal={this.openDateRangeModal}
          //filters
          isFilterAll={this.state.isFilterAll}
          isFilterByPending={this.state.isFilterByPending}
          isFilterBySolves={this.state.isFilterBySolves}
          //back button
          isBackButtonShow={this.state.search || isDateFilter}
          goBackButtonClick={this.goBackButtonClick}
        />
        <div className={tableDivisor}></div>
        <div
          ref={ref => this.desktopTable = ref}
          onScroll={this.onScrollDesktop}
          className={cardTableComponent}>
          {
            loading || isSearchingData
              ? <div className={spinnerContainer}> <Spinner color='white' /> </div>
              : finalFiltering && finalFiltering.length > 0
                ? <IssuesTable
                  users={users}
                  openEditIssue={openEditIssue}
                  rows={finalFiltering}
                  adminData={adminData}
                  databaseRequest={databaseRequest}
                />
                : <div className={spinnerContainer}>
                  <h1 className={noRegistersText}>No hay registros</h1>
                </div>
          }
          <div className={cardTableBottom}></div>
        </div>
        <div
          ref={ref => this.mobileTable = ref}
          onScroll={this.onScrollMobile}
          className={`${cardTableComponent} ${responsive}`}
        >
          {loading || isSearchingData
            ? <div className={spinnerContainer}>
              <Spinner color='white' />
            </div>
            : finalFiltering && finalFiltering.length > 0
              ? finalFiltering
                .map((row, index) => {
                  const user = users.filter(user => user.userKey === row.userData.userKey)
                  return <MobileIssuesTable
                    user={user[0]}
                    row={row}
                    openEditIssue={openEditIssue}
                    adminID={adminData.adminID}
                    key={row.issueUid}
                    index={(index + 1)}
                    databaseRequest={databaseRequest}
                  />
                })
              : <div className={spinnerContainer}>
                <h1 className={noRegistersText}>No hay registros</h1>
              </div>}
        </div>
        <OptionsModalWrapper
          show={this.state.dateRangePickerIsOpen}
          onClose={this.openDateRangeModal}
          width={310}
        >
          <DateRangeModal
            onChange={this.handleSelect}
            moveRangeOnFirstSelection={false}
            ranges={this.state.selection}
            onSearchClick={this.searchDateRange}
          />
        </OptionsModalWrapper>
      </div>
    )
  }
}