import React, { Component } from 'react'
import { css } from 'emotion'
import IssueModal from './IssueModal/IssueModal'
import IssueStatus from './IssueStatus'
// import infoIcon from './../../../../assets/ic-info.svg'
import lockIconRed from './../../../../assets/ic-lock-copy.svg'
import lockIconGreen from './../../../../assets/ic-lock-copy-2.svg'
// import iconNotiYellow from './../../../../assets/ic-notification-yellow.svg'
import { toast } from 'react-toastify'
import OptionsModalWrapper from '../../../general/OptionsModalWrapper'
import AreYouSureModal from '../../../general/modals/areYouSureModal'
import iconEdit from './../../../../assets/icon-edit.svg'
import { postRequest } from '../../../../utils/createAxiosRequest'
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    margin-top: 15px;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const mobileTable = css`
    td{
      padding: 0 22px;
      padding-top: 15px;
      width: 100%
    }
  `
const tdTitle = css`
    font-size: 11px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.09;
    letter-spacing: 0.9px;
    text-align: left;
    color: #f6f7fa;
  `
const tdData = css`
    font-size: 10px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1;
    letter-spacing: normal;
    text-align: right;
    color: #f6f7fa;
  `
const actionsContainer = css`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
  `
export default class MobileIssuesTable extends Component {
  state = ({
    show: false,
    areYouSureModalIsOpenBlock: false,
    areYouSureModalIsOpenUnBlock: false,
    isLoading: false
  })
  showModal = () => {
    this.setState({ show: !this.state.show })
  }
  openAreYouSureModalBlock = () => {
    this.setState({ areYouSureModalIsOpenBlock: !this.state.areYouSureModalIsOpenBlock })
  }
  openAreYouSureModalUnBlock = () => {
    this.setState({ areYouSureModalIsOpenUnBlock: !this.state.areYouSureModalIsOpenUnBlock })
  }
  lockUser = async (event) => {
    event.preventDefault()
    this.setState({ isLoading: true })
    try {
      const uid = this.props.user.userKey
      const adminID = this.props.adminID
      const reason = 'Usuario Bloqueado'
      const action = 'block'
      const data = {
        uid,
        adminID,
        reason,
        action
      }
      // console.log("SEND: ", data)
      let response = await postRequest('users/closeSession', data)
      // console.log("RESPONSE: ", response)
      if (response.data.success) {
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario bloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
      // console.log("error blocking user", error)
    }
  }
  unLockUser = async (event) => {
    event.preventDefault()
    this.setState({ isLoading: true })
    try {
      const uid = this.props.user.userKey
      const adminID = this.props.adminID
      const data = {
        uid,
        adminID,
      }
      let response = await postRequest('users/unBlockUser', data)
      if (response.data.success) {
        this.setState({ isLoading: false, areYouSureModalIsOpenBlock: false, areYouSureModalIsOpenUnBlock: false })
        toast.success('Usuario desbloqueado exitosamente.')
      } else {
        this.setState({ isLoading: false })
        toast.error(response.data.error.message)
      }
    } catch (error) {
      this.setState({ isLoading: false })
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.error(errorMessage)
      // console.log("error unblocking user", error)
    }
  }
  render() {
    const { row, index, adminData, user, openEditIssue } = this.props
    return (
      <div>
        <table className={mobileTable}>
          <tbody>
            <tr>
              <td className={tdTitle}>
                ID
              </td>
              <td className={tdData}>
                {index}
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Nombre</td>
              <td className={tdData}>
                {row.userData.name || 'No registrado'}
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Email</td>
              <td className={tdData}>
                {row.userData.email}
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Teléfono</td>
              <td className={tdData}>{row.userData.phone || 'No registrado'}</td>
            </tr>
            <tr>
              <td className={tdTitle}>Fecha de registro</td>
              <td className={tdData}>{row.createdDate}{' '}{row.createdTime}</td>
            </tr>
            <tr>
              <td className={tdTitle}>Asignado</td>
              <td className={tdData}>{row.assigned === '' ? 'No asignado' : 'Asignado'} </td>
            </tr>
            <tr>
              <td className={tdTitle}>Categoria</td>
              <td className={tdData}>{row.category || 'No registrado'} </td>
            </tr>
            <tr>
              <td className={tdTitle}>Estado</td>
              <td className={tdData}>
                <div>
                  <IssueStatus info={row.status} />
                </div>
                <IssueModal
                  onClose={this.showModal}
                  show={this.state.show}
                  info={row}
                  index={index}
                  adminData={adminData}
                />
                <OptionsModalWrapper
                  show={this.state.areYouSureModalIsOpenBlock}
                  onClose={this.openAreYouSureModalBlock}
                  width="310px">
                  <AreYouSureModal
                    modalShow={this.openAreYouSureModalBlock}
                    onSubmit={this.lockUser}
                    message="Esta seguro que desea bloquear este usuario?"
                    cancelText="CANCELAR"
                    actionText="BLOQUEAR"
                    isLoading={this.state.isLoading}
                  />
                </OptionsModalWrapper>
                <OptionsModalWrapper
                  show={this.state.areYouSureModalIsOpenUnBlock}
                  onClose={this.openAreYouSureModalUnBlock}
                  width="310px">
                  <AreYouSureModal
                    modalShow={this.openAreYouSureModalUnBlock}
                    onSubmit={this.unLockUser}
                    message="Esta seguro que desea desbloquear este usuario?"
                    cancelText="CANCELAR"
                    actionText="DESBLOQUEAR"
                    isLoading={this.state.isLoading}
                  />
                </OptionsModalWrapper>
              </td>
            </tr>
            <tr>
              <td className={tdTitle}>Acciones</td>
              <td className={tdData}>
                <div className={actionsContainer}>
                  {/* <img onClick={this.showModal} src={infoIcon} alt="row" /> */}
                  <button className='btn-transparent' onClick={openEditIssue({ user, issueUid: row.issueUid, userEmail: row.userData.email })}>
                    <img src={iconEdit} alt="edit" />
                  </button>
                  {
                    user
                      ?
                      user.blocked ?
                        <img onClick={this.openAreYouSureModalUnBlock}
                          src={lockIconGreen} alt="lock" />
                        :
                        <img onClick={this.openAreYouSureModalBlock} src={lockIconRed} alt="unlock" />
                      : null
                  }
                  {/* <img src={iconNotiYellow} alt="noti" /> */}
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className={tableDivisor}></div>
      </div>
    )
  }
}