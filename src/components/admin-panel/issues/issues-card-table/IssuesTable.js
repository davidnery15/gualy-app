import React, { Component } from 'react'
import { css } from 'emotion'
import IssueModal from './IssueModal/IssueModal'
import IssuesRow from './IssuesRow'
/* CSS */
const cardTable = css`
    width: 100%;
    padding: 0 15px;
    border-collapse: collapse;
  `
const tHead = css`
    tr{
      th{
        text-align: center; 
        padding: 40px 10px;
        font-size: 11px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: 0.9px;
        color: #f6f7fa;
        span{
          padding-left: 93.19px;
        }
      }
    }
    @media (max-width: 990px) {
      tr{
        th{
          span{
            padding-left: 0;
          }
        }
      }
    }
  `
const thActions = css`
    padding-rigth: 0;
    padding-left: 0;
    @media (max-width: 1270px) {
      display: none;
    }
  `
const thStatus = css`
    @media (max-width: 1270px) {
      width: 70px;
    }
  `

export default class IssuesTable extends Component {

  render() {

    const { rows, adminData, users, openEditIssue } = this.props
    return (
      <table
        className={cardTable}

      >
        <thead className={tHead}>
          <tr>
            <th css={`padding-left:0 !important;`}>ID</th>
            <th> <span> Nombre </span> </th>
            <th>Email</th>
            <th>Teléfono</th>
            <th>Fecha&nbsp;de registro</th>
            <th>Asignado</th>
            <th>Categoria</th>
            <th className={thStatus}>
              Estado
                <IssueModal />
            </th>
            <th className={thActions}>Acciones</th>
          </tr>
        </thead>
        <tbody >
          {
            rows
              ? rows.map((row, index) => {
                const user = users.filter(user => user.userKey === row.userData.userKey)
                return <IssuesRow
                  info={row}
                  user={user[0]}
                  openEditIssue={openEditIssue}
                  key={row.issueUid}
                  index={(index + 1)}
                  adminData={adminData}
                />
              })
              : null
          }
        </tbody>
      </table>
    )
  }
}
