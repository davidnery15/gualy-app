import React from 'react'
import { css } from 'emotion'
// import approvedIcon from './../../../../assets/ic-info-turquoise.svg';
// import pendingIcon from './../../../../assets/icon-edit-yellow.svg';
const statusContainer = css`
    display: flex;
    justify-content: center
  `
const approvedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #95fad8;
    display: flex;
    @media (max-width: 870px) {
      margin-left: auto
    }
  `
const pendingText = css`
    display: flex;
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #f2ff4e;
    @media (max-width: 870px) {
      margin-left: auto
      }
  `
export default class issueStatus extends React.Component {
  render() {
    const { info } = this.props
    return (
      <div>
        <div className={statusContainer}>
          {
            info === "Resolved" ?
              <span
                className={approvedText}>
                Resuelto&nbsp;&nbsp;
              {/* <img src={approvedIcon} alt="Aprobado" /> */}
              </span>
              :
              <span className={pendingText}>
                Pendiente&nbsp;&nbsp;
              {/* <img src={pendingIcon} alt="Pendiente" /> */}
              </span>
          }
        </div>
      </div>
    );
  }
}