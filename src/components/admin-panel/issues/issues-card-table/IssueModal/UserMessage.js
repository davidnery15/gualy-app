import React from 'react'
import { css } from 'emotion'
import moment from 'moment'
const component = css`
    width: 100%;
  `
const header = css`
    width: 100%;
    background-color: #95fad8;
    display: flex;
    justify-content: space-between;
    padding: 16px 10px;
    div{
      display: flex;
      h3{
        font-size: 16px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: 0.8;
        text-align: left;
        color: #1d1f4a;
        margin:0 15px;
      }
      h4{
        font-size: 14px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 0.8;
        text-align: left;
        color: #1d1f4a;
        margin:0 15px;
      }
    }
    @media(max-width: 720px) {
      padding: 10px 5px;
      div{
        flex-direction: column;
        h3{
          margin-top:5px;
        }
        h4{
          margin-top:5px;
        }
      }
    }
  `
const body = css`
    width: 100%;
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.4);
    max-height: 123px;
    display: flex;
    padding: 25px;
    overflow-x: hidden;
    /* Track */
    ::-webkit-scrollbar-track {
      /* background: #f6f7fa; */
      border: 3px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 2px;
    }
    ::-webkit-scrollbar {
      width: 7px;
    }
  `
const fullHeight = css`
    max-height: 500px;
  `
const textContainer = css`
    width: 100%;
    height: 100%;
    word-wrap: break-word;
    p{
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      margin: 0;
    }
  `
const adminHeader = css`
    width: 100%;
    background-color: #8078ff;
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    h3{
      font-size: 16px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;
      color: #f6f7fa;
      margin: 0;
    }
    h4{
      font-size: 14px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;
      color: #f6f7fa;
      margin: 0;
    }
  `
const UserMessage = ({ info, index, first, HeaderText }) => {
  return (
    <div className={component}>
      {
        info.type === 'adminMessage' ?
          <div className={adminHeader}>
            <h3>{info.userData.name}</h3>
            <h4>{info.dateTime ? info.dateTime : moment(info.timestamp).utc(-264).format('DD-MM-YYYY h:mm a')}</h4>
          </div>
          :
          info.userData 
          ?
            <div className={header}>
              <div>
                <h3>{HeaderText}</h3>
              </div>
            </div>
            : 
            null
      }
      <div className={`${body} ${first ? '' : fullHeight}`}>
        <div className={textContainer}>
          <p> {info.message} </p>
        </div>
      </div>
    </div>
  )
}
export default UserMessage