import React, { Component } from 'react'
import { css } from 'emotion'
const component = css`
    width: 100%;
    height: 300px;
    padding: 10px;
  `
export default class Attachments extends Component {
  render() {
    const { image } = this.props
    const slide = css`
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      overflow: hidden;
      object-fit: contain;
      background-image: url("${image}");
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
    `
    return (
      <div className={component}>
        <div className={slide}>
          {
            image === '' ?
              <h1>Sin Imagen para mostrar. </h1>
              :
              null
          }
        </div>
      </div>
    )
  }
}