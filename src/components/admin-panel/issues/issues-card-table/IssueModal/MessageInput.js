import React from 'react'
import { css } from 'emotion'
import sendIcon from './../../../../../assets/Group.svg'
const component = css`
    width: 100%;
    padding: 10px 15px;
    display: flex;
    justify-content: space-between;
    background-color: #242657;
  `
const customInput = css`
    border-radius: 28px;
    background-color: #ffffff;
    padding: 0 20px;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    text-align: left;
    color: #707070;
  `
const sendIconCSS = css`
    width: 40px;
    height: 40px;
    object-fit: contain;
    margin-left: 18px;
  `
const MessageInput = ({ value, onChange, name, formName, onSubmit }) =>
  <form name={formName} className={component} onSubmit={onSubmit} autoComplete='off'>
    <input
      className={customInput}
      type="text"
      placeholder="Enviar"
      name={name}
      onChange={onChange}
      value={value}
    />
    <button className='btn-transparent'>
      <img src={sendIcon} alt="Send Icon" className={sendIconCSS} />
    </button>
  </form>
export default MessageInput