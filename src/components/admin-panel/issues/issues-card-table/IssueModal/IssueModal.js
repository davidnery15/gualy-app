import React, { Component } from 'react'
import { css } from 'emotion'
import moment from 'moment'
import { ToastContainer, toast } from 'react-toastify'
import Attachments from './Attachments'
import UserMessage from './UserMessage'
import MessageInput from './MessageInput'
import iconClose from './../../../../../assets/icon-close.svg'
import iconAttachment from './../../../../../assets/ic_attach_file_24px.svg'
import { db } from './../../../../../firebase'
const modalComponent = css`
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-color: rgba(11, 11, 29, 0.85);
    z-index: 1000;
  `
const modalContainer = css`
    width: 800px;
    height: 70vh;
    min-height: 90%;
    border-radius: 5px;
    background-color: #2a2c6a; 
    box-shadow: 0 9px 18px 0 rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: column;
    position: relative;
    @media(max-width: 870px) {
      width: 90%;
    }
  `
const closeButton = css`
    background: transparent;
    border: none;
    text-align: right;
    width: 25px;
    cursor: pointer;
    outline: none !important;
  `
const modalHeader = css`
    top: -30px;
    padding: 0 5px;
    position: absolute;
    width: 100%;
    display: flex;
    justify-content: space-between;
    h2{
      font-family: Montserrat;
      font-size: 22px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
    }
    button{
      margin-left: 20px;
    }
  `
const modalBody = css`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    position: relative;
  `
const divisor = css`
    width: 100%;
    border: 1px solid black;
    margin: 20px 0;
  `
const scrollContainer = css`
    width: 100%;
    height: 100%;
    padding: 0 30px;
    overflow-x: hidden;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 3px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 2px;
    }
    ::-webkit-scrollbar {
      width: 7px;
    }
  `
const marginBottom = css`
    margin-bottom: 20px;
  `
const bottomFixedContainer = css`
    width: 100%;
    bottom: 0;
  `
const messageContainer = css`
    /* height: 100%; */
    position: relative;
    
  `
export default class IssueModal extends Component {
  state = {
    message: ''
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  sendMessage = e => {
    // console.log("issueUid: ", this.props.info.issueUid)
    e.preventDefault()
    const date = moment().utc(-264).format('YYYY-MM-DD')
    const dateTime = moment().utc(-264).format('YYYY-MM-DD h:mm A')
    const time = moment().utc(-264).format('h:mm A')
    const postData = {
      date: date,
      dateTime: dateTime,
      message: this.state.message,
      time: time,
      type: "adminMessage",
      userData: {
        name: this.props.adminData.displayName,
        userKey: this.props.adminData.adminID
      }
    }
    if (postData.message.length > 5) {
      const newMessageKey = db.ref(`issues/${this.props.info.issueUid}`).child('messages').push().key
      postData['messageId'] = newMessageKey
      let updates = {}
      updates[`/messages/${newMessageKey}/`] = postData
      this.setState({
        message: ''
      })
      return db.ref(`issues/${this.props.info.issueUid}`).update(updates)
    } else {
      return toast.warn('El mensaje debe tener más de 5 caracteres.')
    }
  }
  render() {
    const { show, onClose, info } = this.props
    if (!show) {
      return null
    }
    const data = {
      image: info.attachments ? info.attachments[0].url : '',
      firstMessage: info.messages ? info.messages[0] ? info.messages[0] : '' : '',
      messages: info.messages ? Object.values(info.messages) : '',
      id: info.issueUid
    }
    return (
      <div className={modalComponent}>
        <div className={modalContainer}>
          <div className={modalHeader}>
            <div>
              <h2>INCIDENCIA</h2>
            </div>
            <div>
              <button className={closeButton}>
                <img src={iconAttachment} alt='icon close' css={`width: 25px; height: 25px;`} />
              </button>
              <button className={closeButton} onClick={onClose}>
                <img src={iconClose} alt='icon close' css={`width: 20px; height: 20px;`} />
              </button>
            </div>
          </div>
          <div className={modalBody}>
            <Attachments image={data.image} />
            <UserMessage first={true} index={data.id} info={data.firstMessage} />
            <div className={divisor}></div>
            <div className={scrollContainer}>
              <div className={`${messageContainer}`} id='content'>
                {
                  data.messages ?
                    data.messages.slice(1).map((message, i) => {
                      const rowLen = data.messages.slice(1).length;
                      if (rowLen === i + 1) {
                        setTimeout(function () {
                          document.getElementById('content').scrollIntoView(false, {
                            behavior: 'smooth'
                          })
                        }, 1)
                      }
                      return <div className={marginBottom} key={i}>
                        <UserMessage
                          index={i}
                          info={message}
                        />
                      </div>
                    })
                    : null
                }
              </div>
            </div>
            <div className={bottomFixedContainer}>
              <MessageInput
                value={this.state.message}
                onChange={this.handleInputChange}
                name='message'
                formName={'messageInput'}
                onSubmit={this.sendMessage}
              />
            </div>
          </div>
        </div>
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    );
  }
}