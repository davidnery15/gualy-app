import React from 'react'
import { css } from 'emotion'
import Input from '../../../general/Input'
import icFilter from './../../../../assets/ic-filter.svg'
import icSort from './../../../../assets/ic-sort.svg'
import addIssueIcon from './../../../../assets/createIssueIcon.svg'
const tableHeader = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0px;
    padding: 20px 25px;
    @media (max-width: 767px) {
      padding: 0  22px 25px 22px;
      width:100%;
      flex-direction: column;
    }
  `
const titleContainer = css`
    width: 40%;
    @media (max-width: 767px) {
      display: none;
    }
    `
const mobileTitleContainer = css`
    display: none;
    @media (max-width: 767px) {
      display: flex;
      margin:10px
    }
  `
const tableHeaderTitle = css`
    font-family: Montserrat;
    font-size: 13px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 1.1px;
    margin: 4px 0;
    padding-top: 7px;
    width: 100%;
    @media (max-width: 767px) {
      padding-top: 15px;
    }
  `
const inputContainer = css`
    width: 60%;
    display: flex;
    justify-content: flex-end;
    @media (max-width: 767px) {
      width: 100%;
      justify-content: center;
      display: flex;
      flex-direction: column;
    }
  `
const cursor = css`
    cursor: pointer
  `
const iconContainer = css`
    display: flex;
    justify-content: flex-end;
    position: relative;
    width: 100%;
    div{
      display: flex;
      align-items:center;
      h6{
        margin: 0 32px 0 0;
        font-family: Montserrat;
        font-size: 12.8px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: right;
        color: #95fad8;
        white-space: nowrap;
        @media (max-width: 970px) {
          display: none;
        }
      }
      img{
        width: 18px;
        height: 18px;
        object-fit: contain;
        margin: 0 32px 0 0;
      }
    }
    @media (max-width: 970px) {
      width: 100%;
    }
    @media (max-width: 767px) {
      justify-content: flex-start;
    }
  `
const orderByContainer = css`
    width: 100px;
    height: 100px;
    position: absolute;
    background-color: #2a2c6a;
    border-radius: 5px;
    box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    z-index: 2;
    top: 41px;
    display: flex;
    flex-direction: column;
    justify-content: center 
  `
const orderByMainContainer = css`
    position: relative;
  `
const firstOrderByContainer = css`
    height: 50%;
    cursor: pointer;
    width: 100%;
    p{
      margin: auto;
      font-family: Montserrat;
      font-size: 12.8px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
    }
  `
const secondOrderByContainer = css`
    height: 50%;
    cursor: pointer;
    width: 100%;
    p{
      margin: auto;
      font-family: Montserrat;
      font-size: 12.8px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
    }
  `
  const darkBackground = css`
  border: 2px #95fad8 solid;
  border-radius: 12px;
  `
const TableHeaderIssue = ({
  title,
  updateSearch,
  search,
  placeholder,
  filter,
  store,
  openCreateIssue,
  isOrderBy,
  toggleOrderBy,
  orderByNewers,
  orderByOlders,
  reverseNewers,
  isFilterBy,
  filterBySolves,
  filterByPending,
  filterByAll,
  toggleFilterBy,
  onKeyPress,
  isSearchingData,

  ranges,
  dateRangePickerIsOpen,
  dateRangeFilterHandleSelect,
  searchDateRange,
  openDateRangeModal,
  isBackButtonShow,
  goBackButtonClick,
  isFilterAll,
  isFilterBySolves,
  isFilterByPending,
}) => {
  const filterText = isFilterAll ? "all" : isFilterBySolves ? "Solve" : isFilterByPending ? "Pending" : "all"
  const status = isFilterAll ? "(Todos)" : isFilterBySolves ? "(Resueltos)" : isFilterByPending ? "(Pendientes)" : "(Todos)"
  const isOrderName = reverseNewers ? "(Mas nuevos)" : "(Mas viejos)"
  return (
    <div className={tableHeader}>
      <div className={titleContainer}>
        <h3 className={tableHeaderTitle}>{title}</h3>
      </div>
      <div className={mobileTitleContainer}>
        <h3 className={tableHeaderTitle}>{title}</h3>
      </div>
      <div className={`${inputContainer}`}>
        {filter ?
          (
            <div className={iconContainer}>
              <div className={cursor} onClick={openCreateIssue}>
                <img css={`margin-right: 6px !important;@media(max-width: 970px){margin: 0px 32px 0 0 !important;}; `}
                  src={addIssueIcon} alt="Add issue" />
                <h6>Agregar&nbsp;incidencia</h6>
              </div>
              <div className={orderByMainContainer}>
                <h6 className={cursor} onClick={toggleOrderBy} >{`Ordenar por ${isOrderName}`}</h6>
                <img className={cursor} onClick={toggleOrderBy} src={icSort} alt="Filter Icon" />
                {
                  isOrderBy ?
                    <div className={orderByContainer}>
                      <div 
                      onClick={orderByNewers} 
                      className={`${reverseNewers ? darkBackground : ''} 
                      ${firstOrderByContainer}`}
                      >
                        <p>Mas nuevos</p>
                      </div>
                      <div 
                      onClick={orderByOlders} 
                      className={`${!reverseNewers ? darkBackground : ''} 
                      ${secondOrderByContainer}`}
                      >
                        <p>Mas viejos</p>
                      </div>
                    </div>
                    : null
                }
              </div>
              <div>
                <h6 className={cursor} onClick={toggleFilterBy}>{`Filtrar por ${status}`}</h6>
                <img className={cursor} onClick={toggleFilterBy} src={icFilter} alt="Sort Icon" />
                {
                  isFilterBy ?
                    <div className={orderByContainer}>
                      <div 
                      onClick={filterByAll} 
                      className={`${filterText === 'all' ? darkBackground : ''} 
                      ${firstOrderByContainer}`}
                      >
                        <p>Todos</p>
                      </div>
                      <div 
                      onClick={filterByPending} 
                      className={`${filterText === 'Pending' ? darkBackground : ''} 
                      ${firstOrderByContainer}`}
                      >
                        <p>Pendientes</p>
                      </div>
                      <div 
                      onClick={filterBySolves} 
                      className={`${filterText === 'Solve' ? darkBackground : ''} 
                      ${firstOrderByContainer}`}
                      >
                        <p>Resueltos</p>
                      </div>
                    </div>
                    : null
                }
              </div>
            </div>
          ) : null
        }
        <Input
          dateRangePickerIsOpen={dateRangePickerIsOpen}
          dateRangeFilterHandleSelect={dateRangeFilterHandleSelect}
          ranges={ranges}
          searchDateRange={searchDateRange}
          openDateRangeModal={openDateRangeModal}
          isBackButtonShow={isBackButtonShow}
          goBackButtonClick={goBackButtonClick}
          icon={true}
          updateSearch={updateSearch}
          search={search}
          placeholder={placeholder}
          extraImg={(store)}
          onKeyPress={onKeyPress}
          isSearchingData={isSearchingData}
        />
      </div>
    </div>
  )
}

export default TableHeaderIssue