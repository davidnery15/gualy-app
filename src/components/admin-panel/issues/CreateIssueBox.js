import React from 'react'
import { css } from 'emotion'
import arrowBack from '../../../assets/arrowBack.svg'
const component = css`
margin: auto;
margin-top: 50px;
margin-bottom: 50px;
border-radius: 5px;
width: 1300px;
max-width: 1300px;
background-color: #2a2c6a;
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
@media (max-width: 1300px) {
    width: 95%;
}
@media (max-width: 1230px) {
margin-top: 40px;
}
`
const header = css`
margin: 0px;
padding: 15px 25px;
display: flex;
h1{
margin: 0px;
font-size: 13px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: 1.1px;
color: #f6f7fa;
margin-top: 7px;
}
@media(max-width: 600px){
flex-direction: column
}
`
const divisor = css`
height: 1px;
width: 100%;
padding: 0;
margin: 0;
opacity: 0.9;
background-color: #1d1f4a;
`

const adminNameContainer = css`
margin-left: auto !important;
display: flex;
@media(max-width: 600px){
    flex-direction: column;
    margin: auto;
}
`
const goBackButton = css`
border: none;
background: transparent;
height: 30px;
width: 30px;
margin-right: 12px;
cursor: pointer;
`
const adminNameClass = css`
font-size: 18px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: 0.83;
letter-spacing: normal;
color: #f6f7fa;
margin: auto;
margin-left: 6px;
@media(max-width: 600px){
text-align: center;
margin: auto;
margin-top: 9px;
}
`
const adminNameTitle = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.14;
letter-spacing: 1.2px;
color: #f6f7fa;
@media(max-width: 600px){
    text-align: center;
    }
`
const titleStyle = css`
@media(max-width: 600px){
    text-align: center
}
`
const arrowBackStyles = css`
width: 100%;
height: 100%
`
export default ({ title, goBackFunction, children, adminName, userDescription }) =>
    <div className={component}>
        <div className={header}>
            <button className={goBackButton} onClick={goBackFunction}>
                <img
                    src={arrowBack}
                    alt=""
                    className={arrowBackStyles}
                />
            </button>
            <h1 className={titleStyle}> {title} </h1>
            <div className={adminNameContainer}>
                <h1 className={adminNameTitle}> {userDescription}</h1><p className={adminNameClass}>{adminName}</p>
            </div>
        </div>
        <div className={divisor}></div>
        {children}
    </div>

