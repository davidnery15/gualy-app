import React from 'react'
import { css } from 'emotion'
const CustomTextArea = (props) => {
  const CustomInput = css`
  border-radius: 23px;
  border: solid 2px #95fad8;
  height: ${props.height};
  width: ${props.width};
  margin: auto;
  padding-top: 20px;
  padding-left: 20px;
  background: transparent;
    `
  return (
    <textarea
      {...props}
      className={CustomInput}
      required
    />
  )
}
export default CustomTextArea