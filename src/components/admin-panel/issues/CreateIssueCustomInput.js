import React from 'react'
import { css } from 'emotion'
const CustomInput = (props) => {
  const CustomInput = css`
  border-radius: 23px;
  border: solid 2px #95fad8;
  height: ${props.height};
  width: ${props.width};
  margin: auto;
  padding-left: 20px;
  `
  return (
    <input
      {...props}
      className={CustomInput}
      required
    />
  )
}
export default CustomInput