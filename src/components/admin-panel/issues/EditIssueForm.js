import React from 'react'
import CustomDropdown from '../../general/CustomDropdown'
import UserMessage from './issues-card-table/IssueModal/UserMessage'
import Attachments from './issues-card-table/IssueModal/Attachments'
import sendIcon from './../../../assets/Group.svg'
import { css } from 'emotion'
const formsContainer = css`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    width: 100%;
    @media(max-width: 1000px){
      flex-direction: column
    }
  `
const firstFormSection = css`
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 30%;
  max-width: 350px;
  @media(max-width: 1000px){
    width: 100%;
    max-width: 100%;
  }
  `
const secondFormSection = css`
display: flex;
justify-content: center;
flex-direction: column;
max-width: 600px;
width: 100%;
max-width: 100%;
  `
const thirdFormSection = css`
display: flex;
justify-content: center;
flex-direction: column;
max-width: 350px;
width: 100%;
max-width: 100%;
  `
const body = css`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 15px 20px;
  margin-bottom: 0px;
  @media (max-width: 970px) {
  flex-direction: column;
  }
  `
const selectedNameContainer = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    @media(max-width: 1000px){
      width: 100%;
    }
`
const selectedPhoneContainer = css`
    display: flex;
    margin-left: auto;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    @media(max-width: 1000px){
      width: 100%;
    }
`
const selectedIdNumberContainer = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    @media(max-width: 1000px){
      width: 100%;
    }
`
const selectedTitles = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.14;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
`
const dropdownTitles = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.14;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
margin-bottom: -20px;
margin-top: 20px;
`
const selectedText = css`
font-size: 18px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 0.89;
letter-spacing: 1.5px;
text-align: center;
color: #f6f7fa;
margin-bottom: 70px;
`
const selectedContainer = css`
display: flex;
margin-top: 20px
@media(max-width: 600px){
  flex-direction: column
}
`
const bottomFixedContainer = css`
    width: 100%;
    bottom: 0;
  `
const scrollContainer = css`
    width: 100%;
    height: 100%;
    padding: 0 30px;
    overflow-x: hidden;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 3px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 2px;
    }
    ::-webkit-scrollbar {
      width: 7px;
    }
  `
const marginBottom = css`
    margin-bottom: 20px;
  `
const messagesBody = css`
    width: 100%;
    min-height: 200px;
    max-height: 350px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    position: relative;
    margin-top: 20px;
    position: relative;
  `
const customInput = css`
    border-radius: 28px;
    background-color: #ffffff;
    padding: 0 20px;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    text-align: left;
    color: #707070;
  `
const component = css`
    width: 100%;
    padding: 10px 15px;
    display: flex;
    justify-content: space-between;
    background-color: #242657;
  `
const sendIconCSS = css`
    width: 40px;
    height: 40px;
    object-fit: contain;
    margin-left: 18px;
  `
const secondThirdSeccionContainer = css`
    width: 70%;
    @media(max-width: 1000px){
      width: 100%;
    }
  `
const noComments = css`
  text-align: center;
  font-size: 20px;
  position: absolute;
  left: 0; 
  right: 0; 
  margin-left: auto; 
  margin-right: auto; 
  width: 300px;
  margin-top: 40px;
  `
const buttonsContainer = css`
  display: flex;
  flex-direction: row;
  `
const firstButton = css`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.07;
  letter-spacing: 0.6px;
  text-align: center;
  color: #ffffff;
  margin: 20px;
  cursor: pointer;
  p{
    margin: 10px
  }
  `
const secondButton = css`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.07;
  letter-spacing: 0.6px;
  text-align: center;
  color: #ffffff;
  margin: 20px 20px 20px 30px;
  cursor: pointer;
  p{
    margin: 10px
  }
  `
const borders = css`
  border-radius: 21px;
  border: solid 2px #95fad8;
  `
export default ({
  displayErrors,
  editIssueLoading,
  handleInputChange,
  handleEditIssueChange,
  sended,
  selectedIssue,
  selectedUser,
  users,
  getOptions,
  message,
  sendMessage,
  handleKeyPress,
  goPublicAsnwer,
  goInternNote,
  isInternNote,
  response,
  sendResponse,
  userEmail
}) =>
  <div >
    <div className={`${(displayErrors === true ? displayErrors : '')} ${body}`}>
      <div className={formsContainer}>
        {/* FIRST SECTION */}
        <div className={firstFormSection}>
          <Attachments image={selectedIssue.attachments[0] ? selectedIssue.attachments[0].url : ''} />
        </div>
        <div className={secondThirdSeccionContainer}>
          {/* SECOND SECTION */}
          <div className={secondFormSection}>
            <div className={selectedContainer}>
              <div className={selectedNameContainer}>
                <h1 className={selectedTitles}>Correo</h1>
                <p className={selectedText}>{selectedUser ? selectedUser.email : userEmail}</p>
              </div>
              <div className={selectedIdNumberContainer}>
                <h1 className={selectedTitles}>C.I</h1>
                <p className={selectedText}>{selectedUser ? selectedUser.dni.id : 'No registrado'}</p>
              </div>
              <div className={selectedPhoneContainer}>
                <h1 className={selectedTitles}>Telefono</h1>
                <p className={selectedText}>{selectedUser ? selectedUser.phone : 'No registrado'}</p>
              </div>
            </div>
          </div>
          {/* THIRD SECTION */}
          <div className={thirdFormSection}>
            <UserMessage
              first={true}
              HeaderText="Comentario del Usuario"
              index={selectedIssue.id}
              info={selectedIssue.messages ? selectedIssue.messages[0] : ''} />
            <h1 className={dropdownTitles}>Estado</h1>
            <CustomDropdown
              value={selectedIssue.status}
              handleChange={handleEditIssueChange}
              disabled={editIssueLoading}
              displayErrors={displayErrors}
              name='status'
            >
              <option value="" disabled defaultValue>Estado de Incidencia</option>
              <option value="Pending">Pendiente</option>
              <option value="Resolved">Resuelto</option>
            </CustomDropdown>
            <h1 className={dropdownTitles}>Asignar departamente</h1>
            <CustomDropdown
              value={selectedIssue.departament}
              handleChange={handleEditIssueChange}
              disabled={selectedIssue.status === 'Resolved' || editIssueLoading}
              displayErrors={displayErrors}
              name='departament'
            >
              <option value="" disabled defaultValue>Seleciona un departamento</option>
              <option value="CLIENTS">Clientes</option>
              <option value="COMMERCES">Comercios</option>
              <option value="SUPPORTS">Soporte</option>
            </CustomDropdown>
            <h1 className={dropdownTitles}>Asignar agente</h1>
            <CustomDropdown
              // value={selectedIssue.assigned}
              handleChange={handleEditIssueChange}
              displayErrors={displayErrors}
              disabled={selectedIssue.status === 'Resolved' || editIssueLoading}
              name='assigned'
            >
              <option value="" defaultValue>Asignar a</option>
              {
                getOptions(Object.values(users) || {})
              }
            </CustomDropdown>
            <h1 className={dropdownTitles}>Asignados</h1>
            <CustomDropdown
              // value={selectedIssue.assigned}
              handleChange={handleEditIssueChange}
              displayErrors={displayErrors}
              disabled={selectedIssue.status === 'Resolved' || editIssueLoading}
              name='assigned-users'
            >
              <option value="" defaultValue></option>
              {
                getOptions(Object.values(selectedIssue.assigned || {}))
              }
            </CustomDropdown>
          </div>
        </div>
      </div>
    </div>
    <div className={buttonsContainer}>
      <div onClick={goInternNote} className={`${secondButton} ${isInternNote ? borders : ""}`}>
        <p>NOTAS INTERNAS</p>
      </div>
      <div onClick={goPublicAsnwer} className={`${firstButton} ${!isInternNote ? borders : ""}`}>
        <p>PUBLICAR RESPUESTA</p>
      </div>
    </div>
    {
      isInternNote ?
        <div className={messagesBody}>
          <div className={scrollContainer}>
            <div id='content'>
              {
                Object.values(selectedIssue.messages).length > 1 ?
                  Object.values(selectedIssue.messages).slice(1).map((message, i) => {
                    const rowLen = Object.values(selectedIssue.messages).slice(1).length;
                    if (rowLen === i + 1 && sended) {
                      setTimeout(function () {
                        document.getElementById('content').scrollIntoView(false, {
                          behavior: 'smooth'
                        })
                      }, 1)
                    }
                    return !message.timestamp ? <div className={marginBottom} key={i}>
                      <UserMessage
                        index={i}
                        info={message}
                      />
                    </div>
                      : null
                  })
                  :
                  <h1 className={noComments}>
                    No hay comentarios todavia
                </h1>
              }
            </div>
          </div>
          {
            selectedIssue.status === 'Pending' ?
              <div className={bottomFixedContainer}>
                <div className={component} >
                  <input
                    className={customInput}
                    type="text"
                    onKeyPress={handleKeyPress}
                    disabled={editIssueLoading}
                    placeholder="Enviar"
                    name="message"
                    onChange={handleInputChange}
                    value={message}
                  />
                  <button
                    className='btn-transparent'
                    disabled={editIssueLoading}
                    onClick={sendMessage}
                  >
                    <img
                      src={sendIcon}
                      alt="Send Icon"
                      className={sendIconCSS} />
                  </button>
                </div>
              </div>
              : null
          }
        </div>
        :
        <div className={messagesBody}>
          <div className={scrollContainer}>
            <div id='content'>
              {
                Object.values(selectedIssue.messages).length > 1 ?
                  Object.values(selectedIssue.messages).slice(1).map((message, i) => {
                    const rowLen = Object.values(selectedIssue.messages).slice(1).length;
                    if (rowLen === i + 1 && sended) {
                      setTimeout(function () {
                        document.getElementById('content').scrollIntoView(false, {
                          behavior: 'smooth'
                        })
                      }, 1)
                    }
                    return message.timestamp ? <div className={marginBottom} key={i}>
                      <UserMessage
                        index={i}
                        info={message}
                      />
                    </div>
                      : null
                  })
                  :
                  <h1 className={noComments}>
                    No hay comentarios todavia
                </h1>
              }
            </div>
          </div>
          {
            selectedIssue.status === 'Pending' ?
              <div className={bottomFixedContainer}>
                <div className={component} >
                  <input
                    className={customInput}
                    type="text"
                    onKeyPress={handleKeyPress}
                    disabled={editIssueLoading}
                    placeholder="Enviar"
                    name="response"
                    onChange={handleInputChange}
                    value={response}
                  />
                  <button
                    disabled={editIssueLoading}
                    className='btn-transparent'
                    onClick={sendResponse}
                  >
                    <img
                      src={sendIcon}
                      alt="Send Icon"
                      className={sendIconCSS} />
                  </button>
                </div>
              </div>
              : null
          }
        </div>
    }
  </div>