import React from 'react'
import Button from '../../general/AcceptButton'
import CustomInput from './CreateIssueCustomInput'
// import CustomTextArea from './CreateIssueTextAreaCustom'
import UploadImageButton from '../../general/UploadImageButton'
import CustomDropdown from '../../general/CustomDropdown'
import { css } from 'emotion'
import errorSVG from './../../../assets/ic-info-red.svg'
const formsContainer = css`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    width: 100%;
    @media(max-width: 1000px){
      flex-direction: column
    }
  `
const firstFormSection = css`
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 30%;
  max-width: 350px;
  @media(max-width: 1000px){
    width: 100%;
    max-width: 100%;
  }
  `
const secondFormSection = css`
width: 40%;
display: flex;
justify-content: center;
flex-direction: column;
max-width: 600px;
@media(max-width: 1000px){
  width: 100%;
  max-width: 100%;
}
  `
const thirdFormSection = css`
width: 30%;
display: flex;
justify-content: center;
flex-direction: column;
max-width: 350px;
@media(max-width: 1000px){
  width: 100%;
  max-width: 100%;
}
  `
const firstFormTitle = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.14;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
  `
const body = css`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 15px 20px;
  margin-bottom: 0px;
  @media (max-width: 970px) {
  flex-direction: column;
  }
  `
const createIssueButtonContainer = css`
margin: 50px;
text-align: center;
@media(max-width:400px){
  margin: auto;
  margin-top: 50px;
  margin-bottom: 50px;
}
  `
const selectedNameContainer = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    @media(max-width: 1000px){
      width: 100%;
    }
`
const selectedPhoneContainer = css`
    display: flex;
    margin-left: auto;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    @media(max-width: 1000px){
      width: 100%;
    }
`
const selectedIdNumberContainer = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    @media(max-width: 1000px){
      width: 100%;
    }
`
const selectedTitles = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.14;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
`
const selectedText = css`
font-size: 18px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 0.89;
letter-spacing: 1.5px;
text-align: center;
color: #f6f7fa;
margin-bottom: 70px;
`
const selectedContainer = css`
display: flex;
margin-top: 20px
`
const displayErrorsClass = css`
  input:invalid {
    border-color:red !important;
  }
  textarea:invalid {
    border-color:red !important;
  }
`
const secondSectionFormsContainer = css`
display: flex;
justify-content: center;
flex-direction: column;
input{
  margin-bottom: 20px
}
textarea{
  margin-bottom: 20px
}
`
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
export default ({
  displayErrors,
  handleSubmit,
  handleInputChange,
  encodeImageFileAsURL,
  getOptions,
  users,
  selectedIdNumber,
  selectedName,
  selectedPhone,
  email,
  category,
  departament,
  assigned,
  issueTitle,
  description,
  internNote,
  image,
  imageError,
  ImgURI,
  status,
  createIssueLoading,
  errorMessage
}) =>
  <form
    noValidate
    autoComplete='off'
    onSubmit={handleSubmit}
    id="createIssueForm">
    <div className={`${(displayErrors === true ? displayErrorsClass : '')} ${body}`}>
      <div className={formsContainer}>
        {/* FIRST SECTION */}
        <div className={firstFormSection}>
          <h1 className={firstFormTitle}>DATOS DEL USUARIO O COMERCIO</h1>
          <div css={`padding-bottom:5px;`} className='group mb-1'>
            <input
              type='text'
              placeholder='Correo electrónico *'
              name='email'
              onChange={handleInputChange}
              value={email}
              required
            />
            <span className='highlight'></span>
            <span className='bar'></span>
          </div>
          <CustomDropdown
            value={category}
            handleChange={handleInputChange}
            displayErrors={displayErrors}
            name='category'
          >
            <option value="" disabled defaultValue>Seleciona una categoria</option>
            <option value="SEND_PAYMENT">Enviar Pago</option>
            <option value="RECEIVE_MONEY">Solicitar Pago</option>
            <option value="ADD_MONEY">Añadir Saldo</option>
            <option value="WITHDRAW">Retirar Saldo</option>
            <option value="PAYMENT_METHODS">Metodos de Pago</option>
            <option value="BANK_ACCOUNTS">Cuentas Bancarias</option>
            <option value="MY_PROFILE">Mi Perfil</option>
            <option value="COMMERCES">Comercios</option>
          </CustomDropdown>
          <CustomDropdown
            value={departament}
            handleChange={handleInputChange}
            displayErrors={displayErrors}
            name='departament'
          >
            <option value="" disabled defaultValue>Seleciona un departamento</option>
            <option value="CLIENTS">Clientes</option>
            <option value="COMMERCES">Comercios</option>
            <option value="SUPPORTS">Soporte</option>
          </CustomDropdown>
        </div>
        {/* SECOND SECTION */}
        <div className={secondFormSection}>
          {
            selectedIdNumber && selectedName ?
              <div className={selectedContainer}>
                <div className={selectedNameContainer}>
                  <h1 className={selectedTitles}>Nombre</h1>
                  <p className={selectedText}>{selectedName}</p>
                </div>
                <div className={selectedIdNumberContainer}>
                  <h1 className={selectedTitles}>C.I</h1>
                  <p className={selectedText}>{selectedIdNumber}</p>
                </div>
              </div>
              : null
          }
          <div className={secondSectionFormsContainer}>
            <CustomInput
              name="description"
              height="45px"
              width="90%"
              value={description}
              placeholder="Titulo de Incidencia"
              onChange={handleInputChange}
            />
            {/* <CustomTextArea
              name="description"
              height="133px"
              width="90%"
              value={description}
              placeholder="Descripción"
              onChange={handleInputChange}
            /> */}
            {/* <div className={selectedNameContainer}>
              <h1 className={selectedTitles}>Nota interna</h1>
            </div> */}
            {/* <CustomTextArea
              name="internNote"
              height="133px"
              width="90%"
              value={internNote}
              placeholder="Nota iterna"
              onChange={handleInputChange}
            /> */}
          </div>
        </div>
        {/* THIRD SECTION */}
        <div className={thirdFormSection}>
          {
            selectedPhone ?
              <div className={selectedContainer}>
                <div className={selectedPhoneContainer}>
                  <h1 className={selectedTitles}>Telefono</h1>
                  <p className={selectedText}>{selectedPhone}</p>
                </div>
              </div>
              : null
          }
          <UploadImageButton
            image={image}
            width="200px"
            height="200px"
            imageError={imageError}
            name="ImgURI"
            onChange={encodeImageFileAsURL}
            imageURI={ImgURI}
          />
          <CustomDropdown
            value={status}
            handleChange={handleInputChange}
            displayErrors={displayErrors}
            name='status'
          >
            <option value="" disabled defaultValue>Estado de Incidencia</option>
            <option value="Pending">Pendiente</option>
            <option value="Resolved">Resuelto</option>
          </CustomDropdown>
        </div>
      </div>
    </div>
    {
      displayErrors ?
        <div className={errorContainer}>
          {errorMessage}
          <img
            className={errorSvgCss}
            src={errorSVG}
            alt="error Icon"
          />
        </div>
        : null
    }
    <div className={createIssueButtonContainer}>
      <Button
        type="submit"
        width="226px"
        height="38px"
        content="CREAR INCIDENCIA"
        loading={createIssueLoading}
      />
    </div>
  </form>