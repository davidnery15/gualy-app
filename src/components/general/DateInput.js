import React, { Component } from 'react'
import { css } from 'emotion'
const range = n => Array.from(Array(n), (_, i) => i)
const selectInput = css`
    outline: 0
    font-size: 14px !important;
    background-color: transparent;
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAQCAYAAADAvYV+AAAAAXNSR…gagPyLQOyIrBCkBmwyiAECSDZooSuEqEAjQRpAnkYTHrJcAMmHSf4vzaI/AAAAAElFTkSuQmCC);
    background-repeat: no-repeat;
    background-position: right;
    background-origin: content-box;
    height: 22px;
    width: 65px;
    margin-right: 0;
    padding-left: 7% !important;
    color: #bebebe !important;
    border: none;
`
const selectInputYear = css`
        font-size: 14px !important;
        background-color: transparent;
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAQCAYAAADAvYV+AAAAAXNSR…gagPyLQOyIrBCkBmwyiAECSDZooSuEqEAjQRpAnkYTHrJcAMmHSf4vzaI/AAAAAElFTkSuQmCC);
        background-repeat: no-repeat;
        background-position: right;
        background-origin: content-box;
        height: 22px;
        width: 75px;
        padding-left: 10% !important;
        color: #bebebe !important;
        border: none;
`
const dateInputContainer = css`
    display: flex;
    position: relative;
    justify-content: center;
    width: 200px;
    margin: auto;
    option {
        color: black
    }
    option:first-child {
    color: #ccc;
}
`
/*const firstBackSlash = css`
        width: 0px;
        color: #bebebe;
        font-size: 18px;
        height: 0;
        bottom: 23px;
        position: absolute;
        left: 67px;
`
const secondBackSlash = css`
            width: 0px;
            color: #bebebe;
            font-size: 18px;
            height: 0;
            bottom: 25px;
            position: absolute;
            right: 65px;
`*/
const redBorder = css`
border-bottom: 1px solid red;
padding-bottom: 2px;
`

class DateInput extends Component {
    state = {
        day: "",
        month: "",
        year: "",
    }
    onChange = async ({ target: { name, value } }) => {
        await this.setState({ [name]: value })
        this.props.updateDate({
            day: this.state.day,
            month: this.state.month,
            year: this.state.year,
            currentName: this.props.currentName,
            formType: this.props.formType,
            formIndex: this.props.formIndex,
        })
    }
    generateYears = (from, to) => range(from + to).map(i => (new Date()).getFullYear() - from + i)
    render() {
        const {isValid, disabled} = this.props
        return (
            <div className={`${dateInputContainer} ${!isValid ? redBorder : "" }`}>
                <select
                    style={{ borderRight: "0" }}
                    name="day"
                    value={this.state.day}
                    className={selectInput}
                    onChange={this.onChange}
                    disabled={disabled}
                    >
                    <option disabled value="">dd</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                </select>
                <select
                    style={{ borderRight: "0", borderLeft: "0" }}
                    name="month"
                    value={this.state.month}
                    className={selectInput}
                    onChange={this.onChange}
                    disabled={disabled}
                    >
                    <option disabled value="">mm</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select
                    style={{ borderLeft: "0" }}
                    name="year"
                    value={this.state.year}
                    className={selectInputYear}
                    onChange={this.onChange}
                    disabled={disabled}
                    >
                    <option disabled value="">yyyy</option>
                    {
                        this.generateYears(100,1).map(y => <option key={y} value={y}>{y}</option>).reverse()
                    }
                </select>
            </div>
        )
    }
}

export default DateInput 