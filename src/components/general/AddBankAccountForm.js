import { css } from 'emotion'
import React, { Component } from 'react'
import iconReject from './../../assets/help-icons/icon-bank-blue.svg'
import addIcon from './../../assets/addIcon.svg'
import DeleteBankAccountModal from '../general/modals/DeleteBankAccountModal'
import { customConsoleLog } from '../../utils/customConsoleLog'
import { banks } from '../../constants/bankLogos'
import { db } from './../../firebase'
import { toast } from 'react-toastify'
import Spinner from 'react-spinkit'
import AddBankAccountModal from '../general/modals/AddBankAccountModal'
import { postRequest } from '../../utils/createAxiosRequest'
const bankAccountsHeader = css`
  position: absolute;
  top: -6px;
  left: 2px;
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
  `
const editButtonContainerAdd = css`
  display: flex;
  flex-direction: row;
  position: absolute;
  right: 20px;
  top: 14px;
  cursor: pointer
  `
const spinnerContainer = css`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
`
const banksCardsContainer = css`
display: flex;
justify-content: center;
flex-direction: row;
position: relative;
overflow: auto;
overflow-y: hidden;
height: 100%;
  `
const absotuleCardsContainer = css`
  display: flex;
  flex-direction: row;
  position: absolute;
    left: 0;
  `
const bankCard = css`
margin: 10px
    width: 180px;
    height: 180px;
border-radius: 3.3px;
box-shadow: 0 7px 7px 0 rgba(0,0,0,0.15), 0 0 7px 0 rgba(0,0,0,0.12);
border: solid 0.5px transparent;
background-color: white;
cursor: pointer;
padding: 10px 3px;
display: flex;
flex-direction: column;
justify-content: space-between;
position: relative;
outline: none !important;
div{
  padding: 0 5px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #242656;
  text-align: left;
  width: 104px;
  direction: rtl;
}
span {
  font-size: 9.9px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  letter-spacing: normal;
  text-align: left;
}
`
const cssChange = css`
-webkit-box-shadow: inset 0px 0px 0px 10px #20c997;
-moz-box-shadow: inset 0px 0px 0px 10px #20c997;
box-shadow: inset 0px 0px 0px 5px #20c997;
background-color: white;
border: 1px solid;
`
const bankLogoCSS = css`
width: 104.4px;
    height: 88.5px;
object-fit: contain;
`
const bankCardHoverOrFocus = css({
  ':hover,:focus': cssChange
})
const addIconStyle = css`
width: 17.6px;
  height: 17.5px;
  `
const deleteBankModal = css`
    min-height: auto
`
const displayErrorsClass = css`
    input:invalid {
      border-color:red;
    }
    input:invalid specficError 
  `
const horizontalDivisor = css`
height: 1px;
width: 100%;
padding: 0;
margin: 0;
opacity: 0.9;
background-color: #1d1f4a;
margin-top: 23px;
  `
const BankCard = ({ logo, bankAccountNumber, onClick, selectedBankAccount, bankAccountID }) => {
  return (
    <button
      className={`${bankCard} ${bankCardHoverOrFocus} ${(bankAccountID === selectedBankAccount ? cssChange : '')}`}
      onClick={onClick}>
      <img className={bankLogoCSS} src={logo} alt='bank-logo' />
      <div css={`width: 80px !important; direction: ltr !important;position: absolute;bottom: 24px;`}>
        <span css={`font-weight: normal !important`}>Cuenta No.</span>
      </div>
      <div css={`width: 98px !important;`}>
        <span>{bankAccountNumber}</span>
      </div>
    </button>
  )
}
const getBankLogo = (bankAccountNumber) => {
  const bankId = bankAccountNumber.slice(0, 4)
  const filteredLogo = banks.filter(item => item.id === bankId)[0]
  const filteredLogoPhoto = filteredLogo ? filteredLogo.photo : iconReject
  return filteredLogoPhoto
}

export default class AddBankAccountForm extends Component {
  state = {
    deleteModalIsOpen: false,
    modalIsOpen: false,
    isLoading: false,
    errorMessage: "",
    accountNumber: "",
    displayErrors: false
  }
  //HANDLE INPUTS
  onInputChange = ({ target }) => {
    const value = target.name === 'accountNumber' &&
      target.value.length > 20
      ? target.value.slice(0, -1)
      : target.value
    this.setState({
      [target.name]: value
    })
  }
  //SHOW EDIT BANK ACCOUNT MODAL
  deleteModalShow = config => (event) => {
    event.preventDefault();
    this.setState({ deleteModalIsOpen: !this.state.deleteModalIsOpen, selectedBankAccount: config.bankKey })
  }
  //SHOW EDIT BANK ACCOUNT MODAL
  closeDeleteModal = () => {
    this.setState({ deleteModalIsOpen: !this.state.deleteModalIsOpen })
  }
  renderBankCards = bankAccounts => {
    if (bankAccounts) {
      return Object.values(bankAccounts).map((bankAccount, i) => {
        return <BankCard
          bankAccountNumber={bankAccount.bankAccountNumber}
          bankAccountID={bankAccount.bankAccountID}
          onClick={!this.props.readyOnly
            ? this.props.isAddCommerce
              ? this.props.deleteBankAccountModalShow({bankKey: bankAccount.bankAccount})
              : this.deleteModalShow({ bankKey: bankAccount.bankAccountID })
            : null}
          logo={getBankLogo(bankAccount.bankAccountNumber)}
          key={bankAccount.bankAccountNumber}
        />
      })
    } else {
      return <h1>No hay cuentas registradas</h1>
    }
  }
  deleteBankAccount = (event) => {
    event.preventDefault()
    this.setState({ isLoading: true }, () => {
      setTimeout(() => {
        let bankAccount =
          db.ref()
            .child(`users/${this.props.userInfo.userKey}`)
            .child('bankAccount')
            .child(this.state.selectedBankAccount)
        bankAccount.remove()
        this.setState({ isLoading: false, deleteModalIsOpen: false })
      }, 2000)
    })
  }
  //SHOW ADD BANK ACCOUNTMODAL
  modalShow = () => {
    if (this.state.modalIsOpen) {
      this.setState({
        accountNumber: '',
        displayErrors: false
      })
    } else {
      this.setState({
        modalIsOpen: true,
      })
    }
    this.setState({ modalIsOpen: !this.state.modalIsOpen })
  }
  //ADD BANK ACCOUNT
  addBankAccount = async (event) => {
    event.preventDefault()
    const bankId = this.state.accountNumber.slice(0, 4)
    const filteredLogo = banks.filter(item => item.id === bankId)[0]
    this.toastAddBankId = toast('Agregando cuenta...', { autoClose: false })
    this.setState({ isLoading: true, displayErrors: false, errorMessage: '' })
    if (
      this.state.accountNumber.length === 20 &&
      filteredLogo
    ) {
      try {
        const data = {
          uid: this.props.userInfo.userKey,
          bankAccountNumber: this.state.accountNumber,
          bankPicture: ''
        }
        let response = await postRequest('bankAccount/addBankAccount', data)
        if (response.data.success) {
          this.setState({
            isLoading: false,
            accountNumber: '',
            idnumber: '',
            IdType: '',
            firstName: '',
            secondFirstName: '',
            lastName: '',
            secondLastName: '',
            modalIsOpen: false,
            displayErrors: false
          })
          toast.update(this.toastAddBankId, {
            render: 'Su cuenta bancaria ha sido agregada exitosamente.',
            autoClose: 5000,
          })
        } else {
          this.setState({
            isLoading: false,
            errorMessage: response.data.error.message
          })
          toast.update(this.toastAddBankId, {
            render: response.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        this.setState({
          isLoading: false,
          errorMessage: 'Verifique su conexión y vuelva a intentarlo.'
        })
        toast.update(this.toastAddBankId, {
          render: 'Verifique su conexión y vuelva a intentarlo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        customConsoleLog(error)
      }
    } else {
      this.setState({
        displayErrors: true,
        isLoading: false
      })
      if (this.state.accountNumber.length !== 20) {
        this.setState({ errorMessage: 'Nro de cuenta debe ser de 20 dijitos' })
        return toast.update(this.toastAddBankId, {
          render: 'Nro de cuenta debe ser de 20 dijitos',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!filteredLogo) {
        this.setState({ errorMessage: 'Nro de cuenta invalida.' })
        return toast.update(this.toastAddBankId, {
          render: 'Nro de cuenta invalida.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  render() {
    const {
      bankAccount,
      maxWidth = "520px",
      minWidth = "50%",
      userInfo,
      //commerces
      readyOnly = false,
      //add commerce
      addBankAccount = this.addBankAccount,
      deleteBankAccount = this.deleteBankAccount,
      addCommerceAccountNumber = this.state.accountNumber,
      errorMessage = this.state.errorMessage,
      displayErrors = this.state.displayErrors,
      loading = this.state.isLoading,
      addBankAccountModalIsOpen = this.state.modalIsOpen,
      deleteBankAccountModalIsOpen = this.state.deleteModalIsOpen,
      deleteBankAccountModalClose = this.closeDeleteModal,
      addBankAccountModalShow = this.modalShow,
      isAddCommerce,
      onChange = this.onInputChange,
    } = this.props
    const banksDataContainer = css`
  margin: auto;
  margin-top: 50px;
  margin-bottom: 30px;
  border-radius: 5px;
  background-color: #2a2c6a;
  -webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
  box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
  padding-top: 21px;
  display: -ms-flexbox;
  display: flex;
  height: auto;;
  position: relative;
  width: auto;
  flex-direction: column;
  min-width: ${minWidth};
  height: 262px;
  max-width: ${maxWidth}
  @media(max-width: 1200px){
    width: 90%
  }
    `
    return (
      <div className={banksDataContainer}>
        <div className={bankAccountsHeader}>
          <h1> DATOS BANCARIOS </h1>
        </div>
        {
          !readyOnly
            ? <div onClick={addBankAccountModalShow} className={editButtonContainerAdd}>
              <img src={addIcon} alt="EditIcon" className={addIconStyle}></img>
            </div>
            : null
        }
        <span className={horizontalDivisor}></span>
        {
          !bankAccount
            ?
            <div className={spinnerContainer}> <Spinner color='white' /> </div>
            :
            bankAccount[0]
              ?
              <div className={banksCardsContainer}>
                <div className={absotuleCardsContainer}>
                  {
                    this.renderBankCards(bankAccount)
                  }
                </div>
              </div>
              :
              <span
                style={{ margin: 'auto' }}
              >
                <h1
                  style={{ fontSize: '20px', padding: '20px' }}
                >
                  Ninguna cuenta agregada todavia.
               </h1>
              </span>
        }
        <DeleteBankAccountModal
          show={deleteBankAccountModalIsOpen}
          onClose={deleteBankAccountModalClose}
          width="310px"
          className={deleteBankModal}
          modalShow={deleteBankAccountModalClose}
          onSubmit={deleteBankAccount}
          isLoading={loading}
        />
        <AddBankAccountModal
          width={310}
          show={addBankAccountModalIsOpen}
          onClose={addBankAccountModalShow}
          errorMessage={errorMessage}
          modalShow={addBankAccountModalShow}
          onSubmit={addBankAccount}
          onChange={onChange}
          accountNumber={addCommerceAccountNumber}
          commerceName={userInfo ? userInfo.name : ''}
          idnumber={
            userInfo
              ? userInfo.dni
                ? userInfo.dni.id
                : ''
              : ''
          }
          IdType={
            userInfo
              ? userInfo.dni
                ? userInfo.dni.type
                : ''
              : ''
          }
          displayErrors={displayErrors}
          displayErrorsClass={displayErrorsClass}
          isLoading={loading}
          getBankLogo={getBankLogo(String(addCommerceAccountNumber) || '')}
          isAddCommerce={isAddCommerce}
        />
      </div>
    )
  }
}