import React from 'react'
import { css } from 'emotion'
import Loader from './../general/Loader'
export default ({ loading, content, width, height, className, onClick }) => {
    const AcceptButton = css`
    width: ${width || "100%"};
    height: ${height || "100%"};
    padding: 8px 18px;
    border-radius: 5px;
    border-radius: 100px;
    border: 2px solid #ff6061
    background: transparent;
    cursor: pointer;
    outline: none !important;
    position: relative;
`
    const rejectText = css`
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: center;
    color: #ff6061;
    margin-top: -4px !important;
`
    return (
        <button disabled={loading} onClick={onClick} className={`${AcceptButton} ${className}`}>
            {
                loading ?
                    <Loader color="#ff6061" />
                    : <p className={rejectText}>{content}</p>
            }
        </button>
    )
}

