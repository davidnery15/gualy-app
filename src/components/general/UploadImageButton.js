import React from 'react'
import { css } from 'emotion'
import iconCamera from './../../assets/icon-camera.svg'
const specficError = css`
border: 3px solid red !important;
`
const iconCameraClass = css`
    width: 41px;
    height: 37.4px;
    object-fit: contain;
    @media(max-width: 630px) {
      width: 35px;
      height: 32px;
    }
  `
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const UploadImageButton = ({
  width,
  height,
  imageError,
  name,
  onChange,
  image,
  imageURI,
}) => {
  const UploadBtn = css`
    width: ${width};
    height: ${height};
    color: #242656;
    display: flex;
    justify-content: center;
    align-items: center;
    outline: none !important;
    cursor: pointer;
    background-color: #95fad8;
    border: none;
    border-radius: 11.9px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    position: relative;
    margin: auto;
  `
  const imagePreview = css`
    width: ${width};
    height: ${height};
    position: absolute;
    cursor: pointer;
    border-radius: 11.9px;
    object-fit: cover;
    border: 2px solid white;
  `
  return (
    <button className={`${UploadBtn} ${imageError ? specficError : ''}`} >
      <img
        src={image === '' ? iconCamera : image}
        className={image === '' ? iconCameraClass : imagePreview}
        alt="uploadPicture"
      />
      <input
        className={inputFileHidden}
        value={imageURI}
        name={name}
        type='file'
        accept="image/*"
        onChange={onChange}
      />
    </button>
  )
}
export default UploadImageButton