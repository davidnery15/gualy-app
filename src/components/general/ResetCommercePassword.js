import React, { Component } from 'react'
import { css } from 'emotion'
import styled from 'react-emotion'
import { connect } from 'react-redux'
import { Card, Button, CardHeader, CardBody } from 'reactstrap'
import { Redirect } from 'react-router'
import { db, auth } from './../../firebase'
import { getUserInfo } from '../../redux/actions/general'
import { securityQuestionKeys } from './../../constants/securityQuestionKeys'
import iconCamera from './../../assets/icon-camera.svg'
import iconVisibilityOn from './../../assets/baseline-visibility-on.svg'
import arrowIcon from './../../assets/drop-down-arrow.svg'
import logo from '../../assets/logos/logo-gualy-dark-bg.png'
import Loader from './Loader'
import errorSVG from './../../assets/ic-info-red.svg'
import { toast, ToastContainer } from 'react-toastify'
import { numbers, letters } from '../../constants/regex'
import { customConsoleLog } from '../../utils/customConsoleLog'
import { postRequest } from '../../utils/createAxiosRequest'
/* CSS */
const gualyNavbar = css`
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    color: white;
    transition: all 0.2s;
    padding: 25px 0;
    display:flex;
    justify-content: center;
  `
const navbarBrandImage = css`
    margin: 0;
    height: 50px;
    width: 150px;
  `
const loginCard = css`
    /* height: 568px; */
    width: 400px;
    max-width: 400px;
    margin-bottom: 40px;
    input {
      border-color: white;
      color: white;
    }
    @media (max-width: 480px) {
      width: 100%;
      max-width: 350px;
    }
  `
const dropDownDivContainer = css`
    border-bottom: 1px solid white;
    width: 100%;
    div{
      margin-top: 0;
    }
  `
const noBorderInput = css`
    border: none !important;
  `
const dropDownDiv = css`
    position: relative;
    display: flex;
    align-items:center;
  `
const visibilityContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    padding-right: 15px;
  `
const visibilityOn = css`
    width: 25px;
    height: 25px;
    object-fit: contain;
    cursor: pointer;
  `
const dropdownError = css`
   border-color: red !important;
  `
const CustomDropdown = styled('select')`
    height: 43.3px;
    width:100%;
    padding: 7px 4px;
    background: transparent;
    border: none;
    cursor: pointer;
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    appearance: none; 
    outline: none !important;
    option{
      background-color: #242657;
    }
    @media(max-width: 380px){
      font-size: 13px
    }
  `
const arrowContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    pointer-events: none;
  `
const dropdownQuestionPadding = css`
    padding-right: 15px;
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const commerceImgCSS = css`
    width: 170px;
    height: 169px;
    border-radius: 50%;
    border: 1px solid;
    object-fit: cover;
  `
const titleComponent = css`
    font-size: 50px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.22;
    letter-spacing: normal;
    text-align: center;
    @media(max-width: 380px){
      font-size: 40px
    }
  `
const textContainer = css`
    width: 434px;
    text-align: center;
    @media(max-width: 480px) {
      width: 100%;
    }
  `
const form = css`
    padding: 0 20px;
  `
const marginTop = css`
    margin-top: 20px !important;
  `
const container = css`
    @media(max-width:480px){
      width: 100%;
      padding: 0 10px;
    }
  `
const logInButton = css`
    cursor: pointer;
    position:relative;
    min-height: 48px;
    margin-top: 20px !important;
    width: 100%;
  `
const errorContainer = css`
  color: red;
  display: flex;
  justify-content: center;
  align-items: center; 
  font-size: 12px;
  margin-top: 15px;
`
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const GualyHeader = props =>
  <div className={gualyNavbar}>
    <img className={navbarBrandImage} src={logo} alt="Gualy Logo" />
  </div>

const createObjectMapper = objectArray => objectArray.reduce(
  (acumulator, current) => ({
    ...acumulator,
    [current.customId]: current.questionKey
  }),
  { default: null }
)

class ResetCommercePassword extends Component {
  state = {
    password: '',
    questionKey: '',
    answer: '',
    idToUidObjectMapper: null,
    passwordVisibility: false,
    userLogged: true,
    userData: null,
    loading: false,
    displayErrors: false,
    errorMessage: 'Faltan campos por llenar'
  }

  componentDidMount = () => {
    this.setState({
      idToUidObjectMapper: createObjectMapper(securityQuestionKeys)
    })
    auth.onAuthStateChanged((user) => {
      if (user) {
        const userKey = user.uid ? user.uid : null
        this.props.dispatch(getUserInfo(userKey))
        const usersRef = db.ref('users').orderByChild('userKey').equalTo(userKey.toString())
        usersRef.on('value', async (snapshot) => {
          const data = Object.values(snapshot.val())
          const userData = {
            userType: data[0].type,
            isPasswordSettedByUser: data[0].isPasswordSettedByUser
          }
          this.setState({
            userData,
            userLogged: true
          })
        })
      } else {
        this.setState({
          userLogged: false
        })
      }

    })
  }

  getOptions = items => items.map(item => {
    return <option key={item.customId} value={item.customId}>{item.text}</option>
  })

  handleInputChange = ({ target }) => {
    const value = target.name === 'password'
      ? target.value.length < 16 ? target.value : target.value.slice(0, -1)
      : target.value
    this.setState({
      [target.name]: value
    })
  }

  handleQuestionSelect = ({ target: { value }, target }) => {
    this.setState({
      [target.name]: value
    })
  }

  handlePasswordVisibility = (e) => {
    e.preventDefault()
    this.setState({
      passwordVisibility: !this.state.passwordVisibility
    })
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    this.setState({ loading: true, displayErrors: false, errorMessage: '' })
    this.resetSpecialPassToast = toast('Iniciando sesión...', { autoClose: false })
    const { userKey } = this.props.userInCollectionData
    const { password, questionKey, answer, idToUidObjectMapper } = this.state
    const idToUid = id => idToUidObjectMapper[id] || idToUidObjectMapper['default']
    const data = {
      uid: userKey,
      password,
      questionKey: idToUid(questionKey),
      answer
    }
    customConsoleLog('SENDED: ', data)
    if (
      password &&
      password.match(numbers) &&
      password.match(letters) &&
      questionKey &&
      answer
    ) {
      try {
        let resp = await postRequest('security/updateSecurityData', data)
        customConsoleLog('RESPONSE', resp)
        if (resp.data.data.success) {
          this.props.dispatch(getUserInfo())
          this.setState({ loading: false, password: '' })
        } else {
          this.setState({
            loading: false,
            displayErrors: true,
            errorMessage: `${resp.data.error.message}`
          })
        }
      } catch (error) {
        customConsoleLog('error: ', error)
        toast.update(this.resetSpecialPassToast, {
          render: "Verifique su conexión y vuelva a intentarlo.",
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          loading: false,
          displayErrors: true,
          errorMessage: "Verifique su conexión y vuelva a intentarlo."
        })
      }
    } else {
      this.setState({
        loading: false,
        displayErrors: true,
      })
      if (password === '') {
        event.target.password.focus()
        this.setState({ errorMessage: 'Debe colocar una contraseña.' })
        return toast.update(this.resetSpecialPassToast, {
          render: 'Debe colocar una contraseña.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (password.length <= 7) {
        event.target.password.focus()
        this.setState({ errorMessage: 'Debe colocar una contraseña entre 8 y 15 caracteres.' })
        return toast.update(this.resetSpecialPassToast, {
          render: 'Debe colocar una contraseña entre 8 y 15 caracteres.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!password.match(numbers)) {
        event.target.password.focus()
        this.setState({ errorMessage: 'La contraseña debe contener un número entre (0-9).' })
        return toast.update(this.resetSpecialPassToast, {
          render: 'La contraseña debe contener un número entre (0-9).',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!password.match(letters)) {
        event.target.password.focus()
        this.setState({ errorMessage: 'La contraseña debe contener una letra mayúscula.' })
        return toast.update(this.resetSpecialPassToast, {
          render: 'La contraseña debe contener una letra mayúscula.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (questionKey === '') {
        this.setState({ errorMessage: 'Debe colocar una pregunta de seguridad.' })
        return toast.update(this.resetSpecialPassToast, {
          render: 'Debe colocar una pregunta de seguridad.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (answer === '') {
        event.target.answer.focus()
        this.setState({ errorMessage: 'Debe colocar una respuesta de seguridad.' })
        return toast.update(this.resetSpecialPassToast, {
          render: 'Debe colocar una respuesta de seguridad.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }

  redirect = (state) => {
    const { userLogged, userData } = state
    if (userData) {
      const { userType, isPasswordSettedByUser } = userData
      if (userLogged) {
        if (userType === 'commerce') {
          if (isPasswordSettedByUser) {
            return <Redirect to="/dashboard" />
          } else {
            return null
          }
        } else {
          return <Redirect to="/" />
        }
      } else {
        return <Redirect to="/" />
      }
    } else {
      return null
    }
  }

  render() {
    const { password, answer, loading } = this.state
    const commerceImg = this.props.userInCollectionData.profilePicture
    return (
      <div>
        <GualyHeader />
        <div className="login-container" css={`height:auto !important`}>
          <div className={`standard-flex ${container}`}>
            <div className="standard-flex">
              <br /><br />
              <h1 className={titleComponent}>¡Bienvenido a Gualy!</h1>
              <br />
              <img src={commerceImg ? commerceImg : iconCamera} className={commerceImgCSS} alt="Imagen del Comercio" />
              <br />
              <div className={textContainer}>
                <p>Para culminar el proceso de registro, por favor llene el siguiente formulario con la contraseña, pregunta y respuesta definitiva.</p>
              </div>
              <br />
            </div>
            <Card className={`${loginCard} box-shadow justify-content-center`} fluid="true">
              <CardHeader className='text-center' css={`border-bottom: 0px solid transparent !important;padding-top: 30px`} fluid="true">
                <span css={`font-weight: bold; font-size: 24px;`}>{this.props.userInCollectionData.name ? this.props.userInCollectionData.name : 'Comercio'}</span>
              </CardHeader>
              <CardBody css={`padding-bottom: 30px`}>
                <form onSubmit={this.handleSubmit} className={`${form} standard-flex`} noValidate>
                  <div className={`${dropDownDivContainer} ${noBorderInput}`}>
                    <div className={dropDownDiv}>
                      <input
                        onChange={this.handleInputChange}
                        value={password}
                        max="15"
                        placeholder="Contraseña"
                        type={this.state.passwordVisibility === true ? 'text' : 'password'}
                        id='password'
                        name='password'
                        required
                      />
                      <div className={`${visibilityContainer}`} onClick={this.handlePasswordVisibility}>
                        <img src={iconVisibilityOn} className={visibilityOn} alt="passwordVisibility" />
                      </div>
                    </div>
                  </div>
                  <div css={`padding-bottom:5px;`} className={`${marginTop} ${dropDownDivContainer} ${(this.state.questionKeyError === true ? dropdownError : '')}`}>
                    <div className={dropDownDiv}>
                      <CustomDropdown className={dropdownQuestionPadding} id='questionKey' name='questionKey' onChange={this.handleQuestionSelect} required>
                        <option value="" disabled selected>Pregunta de seguridad</option>
                        {
                          this.getOptions(securityQuestionKeys)
                        }
                      </CustomDropdown>
                      <div className={arrowContainer}>
                        <img src={arrowIcon} className={arrow} alt="dropdown" />
                      </div>
                    </div>
                  </div>
                  <input
                    className={marginTop}
                    type="text"
                    placeholder="Respuesta"
                    name="answer"
                    value={answer}
                    onChange={this.handleInputChange}
                    required
                  />
                  {
                    this.state.displayErrors ?
                      <div className={errorContainer}>
                        {this.state.errorMessage}
                        <img
                          className={errorSvgCss}
                          src={errorSVG}
                          alt="error Icon"
                        />
                      </div>
                      : null
                  }
                  <Button disabled={loading} className={`${logInButton} mt-5`} color='primary' size='lg'>
                    {
                      !loading
                        ? 'INICIAR SESIÓN'
                        : <Loader color="#2a2c6a" />
                    }
                  </Button>
                </form>
              </CardBody>
            </Card>
          </div>
        </div>
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
        {
          this.redirect(this.state)
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(ResetCommercePassword)