import React from 'react'
import { Link } from 'react-router-dom'
import { css } from 'emotion'

const absotuleCardsContainer = css`
  display: flex;
  flex-direction: row;
  position: absolute;
    left: 0;
  `
const bankContainer = css`
    margin-top: 20px;
    overflow-y: hidden;
    width:100%;
    display: flex;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 8px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 10px;
    }
    ::-webkit-scrollbar {
      width: 1px;
    }
  `
const bankContainerJustifyContent = css`
    margin-top: 20px;
    overflow-y: hidden;
    width:100%;
    display: flex;
    justify-content: center;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 8px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 10px;
    }
    ::-webkit-scrollbar {
      width: 1px;
    }
  `
const ChooseAccountNumber = ({
  userBankAccounts,
  banksCards,
  onCloseCurrentModal
}) => 
    <div
      css={`height: 120px; position: relative;`}
      className={
        userBankAccounts
          ? (userBankAccounts.length > 3 ? bankContainer : bankContainerJustifyContent)
          : bankContainer}
    >
      {
        userBankAccounts.length > 0
          ? <div className={absotuleCardsContainer}>
            {
              banksCards
            }
          </div>
          : <Link css={`margin: auto;`} to='/perfil'>
            <button onClick={onCloseCurrentModal} className='btn-accept'> AGREGAR CUENTA </button>
          </Link>
      }
    </div>

export default ChooseAccountNumber