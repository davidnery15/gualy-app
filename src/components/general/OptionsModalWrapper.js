import React from 'react'
import { css } from 'emotion'
import iconClose from './../../assets/icon-close.svg'
const modalComponent = css`
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-color: rgba(11, 11, 29, 0.85);
    z-index: 1000;
  `
const closeButton = css`
    background: transparent;
    border: none;
    text-align: right;
    cursor: pointer;
    outline: none !important;
    position: absolute;
    right: 10px;
    top: -30px
  `
const overflowToChildren = css`
    overflow: auto;
    overflow-x: hidden
  `
const OptionsModalWrapper = ({ show, onClose, children, width, height, className }) => {
  if (!show) {
    return null;
  }
  const modalContainer = css`
    width: ${width ? `${width}` : '400px'};
    height: ${height || ''};
    border-radius: 5px;
    background-color: #2a2c6a; 
    box-shadow: 0 9px 18px 0 rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: column;
    position: relative;
    max-height: 85%;
    @media(max-width: 340px){
      width:95%
    }
  `
  return (
    <div className={modalComponent}>
      <div className={`${modalContainer} ${className}`}>
        <button className={closeButton} onClick={onClose}>
          <img src={iconClose} alt='icon close' />
        </button>
        <div className={overflowToChildren}>
          {children}
        </div>
      </div>
    </div>
  )
}
export default OptionsModalWrapper