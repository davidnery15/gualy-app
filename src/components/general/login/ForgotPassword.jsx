import React, { Component } from 'react'
import { css } from 'emotion'
import { NavLink } from 'react-router-dom'
import { Card, Button, CardBody } from 'reactstrap'
import errorSVG from './../../../assets/ic-info-red.svg'
import Loader from './../Loader'
import GoBackButton from '../goBackButton'
import { postRequest } from '../../../utils/createAxiosRequest'
/**CSS */
const loginCard = css`
    width: 350px;

    @media (max-width: 420px) {
      width: 300px;
      max-width: 300px;
      margin: auto 10px;
    }
  `
const recoverTitleCard = css`
    margin-top: 35px !important;
    font-size: 22px;
    font-weight: 900;
    line-height: 1.2;
  `
const preguntaSecreta = css`
    font-family: Montserrat;
    font-size: 16px;
    font-weight: 300;
    text-align: center;
    color: #ffffff;
  `
const buttonLogIn = css`
    cursor: pointer;
    min-height: 48px;
    position:relative;
  `
const responsive = css`
    @media(max-width: 420px) {
      align-items: flex-start !important;
    }
  `
const errorContainer = css`
    padding: 0 5px;
    color: red;
    opacity: 0.9;
    display:flex;
    justify-content: center;
    font-size: 12px;
    margin-top:5px;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin-left: 5px;
  `
const goBackButtonClass = css`
  top: 10px;
  left: 10px
  position: absolute
  `
export default class ForgotPassword extends Component {
  state = {
    email: '',
    loading: false,
    sent: false,
    showError: false,
    errorMessage: 'Debe colocar un correo válido'
  }

  sendEmail = async e => {
    e.preventDefault()
    if (e.target.checkValidity()) {
      const data = {
        email: this.state.email
      }
      try {
        this.setState({ loading: true })
        // console.log('ForgotPassword data', data)
        let resp = await postRequest('security/generatePasswordTokenByEmail', data)
        if (resp.data.success) {
          this.setState({
            loading: false,
            sent: true,
            email: ''
          })
        } else {
          this.setState({
            loading: false,
            showError: true,
            errorMessage: `${resp.data.error.message}`
          })
          // console.log(resp)
        }
      } catch (err) {
        this.setState({
          loading: false,
          showError: true,
        })
        // console.log('catch err', err)
      }
    } else {
      this.setState({ showError: true })
    }
  }

  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }

  render() {
    const { email, loading, sent, showError, errorMessage } = this.state
    return (
      <div className={`login-container ${responsive}`}>
        <Card className={`${loginCard} box-shadow justify-content-center`} fluid="true">
          <GoBackButton
            height="21px"
            width="21px"
            route='/'
            style={goBackButtonClass}
          />
          {
            !sent ?
              (
                <CardBody className={`p-2`}>
                  <form onSubmit={this.sendEmail} noValidate>
                    <div className="p-6 text-center">
                      <span className={recoverTitleCard}><br /> ¿Olvidaste tu contraseña?<br />  <br /> No hay problema</span>
                    </div>
                    <div className='ml-3 mr-3 justify-content-center d-flex flex-column mt-4'>
                      <div className={preguntaSecreta}>
                        Ingresa tu correo electrónico
                    </div>
                      <br />
                      <div className='group' css={`
                      margin-left: 20px;
                      margin-right: 20px;
                      margin-bottom: 35px;`}>
                        <input
                          onChange={this.handleInputChange}
                          value={email}
                          type="email"
                          placeholder="Email"
                          name="email"
                          css={`text-align: center;`}
                          required
                        />
                        <span className='highlight'></span>
                        <span className='bar'></span>
                        {
                          showError &&
                          <div className={errorContainer}>
                            {errorMessage}
                            <img
                              src={errorSVG}
                              alt="error icon"
                              className={errorSvgCss}
                            />
                          </div>
                        }
                      </div>
                      <div className='login-button-container'>
                        <Button
                          color='primary'
                          size='lg'
                          className={`${buttonLogIn} mb-3`}
                          block
                          disabled={(loading)}>
                          {
                            loading ?
                              <Loader color="#2a2c6a" />
                              : 'ENVIAR EMAIL'
                          }
                        </Button>
                      </div>
                    </div>
                  </form>
                </CardBody>
              )
              :
              (
                <CardBody className={`p-4`}>
                  <div className="p-6 text-center">
                    <span className={recoverTitleCard}><br /> Casi listo, ahora ingresa a tu correo y sigue las instrucciones</span>
                  </div>
                  <div className='ml-3 mr-3 justify-content-center d-flex flex-column mt-4'>
                    <div className={preguntaSecreta}>
                      Ingresa a tu correo electrónico
                  </div>
                    <br />
                    <div className='login-button-container'>
                      <NavLink to='/'>
                        <Button
                          color='primary'
                          size='lg'
                          className={`${buttonLogIn} mb-3`}
                          block>
                          IR A GUALY
                    </Button>
                      </NavLink>
                    </div>
                  </div>
                </CardBody>
              )
          }
        </Card>
      </div>
    );
  }
}
