import React, { Component } from 'react'
import { css } from 'emotion'
import { Card, CardHeader, CardBody } from 'reactstrap'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Redirect } from 'react-router'
import { auth } from '../../../firebase'
import redirect from './../../../utils/redirect'
import logo from '../../../assets/logos/logo-gualy-dark-bg.svg'
import { loginUserEmailPass, getUserInfo } from '../../../redux/actions/general'
import iconVisibilityOn from './../../../assets/baseline-visibility-on.svg'
import iconVisibilityOff from './../../../assets/baseline-visibility-off.svg'
import errorSVG from './../../../assets/ic-info-red.svg'
import AcceptButton from './../../general/AcceptButton'
const loginCard = css`
    width: 400px;
    max-width: 400px;
    @media (max-width: 420px) {
      width: 350px;
      max-width: 350px;
      margin: 0 10px;
      margin-top: 40px;
    }
  `
const logoGualy = css`
    width: 155.7px;
    max-width: 155.7px;
    height: auto;
    margin-top: 28px;
  `
const hSeparator = css`
    width: 270px;
    min-height: 1px;
    opacity: 0.5;
    background-color: #1d1f4a;
    align-self: center;
    margin-top: 30px;
  `
const anchorLink = css`
    text-decoration: none;
    color: #95fad8 !important;
    p{
      margin-top: 8px !important
    }
    &:active {
      color: #95fad8;
      text-decoration: none;
    }
    &:visited {
      color: #95fad8;
      text-decoration: none;
    }
    &:hover {
      color: #95fad8;
      text-decoration: none;
    }
  `
const buttonLogIn = css`
    min-height: 48px;
    font-size: 1.25rem !important;
  `
const responsive = css`
    @media(max-width: 420px) {
      align-items: flex-start !important;
    }
  `
const passwordContainer = css`
    position: relative;
  `
const visibilityOnCSS = css` 
    width: 25px; 
    height: 25px; 
    object-fit: contain; 
    cursor: pointer;
  `
const btnVisibility = css`
    background: transparent;
    border: none;
    position: absolute;
    right: 5px;
    top: 10px;
  `
const errorContainer = css`
    color: red;
    opacity: 0.9;
    font-size: 12px;
    margin-bottom: 5px;
    text-align: center;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const errorMessageContainer = css`
    margin-bottom: 20px;
  `
const group = css`
position: relative;
margin-bottom: 35px;
  `
class Login extends Component {
  state = {
    email: '',
    password: '',
    passwordVisibility: false,
    firstTry: false,
    token: null,
    forgot: null
  }
  componentDidMount = () => {
    const url_string = window.location.href;
    const url = new URL(url_string);
    const token = url.searchParams.get('token')
    const forgot = url.searchParams.get('forgot')
    if (token) {
      this.setState({ token })
    }
    if (forgot) {
      this.setState({ forgot })
    }
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loading: false })
        if (user.emailVerified)
          this.props.dispatch(getUserInfo(user.uid))
      } else {
        this.setState({ loading: false })
      }
    }, (err) => {
      // console.log('err', err)
    })
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  redirectToValidate = (token) => {
    if (token) {
      return <Redirect to={`/validacion?token=${token}`} />
    }
  }
  redirectToResetPassword = (forgot) => {
    if (forgot) {
      return <Redirect to={`/reseteo-contrasena?forgot=${forgot}`} />
    }
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    this.setState({ firstTry: true, passwordVisibility: false })
    const { email, password } = this.state
    await this.props.dispatch(loginUserEmailPass({ email, password }))
  }
  checkErrorCodes = (code) => {
    switch (code) {
      case 'auth/user-disabled':
        return <div className={errorContainer}>Este usuario ha sido bloqueado. <img src={errorSVG} alt="error icon" className={errorSvgCss} /> </div>
      case 'auth/user-not-found':
        return <div className={errorContainer}>No se ha registrado este usuario. <img src={errorSVG} alt="error icon" className={errorSvgCss} /> </div>
      case 'auth/network-request-failed':
        return <div className={errorContainer}>Verifique su conexión y vuelva a intentarlo. <img src={errorSVG} alt="error icon" className={errorSvgCss} /> </div>
      case 'email-not-verified':
        return <div className={errorContainer}>El email no ha sido verificado. <img src={errorSVG} alt="error icon" className={errorSvgCss} /> </div>
      default:
        return <div className={errorContainer}>Email o contraseña inválida. <img src={errorSVG} alt="error icon" className={errorSvgCss} /> </div>
    }
  }
  handleVisibility = (e) => {
    e.preventDefault()
    this.setState({ passwordVisibility: !this.state.passwordVisibility })
  }
  handleKeyPress = e => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.handleSubmit(e)
    }
  }
  renderError = (error, errorCode) =>
    error ? <p css={`color:red;`}>{errorCode}</p> : null

  render() {
    const { passwordVisibility, firstTry, token, forgot } = this.state
    const { isFetching, errorCode } = this.props
    return (
      <div className={`login-container ${responsive}`}>
        <Card className={`${loginCard} box-shadow justify-content-center`} fluid="true">
          <CardHeader className='pt-5 text-center' css={`border-bottom: 0px solid transparent !important;`} fluid="true">
            <span css={`font-weight: bold; font-size: 24px;`}>Bienvenido a </span>
            {/* <span css={`font-weight: bold; font-size: 24px;`}>Panel de Comercio</span> */}
            {/* <span css={`font-weight: bold; font-size: 24px;`}>Panel Administrativo</span> */}
            <br />
            <img src={logo} alt="Logo Gualy" className={`${logoGualy} img-fluid`} />
          </CardHeader>
          <div className={hSeparator} />
          {/* <div className={helpText}>
            Obtén una visión global o detallada de cada transacción y traslada el dinero a la cuenta bancaria de tu comercio cuando lo desees.
          </div> */}
          <CardBody>
            <form
              onSubmit={this.handleSubmit}
              autoComplete="off"
              noValidate
            >
              <div className='ml-3 mr-3 pl-1 pr-1'>
                <div className='group mb-1'>
                  <input
                    type='text'
                    placeholder='Correo electrónico *'
                    name='email'
                    disabled={isFetching ? true : false}
                    onChange={this.handleInputChange}
                    value={this.state.email}
                    required
                  />
                  <span className='highlight'></span>
                  <span className='bar'></span>
                </div>
                <div className={` ${passwordContainer} ${group}`}>
                  <input
                    type={passwordVisibility ? 'text' : 'password'}
                    placeholder='Contraseña *'
                    name='password'
                    disabled={isFetching ? true : false}
                    onChange={this.handleInputChange}
                    value={this.state.password}
                    onKeyPress={this.handleKeyPress}
                    required
                  />
                  {
                    !isFetching
                      ? <button
                        className={btnVisibility}
                        onClick={this.handleVisibility}>
                        <img
                          className={visibilityOnCSS}
                          src={passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
                          alt="visibilidad"
                        />
                      </button>
                      : null
                  }
                  <span className='highlight'></span>
                  <span className='bar'></span>
                  {
                    !isFetching ?
                      <NavLink className={`${anchorLink} font-weight-bold text-uppercase`} to='/olvido-contrasena'>
                        <p className='text-center'>Olvidé mi contraseña</p>
                      </NavLink>
                      : null
                  }
                </div>
              </div>
              {
                errorCode && firstTry && !isFetching
                  ? <div className={errorMessageContainer}>
                    {this.checkErrorCodes(errorCode)}
                  </div>
                  : null
              }
              <div className='login-button-container'>
                <AcceptButton
                  loading={isFetching && firstTry}
                  width="100%"
                  className={`${buttonLogIn} mb-3`}
                  type="submit"
                  content="INICIAR SESIÓN"
                />
                {
                  !isFetching
                    ? <NavLink className={`${anchorLink} font-weight-bold text-uppercase`} to='/registro'>
                      <p className='text-center'>Regístrate</p>
                    </NavLink>
                    : null
                }
              </div>
            </form>
          </CardBody>
        </Card>
        {
          redirect(this.props)
        }
        {
          this.redirectToValidate(token)
        }
        {
          this.redirectToResetPassword(forgot)
        }
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn,
    isFetching: state.general.isFetching,
    errorCode: state.general.errorCode
  }
}
export default connect(mapStateToProps)(Login)