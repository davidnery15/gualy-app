import React from 'react'
import { css } from 'emotion'
import Loader from './Loader'
export default ({ loading, content, width, height, className, onClick, type }) => {
  const AcceptButton = css`
    height: 100%;
    width: ${width};
    height: ${height};
    padding: 8px 18px;
    border-radius: 5px;
    border-radius: 100px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: center;
    color: #242656;
    cursor: pointer;
    outline: none !important;
    position: relative;
`
  return (
    <React.Fragment>
      <button disabled={loading} type={type} onClick={onClick} className={`${AcceptButton} ${className}`}>
        {
          loading
            ? <Loader color="#2a2c6a" />
            :
            content
        }
      </button>
    </React.Fragment>
  )
}

