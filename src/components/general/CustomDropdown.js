import React from 'react'
import styled from 'react-emotion'
import { css } from 'emotion'
import arrowIcon from './../../assets/drop-down-arrow.svg'
const displayRedBorders = css`
    border-color:red;
`
const dropDownDivContainer = css`
border-bottom: 1px solid white;
width: 100%;
div{
margin-top: 0;
}
`
const marginTop = css`
margin-top: 20px !important;
`
const dropDownDiv = css`
position: relative;
display: flex;
align-items:center;
`
const CustomDropdown = styled('select')`
height: 43.3px;
width:100%;
padding: 7px 4px;
background: transparent;
border: none;
cursor: pointer;
font-size: 15px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.68;
letter-spacing: normal;
text-align: left;
color: #ffffff;
appearance: none; 
outline: none !important;
option{
background-color: #242657;
}
`
const dropdownQuestionPadding = css`
padding-right: 15px;
@media(max-width: 500px){
option{
    font-size: 8px;
}
}
`
const arrowContainer = css`
width: 15px !important;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
position: absolute;
right: 0;
top: 0;
pointer-events: none;
`
const arrow = css`
width: 10.4px;
height: 6.6px;
object-fit: contain;
`
export default ({ children, displayErrors, value, handleChange, name, disabled }) =>
  <div className='group mb-1'>
    <div css={`padding-bottom:5px;`} className={`${marginTop} ${dropDownDivContainer} ${displayErrors && value === '' ? displayRedBorders : ''}`}>
      <div className={dropDownDiv}>
        <CustomDropdown
          disabled={disabled ? disabled : false}
          className={dropdownQuestionPadding}
          value={value}
          name={name}
          onChange={handleChange}
        >
          {children}
        </CustomDropdown>
        <div className={arrowContainer}>
          <img src={arrowIcon} className={arrow} alt="dropdown" />
        </div>
      </div>
    </div>
  </div>