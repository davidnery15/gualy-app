import React from 'react'
import { css } from 'emotion'

import iconVisibilityOn from './../../assets/baseline-visibility-on.svg'
import iconVisibilityOff from './../../assets/baseline-visibility-off.svg'

/** CSS */
  const passwordContainer = css`
    position: relative;
  `
  const btnVisibility = css`
    background: transparent;
    border: none;
    position: absolute;
    right: 5px;
    top: 10px;
  `
  const visibilityOnCSS = css` 
    width: 25px; 
    height: 25px; 
    object-fit: contain; 
    cursor: pointer;
  `

const InputPassword = ({ 
    passwordVisibility,
    value,
    onChange, 
    onKeyPress, 
    onClick, 
    name,
  }) =>
  <div className={` ${passwordContainer} group`}>
    <input 
      type={ passwordVisibility ? 'text' : 'password' }
      placeholder='Contraseña *'
      name={name}
      onChange={onChange}
      value={value}
      onKeyPress={onKeyPress}
      autoComplete="off"
      required
    />
    <button
      className={btnVisibility}
      onClick={onClick}>
        <img
          className={visibilityOnCSS}
          src={ passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
          alt="visibilidad"
        />
    </button>
    <span className='highlight'></span>
    <span className='bar'></span>
  </div>

export default InputPassword