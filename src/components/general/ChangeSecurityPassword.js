import React, { Component } from 'react'
import { css } from 'emotion'
import { Card, CardHeader, CardBody, Col } from 'reactstrap'
import { NavLink } from 'react-router-dom'
import errorSVG from './../../assets/ic-info-red.svg'
import { toast, ToastContainer } from 'react-toastify'
import AcceptButton from './AcceptButton'
import GoBackButton from './goBackButton'
import FourPinsInput from './FourPinInput'
import {customConsoleLog} from '../../utils/customConsoleLog'
import { postRequest } from '../../utils/createAxiosRequest'
/**CSS */
const recoverCard = css`
    width:  400px;
    max-width: 400px;
    height: auto;
    border-radius: 5px;
    margin-top: 72px;
    position: relative;
    @media (max-width: 420px) {
      width: 350px;
      max-width: 350px;
      margin: 0 10px;
      margin-top: 40px;
    }
  `
const successCard = css`
    width:  400px;
    max-width: 400px;
    height: auto;
    border-radius: 5px;
    margin-top: 72px;

    @media (max-width: 420px) {
      width: 350px;
      max-width: 350px;
      margin: 0 10px;
      margin-top: 40px;
    }
  `
const recoverTitleCard = css`
    margin-top: 35px !important;
    font-size: 24px;
    font-weight: 900;
    line-height: 1.2;
  `
const sucessCardBody = css`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    padding: 20px !important;
  `
const sucessText = css`
    font-size: 36px;
    line-height: 0.8;
    color: #8078ff;
    font-style: bold;
    margin:20px
  `
const secondaryText = css`
    font-size: 24px;
    font-weight: 900;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    text-align: center;
    color: #f6f7fa;
    margin:20px
  `
const errorContainer = css`
padding: 0 5px;
color: red;
opacity: 0.9;
display: flex;
font-size: 12px;
margin-bottom: 15px;
justify-content: center;
margin-top: 15px;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin-left: 5px;
  `
const goBackButtonClass = css`
  top: 10px;
  left: 10px
  position: absolute
  `
const acceptButton = css`
  margin: auto;
  margin-top: 20px
  `
export default class ChangeSecurityPassword extends Component {
  state = {
    newPassword: false,
    loading: false,
    token: null,
    displayErrors: false,
    errorMessage: '',
    //special password
    firstNumber: '',
    secondNumber: '',
    thirdNumber: '',
    fourthNumber: '',
  }

  componentDidMount = () => {
    const url_string = window.location.href;
    const url = new URL(url_string);
    const token = url.searchParams.get('token')
    this.setState({
      token
    })
  }

  handleInputChange = ({ target }) => {
    const value = target.value.length < 16 ? target.value : target.value.slice(0, -1)
    this.setState({
      [target.name]: value
    })
  }

  handlePasswordVisibility = (e) => {
    e.preventDefault()
    this.setState({
      passwordVisibility: !this.state.passwordVisibility
    })
  }

  handleKeyPress = e => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.handleSubmit(e)
    }
  }
  toastRegisterId = null
  handleSubmit = async (event) => {
    event.preventDefault();
    this.setState({ loading: true, displayErrors: false, errorMessage: '', passwordVisibility: false })
    this.toastRegisterId = toast('Cambiando contraseña...', { autoClose: false })
    const { token } = this.state
    const data = {
      password:
        this.state.firstNumber +
        this.state.secondNumber +
        this.state.thirdNumber +
        this.state.fourthNumber,
      token: token
    }
    customConsoleLog('SENDED: ', data)
    if (
      token &&
      this.state.firstNumber &&
      this.state.secondNumber &&
      this.state.thirdNumber &&
      this.state.fourthNumber
    ) {
      try {
        let response = await postRequest('security/resetPassword', data)
        customConsoleLog("RESPONSE: ", response)
        if (response.data.success) {
          toast.update(this.toastRegisterId, {
            render: 'Contraseña ha sido cambiada exitosamente.',
            autoClose: 5000,
          })
          this.setState({ loading: false, newPassword: true })
        } else {
          this.setState({
            loading: false,
            displayErrors: true,
            errorMessage: `${response.data.error.message}`
          })
          toast.update(this.toastRegisterId, {
            render: response.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        customConsoleLog('',error)
        this.setState({ loading: false, errorMessage: "Verifique su conexión y vuelva a intentarlo." })
        toast.update(this.toastRegisterId, {
          render: "Verifique su conexión y vuelva a intentarlo.",
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        loading: false
      })
      if (!token) {
        this.setState({ errorMessage: 'Necesita un token para resetear la contraseña.' })
        return toast.update(this.toastRegisterId, {
          render: "Necesita un token para resetear la contraseña",
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.toastRegisterId, {
          render: 'Contraseña especial incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }

  render() {
    customConsoleLog("token: ", this.state.token)
    const {
      newPassword,
      displayErrors,
      errorMessage,
      loading } = this.state
    if (newPassword) {
      return (
        <Col className='justify-content-center d-flex'>
          <Card className={`${successCard} box-shadow`}>
            <CardBody className={`${sucessCardBody}`}>
              <span className={sucessText}>¡Excelente!</span>
              <span className={secondaryText}>Tu nueva contraseña ha sido creada con éxito.</span>
              <NavLink to="/">
                <AcceptButton
                  height="38px"
                  width="100%"
                  content="INICIAR SESIÓN"
                />
              </NavLink>
            </CardBody>
          </Card>
        </Col>
      )
    } else {
      return (
        <Col className='justify-content-center d-flex'>
          <Card className={`${recoverCard} box-shadow`}>
            <GoBackButton
              height="21px"
              width="21px"
              route='/'
              style={goBackButtonClass}
            />
            <CardHeader
              className='p-4 text-center mt-2'
              css={`border-bottom: 0px solid transparent !important;padding-bottom: 0 !important`}
            >
              <span className={recoverTitleCard}>Ahora, crea una <br /> nueva contraseña especial</span>
            </CardHeader>
            <CardBody className={`pr-4 pl-4`}>
              <form
                onSubmit={this.handleSubmit}
                className='ml-3 mr-3 justify-content-center d-flex flex-column mt-4'
                css={`
              padding-bottom: 20px; 
              margin-top: 0!important;
              `}
                noValidate
              >
                <FourPinsInput
                  firstNumber={this.state.firstNumber}
                  secondNumber={this.state.secondNumber}
                  thirdNumber={this.state.thirdNumber}
                  fourthNumber={this.state.fourthNumber}
                  onChange={this.handleInputChange}
                  disabled={loading}
                  displayErrors={displayErrors}
                  hasSecurityPassword={true}
                  title="Ingresa cuatro números"
                  canResetPass={false}
                  canShowPassword={true}
                  loading={loading}
                />
                {
                  displayErrors ?
                    <div className={errorContainer}>
                      {errorMessage}
                      <img
                        className={errorSvgCss}
                        src={errorSVG}
                        alt="error Icon"
                      />
                    </div>
                    : null
                }
                <AcceptButton
                  height="38px"
                  width="160px"
                  content="CONFIRMAR"
                  loading={loading}
                  className={acceptButton}
                />
              </form>
            </CardBody>
          </Card>
          <ToastContainer
            className={{
              fontSize: "15px"
            }} />
        </Col>
      )
    }

  }
}
