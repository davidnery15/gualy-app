import React from 'react'
import { css } from 'emotion'
/* CSS */
  const userImg = css`
    width: 34.2px;
    height: 34.2px;
    border-radius: 50%;
    object-fit: cover;

    @media (max-width: 1000px) {
      display: none;
    }
  `
  const userName = css`
    font-size: 16px;
    font-weight: 900;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.98;
    letter-spacing: normal;
    @media (max-width: 1070px) {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    
  `
  const userEmail = css`
    font-family: Montserrat;
    font-size: 12px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    @media (max-width: 1070px) {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
  const userTextContainer = css`
    text-align: middle;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 1px;
    padding: 15px 6px 3px 6px;
    @media (max-width: 1070px) {
      width: 100px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `
  const User = ({ info, textAlign }) => {
    const userContainer = css`
      display: flex;
      margin: 1px;
      padding: 1px;
      width: 100%;
      display: flex;
      justify-content: ${textAlign};
      align-items:center;
    `
  return (
    <div className={userContainer}>
      <img className={userImg} src={info.profileUri} alt="i" />
      <div className={userTextContainer}>
        <h2 className={userName}>{info.name}</h2>
        <p className={userEmail}>{info.email}</p>
      </div>
    </div>);
}
export default User