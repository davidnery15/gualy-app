import React from 'react'
class AutoFocusInput extends React.PureComponent {
    componentDidUpdate() {
      this[this.props.name].focus();
    }
    render() {
        const { 
            name,
            value,
            onChange,
            readOnly,
            placeholder,
         } = this.props
        return (
          <React.Fragment>
                <input
                   ref={c => this[name] = c}
                    placeholder={placeholder}
                    type="text"
                    readOnly={readOnly}
                    onChange={onChange}
                    value={value}
                    name={name}
                  />
          </React.Fragment>
        )
      }
    }
    export default AutoFocusInput
  