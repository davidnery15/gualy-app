import React, { Component } from 'react'
import { css } from 'emotion'
import { NavLink } from 'react-router-dom'
import gualyLogo from '../../assets/logos/logo-gualy-dark-bg.svg'
import { getRequest } from '../../utils/createAxiosRequest'
const title = css`
    font-size: 72px;
    color:white;
    text-align: center;
    @media(max-width: 900px){
      font-size: 50px;
    }
  `
const anchorLink = css`
    text-decoration: none;
    color: #242656 !important;
    &:active {
      color: #242656;
      text-decoration: none;
    }
    &:visited {
      color: #242656;
      text-decoration: none;
    }
    &:hover {
      color: #242656;
      text-decoration: none;
    }
  `
const gualyLogoCSS = css`
    width:200px;
    height: 200px;
  `
const centerText = css`
  text-align: center;
  `
export default class Validate extends Component {
  state = {
    message: 'Validando...',
    loading: true
  }
  componentDidMount = async () => {
    const url_string = window.location.href;
    const url = new URL(url_string);
    const user = url.searchParams.get("token");
    try {
      let response = await getRequest(`security/validate?verificationCode=` + user)
      // console.log(response)
      if (response.data.success) {
        this.setState({
          message: response.data.message,
          loading: false
        })
      } else {
        this.setState({
          message: 'Su token ha caducado.',
          loading: false
        })
      }
    } catch (error) {
      // console.log(error);
      this.setState({
        message: 'Verifique su conexión y vuelva a intentarlo.',
        loading: false
      })
    }
  }
  render() {
    return (
      <div className='login-container'>
        <div className='standard-flex'>
          <img className={gualyLogoCSS} src={gualyLogo} alt="gualy logo" />
          <br />
          <h1 className={title}>Bienvenido a GUALY</h1>
          <br />
          {
            this.state.loading ?
              <h3> {this.state.message} </h3>
              :
              (
                <div className='standard-flex'>
                  <h3 className={centerText}> {this.state.message} </h3>
                  <br />
                  <button className={`btn-accept mb-3`} css={`cursor: pointer;font-size:18px;`} color='primary' size='lg'>
                    <NavLink className={anchorLink} to='/'>
                      Ir a Gualy
                    </NavLink>
                  </button>
                </div>
              )
          }
        </div>
      </div>
    )
  }
}