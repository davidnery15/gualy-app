import React from 'react'
import { css } from 'emotion'
import arrowBack from '../../assets/arrowBack.svg'
import { NavLink } from 'react-router-dom'
export default ({ route, height, width, style, onClick }) => {
  const goBackButton = css`
  border: none;
  background: transparent;
  height: ${height};
  width: ${width};
  cursor: pointer;
  `
  return route ? <NavLink to={route}>
    <img
      alt=""
      src={arrowBack}
      className={`${goBackButton} ${style}`}
    />
  </NavLink>
    : <img
      alt=""
      src={arrowBack}
      className={`${goBackButton} ${style}`}
      onClick={onClick}
    />
}
