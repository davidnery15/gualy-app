import React from 'react'
import { css } from 'emotion'
import iconVisibilityOn from './../../assets/baseline-visibility-on.svg'
import iconVisibilityOff from './../../assets/baseline-visibility-off.svg'

const fourPinMainContainer = css`
 display: flex;
 flex-direction: column;
 justify-content: center;
 width:100%;
 input{
   text-align: center
 }
`
const fourPinContainer = css`
 display: flex;
 flex-direction: row;
 justify-content: center;
 position: relative;
 max-width: 270px;
 margin: auto;
 margin-left: 37px;
 input{
  font-size: 35px !important;
}
input:-webkit-autofill, input:-webkit-autofill:hover{
  -webkit-text-fill-color: #f6f7fa !important;
  -webkit-box-shadow: none
 }
`
const pinInput = css`
width: 39.2px;
height: 39.2px;
border-radius: 9px;
background-color: #242657;
border: none;
color: #ffff;
margin: 6px;
padding-left: 10px;
`
const fourPinsTitle = css`
opacity: 0.5;
font-family: Montserrat;
font-size: 15.7px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: 1.5;
letter-spacing: normal;
text-align: center;
color: #f6f7fa;
margin-bottom: 0;
margin-top: 20px;
`
const createPasswordText = css`
font-size: 11.5px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.21;
letter-spacing: 0.4px;
text-align: center;
color: #95fad8;
margin-bottom: 10px;
margin-top: 10px;
cursor: pointer;
`
const displayRedBorders = css`
    border-color:red !important;
    width: 39.2px;
    height: 39.2px;
    border-radius: 9px;
    background-color: #242657;
    color: #ffff;
    margin: 6px;
    padding-left: 10px;
`
const btnVisibility = css`
    background: transparent;
    border: none;
  `
const visibilityOnCSS = css` 
    width: 25px; 
    height: 25px; 
    object-fit: contain; 
    cursor: pointer;
  `
const removeNonNumbers = (number) => {
  const amountString = number
    .toString()
    .split('')
    .filter(chr => !chr.match(/^([^0-9]*)$/))
    .join('')
  return amountString === "" ? "" : Number(amountString)
}
class FourPinsInput extends React.Component {
  state = {
    passwordVisibility: false,
  }
  handleVisibility = (event) => {
    event.preventDefault();
    this.setState({ passwordVisibility: !this.state.passwordVisibility })
  }
  onKeyUp = (e) => {
    var target = e.srcElement || e.target;
    if (target.value.length === 1) {
      var next = target.nextElementSibling;
      if (next.tagName.toLowerCase() === "input") {
        next.focus();
        next.select();
      }
    }
    // Move to previous field if empty (user pressed backspace)
    else if (target.value.length === 0 && target.name !== "firstNumber") {
      var previous = target.previousElementSibling;
      if (previous.tagName.toLowerCase() === "input") {
        previous.focus();
        previous.select();
      }
    }
  }

  render() {
    const {
      passwordVisibility
    } = this.state
    const {
      displayErrors,
      firstNumber,
      secondNumber,
      thirdNumber,
      fourthNumber,
      onChange,
      disabled,
      openCreatePasswordClick,
      hasSecurityPassword,
      title,
      openResetPassword,
      canResetPass,
      loading,
      canShowPassword,
    } = this.props
    return (
      <div className={fourPinMainContainer}>
        {
          hasSecurityPassword
            ? <div>
              <p className={fourPinsTitle}>
                {title}
              </p>
              <div className={`${fourPinContainer}`} onKeyUp={e => this.onKeyUp(e)}>
                <input
                  inputMode='numeric'
                  type={passwordVisibility ? 'text' : 'password'}
                  disabled={disabled}
                  value={removeNonNumbers(firstNumber)}
                  name="firstNumber"
                  onChange={onChange}
                  className={displayErrors && firstNumber === '' ? displayRedBorders : pinInput}
                  maxLength={1}
                />
                <input
                  inputMode='numeric'
                  type={passwordVisibility ? 'text' : 'password'}
                  disabled={disabled}
                  value={removeNonNumbers(secondNumber)}
                  name="secondNumber"
                  onChange={onChange}
                  className={displayErrors && secondNumber === '' ? displayRedBorders : pinInput}
                  maxLength={1}
                />
                <input
                  inputMode='numeric'
                  type={passwordVisibility ? 'text' : 'password'}
                  disabled={disabled}
                  value={removeNonNumbers(thirdNumber)}
                  name="thirdNumber"
                  onChange={onChange}
                  className={displayErrors && thirdNumber === '' ? displayRedBorders : pinInput}
                  maxLength={1}
                />
                <input
                  inputMode='numeric'
                  type={passwordVisibility ? 'text' : 'password'}
                  disabled={disabled}
                  value={removeNonNumbers(fourthNumber)}
                  name="fourthNumber"
                  onChange={onChange}
                  className={displayErrors && fourthNumber === '' ? displayRedBorders : pinInput}
                  maxLength={1}
                />
                {
                  !loading && canShowPassword
                    ? <button
                      className={btnVisibility}
                      onClick={this.handleVisibility}>
                      <img
                        className={visibilityOnCSS}
                        src={passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
                        alt="visibilidad"
                      />
                    </button>
                    : null
                }
              </div>
              {
                hasSecurityPassword && canResetPass
                  ? <p onClick={openResetPassword} className={createPasswordText}>OLVIDÉ MI CONTRASEÑA ESPECIAL</p>
                  : null
              }
            </div>
            : <p onClick={openCreatePasswordClick} className={createPasswordText}>CREAR MI CONTRASEÑA ESPECIAL…</p>
        }
      </div>
    )

  }
}
export default FourPinsInput
