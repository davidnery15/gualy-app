
import React, { Component } from 'react'
import OptionsModalWrapper from '../OptionsModalWrapper'
import { css } from 'emotion'
import { Cropper } from 'react-image-cropper'
import { toast } from 'react-toastify'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { db } from '../../../firebase'
import AcceptButton from '../AcceptButton'

const myProfileContainer2 = css`
margin-bottom: 0px;
border-radius: 5px;
background-color: #2a2c6a;
-webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
padding-top: 21px;
display: -ms-flexbox;
display: flex;
flex-direction: column;
width: 100%;
min-height:300px;
height:auto;
position: relative;
max-width: 420px
  `
const header = css`
  position: absolute;
  top: -6px;
  left: 2px;
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
  `
const codeQrContainer = css`
  background-color: #fff;
  width: 100%;
  height: 80%;
  border: 12px solid #2a2c6a;
  border-radius: 5%;
  margin: auto;
  // max-height: 300px;
  max-width: 300px;
`
const copyTextContainer = css`
display: flex;
    justify-content: center;
    margin: 20px;
    flex-direction: column;
    height: 20%;
    textarea{
      background-color: #2a2c6a;
      resize: none;
      border: none;
      color: #95fad8
    }
`
const copyTextButton = css`
padding: 8px 18px;
    border-radius: 5px;
    border-radius: 100px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: center;
    color: #242656;
    cursor: pointer;
`
const afterImage = css`
width: 100%;
height: 100%
`
const buttonsContainer = css`
display: flex;
flex-direction: column;
`
const marginButton = css`
margin-bottom: 13px
`
class CropImageModal extends Component {
  state = {
    image: "",
    imageLoaded: false,
    loading: false
  }
  handleImageLoaded = () => {
    this.setState({
      imageLoaded: true
    })
  }
  handleClick = (image) => {
    this.setState({
      image: image.crop()
    })
  }
  cancelCrop = () => {
    this.setState({
      image: ""
    })
  }
  changeProfileImage = () => {
    this.setState({ loading: true })
    this.toastChangingProfileImageId = toast('Cambiando imagen...', { autoClose: false })
    try {
      db.ref(`users/${this.props.userKey}`).update({
        profilePicture: this.state.image
      }, () => {
        this.props.onClose()
        this.setState({ image: "", loading: false })
        toast.update(this.toastChangingProfileImageId, {
          render: 'Imagen cambiada.',
          autoClose: 5000,
        })
      })
    } catch (error) {
      customConsoleLog(error)
      this.setState({ loading: false })
      return toast.update(this.toastChangingProfileImageId, {
        render: 'Verifique su conexión y vuelva a intentarlo.',
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
    }
  }
  render() {
    const {
      show,
      onClose,
      image,
      onSave,
    } = this.props
    this.imageCroped = ""
    return (
      <OptionsModalWrapper
        show={show}
        onClose={this.state.loading ? null : onClose}
        width="310px"
      >
        <div>
          <div className={myProfileContainer2}>
            <div className={header}>
              <h1>MI CÓDIGO QR</h1>
            </div>
            <div className='horizontal-divisor'></div>
            <div className={codeQrContainer}>
              {
                this.state.image
                  ? <img
                    className={afterImage}
                    src={this.state.image}
                    alt=""
                  />
                  : <Cropper
                    width={200}
                    height={200}
                    allowNewSelection={false}
                    styles={{
                      source_img: {
                        WebkitFilter: 'blur(3.5px)',
                        filter: 'blur(3.5px)'
                      },
                      modal: {
                        opacity: 0.5,
                        backgroundColor: '#2a2c6a',
                      },
                      dotInner: {
                        borderColor: '#2a2c6a'
                      },
                      dotInnerCenterVertical: {
                        backgroundColor: '#ff0000'
                      },
                      dotInnerCenterHorizontal: {
                        backgroundColor: '#ff0000'
                      },
                      lineS: {
                        height: 40,
                      },
                      lineN: {
                        height: 40,
                      },
                      lineE: {
                        width: 40,
                      },
                      lineW: {
                        width: 40,
                      }
                    }}
                    src={image}
                    ref={ref => { this.imageCroped = ref }}
                    onImgLoad={() => this.handleImageLoaded()}
                  />
              }
            </div>
            <div className={copyTextContainer}>
              {
                !this.state.image
                  ? <button
                    onClick={() => this.handleClick(this.imageCroped)}
                    className={copyTextButton}
                  >
                    CORTAR
              </button>
                  : <div
                    className={buttonsContainer}
                  >
                    {
                      onSave
                        ? <AcceptButton
                          content="GUARDAR"
                          width="100%"
                          height="32px"
                          onClick={() => onSave(this.state.image, this.cancelCrop)}
                          className={marginButton}
                        />
                        : <AcceptButton
                          loading={this.state.loading}
                          content="GUARDAR"
                          width="100%"
                          height="32px"
                          onClick={this.changeProfileImage}
                          className={marginButton}
                        />

                    }
                    {
                      !this.state.loading
                        ? <AcceptButton
                          content=" CANCELAR"
                          width="100%"
                          height="32px"
                          onClick={this.cancelCrop}
                          className={marginButton}
                        />
                        : null
                    }
                  </div>
              }
            </div>
          </div>
        </div>
      </OptionsModalWrapper>
    )
  }
}
export default CropImageModal