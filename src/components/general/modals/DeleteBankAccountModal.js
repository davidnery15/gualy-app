import React from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import OptionsModalWrapper from '../OptionsModalWrapper'
const spinnerContainerDeleteBankModal = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 20px;
  `
const component = css`
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      // color: #f6f7fa;
      margin-top: 10px;
    }
  `
const contentContainer = css`
width: 100%;
  display: flex;
  flex-direction: column;
  `
const addBankAccountModalInputContainer = css`
  display: flex;
  flex-direction: row;
  justify-content: center;
  input{
    color: #242656;
  }
  `
const relative = css`
  position: relative
  `
const deletModalText = css`
  color: black
  `
  const deleteBankAccountModalContainer = css`
width: 100%;
height: auto;
border-radius: 4.2px;
box-shadow: 0 8px 8px 0 rgba(0, 0, 0, 0.15), 0 0 8px 0 rgba(0, 0, 0, 0.12);
border: solid 0.5px transparent;
background-image: linear-gradient(#f6f7fa, #f6f7fa), linear-gradient(to bottom, rgba(255, 255, 255, 0.2), rgba(255, 255, 255, 0.1) 5%, rgba(255, 255, 255, 0) 20%, rgba(255, 255, 255, 0));
background-origin: border-box;
background-clip: content-box, border-box;
  `
  const addBankAccountModalInputsContainer = css`
  display: flex;
  flex-direction: column;
  margin: 5px;
  margin-left: 15px;
  margin-right: 15px;
  `
  const addBankAccountButton = css`
width: 115px;
height: 38px;
border: none;
background: transparent;
margin: 10px;
cursor: pointer;
margin-top: 20px
  `
  const addBankAccountButtonText = css`
    font-size: 14.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    -webkit-letter-spacing: 0.5px;
    -moz-letter-spacing: 0.5px;
    -ms-letter-spacing: 0.5px;
    letter-spacing: 0.5px;
    text-align: center;
    color: #8078ff;
  `
const DeleteBankAccountModal = ({
  modalShow,
  onSubmit,
  show,
  width,
  onClose,
  isLoading,
  className
 }) => (
    <OptionsModalWrapper
      show={show}
      onClose={onClose}
      width={width}
      className={className}
    >
        <div
          className={`${contentContainer} ${component}`}>
          <div className={deleteBankAccountModalContainer}>
            <div className={addBankAccountModalInputsContainer}>
              <div className={addBankAccountModalInputContainer}>
                <p className={deletModalText}>Seguro que quieres eliminar esta cuenta?</p>
              </div>
              <div className={`${addBankAccountModalInputContainer} ${relative}`}>
                {
                  !isLoading ?
                    <div>
                      <button
                        onClick={modalShow}
                        className={addBankAccountButton}>
                        <p className={addBankAccountButtonText}>
                          CANCELAR
                        </p>
                      </button>
                    </div>
                    : null
                }
                {
                  !isLoading ?
                    <div>
                      <button
                        onClick={onSubmit}
                        className={addBankAccountButton}>
                        <p className={addBankAccountButtonText}>
                          ELIMINAR
                        </p>
                      </button>
                    </div>
                    : null
                }
                {
                  isLoading ?
                    <div className={`${spinnerContainerDeleteBankModal}`}> <Spinner color='blue' /> </div>
                    : null
                }
              </div>
            </div>
          </div>
        </div>
    </OptionsModalWrapper>
  )
export default DeleteBankAccountModal