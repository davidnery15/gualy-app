import React from 'react'
import { css } from 'emotion'
// import moment from 'moment'
// import axios from 'axios'
// import { ToastContainer, toast } from 'react-toastify'
import { style } from "react-toastify"
// import { BS } from '../../../config/currencies.js'
// import { banks } from '../../../constants/bankLogos'
// import iconReject from '../../../assets/ic-reject.svg'
import OptionsModalWrapper from '../OptionsModalWrapper'
// import { CLOUD_FUNCTIONS_URL } from '../../../config/urls'
import AmountInput from '../AmountInput'
import AcceptButton from '../AcceptButton'
import errorSVG from '../../../assets/ic-info-red.svg'
import ChooseAccountNumber from '../ChoseeAccountNumber'
style({
  colorProgressDefault: "#95fad8",
  fontFamily: "Montserrat",
})
const amountContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 20px;
    padding: 0 10px;
    span{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.2px;
      text-align: center;
      color: #f6f7fa;
    }
    input {
      padding: 3px;
    }
  `



const controlContainer = css`
    margin-top: 10px;
    padding: 0 !important;
    align-items: center;
    input {
      margin: 0 15px;
      margin-top: 20px;
      width: 85%;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
    }
    button {
      margin: 20px 15px;
      margin-top: 20px;
      width: 85%;
    }
  `
const divClass = css`
      margin: 0 15px;
      margin-top: 20px;
      width: 85%;
      font-size: 11px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
      opacity: 0.5;
  `

const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
const displayRedBorders = css`
    border-color:red !important;
    border-bottom: 1px solid #757575;
`
const WithdrawModal = ({
  show,
  onClose,
  onSubmit,
  amount,
  isLoading,
  handleInputChange,
  banksCards,
  userBankAccounts,
  displayErrors,
  errorMessage,
  amountInputHandleChange
}) => {
  return (
    <OptionsModalWrapper
      width="310px"
      show={show}
      onClose={!isLoading ? onClose : null}>
      <form className='content-container' name="withdrawForm" id="withdrawForm" onSubmit={onSubmit} noValidate>
        <div className={amountContainer}>
          <span>¿CUÁNTO QUIERES RETIRAR?</span>
          <AmountInput
            amount={amount}
            disabled={isLoading}
            spanBorder={`${(displayErrors === true &&
              !Number(amount.replace(/\./g, '').replace(',', '.')) > 0 ? displayRedBorders : ''
                ? displayRedBorders : '')}`}
            onChange={amountInputHandleChange}
          />
          <span>ELIGE TU CUENTA BANCARIA</span>
        </div>
        <ChooseAccountNumber 
          userBankAccounts={userBankAccounts}
          banksCards={banksCards}
          onCloseCurrentModal={onClose}
        />
        {
          displayErrors ?
            <div className={errorContainer}>
              {errorMessage}
              <img
                className={errorSvgCss}
                src={errorSVG}
                alt="error Icon"
              />
            </div>
            : null
        }
        {
          userBankAccounts.length > 0
            ? <div className={`content-container ${controlContainer}`}>
              <div className={divClass}>
                Los retiros pueden tardar hasta 72 Hrs en hacerse efectivos en tu cuenta bancaria
              </div>
              <AcceptButton
                content="CONFIRMAR RETIRO"
                width="100%"
                height="34px"
                loading={isLoading}
                type="submit"
              />
            </div>
            : null
        }
      </form>
    </OptionsModalWrapper>
  )
}
export default WithdrawModal