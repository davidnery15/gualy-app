import React from 'react'
import { css } from 'emotion'
import { QRCode } from 'react-qr-svg'
import AcceptButton from '../AcceptButton'
import OptionsModalWrapper from '../OptionsModalWrapper'
import AmountInput from '../AmountInput'
import QRcode from '../../../assets/ic-qr-code.svg'
import errorSVG from '../../../assets/ic-info-red.svg'
import AutoFocusInput from '../AutoFocusInput'

/**CSS */
const component = css`
    padding: 10px !important;
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
      padding: 3px;
      margin-top: 10px;
    }
  `
const amountContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 20px;
    padding: 0 10px;
    span{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.2px;
      text-align: center;
      color: #f6f7fa;
    }
    input:-webkit-autofill, input:-webkit-autofill:hover{
      -webkit-text-fill-color: #f6f7fa !important;
     }
  `
const qrText = css`
    font-size: 12px !important;
    font-weight: bold !important;
    font-style: normal !important;
    font-stretch: normal !important;
    line-height: normal !important;
    letter-spacing: 0.4px !important;
    text-align: center !important;
    color: #95fad8 !important;
    margin-top: 20px;
    cursor: pointer;
  `
const codeQrContainer = css`
    background-color: #fff;
    width: 200px;
    height: 200px;
    border: 12px solid #fff;
    border-radius: 5%;
    margin-top: 20px;
  `
const primaryButton = css`
    margin: 20px 20px;
    width: 100%;
  `
const displayRedBorders = css`
    border-color:red !important;
    border-bottom: 1px solid #757575;
`
const errorSvgCss = css`
width: 14.7px;
height: 13px;
object-fit: contain;
margin: 5px;
`
const errorContainer = css`
color: red;
display: flex;
justify-content: center;
align-items: center; 
font-size: 12px;
margin-top: 15px;
`
const RequestModal = ({
  onChange,
  onSubmit,
  data,
  loading,
  onClose,
  showModal,
  requestQR,
  onChangeQR,
  displayErrors,
  errorMessage,
  amountInputHandleChange
}) => {
  const { amount, description, email, userKey, formNames } = data
  const { idFormName, descriptionName, emailName } = formNames
  const codeQR = amount === '' && description === '' ? `userQR-${userKey}` : `dynamicQR-${userKey}-${amount}-${description}`
  return (
    <OptionsModalWrapper show={showModal} onClose={onClose} width="310px">
      <form
        onSubmit={onSubmit}
        noValidate autoComplete='off'
        id={idFormName}
        className={`content-container ${component}`}>
        <div className={amountContainer}>
          <span>¿CUÁNTO&nbsp;QUIERES&nbsp;SOLICITAR?</span>
          <AmountInput
            amount={amount}
            disabled={loading}
            spanBorder={`${(displayErrors === true &&
              !Number(amount.replace(/\./g, '').replace(',', '.')) > 0 ? displayRedBorders : ''
                ? displayRedBorders : '')}`}
            onChange={amountInputHandleChange}
          />
          <AutoFocusInput
            value={description}
            name={descriptionName}
            readOnly={loading}
            onChange={onChange}
            placeholder="Descripción del pago"
            type="text"
          />
          {
            !requestQR &&
            <AutoFocusInput
              value={email}
              name={emailName}
              readOnly={loading}
              onChange={onChange}
              placeholder="Email"
              type="email"
              autoComplete='email'
              required
            />
          }
          {
            !loading
              ? <span
                onClick={onChangeQR}
                className={qrText}>
                {requestQR ? 'OCULTA EL CÓDIGO QR' : 'O GENERA UN CÓDIGO QR'}
                <img css={`margin-bottom: 4px;margin-left:5px;`} src={QRcode} alt="qr" />
              </span>
              : null
          }
          {
            requestQR &&
            <div className={codeQrContainer} css={`margin-bottom: 4px;`}>
              <QRCode
                bgColor="#FFFFFF"
                fgColor="#2a2c6a"
                level="L"
                style={{ width: "100%" }}
                value={codeQR}
              />
            </div>
          }
          {
            !requestQR
              ? displayErrors ?
                <div className={errorContainer}>
                  {errorMessage}
                  <img
                    className={errorSvgCss}
                    src={errorSVG}
                    alt="error Icon"
                  />
                </div>
                : null
              : null
          }
          {
            !requestQR
              ? <AcceptButton
                content="SOLICITAR PAGO"
                width="100%"
                height="34px"
                loading={loading}
                type="submit"
                className={primaryButton}
              />
              : null}
        </div>
      </form>
    </OptionsModalWrapper>
  )
}

export default RequestModal