import React from 'react'
import { css } from 'emotion'
import OptionsModalWrapper from '../OptionsModalWrapper'
import AcceptButton from '../AcceptButton'
import RejectButton from '../RejectButton'
import CountUp from 'react-countup'
import { BS } from '../../../config/currencies.js'
import FourPinsInput from '../FourPinInput'
import CreateSpecialPassModal from './CreateSpecialPassModal'
import ResetSpecialPassModal from './ResetSpecialPassModal'
import errorSVG from '../../../assets/ic-info-red.svg'
const component = css`
    padding: 10px !important;
    posotion: relative;
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
      padding: 3px;
      margin-top: 10px;
    }
  `
const amountContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 65px;
    span{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.2px;
      text-align: center;
      color: #f6f7fa;
    }
  `
const descriptionText = css`
    margin: 20px 20px;
    width: 100%;
    text-align: center;
    white-space: pre-wrap;
  `
const senderPictureStyle = css`
    height: 96px;
    width: 96px;
    position: absolute;
    top: -48px;
    left: 0; 
    right: 0; 
    margin-left: auto; 
    margin-right: auto; 
    border-radius: 7px;
  `
const inputContainer = css`
  position: relative;
  padding: 0 !important;
  margin: 20px;
  overflow: hidden;
  span {
    opacity: 0.65;
    font-size: 39.5px !important;
    font-weight: normal !important;
    font-style: normal !important;
    font-stretch: normal !important;
    line-height: normal !important;
    letter-spacing: normal !important;
    text-align: center !important;
    color: #95fad8 !important;
    white-space: normal !important;
    margin: auto !important;
  }
`
const modalButtons = css`
margin: 11px;
`
const userNameClass = css`
margin-bottom: 5px;
color: #8078ff !important;
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: 1.2px;
text-align: center;
`
const mainText = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
`
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
export default ({
  description,
  loading,
  onClose,
  showModal,
  amount,
  userName,
  onSendMoney,
  onRejectMoney,
  senderPicture,
  isReject,
  //security password
  firstNumber,
  secondNumber,
  thirdNumber,
  fourthNumber,
  onCreatePasswordClick,
  isCreatePasswordOpen,
  cancelCreatePasswordClick,
  onCreateSpecialPasswordClick,
  openCreatePasswordClick,
  hasSecurityPassword,
  onChange,
  displayErrors,
  errorMessage,
  //reset special password
  isResetPasswordOpen,
  onResetSpecialPasswordClick,
  cancelResetPasswordClick,
  openResetPassword,
  isSended,
  specialPasswordIsShow
}) => {
  return (
    <OptionsModalWrapper show={showModal} onClose={onClose} width="328px" height="auto">
      {
        isCreatePasswordOpen && !isReject
          ? <CreateSpecialPassModal
            onChange={onChange}
            loading={loading}
            firstNumber={firstNumber}
            secondNumber={secondNumber}
            thirdNumber={thirdNumber}
            fourthNumber={fourthNumber}
            onCreatePasswordClick={onCreatePasswordClick}
            cancelCreatePasswordClick={cancelCreatePasswordClick}
            onCreateSpecialPasswordClick={onCreateSpecialPasswordClick}
            displayErrors={displayErrors}
            errorMessage={errorMessage}
          />
          : isResetPasswordOpen && !isReject
          ?  <ResetSpecialPassModal
          loading={loading}
          onResetSpecialPasswordClick={onResetSpecialPasswordClick}
          cancelResetPasswordClick={cancelResetPasswordClick}
          displayErrors={displayErrors}
          errorMessage={errorMessage}
          isSended={isSended}
        />
         : <div className={`content-container ${component}`}>
            <img
              src={senderPicture}
              alt=""
              className={senderPictureStyle}
            />
            <div className={amountContainer}>
              {
                isReject
                  ? <p className={mainText}>
                    VAS A RECHAZAR LA SOLICITUD DE
                </p>
                  :
                  <p className={mainText}>
                    VAS A ENVIAR UN PAGO A
                </p>
              }
              <p
                className={userNameClass}
              >
                {userName ? userName.toUpperCase() : ""}
              </p>
              <p className={mainText}>
                DE
                </p>
              <div className={`${inputContainer} content-container`}>
                <span
                  style={{ width: "100%" }}
                >
                  <CountUp
                    start={amount}
                    end={amount}
                    duration={0}
                    separator="."
                    decimals={2}
                    decimal=","
                    prefix={`${BS}`}
                  />
                </span>
              </div>
              {
                description
                  ? <p
                    className={descriptionText}
                  >
                    {`POR: ${description.toUpperCase()}`}
                  </p>
                  : null
              }
              {
                !loading && !isReject && specialPasswordIsShow
                  ? <FourPinsInput
                    firstNumber={firstNumber}
                    secondNumber={secondNumber}
                    thirdNumber={thirdNumber}
                    fourthNumber={fourthNumber}
                    onChange={onChange}
                    disabled={loading}
                    onCreatePasswordClick={onCreatePasswordClick}
                    openCreatePasswordClick={openCreatePasswordClick}
                    displayErrors={displayErrors}
                    hasSecurityPassword={hasSecurityPassword}
                    title="Contraseña Especial"
                    openResetPassword={openResetPassword}
                    canResetPass={true}
                    loading={loading}
                    canShowPassword={true}
                  />
                  : null
              }
              {
                displayErrors ?
                  <div className={errorContainer}>
                    {errorMessage}
                    <img
                      className={errorSvgCss}
                      src={errorSVG}
                      alt="error Icon"
                    />
                  </div>
                  : null
              }
              {
                isReject
                  ? <RejectButton
                    onClick={onRejectMoney}
                    loading={loading}
                    className={modalButtons}
                    content="RECHAZAR"
                    width="200px"
                    height="30px"
                  />
                  : <AcceptButton
                    onClick={onSendMoney}
                    loading={loading}
                    className={modalButtons}
                    content="DE ACUERDO"
                    width="200px"
                    height="30px"
                  />
              }

            </div>
          </div>
      }
    </OptionsModalWrapper>
  )
}
