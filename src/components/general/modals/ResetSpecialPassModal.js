import React from 'react'
import AcceptButton from '../AcceptButton'
import { css } from 'emotion'
import createLockIcon from '../../../assets/createPasswordLockIcon.svg'
import errorSVG from '../../../assets/ic-info-red.svg'
const primaryButton = css`
    margin: auto;
    margin-top: 20px;
    margin-bottom: 20px;
    width: 100%;
  `
const createPasswordContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center
  `
const createLockIconClass = css`
    width: 30.5px;
    height: 40px;
    margin: auto;
    margin-top: 34px;
    margin-bottom: 24px;
  `
const createSpecialPasswordTitle = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.21;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
margin-left: 20px;
margin-right: 20px;
  `
const cancelButtonText = css`
  font-size: 11.5px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: 0.4px;
  text-align: center;
  color: #95fad8;
  margin-bottom: 25px;
  cursor: pointer;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
  const recoverTitleCard = css`
    margin-top: 35px !important;
    font-size: 22px;
    font-weight: 900;
    line-height: 1.2;
  `
const ResetSpecialPassModal = ({
  loading,
  cancelResetPasswordClick,
  onResetSpecialPasswordClick,
  displayErrors,
  errorMessage,
  isSended
}) => (
    <div className={createPasswordContainer}>
      {
        isSended
          ? <div className="p-6 text-center" css={`margin: 25px`}>
            <span className={recoverTitleCard}><br /> Casi listo, ahora ingresa a tu correo y sigue las instrucciones</span>
            <AcceptButton
              content="ENTENDIDO"
              width="150px"
              height="34px"
              loading={loading}
              onClick={cancelResetPasswordClick}
              className={primaryButton}
            />
          </div>
          : <div className={createPasswordContainer}>
            <img
              src={createLockIcon}
              className={createLockIconClass}
              alt=""
            />
            <p className={createSpecialPasswordTitle}>RESTABLECER CONTRASEÑA ESPECIAL</p>
            {
              displayErrors ?
                <div className={errorContainer}>
                  {errorMessage}
                  <img
                    className={errorSvgCss}
                    src={errorSVG}
                    alt="error Icon"
                  />
                </div>
                : null
            }
            <AcceptButton
              content="RESTABLECER"
              width="150px"
              height="34px"
              loading={loading}
              onClick={onResetSpecialPasswordClick}
              className={primaryButton}
            />
            <p onClick={cancelResetPasswordClick} className={cancelButtonText}>CANCELAR</p>
          </div>
      }
    </div>
  )
export default ResetSpecialPassModal