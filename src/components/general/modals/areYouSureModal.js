import React, { Component } from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
const contentContainer = css`
  width: 100%;
  display: flex;
  flex-direction: column;
  `
const component = css`
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      // color: #f6f7fa;
      margin-top: 10px;
    }
  `
const ModalContainer = css`
width: 100%;
height: 135px;
border-radius: 5px;
ackground-color: #2a2c6a;
box-shadow: 0 9px 18px 0 rgba(0,0,0,0.25);
  `
const ModalInputsContainer = css`
  display: flex;
  flex-direction: column;
  margin: 5px;
  margin-left: 15px;
  margin-right: 15px;
  `
const ModalInputContainer = css`
  display: flex;
  flex-direction: row;
  justify-content: center;
  input{
    color: #242656;
  }
  `
const ModalText = css`
  color: #F6F7FA;
  margin-top: 24px;
  `
const relative = css`
  position: relative
  `
const Button = css`
width: 115px;
height: 38px;
border: none;
background: transparent;
margin: 10px;
cursor: pointer;
margin-top: 20px
  `
const ButtonText = css`
    font-size: 14.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    -webkit-letter-spacing: 0.5px;
    -moz-letter-spacing: 0.5px;
    -ms-letter-spacing: 0.5px;
    letter-spacing: 0.5px;
    text-align: center;
  `
const positiveColor = css`
  color: #95FAD8
  `
const negativeColor = css`
  color: #FF6061
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
const spinnerMargin = css`
  margin-top: 20px
  `
export default class AreYouSureModal extends Component {
  render() {
    const {
      modalShow,
      onSubmit,
      message,
      cancelText,
      actionText,
      isLoading
    } = this.props
    return (
      <div>
        <form
          className={`${contentContainer} ${component}`}>
          <div className={ModalContainer}>
            <div className={ModalInputsContainer}>
              <div className={ModalInputContainer}>
                <p className={ModalText}>{message}</p>
              </div>
              <div className={`${ModalInputContainer} ${relative}`}>
                {
                  !isLoading ?
                    <div>
                      <button
                        onClick={modalShow}
                        className={Button}>
                        <p
                          className={`${ButtonText} ${negativeColor}`}>
                          {cancelText}
                        </p>
                      </button>
                    </div>
                    : null
                }
                {
                  !isLoading ?
                    <div>
                      <button
                        onClick={onSubmit}
                        className={Button}>
                        <p className={`${ButtonText} ${positiveColor}`}>
                          {actionText}
                        </p>
                      </button>
                    </div>
                    : null
                }
                {
                  isLoading ?
                    <div
                      className={`${spinnerContainer} ${spinnerMargin}`}>
                      <Spinner color='white' />
                    </div>
                    : null
                }
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}