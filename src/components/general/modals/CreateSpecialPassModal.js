import React from 'react'
import AcceptButton from '../AcceptButton'
import { css } from 'emotion'
import FourPinsInput from '../FourPinInput'
import createLockIcon from '../../../assets/createPasswordLockIcon.svg'
import errorSVG from '../../../assets/ic-info-red.svg'
const primaryButton = css`
    margin: auto;
    margin-top: 20px;
    margin-bottom: 20px;
    width: 100%;
  `
const createPasswordContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center
  `
const createLockIconClass = css`
    width: 30.5px;
    height: 40px;
    margin: auto;
    margin-top: 34px;
    margin-bottom: 24px;
  `
const createSpecialPasswordTitle = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.21;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
  `
const createPasswordDescriptionText = css`
  font-size: 19px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: normal;
  text-align: left;
  color: #f6f7fa;
  margin-left: 30px;
  margin-right: 20px;
  `
const cancelButtonText = css`
  font-size: 11.5px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: 0.4px;
  text-align: center;
  color: #95fad8;
  margin-bottom: 25px;
  cursor: pointer;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
const CreateSpecialPassModal = ({
  onChange,
  loading,
  firstNumber,
  secondNumber,
  thirdNumber,
  fourthNumber,
  cancelCreatePasswordClick,
  onCreateSpecialPasswordClick,
  displayErrors,
  errorMessage
}) => (
    <div className={createPasswordContainer}>
      <img
        src={createLockIcon}
        className={createLockIconClass}
        alt=""
      />
      <p className={createSpecialPasswordTitle}>CREAR CONTRASEÑA ESPECIAL</p>
      <p className={createPasswordDescriptionText}>Esta contraseña especial, será solicitada para verificar la identidad del operador para realizar operaciones específicas.</p>
      {
        !loading
          ? <FourPinsInput
            firstNumber={firstNumber}
            secondNumber={secondNumber}
            thirdNumber={thirdNumber}
            fourthNumber={fourthNumber}
            onChange={onChange}
            disabled={loading}
            displayErrors={displayErrors}
            hasSecurityPassword={true}
            title="Ingresa cuatro números"
            canResetPass={false}
            loading={loading}
            canShowPassword={true}
          />
          : null
      }
      {
        displayErrors ?
          <div className={errorContainer}>
            {errorMessage}
            <img
              className={errorSvgCss}
              src={errorSVG}
              alt="error Icon"
            />
          </div>
          : null
      }
      <AcceptButton
        content="CREAR CONTRASEÑA"
        width="210px"
        height="34px"
        loading={loading}
        onClick={onCreateSpecialPasswordClick}
        className={primaryButton}
      />
      <p onClick={cancelCreatePasswordClick} className={cancelButtonText}>CANCELAR</p>
    </div>
  )
export default CreateSpecialPassModal