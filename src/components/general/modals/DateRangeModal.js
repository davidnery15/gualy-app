import React from 'react'
import { DateRange } from 'react-date-range'
import AcceptButton from '../AcceptButton'
import 'react-date-range/dist/styles.css' // main style file
import 'react-date-range/dist/theme/default.css' // theme css file
const DateRangeModal = ({
    onChange,
    moveRangeOnFirstSelection,
    ranges,
    onSearchClick
}) => {
    return <div css={`
    text-align: center;
    padding-bottom: 25px;
    `}>
        <DateRange
            onChange={onChange}
            moveRangeOnFirstSelection={moveRangeOnFirstSelection}
            ranges={[ranges]}
        />
        <AcceptButton
            onClick={onSearchClick}
            content="Buscar"
        />
    </div>
}
export default DateRangeModal