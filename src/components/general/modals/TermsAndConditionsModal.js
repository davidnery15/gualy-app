import React from 'react'
import OptionsModalWrapper from '../OptionsModalWrapper'
import { css } from 'emotion'
import { Button } from 'reactstrap'
const fontWeight = css`
font-weight: 400
  `
const termsAndConditionsModal = css`
    width: 90%;
    // height: 80%
    display: flex;
    justify-content: center;
    flex-direction: column;
    position: relative;
    margin: auto;
`
const termsAndConditionsTextContainer = css`
  position: absotule;
  overflow: auto;
  p{
    color:#ffffff;
  }
`
const termsAndCnditionsTitle = css`
margin-top: 0;
text-align: center;
 h1{
  color: #ffffff;
  font-size: 34px;
  line-height: 1.1em;
  margin-top: 35px;
  margin-bottom: 35px;
 }
`
const buttonLogIn = css`
    cursor: pointer;
    position:relative;
    min-height: 48px;
  `
const registerButton = css`
width: 299px !important;
height: 51.8px;
margin: auto;
@media(max-width: 450px){
  width: 100% !important;
}
  `
const loginButtonContainer = css`
    margin: 0 20px;
    margin-bottom: 35px;
    margin-top: 30px;
  `
const TermsAndConditionsModal = ({
    onAccepted,
    height,
    width,
    show,
    onClose,
}) => {
    return (
        <OptionsModalWrapper
            show={show}
            onClose={onClose}
            width={width}
            height={height}
        >
            <div className={termsAndConditionsModal}>
                <div className={termsAndCnditionsTitle}>
                    <h1>TÉRMINOS Y CONDICIONES DE GUALY</h1>
                </div>
                <div className={termsAndConditionsTextContainer}>
                    <ol>
                        <li><b> INTRODUCCIÓN</b></li>
                    </ol>
                    <p><span className={fontWeight}>El presente Acuerdo constituye un </span><b>DOCUMENTO IMPORTANTE</b><span className={fontWeight}>. </span></p>
                    <p><span className={fontWeight}>Se trata de un contrato electrónico</span> <span className={fontWeight}>vinculante entre </span><b>GUALY PAYMENT, C.A</b><span className={fontWeight}>.,</span><b> RIF</b> <b>J410269367,</b><span className={fontWeight}> sociedad mercantil inscrita y domiciliada en la ciudad de Maracaibo del Estado Zulia, en Venezuela (en lo sucesivo denominado como Gualy) y tú (en lo sucesivo denominado como usuario), que se aplica a tu uso de: </span><b>el sitio web, la aplicación móvil, la aplicación web, los productos o servicios que podamos proporcionar u ofrecer, tu cuenta Gualy (cuenta de miembro), cualquier otro sitio web o aplicaciones asociados y sujetos a un Acuerdo para developers.</b></p>
                    <p><span className={fontWeight}>Podemos modificar o actualizar el presente Acuerdo periódicamente y te notificaremos dichas modificaciones bajo nuestra Política de prestación de comunicaciones electrónicas expuesta en la Sección 2. Tu uso continuado del Servicio estará sujeto a las modificaciones que introduzcamos.</span></p>
                    <p><span className={fontWeight}>Gualy es un servicio para y entre miembros únicamente. No puedes utilizar el Servicio a menos que pases a ser miembro registrado y verificado de Gualy y te crees una Cuenta suministrando la información personal o comercial requerida para formar parte de nuestra comunidad. Para registrarte en Gualy deberás indicar tildando la casilla de verificación que </span><b>ACEPTAS</b><span className={fontWeight}> expresamente haber </span><b>LEÍDO</b><span className={fontWeight}>, </span><b>ENTENDIDO</b><span className={fontWeight}> y </span><b>ACEPTADO</b><span className={fontWeight}> todos los términos y condiciones del presente Acuerdo. Al registrarte como miembro de Gualy y/o al utilizar el Servicio, nuestro sistema te enviará al correo electrónico que hayas registrado en nuestra plataforma un mensaje de dato el cual contendrá un link de verificación, con lo cual al recibir dicho mensaje de dato en tu correo electrónico y dar clic en el link de verificación del correo electrónico ya podrás comenzar a usar la aplicación; en este correo también encontrarás un link de acceso a los presentes términos y condiciones, el cual podrás imprimir y/o guardar en tus archivos.</span></p>
                    <p><span className={fontWeight}>También </span><b>RECONOCES </b><span className={fontWeight}>expresamente haber </span><b>LEÍDO</b><span className={fontWeight}> y </span><b>ENTENDIDO</b><span className={fontWeight}> nuestra&nbsp;Política de datos y Privacidad, nuestra&nbsp;Política de Cookies, nuestra Política de Prestación de Comunicaciones electrónicas (expuesta en la Sección 2 a continuación) y nuestro&nbsp;Acuerdo para Developers.</span></p>
                    <p><span className={fontWeight}>Si no aceptas los términos del Acuerdo, no podrás unirte como miembro, no podrás crear una Cuenta Gualy y no tendrás la autorización para utilizar el Servicio con ningún fin. Si tienes alguna pregunta, puedes ponerte en contacto con nuestro centro de asistencia para miembros y usuarios al correo info@gualy.com.</span></p>
                    <p><span className={fontWeight}>De igual manera con el uso de nuestro servicio por parte del usuario se entenderá que existe una aceptación voluntaria de nuestros términos y condiciones establecidos en el presente Acuerdo, de nuestra&nbsp;Política de Datos y Privacidad, nuestra&nbsp;Política de Cookies, nuestra Política de Prestación de Comunicaciones Electrónica.</span></p>
                    <p><span className={fontWeight}>Si aceptas los términos y condiciones de este Acuerdo en nombre de una persona jurídica, confirmas que eres agente autorizado para hacerlo en nombre de dicha entidad, sin limitación para que en cualquier momento te sea solicitado cualquier documento referido a tu cualidad de representante de la persona jurídica.</span></p>
                    <p><b>ÉSTE ES UN DOCUMENTO DE GRAN IMPORTANCIA, QUE DEBES ANALIZAR ATENTAMENTE PARA DECIDIR SI ACCEDES Y/O PASAS A SER MIEMBRO Y/O UTILIZAR EL SERVICIO. </b></p>
                    <p><b>ESTE ACUERDO CONTIENE DISPOSICIONES QUE RIGEN LA RESOLUCIÓN DE LAS RECLAMACIONES QUE TÚ Y NOSOTROS PODAMOS TENER ENTRE NOSOTROS (VER SECCIÓN 15 “CONTROVERSIAS”). </b></p>
                    <p><b>CONTIENE ASIMISMO UN ACUERDO PARA EL ARBITRAJE, QUE REQUERIRÁ QUE TANTO TÚ COMO NOSOTROS PRESENTEMOS CUALQUIER RECLAMACIÓN ANTE UN ARBITRAJE VINCULANTE Y FINAL, A MENOS QUE RECHACES EL ACUERDO SOBRE ARBITRAJE (VER SECCIÓN 15 “CONTROVERSIAS”) ANTES DE QUE PASEN 30 DÍAS TRAS LA FECHA DE ACEPTACIÓN DE ESTE ACUERDO POR PRIMERA VEZ. </b></p>
                    <p><b>CONSIDERAREMOS LA FECHA DE ACEPTACIÓN EL DÍA EN QUE COMIENCES EL PROCESO DE REGISTRO PARA SER MIEMBRO DE GUALY, RECIBIENDO EL MENSAJE DE DATO EN TU CORREO ELECTRÓNICO REGISTRADO Y HACIENDO CLIC EN EL LINK (BOTÓN) DE VERIFICACIÓN DE CORREO ELECTRÓNICO. A MENOS QUE PRESENTES EL RECHAZO POR ESCRITO EN LOS 30 DÍAS POSTERIORES A LA ACEPTACIÓN DEL PRESENTE ACUERDO POR PRIMERA VEZ: (A) SOLO SE TE PERMITIRÁ PRESENTAR RECLAMACIONES CONTRA GUALY DE FORMA INDIVIDUAL, NO COMO DEMANDANTE NI COMO PARTE DE UNA DEMANDA COLECTIVA, NI REPRESENTATIVA, Y (B) SOLO SE TE PERMITIRÁ BUSCAR COMPENSACIÓN (SEA MONETARIA, MEDIDAS CAUTELARES Y REPARACIÓN DECLARATIVA) DE MANERA INDIVIDUAL.</b></p>
                    <p><b>NUESTRA RESPONSABILIDAD CONTIGO ESTÁ LIMITADA POR EL PRESENTE ACUERDO (VER SECCIÓN 17 “SIN GARANTÍAS, DESCARGO DE RESPONSABILIDAD Y DE OBLIGACIONES”).</b></p>
                    <p><b>2.COMUNICACIONES ELECTRÓNICAS.</b></p>
                    <p><span className={fontWeight}>2.1 Podemos estar obligados a facilitarte ciertas informaciones, avisos y otras comunicaciones (colectivamente “Comunicaciones”) por escrito. En virtud del presente Acuerdo, te enviaremos dichas Comunicaciones en formato electrónico. Al aceptar este Acuerdo, confirmas la posibilidad y consentimiento para recibir dichas Comunicaciones electrónicamente, en lugar de en papel.</span></p>
                    <p><span className={fontWeight}>2.2 Aceptas y consientes la recepción electrónica de todas las Comunicaciones, incluyendo, entre otras, este Acuerdo, las enmiendas a este Acuerdo y otros documentos, avisos e informaciones que proporcionemos en relación con tu uso del Servicio. Las “Comunicaciones” incluyen, entre otros:</span></p>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>acuerdos y políticas que aceptas (ej. este Acuerdo y la&nbsp;</span><a href="https://gualy.com/politica-de-datos-y-de-privacidad/"><span className={fontWeight}>Política de datos y de privacidad</span></a><span className={fontWeight}>), incluyendo actualizaciones a dichos acuerdos o políticas;</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>confirmaciones de transacciones, acuses de recibos, autorizaciones, divulgaciones, facturas o confirmaciones;</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>estados e historiales de transacciones;</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>declaraciones fiscales nacionales, regionales y estatales, si nos exigen ponerlas a tu disposición; y</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>todas las demás comunicaciones o documentos relacionados contigo o con tu Cuenta Gualy (Cuenta de Miembro) y el uso que hagas del Servicio.</span></li>
                    </ul>
                    <p><span className={fontWeight}>2.3 Las Comunicaciones electrónicas se considerarán recibidas del modo siguiente:</span></p>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>al ser recibidas en la dirección de correo electrónico que tienes registrado en nuestro sistema durante el proceso de registro</span><span className={fontWeight}> en el Servicio;</span></li>
                        <li className={fontWeight}><span className={fontWeight}>al publicarlas en tu Cuenta o en una aplicación de móvil asociada;</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>al publicarlas en el sitio web o en una aplicación móvil asociada a nosotros o al Servicio, </span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>al comunicártelas de otro modo a través del Servicio.</span></li>
                    </ul>
                    <p><span className={fontWeight}>2.4 Es responsabilidad tuya el abrir y revisar las Comunicaciones que te enviamos a través de los métodos descritos arriba. Podemos, pero no estamos obligados a ello, avisarte de la disponibilidad de una Comunicación enviada a través de alguno de los métodos descritos arriba (por ejemplo, informándote de dicha Comunicación mediante notificación a tu dispositivo móvil).</span></p>
                    <p><span className={fontWeight}>2.5 Es responsabilidad tuya registrar tu dirección de correo electrónico principal y mantenerla actualizada para que podamos comunicarnos contigo electrónicamente. Entiendes y aceptas que si te enviamos una Comunicación electrónica pero no la recibes porque la dirección de e-mail principal registrada con nosotros es incorrecta, está desactualizada, bloqueada por tu proveedor o no puedes recibir Comunicaciones electrónicas por alguna otra razón, consideraremos que sí te hemos enviado la Comunicación.</span></p>
                    <p><span className={fontWeight}>2.6 Ten en cuenta que, si utilizas filtros de spam que bloquee o desvíe los correos electrónicos de remitentes no incluidos en tu libreta de direcciones, puede que tengas que añadirnos para poder recibir las Comunicaciones que te enviamos. No es responsabilidad de Gualy que las comunicaciones enviadas sean distribuidas al correo spam o correo no deseado, estos mensajes de datos o comunicaciones se considerarán efectivamente enviadas y aceptadas por el USUARIO. Es única y exclusiva RESPONSABILIDAD DEL USUARIO revisar la carpeta de spam o correo no deseado para verificar que el mensaje de datos o comunicaciones enviadas por Gualy no se encuentren en dichas carpetas y en caso de encontrarse allí deberá el usuario marcar nuestras comunicaciones como correo deseado o no spam, para evitar que los mensajes de datos enviados por Gualy entren en esta categoría.</span></p>
                    <p><span className={fontWeight}>2.7 Puedes actualizar tu dirección de correo electrónica principal y otra información de contacto en cualquier momento iniciando sesión en el Servicio y navegado por tu perfil. Si tu dirección de correo electrónico deja de ser válida y las Comunicaciones electrónicas que te enviamos nos son devueltas, podemos considerar tu Cuenta inactiva y quizás no puedas utilizar el Servicio hasta que nos mandes una dirección de correo electrónico principal válida.</span></p>
                    <p><span className={fontWeight}>2.9 Si solicitas copias adicionales de las Comunicaciones o si retiras tu consentimiento, se aplicarán los términos adicionales siguientes:</span></p>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>puedes ponerte en contacto con nosotros, o con otro proveedor de servicios aplicable, para solicitar otra copia electrónica de la Comunicación, de manera gratuita;</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>puedes solicitar una copia en papel de dicha Comunicación electrónica en los noventa días posteriores a la fecha de publicación de la Comunicación original, y nos reservamos el derecho a cobrarte dicha copia en papel;</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>puedes ponerte en contacto con nosotros para actualizar tu información de registro utilizada para Comunicaciones electrónicas o para retirar el consentimiento a recibir Comunicaciones electrónicas de publicidad.</span></li>
                        <li className={fontWeight}><span className={fontWeight}>La revocatoria de la autorización para recibir comunicaciones electrónicas en tu correo electrónico solo aplica para comunicaciones de publicidad y promociones, pero NUNCA para comunicaciones vinculantes entre Gualy y el usuario, tales como contrato de términos y condiciones; nuestra&nbsp;Política de datos y Privacidad, nuestra&nbsp;Política de Cookies, nuestra Política de prestación de comunicaciones electrónica, notificaciones de acciones generadas en tu cuenta; y</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>nos reservamos el derecho a rescindir tu uso del Servicio y de cualquier producto o servicio de proveedores de servicios asociados si rechazas o retiras el consentimiento a recibir Comunicaciones electrónicas.</span></li>
                    </ul>
                    <p><span className={fontWeight}>Puedes&nbsp;</span><span className={fontWeight}>ponerte en contacto con nosotros mediante el correo electrónico </span><a href="mailto:soporte@gualy.com"><span className={fontWeight}>soporte@gualy.com</span></a><span className={fontWeight}>&nbsp;en relación con esta Sección 2.</span></p>
                    <ol start="3">
                        <li><b> DECLARACIONES QUE NOS PRESENTAS.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Declaras y nos garantizas que las afirmaciones siguientes son y serán ciertas:</span></p>
                    <p><span className={fontWeight}>3.1 Eres mayor de 18 años y estás facultado para celebrar un acuerdo legalmente vinculante, en el caso de una persona natural. </span></p>
                    <p><span className={fontWeight}>3.1.1 En el caso de una persona jurídica, declaras que eres el representante de la misma y estas facultado para representarla, asumiendo la plena responsabilidad del uso que haga la persona jurídica de nuestros servicios.</span></p>
                    <p><span className={fontWeight}>3.2 Resides legalmente en Venezuela. En caso de una persona jurídica el domicilio fiscal es en Venezuela.</span></p>
                    <p><span className={fontWeight}>3.3 No participarás en ningún Negocio prohibido ni en ningún Uso prohibido. </span></p>
                    <p><span className={fontWeight}>3.4 Nos facilitarás tu identidad legal y toda información y documentos de apoyo que solicitemos, incluyendo, entre otros, una copia de tu cédula de identidad, licencia de conducir y/o una copia de tu pasaporte. Adicional a los anteriores documentos en el caso de una persona jurídica nos facilitaras, documento constitutivo estatutario, ultima acta de asamblea, RIF y cualquier otro documento relacionado, en caso de ser solicitado.</span></p>
                    <p><span className={fontWeight}>3.4.1 Durante el proceso de registro, registrarás tu dirección de correo electrónico principal, el cual se debe encontrar activo y ser revisado constantemente, ya que la misma servirá de canal para enviar mensajes de datos o comunicaciones oficiales y recibir por parte del usuario los mismos.</span></p>
                    <p><span className={fontWeight}>3.5 Cualquier valor (moneda o criptomoneda) que deposites en tu cuenta virtual te pertenecen o tienes la autoridad legal para utilizarlos y no están sujetos a ningún gravamen, interés de garantía o reclamación de cualquier tipo. Adicionalmente, estos provienen de actividades lícitas y así lo declaras bajo fe de juramento.</span></p>
                    <p><span className={fontWeight}>3.6 El uso que hagas del Servicio cumple con la legislación y reglamento aplicables, en la República Bolivariana de Venezuela, así como los acuerdos y tratados internacionales tendientes a prevenir delitos económicos, la legitimación de capitales </span><i><span className={fontWeight}>o </span></i><span className={fontWeight}>lavado de dinero, narcotráfico, trata de personas, contrabando de extracción, entre otros.</span></p>
                    <p><span className={fontWeight}>3.7 No accederás al Servicio utilizando procedimiento automatizados de ningún tipo. Para los developers, por favor ver nuestro&nbsp;Acuerdo para Developers </span></p>
                    <p><span className={fontWeight}>3.8 No enviarás, utilizarás o subirás ningún script, virus o código malicioso. No programarás extensiones, plugins o aplicaciones excepto las permitidas en virtud de nuestro Acuerdo para Developers.</span></p>
                    <p><span className={fontWeight}>3.9 No harás nada que pueda desactivar, sobrecargar o dañar el funcionamiento, procesamiento o apariencia del Servicio, incluyendo la denegación de servicio u otros ataques.</span></p>
                    <p><span className={fontWeight}>3.10 Toda la información que nos facilites estará completa y será precisa, y te comprometes a mantener toda la información de este modo en todo momento, en especial actualizada.</span></p>
                    <p><span className={fontWeight}>3.11 Solo podrás crear, poseer, administrar y/o controlar una Cuenta Si necesitas una Cuenta Gualy adicional, tendrás que conseguir primero nuestro permiso por escrito. Puedes&nbsp;</span><span className={fontWeight}>ponerte en contacto con nosotros mediante el correo electrónico </span><a href="mailto:soporte@gualy.com"><span className={fontWeight}>soporte@gualy.com</span></a><span className={fontWeight}>&nbsp;para solicitar una Cuenta Gualy adicional, con el objetivo propuesto de una Cuenta Gualy adicional. No tenemos ninguna obligación de aprobar ninguna Cuenta Gualy adicional. Si descubrimos que has abierto, has gestionado y/o gestionas una o más Cuentas Gualy adicionales, sin nuestro permiso, nos reservamos todos los derechos, incluyendo, entre otros, cancelar tu(s) Cuenta(s) Gualy, revocar tu licencia limitada para utilizar el Servicio y emprender cualquier otra medida que consideremos necesaria para proteger a nuestros miembros, a nosotros mismos y al Servicio.</span></p>
                    <ol start="4">
                        <li><b> PREGUNTAS ACERCA DE TI.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Necesitamos saber quién eres por para cumplir el reglamento legal. Podemos pedirte que nos facilites datos personales tales como tu nombre legal, dirección, cédula de identidad, pasaporte, identificación gubernamental, fecha de nacimiento, número de información fiscal (RIF), licencia de conducir, información bancaria o de tarjetas de crédito. También podemos hacerte preguntas y comprobar la información nosotros mismos o a través de entidades de confianza con las que trabajamos en asuntos de verificación y cumplimiento. Aceptas que podamos utilizar la información que nos facilites y otros datos para comprobar tu identidad. Esto puede incluir la consulta de información pública disponible y preguntas a otras entidades, como bancos, para obtener información sobre ti. Entiendes y aceptas que podamos realizar toda pesquisa que consideremos necesaria, bien directamente o a través de terceros en relación con tu identidad y tu solvencia, incluyendo, entre otras, que te solicitemos que tomes ciertas medidas para confirmar la titularidad de tu dirección de correo electrónico o de instrumentos financieros, solicitar un informe de crédito o contrastar información con bases de terceros o mediante otras fuentes. Puedes consultar la información adicional en nuestra&nbsp;</span><a href="https://gualy.com/politica-de-datos-y-de-privacidad/"><span className={fontWeight}>Política de datos y de privacidad</span></a><span className={fontWeight}>.</span><span className={fontWeight}> Si suscribes el presente Acuerdo en nombre de una persona jurídica o si abres una Cuenta en el Servicio corporativo, por la presente nos facilitas instrucciones escritas y autorización de conformidad con el Derecho Mercantil vigente para tal fin. </span></p>
                    <ol start="5">
                        <li><b> DESCRIPCIÓN DEL SERVICIO, TRANSFERENCIA DE VALOR Y SERVICIOS QUE NO PROPORCIONAMOS.</b></li>
                    </ol>
                    <p><span className={fontWeight}>5.1 Proporcionamos una billetera virtual que muestra la disponibilidad del saldo en tu cuenta en Bolívares. </span></p>
                    <p><span className={fontWeight}>5.2 Para introducir saldo en la billetera virtual es necesaria una transferencia bancaria, afiliar una tarjeta de crédito o hacer un depósito bancario. </span></p>
                    <p><span className={fontWeight}>5.3 Esta billetera permitirá hacer pagos, transferencias de saldos, recibir pagos y transferencias de saldos y en general administrar el saldo desde y hacia tu cuenta. Las transferencias y pagos sólo podrán hacerse entre usuarios. </span></p>
                    <p><span className={fontWeight}>5.4 Los usuarios podrán transferir el saldo de su billetera virtual a su cuenta bancaria asociada para retirar esos fondos. En caso de no poseer una cuenta bancaria el usuario solo podrá administrar su saldo, haciendo operaciones, pero en ningún caso hacer retiro de fondos. </span></p>
                    <p><span className={fontWeight}>5.5 Los usuarios que no tengan cuentas bancarias sólo podrán introducir saldos a su cuenta con depósitos bancarios verificados o recibiendo transferencias de fondos de otros usuarios.</span></p>
                    <p><span className={fontWeight}>5.6 Los comercios afiliados con cuentas corporativas podrán recibir pagos de usuarios, pero adicionalmente podrán hacer pagos a otros usuarios Gualy. </span></p>
                    <p><span className={fontWeight}>5.6 Proporcionamos las herramientas para rastrear, transferir y gestionar billetera digital y la información de tu Cuenta. Dentro de los límites del Servicio, facilitamos transacciones de Valor de acuerdo con las instrucciones recibidas de tu Cuenta y nos comunicamos con la transferencia del Valor hacia el destinatario.</span></p>
                    <p><span className={fontWeight}>5.7 Podemos cobrar una tarifa por añadir o quitar Valor para sufragar gastos por transferencias, tarifas bancarias u otros cargos. Mostraremos las tarifas aplicables para personas naturales en </span><a href="https://gualy.com/politica-de-datos-y-de-privacidad/"><span className={fontWeight}>www.gualy.com</span></a><span className={fontWeight}>. Además, tu banco o emisor de tarjetas puede cobrarte tarifas por las transacciones. </span></p>
                    <p><span className={fontWeight}>5.9 Las tarifas publicadas en&nbsp;nuestra pueden no aplicarse a partir de un cierto volumen y/o a clientes institucionales o de negocios, para los que exigimos la celebración de un acuerdo separado.</span></p>
                    <p><span className={fontWeight}>5.10 Tu banco puede cobrarte una tarifa por fondos insuficientes o por descubiertos si no tienes fondos suficientes para completar la transacción. Es responsabilidad tuya mantener un balance suficiente en la Cuenta y límites de crédito suficientes en tus tarjetas de crédito para evitar descubiertos, falta de fondos o cobros similares. Toda falta de fondos suficientes serán tu responsabilidad exclusiva.</span></p>
                    <p><span className={fontWeight}>5.11 Una vez abierta tu cuenta como usuario, esta comenzará con saldo en cero (0), hasta hacer la primera recarga de fondos, cuando se acreditará como saldo la cantidad ingresada a través de cualquiera de los mecanismos disponibles y una vez aplicadas las tarifas y deducciones que correspondan. </span></p>
                    <p><span className={fontWeight}>5.12 Nos otorgas un derecho de garantía por todo el Valor que llegue a nuestro poder y esté relacionado con tu Cuenta, con la única finalidad de satisfacer las obligaciones que tienes para con nosotros en virtud del presente Acuerdo o de otro tipo. Acuerdas ejecutar, prestar y abonar los cargos aplicables por cualquier documento que solicitemos para crear, perfeccionar, mantener y aplicar este derecho de garantía.</span></p>
                    <p><span className={fontWeight}>5.13 Aceptas que podamos deducir cualquier Valor asociado con tu Cuenta si los fondos añadidos o la transacción de Valor añadido realizada utilizando una cuenta bancaria o tarjeta de crédito se cancela mediante devolución, revocación, reclamación, o si se invalida por cualquier otra razón. También podemos deducir cualquier Valor que esté incluido en tu billetera, para satisfacer todo importe que nos adeudes en virtud del presente Acuerdo u otros. Nos reservamos el derecho a denunciar, suspender y/o rescindir Cuentas por abuso de devoluciones.</span></p>
                    <p><span className={fontWeight}>5.14 Con fines reglamentarios y de cumplimiento, nos reservamos el derecho a negarnos a procesar, cancelar o revertir cualquier fondo añadido y/o transacción(es) de Valor añadido si sospechamos que la(s) transacción(es) puede implicar una actividad ilícita como blanqueo de capitales, financiación a terroristas, fraude, cualquier delito o si lo requiere un proceso legal válido o si está relacionado con un Uso prohibido o Negocio prohibido. Nos reservamos el derecho a denunciar, suspender y/o cancelar Cuentas por dichas actividades sospechosas.</span></p>
                    <p><span className={fontWeight}>5.15 No siempre podemos garantizar que no vaya a producirse un retraso en una transacción solicitada y aceptas nuestra exención de responsabilidad por cualquier tema relativo a dichos retrasos. Dependemos de terceros para realizar operaciones y en consecuencia no incurriremos en reparación de ningún tipo de daños ni restitución por estos retrasos o fallas en la prestación de servicios.</span></p>
                    <p><span className={fontWeight}>5.15.1 Una vez que acredites un valor (dinero) a través de los canales disponibles a tu cuenta Gualy, éste podría tardar en reflejarse ya que dependerá del canal utilizado, por lo que una vez que sea confirmado el importe se reflejará de forma automática en cuenta Gualy.</span></p>
                    <p><span className={fontWeight}>5.15.2 Los importes realizados entre cuentas Gualy se reflejarán de forma inmediata, salvo algún hecho fortuito o de fuerza mayor.</span></p>
                    <ol start="6">
                        <li><b> NO SOMOS UN BANCO.</b></li>
                    </ol>
                    <p><span className={fontWeight}>No somos un banco, por lo que los saldos no son depósitos y no devengan intereses. No abrimos cuentas bancarias, no otorgamos créditos, no ofrecemos ni administramos productos bancarios o crediticios, tampoco acreditamos intereses.</span></p>
                    <p><span className={fontWeight}>No utilizamos tu dinero para financiar ningún tipo de actividad, no movemos ni utilizamos tu dinero para invertir. </span></p>
                    <p><span className={fontWeight}>Tu dinero solo podrá ser movido dentro de la aplicación (de una cuenta Gualy a otra cuenta Gualy) y para retiro de fondo de tu cuenta Gualy a tu cuenta bancaria una vez que así lo solicites desde la aplicación.</span></p>
                    <ol start="7">
                        <li><b> INTERCAMBIO DE INFORMACIÓN.</b></li>
                    </ol>
                    <p><span className={fontWeight}>No compartimos tu información excepto cuando la ley así lo exige y/o a menos que contemos con tu consentimiento expreso por escrito. Si la ley nos exige compartir tu información, lo haremos de conformidad con nuestra&nbsp;</span><a href="https://gualy.com/politica-de-datos-y-de-privacidad/"><span className={fontWeight}>Política de datos y de privacidad</span></a><span className={fontWeight}>. La información relativa a cómo interactuamos con los agentes de justicia está disponible en </span><a href="https://gualy.com/politica-de-datos-y-de-privacidad/"><span className={fontWeight}>Política de datos y de privacidad</span></a><span className={fontWeight}>. Podemos contactar con cualquier institución financiera, agentes del orden público o terceros afectados (incluyendo otros miembros) y compartir los detalles de cualquier transacción en la que hayas participado, si consideramos que, al hacerlo, podemos evitar pérdidas financieras o una infracción de la ley.</span></p>
                    <p><span className={fontWeight}>Contamos con un sistema de seguridad que resguardará tus datos personales y datos sensibles, el cual puedes revisar accediendo a este enlace </span><a href="https://gualy.com/seguridad/#toggle-id-1-closed"><span className={fontWeight}>Procedimientos para garantizar la protección de datos personales de clientes</span></a><b>.</b></p>
                    <ol start="8">
                        <li><b> RIESGO ASUMIDO CON LAS TRANSACCIONES.</b></li>
                    </ol>
                    <p><span className={fontWeight}>El uso que tú hagas del Servicio, o de alguna de sus partes, corre por tu propia cuenta y riesgo y no asumimos responsabilidad alguna por las operaciones subyacentes de fondos, o por las acciones e identidad de cualquier receptor o remitente de transferencias. Si algún tercero presenta una reclamación por reembolso o se nos informa de una controversia entre otra parte y tú, no nos responsabilizamos de comprobar la veracidad de las reclamaciones o la resolución de la controversia, incluyendo la distribución de todo Valor asociado.</span></p>
                    <p><span className={fontWeight}>El usuario es exclusivamente responsable del uso que haga de nuestros servicios y asume toda obligación y responsabilidad derivadas del uso y operaciones de fondo exonerando de toda responsabilidad a Gualy.</span></p>
                    <p><span className={fontWeight}>De igual manera el usuario se obliga a asumir las obligaciones tributarias y financieras con los terceros contratantes ajenos a la relación de servicio Gualy – Usuario y cumplir con la emisión de facturas fiscales y demás leyes tributarias, ya que la misma es plena responsabilidad del usuario exonerando a Gualy de cualquier responsabilidad en ese sentido.</span></p>
                    <ol start="9">
                        <li><b> NO RENUNCIA A LOS DERECHOS.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Nuestra inacción respecto a un incumplimiento por tu parte o por parte de otros, no representa una renuncia a nuestro derecho a actuar en lo que respecta a incumplimientos posteriores o similares. Este Acuerdo no ha de interpretarse como una renuncia a los derechos irrenunciables bajo la legislación o reglamento aplicables de protección del consumidor.</span></p>
                    <ol start="10">
                        <li><b> TU LICENCIA LIMITADA PARA UTILIZAR NUESTRO SERVICIO.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Te concedemos una licencia limitada, no exclusiva, no transferible, no sublicenciable para acceder y utilizar el Servicio, únicamente para fines informativos, de transacciones u otros propósitos aprobados, y nos reservamos todos los demás derechos en el Servicio, incluyendo entre otros, nuestros sitios web, API y otro contenido. Aceptas no tener otros derechos más allá de esta licencia limitada. Acuerdas que no copiarás, transmitirás, distribuirás, venderás, revenderás, autorizarás, descompilarás, harás ingeniería inversa, desmontarás, modificarás, publicarás, participarás en la transferencia o venta, crearás trabajos derivados, ejecutarás, mostrarás, incorporarás en otro sitio web, ni explotarás de ningún otro modo ningún contenido ni cualquier otra parte del Servicio, con ningún fin. También acuerdas no enmarcar ni exhibir ninguna parte del Servicio sin permiso previo por escrito y no utilizar nuestras marcas sin nuestro permiso.</span></p>
                    <ol start="11">
                        <li><b> ACUERDOS CON OTRAS EMPRESAS DE SERVICIOS FINANCIEROS.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Para prestar el servicio, es necesario contratar periódicamente con otros proveedores de productos y servicios financieros. En la medida requerida por la ley, el reglamento o cualquier acuerdo aplicable con dichos proveedores, proporcionaremos y actualizaremos una lista de socios que prestan servicios financieros a través del Servicio. </span></p>
                    <p><span className={fontWeight}>Gualy es un servicio cerrado, entre y para miembros. Como tal, en calidad de miembro, nos autorizas expresamente a trabajar con terceros proveedores de servicios, incluyendo proveedores de productos y servicios financieros para prestar el Servicio y hacer que el Servicio funcione incluyendo, entre otros, permitir la subida, transferencia, recepción y almacenamiento de fondos. Al contratar a otros socios o proveedores de servicios financieros, estos pueden exigirnos que compartamos tu información personal. Para más información, puedes ver nuestra&nbsp;</span><a href="https://gualy.com/politica-de-datos-y-de-privacidad/"><span className={fontWeight}>Política de datos y de privacidad</span></a><span className={fontWeight}>.</span></p>
                    <ol start="12">
                        <li><b> QUÉ DEBERÍAS SABER SOBRE INTERNET.</b></li>
                    </ol>
                    <p><span className={fontWeight}>El uso de Internet presenta sus propios riesgos. Contamos con ciertos requisitos para contraseñas. Sin embargo, independientemente de lo segura que sea tu contraseña, debes garantizar la seguridad de tus credenciales de Cuenta — incluyendo las credenciales para la cuenta de correo electrónico que utilizas en relación con el Servicio. Si no lo están, cualquiera puede poner en peligro y realizar acciones en tu Cuenta. Debes evitar en todo momento copiar los scripts en tu barra de direcciones del navegador, y evitando pinchar en enlaces, abrir adjuntos o visitar recursos de Internet en sitios que no sean de tu confianza. Es responsabilidad tuya mantener la seguridad y control adecuados de todas las ID, contraseñas, indicaciones, número personal de identificación (PIN), claves API o cualquier otro código que utilices para acceder o en relación con el Servicio. No asumimos responsabilidad alguna por pérdidas resultantes de la puesta en peligro de tu Cuenta.</span></p>
                    <ol start="13">
                        <li><b> RESCISIÓN Y CANCELACIÓN.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Queremos estar juntos durante mucho tiempo. Trabajamos arduamente para mantenerte contento y esperamos que sigas siendo miembro durante muchos años.</span></p>
                    <p><span className={fontWeight}>13.1 En caso de que quieras dejar de utilizar el Servicio, puedes cancelar tu Cuenta Gualy, en cualquier momento, si todo está en regla y cumples los términos de este Acuerdo y otras políticas. </span><span className={fontWeight}>Para cancelarla, basta con que nos envíes un correo electrónico a </span><a href="mailto:soporte@gualy.com"><span className={fontWeight}>soporte@gualy.com</span></a><span className={fontWeight}> indicando que deseas cerrar definitivamente tu Cuenta Gualy</span><span className={fontWeight}>. Decidiremos entonces si cancelamos o suspendemos toda transacción pendiente, y tendremos que mantener los fondos hasta que se completen todas las ventanas de revocación aplicables y hasta que hayas satisfecho todas las obligaciones que tengas con nosotros. Por supuesto, no podrás cancelar tu Cuenta con el objetivo de evitar pagar importes que debas o si tu actividad está siendo investigada.</span></p>
                    <p><span className={fontWeight}>13.2 En algunos casos podemos rescindir, suspender o restringir tu Cuenta y uso del Servicio, incluyendo si recibimos un proceso legal válido, si sospechamos que tu Cuenta Gualy pueda estar relacionada con algún Uso o Negocio prohibido, en comisión de algún delito a causa de la violación de este Acuerdo para miembros, de políticas, o para evitar pérdidas. Tu uso del Servicio es un privilegio, no un derecho, y nos reservamos el derecho a rescindir, suspender o restringir tu acceso al Servicio en cualquier momento para protegerte, a nuestros miembros y/o a nosotros mismos, cuando lo consideremos necesario.</span></p>
                    <p><span className={fontWeight}>13.3 Si se rescinde tu Cuenta Gualy, sujeto a requisitos de verificación, normalmente podrás transferir fondos durante treinta (30) días, a menos que dichas transferencias estén prohibidas por alguna razón, la cual será informada oportunamente por los canales previstos.</span></p>
                    <ol start="14">
                        <li><b> CONTROVERSIAS.</b></li>
                    </ol>
                    <p><span className={fontWeight}>A través de nuestro equipo de asistencia, trabajamos arduamente para resolver cualquier disputa que pueda producirse. Si no lo logramos, acordamos que en cualquier disputa que surja por o en relación con el uso que hagas del Servicio o cualquier acción que realicemos y afecte a tu Cuenta, tanto tú como nosotros podemos elegir resolverla mediante un arbitraje vinculante, en lugar de en los tribunales. Toda reclamación (excepto aquellas relativas a violaciones de la propiedad intelectual, abuso del Servicio por parte de miembros o la validez o la aplicabilidad de esta disposición sobre arbitraje, incluyendo la renuncia a emprender demandas colectivas) habrá de resolverse mediante arbitraje vinculante si cada parte así lo solicita.</span></p>
                    <p><span className={fontWeight}>ESTO SIGNIFICA QUE SI TÚ O NOSOTROS ELEGIMOS EL ARBITRAJE, NINGUNA DE LAS PARTES TENDRÁ DERECHO A LITIGAR DICHA RECLAMACIÓN ANTE LOS TRIBUNALES O SOMETERSE A UN JUICIO. </span></p>
                    <p><span className={fontWeight}>RENUNCIA A DEMANDAS COLECTIVAS. EL ARBITRAJE HA DE PRODUCIRSE DE MANERA INDIVIDUAL. ESTO SIGNIFICA QUE NI TÚ NI NOSOTROS PODEMOS UNIR NI ACUMULAR RECLAMACIONES EN EL ARBITRAJE POR PARTE O CONTRA OTROS, NI LITIGAR EN LOS TRIBUNALES COMO REPRESENTANTES O MIEMBROS DE UNA DEMANDA COLECTIVA, NI TAMPOCO A TÍTULO PERSONAL.</span></p>
                    <p><span className={fontWeight}>Continuidad de esta disposición. La presente disposición de arbitraje continuará: tras el cierre de tu Cuenta; el pago voluntario de tu Cuenta o alguna parte de la misma; cualquier procedimiento legal para cobrar el dinero que debas; tu bancarrota; y toda venta que hagamos de tu Cuenta.</span></p>
                    <p><span className={fontWeight}>Puedes rechazar el arbitraje. Puedes rechazar el acuerdo de arbitraje estipulado en esta Sección 15, pero tan solo si nos envías un aviso por escrito de rechazo en los 30 días posteriores a la creación de tu Cuenta. Has de enviar la notificación de rechazo a: Gualy Payment, C.A, desde tu correo electrónico registrado en tu Cuenta Gualy al correo electrónico soporte@gualy.com. La notificación de rechazo debe incluir tu nombre, dirección, número de teléfono, e-mail de registro, ID de usuario y firma personal. Nadie puede firmar la notificación de rechazo por ti. Has de enviar dicha notificación de manera independiente a cualquier otra correspondencia. El rechazo al arbitraje no afectará al resto de derechos o responsabilidades que emanan del presente Acuerdo. Si rechazas el arbitraje, ni tú ni nosotros estaremos sujetos a las disposiciones sobre arbitraje y en consecuencia estaremos sujetos a resolver cualquier controversia ante los Tribunales Venezolanos, ateniéndonos a las reglas de la competencia. </span></p>
                    <ol start="15">
                        <li><b> INDEMNIZACIÓN.</b></li>
                    </ol>
                    <p><span className={fontWeight}>Si alguien presenta una reclamación contra nosotros, nuestros socios o proveedores, y/o alguno de nuestros o sus trabajadores, directores, agentes, partícipes, empleados o representantes, relacionada con el uso que hagas del Servicio, o alguna parte del mismo, incluyendo, entre otros, cualquier herramienta de developer o la supuesta violación de leyes, normas o derechos, nos indemnizarás y mantendrás indemnes por cuantos daños, pérdidas y gastos de cualquier tipo (incluyendo honorarios legales y costes razonables) se deriven de dicha reclamación.</span></p>
                    <ol start="16">
                        <li><b> SIN GARANTÍAS, DESCARGO DE RESPONSABILIDAD Y DE OBLIGACIONES.</b></li>
                    </ol>
                    <p><span className={fontWeight}>TRATAMOS DE MANTENER EL SERVICIO SEGURO, LIBRE DE ERRORES Y VIRUS, PERO LO UTILIZAS BAJO TU RESPONSABILIDAD. EL SERVICIO SE PROPORCIONA “TAL CUAL” Y “SEGÚN DISPONIBILIDAD” SIN REPRESENTACIÓN NI GARANTÍA, YA SEA EXPRESA, IMPLÍCITA O LEGAL. RECHAZAMOS EXPRESAMENTE CUALQUIER GARANTÍA IMPLÍCITA JURÍDICA, COMERCIABILIDAD, ADECUACIÓN PARA UN PROPÓSITO Y NO INFRACCIÓN. NO OFRECEMOS NINGUNA REPRESENTACIÓN O GARANTÍA DE QUE EL ACCESO A CUALQUIERA DE LAS PARTES DEL SERVICIO, O CUALQUIERA DE LOS MATERIALES INCLUIDOS EN ÉL, SERÁ CONTINUO, ININTERRUMPIDO, OPORTUNO, LIBRE DE ERRORES O SEGURO. EL FUNCIONAMIENTO DEL SERVICIO PUEDE VERSE AFECTADO POR NUMEROSOS FACTORES QUE ESCAPAN A NUESTRO CONTROL. REALIZAREMOS ESFUERZOS RAZONABLES PARA GARANTIZAR EL PROCESAMIENTO DE MANERA PUNTUAL DE TODA SOLICITUD DE DÉBITO O CRÉDITO ELECTRÓNICO QUE INCLUYA CUENTAS BANCARIAS, TARJETAS DE CRÉDITO, PERO NO OFRECEMOS REPRESENTACIÓN NI GARANTÍAS RELATIVAS AL TIEMPO NECESARIO PARA COMPLETAR LA TRAMITACIÓN.</span></p>
                    <p><span className={fontWeight}>NO NOS RESPONSABILIZAMOS DE LAS ACCIONES, CONTENIDO, INFORMACIÓN O DATOS DE TERCEROS, Y NOS EXIMES A NOSOTROS, NUESTROS DIRECTORES, EMPLEADOS Y AGENTES, ASÍ COMO A NUESTROS SOCIOS Y PROVEEDORES, Y A SUS EMPLEADOS, DIRECTORES, AGENTES, PARTÍCIPES Y REPRESENTANTES, DE TODA RECLAMACIÓN Y DAÑOS, CONOCIDOS O DESCONOCIDOS, QUE SURJAN O ESTÉN CONECTADOS DE ALGÚN MODO CON CUALQUIER RECLAMACIÓN QUE TENGAN CONTRA ALGUNO DE ESTOS TERCEROS.</span></p>
                    <ol start="17">
                        <li><b> OTROS ASUNTOS.</b></li>
                    </ol>
                    <p><span className={fontWeight}>17.1 Acuerdos adicionales con nosotros. Salvo que se acuerde otra cosa por escrito, el presente Acuerdo contiene el acuerdo completo entre tú y nosotros respecto al asunto tratado, y sustituye toda conversación, acuerdo o escrito de cualquier tipo, e índole entre tú y nosotros. Si eres developer, aceptas adicionalmente nuestro&nbsp;Acuerdo para Developers.</span></p>
                    <p><span className={fontWeight}>17.2 Cesión de estos derechos y obligaciones a otros. No puedes ceder ni transferir este Acuerdo, ni los derechos, obligaciones y licencias concedidas en virtud del mismo, aunque sí podemos cederlo nosotros sin aviso ni restricciones, incluyendo sin limitación a cualquiera de nuestros socios, sucursales o filiales, o a cualquier sucesor en interés. Todo intento de cesión o transferencia que viole esto se considerará nula excepto cuando, sujeto a los límites aquí expuestos, nuestro acuerdo sea obligatorio y redunde en beneficio de las partes, sus sucesores y cesionarios autorizados.</span></p>
                    <p><span className={fontWeight}>17.3 Interpretación de este Acuerdo; Derechos de terceros. En caso de que un tribunal invalide o determine que no es ejecutable alguna de las disposiciones de este Acuerdo, se modificará dicha disposición y se interpretará para cumplir los objetivos de la disposición en la medida de lo posible, y esto no afectará a la ejecutabilidad del resto de disposiciones. Cualquier persona que no forme parte del presente Acuerdo no tiene derechos según las disposiciones legales vigentes.</span></p>
                    <p><span className={fontWeight}>17.4 Obligaciones vigentes tras la finalización de este acuerdo. Las disposiciones relativas a la suspensión, rescisión, cancelación, deudas, uso general del Servicio, pagos, controversias, tu responsabilidad, indemnización y disposiciones generales seguirán teniendo validez tras la rescisión de este Acuerdo.</span></p>
                    <p><span className={fontWeight}>17.5 Traducciones y resúmenes. Toda traducción o resumen del Servicio, de este Acuerdo y/o de las políticas, se proporcionan únicamente para tu conveniencia y no pretenden modificar el Servicio, este Acuerdo y/o las políticas. Aceptas que la versión en español del Servicio, Acuerdo y políticas prevalecerá en caso de conflicto con la versión en cualquier otro idioma.</span></p>
                    <p><span className={fontWeight}>17.6 Partes contratantes y legislación aplicable. Este Acuerdo se celebra entre tú y Gualy Payment, C.A., empresa domiciliada en Venezuela. Aceptas que este Acuerdo y la relación entre nosotros se regirá por la legislación de venezolana, independientemente de los principios de conflictos entre las leyes, y toda reclamación y controversia entre tú y nosotros.</span></p>
                    <p><b>APÉNDICE 1: NEGOCIOS PROHIBIDOS Y USO PROHIBIDO</b></p>
                    <p><span className={fontWeight}>Las siguientes categorías de negocio, prácticas empresariales y venta de artículos tienen prohibido el uso de nuestros servicios (“Negocios prohibidos”). Al abrir una Cuenta, confirmas que no utilizarás el Servicio para efectuar transacciones relacionadas con los siguientes negocios, actividades, prácticas o artículos:</span></p>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Operar sin licencia como transmisor de dinero, servicios monetarios, proveedor de servicios monetarios, de dinero electrónico o cualquier otro negocio de servicios financieros que requiera licencia, incluyendo, entre otros, intercambios de moneda virtual, venta de giros postales o cheques de viajero, y servicios de depósito</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Productos falsificados o cualquier productos o servicio que viole el copyright, marca o secreto comercial de terceros</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Bienes robados</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Narcóticos, sustancias reguladas, servicios farmacéuticos o que requieran receta, material para consumir droga, o cualquier sustancia diseñada para imitar sustancias ilegales</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Apuestas.</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Pronóstico deportivo o análisis de probabilidades</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Prostitución</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Actos violentos contra uno mismo u otros, o actividades o artículos que fomenten, promuevan, faciliten o instruyan a otros a hacer eso mismo</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Financiación de alguno de los artículos incluidos en la lista de Negocios prohibidos</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Extorsión, chantaje o esfuerzos por inducir pagos inmerecidos</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Venta sin licencia de armamento y armas de fuego</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Prácticas de engañosas de marketing</span></li>
                        <li className={fontWeight}><span className={fontWeight}>Cambio de divisas</span></li>
                        <li className={fontWeight}><span className={fontWeight}>Legitimación de Capitales</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Todo negocio que viole leyes, estatutos, decretos o reglamentos</span></li>
                    </ul>
                    <p><span className={fontWeight}>No podrás utilizar tu Cuenta o el Servicio para participar en las siguientes categorías de actividad (“Uso prohibido”). Confirmas que no utilizarás tu Cuenta para hacer alguna de las siguientes actividades:</span></p>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Violar o contribuir a que alguien viole alguna ley, estatuto, decreto, reglamento o las reglas de algún organismo de autorregulación o similar del que seas o tengas que ser miembro (por ejemplo, aquellas leyes, reglas o reglamentos que rijan servicios financieros, sustancias controladas o protección al consumidor)</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Participar en transacciones que incluyan los beneficios de alguna actividad ilícita</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Participar en transacciones que incluyan apuestas online)</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Defraudar o intentar defraudarnos a nosotros o a nuestros miembros</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Infringir nuestra propiedad intelectual o la de terceros</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Proporcionar información falsa, imprecisa o engañosa</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Realizar acciones que impongan una carga no razonable o desproporcionadamente grande en nuestra infraestructura, o que interfieran perniciosamente, intercepten o expropien cualquier sistema, datos o información</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Interferir con el acceso o uso de otra persona o entidad a cualquier parte del Servicio</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Difamar, injuriar, acosar, perseguir, amenazar, violar o infringir los derechos legales de otros</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Publicar, distribuir o difundir todo material o información ilegal</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Transmitir o subir virus, troyanos, gusanos o cualquier otro programa malicioso</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Acceder al Servicio mediante medios programáticos</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Actuar como intermediario o agregador de pago o revender nuestros Servicios, a menos que te hayamos autorizado por escrito</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Transferir cualquier derecho que te hayamos concedido nosotros o cualquier otro</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Utilizar la contraseña de otro miembro para cualquier propósito</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Poner en peligro Cuentas, sistemas o redes informáticas conectados al Servicio a través de cualquier medio</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Utilizar cualquier Cuenta que no sea la cuenta única creada con tu identidad real</span></li>
                    </ul>
                    <p><b>APÉNDICE 2: PROCEDIMIENTOS DE VERIFICACIÓN </b></p>
                    <p><span className={fontWeight}>Utilizamos sistemas y procedimientos de multinivel para recoger y verificar tu información para que tanto nosotros como nuestros miembros y socios estemos libres de actividades fraudulentas, así como con fines administrativos. </span></p>
                    <p><span className={fontWeight}>Establecer una Cuenta con nosotros proporcionando su nombre legal, comprobando su dirección de correo electrónico, su fecha de nacimiento, número de teléfono, facilitando dos documentos de identificación nacional – como puedan ser la cédula de identidad, el pasaporte y/o la licencia de conducir, y aceptar el pleno de nuestro Acuerdo para miembros</span></p>
                    <p><span className={fontWeight}>Los miembros que deseen añadir o eliminar Valor utilizando una transferencia fiduciaria disponible deben, como mínimo:</span></p>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Añadir o verificar una cuenta bancaria, una tarjeta de crédito y/o débito</span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>Proporcionar detalles adicionales, incluyendo su dirección y documento nacional de identidad </span></li>
                    </ul>
                    <ul>
                        <li className={fontWeight}><span className={fontWeight}>También podemos pedir que nos proporciones o verifiques información adicional de identificación o que esperes una transacción durante cierto tiempo antes de permitir que utilices el Servicio por completo. </span></li>
                    </ul>
                </div>
                <div className={loginButtonContainer}>
                    <Button
                        color='primary'
                        size='lg'
                        onClick={onAccepted}
                        className={`${buttonLogIn} ${registerButton}`}
                        block
                    >
                        Aceptar
                    </Button>
                </div>
            </div>
        </OptionsModalWrapper>
    )
}
export default TermsAndConditionsModal