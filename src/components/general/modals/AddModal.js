import React from 'react'
import { css } from 'emotion'
import OptionsModalWrapper from '../OptionsModalWrapper'
import AmountInput from '../AmountInput'
import arrowIcon from '../../../assets/drop-down-arrow.svg'
import styled from 'react-emotion'
import AcceptButton from '../AcceptButton'
import errorSVG from '../../../assets/ic-info-red.svg'
import copyIcon from './../../../assets/copyFile.svg'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { toast } from 'react-toastify'
import ChooseAccountNumber from '../ChoseeAccountNumber'
import DateInput from '../../general/DateInput'
/**CSS */
const component = css`
    padding: 10px !important;
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
      padding: 3px;
      margin-top: 10px;
    }
  `
const amountContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 20px;
    padding: 0 10px;
  `
const spanClass = css`
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.2px;
      text-align: center;
      color: #f6f7fa;
  `
const divisorInput = css`
    width: 100%;
    height: 1.1px;
  opacity: 0.5;
  background-color: #1d1f4a;
  `
const primaryButton = css`
    margin: 20px 20px;
    width: 100%;
  `
const marginTop = css`
  margin-top: 20px !important;
`
const dropDownDivContainer = css`
width: 100%;
div{
  margin-top: 0;
}
`
const dropDownDiv = css`
position: relative;
display: flex;
align-items:center;
`
const CustomDropdown = styled('select')`
height: 43.3px;
width:100%;
padding: 7px 4px;
background: transparent;
border: none;
cursor: pointer;
font-size: 15px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.68;
letter-spacing: normal;
text-align: left;
color: #ffffff;
appearance: none; 
outline: none !important;
option{
  background-color: #242657;
}
`
const dropdownQuestionPadding = css`
padding-right: 15px;
@media(max-width: 500px){
  option{
    font-size: 8px;
  }
}
`
const arrowContainer = css`
width: 15px !important;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
position: absolute;
right: 0;
top: 0;
pointer-events: none;
`
const arrow = css`
width: 10.4px;
height: 6.6px;
object-fit: contain;
`
const AccountNumberTitleStyle = css`
opacity: 0.65;
font-family: Montserrat;
font-size: 10px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
color: #f6f7fa;
color: var(--pale-grey);
position: absolute;
    top: 13px;
}
`
const NameTitleStyle = css`
opacity: 0.65;
font-family: Montserrat;
font-size: 10px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
color: #f6f7fa;
color: var(--pale-grey);
height: 0;
`
const companyNameContainer = css`
width: 100%;
position: relative;
margin-top: 10px
`
const companyInfoTitle = css`
text-align: center
`
const displayRedBorders = css`
    border-color:red !important;
    border-bottom: 1px solid #757575;
`
const errorSvgCss = css`
width: 14.7px;
height: 13px;
object-fit: contain;
margin: 5px;
`
const errorContainer = css`
color: red;
display: flex;
justify-content: center;
align-items: center; 
font-size: 12px;
margin-top: 15px;
`
const copyIconClass = css`
cursor: pointer;
margin-left: 9px;
`
const chooseAcountNumberContainer = css`
margin-top: 45px;
`
const onCopyClick = () => {
  toast.success('Copiado!')
}
const AddModal = ({
  onChange,
  onSubmit,
  loading,
  onClose,
  showModal,
  companyName,
  rifNumber,
  amount,
  handleAccountNumberSelect,
  idFormName,
  extraIdFormName,
  paymentMethod,
  requestLoadingAddModal,
  handlepaymentMethodSelect,
  companyAccountNumbers,
  getCompanyAccountNumbers,
  companyAccountNumber,
  displayErrors,
  nextSeccion,
  isNextSeccion,
  userBank,
  transferNumber,
  operationDate,
  errorMessage,
  amountInputHandleChange,
  banksCards,
  userBankAccounts,
  updateDate,
}) => {
  return (
    <OptionsModalWrapper show={showModal} onClose={onClose} width="310px">
      {
        !isNextSeccion ?
          <form
            onSubmit={nextSeccion}
            noValidate autoComplete='off'
            id={extraIdFormName}
            className={`content-container ${component}`}>
            <div className={`${amountContainer}`}>
              <span className={spanClass}>¿Cuánto quieres añadir?</span>
              <AmountInput
                amount={amount}
                disabled={loading}
                spanBorder={`${(displayErrors === true &&
                  !Number(amount.replace(/\./g, '').replace(',', '.')) > 0 ? displayRedBorders : ''
                    ? displayRedBorders : '')}`}
                onChange={amountInputHandleChange}
              />
              {/* <div className={divisorInput}></div> */}
              <span className={spanClass}>Elige cómo deseas añadir</span>
              <div className='group mb-1' css={`width: 90%;`}>
                <div
                  css={`padding-bottom:5px;`}
                  className={`
                ${marginTop} 
                ${dropDownDivContainer} 
                ${displayErrors && paymentMethod === '' ? displayRedBorders : ''}
                `}
                >
                  <div className={dropDownDiv}>
                    <CustomDropdown
                      className={dropdownQuestionPadding}
                      value={paymentMethod}
                      disabled={loading}
                      name='paymentMethod'
                      onChange={handleAccountNumberSelect}
                      required
                    >
                      <option defaultValue value="Transfer">Transferencia</option>
                    </CustomDropdown>
                    <div className={arrowContainer}>
                      <img src={arrowIcon} className={arrow} alt="dropdown" />
                    </div>
                  </div>
                </div>
              </div>
              <span className={spanClass} style={{ padding: "14px" }}>Realiza la transferencia con los siguientes datos</span>
              <div className={companyNameContainer}>
                <p className={NameTitleStyle}>A nombre de</p>
                <div css={`
                    display: flex;
                    justify-content: center;
                `}>
                  <p className={companyInfoTitle}>{companyName}</p>
                  <CopyToClipboard
                    text={companyName}
                    onCopy={onCopyClick}>
                    <img
                      src={copyIcon}
                      className={copyIconClass}
                      css={`margin-bottom: 16px;`}
                      alt="copy button"
                    />
                  </CopyToClipboard>
                </div>
              </div>
              <div className={divisorInput}></div>
              <div className='group mb-1' css={`width: 100%;`}>
                <p className={AccountNumberTitleStyle}>No. de cuenta</p>
                <div
                  css={`padding-bottom:5px;display: flex;`}
                  className={`
                ${marginTop} 
                ${dropDownDivContainer} 
                ${displayErrors && companyAccountNumber === '' ? displayRedBorders : ''}
                `}
                >
                  <div className={dropDownDiv}>
                    <CustomDropdown
                      className={dropdownQuestionPadding}
                      value={companyAccountNumber}
                      name='companyAccountNumber'
                      disabled={loading}
                      onChange={handleAccountNumberSelect}
                      required
                    >
                      <option value="" defaultValue>{companyAccountNumbers.length > 0 ? "Seleccione un numero de cuenta" : "Cargando numeros de cuenta..."}</option>
                      {
                        getCompanyAccountNumbers(companyAccountNumbers)
                      }
                    </CustomDropdown>
                    <div className={arrowContainer}>
                      <img src={arrowIcon} className={arrow} alt="dropdown" />
                    </div>
                  </div>
                  {
                    companyAccountNumber
                      ? <CopyToClipboard
                        text={companyAccountNumber}
                        onCopy={onCopyClick}>
                        <img
                          src={copyIcon}
                          className={copyIconClass}
                          alt="copy button"
                        />
                      </CopyToClipboard>
                      : null
                  }
                </div>
              </div>
              <div className={divisorInput}></div>
              <div className={companyNameContainer}>
                <p className={NameTitleStyle}>Rif</p>
                <div css={`
                    display: flex;
                    justify-content: center;
                `}>
                  <p className={companyInfoTitle}>{rifNumber}</p>
                  <CopyToClipboard
                    text={rifNumber}
                    onCopy={onCopyClick}>
                    <img
                      css={`margin-bottom: 16px;`}
                      src={copyIcon}
                      className={copyIconClass}
                      alt="copy button"
                    />
                  </CopyToClipboard>
                </div>
              </div>
              {
                displayErrors ?
                  <div className={errorContainer}>
                    {errorMessage}
                    <img
                      className={errorSvgCss}
                      src={errorSVG}
                      alt="error Icon"
                    />
                  </div>
                  : null
              }
              <button
                type="submit"
                disabled={loading}
                className={` ${primaryButton} btn-accept`}>
                REPORTAR LA TRANSFERENCIA
              </button>
            </div>
          </form>
          :
          <form
            onSubmit={onSubmit}
            noValidate autoComplete='off'
            id={idFormName}
            className={`content-container ${component}`}>
            <div className={`${amountContainer}`}>
              <div className='group mb-1' css={`width: 100%;`}>
                <p className={AccountNumberTitleStyle}>Tipo de operación</p>
                <div
                  css={`padding-bottom:5px;`}
                  className={`
                ${marginTop} 
                ${dropDownDivContainer} 
                ${displayErrors && companyAccountNumber === '' ? displayRedBorders : ''}`}
                >
                  <div className={dropDownDiv}>
                    <CustomDropdown
                      className={dropdownQuestionPadding}
                      disabled={loading}
                      onChange={handleAccountNumberSelect}
                      required
                    >
                      <option value="Transfer" defaultValue>Transferencia</option>
                    </CustomDropdown>
                    <div className={arrowContainer}>
                      <img src={arrowIcon} className={arrow} alt="dropdown" />
                    </div>
                  </div>
                </div>
              </div>
              <div className='group mb-1' css={`width: 100%;`}>
                <p className={AccountNumberTitleStyle}>Selecciona tu cuenta bancaria desde donde se realizó la operación</p>
                <div className={chooseAcountNumberContainer}>
                  <ChooseAccountNumber
                    userBankAccounts={userBankAccounts}
                    banksCards={banksCards}
                    onCloseCurrentModal={onClose}
                  />
                </div>
              </div>
              <div className={companyNameContainer}>
                <p className={NameTitleStyle}>No. de transferencia o depósito</p>
                <input
                  value={transferNumber}
                  name='transferNumber'
                  disabled={loading}
                  required
                  onChange={onChange}
                  className={`${displayErrors && transferNumber === '' ? displayRedBorders : ''}`}
                  placeholder="Reporta aquí tu pago *"
                  type="number"
                />
              </div>
              <div className={companyNameContainer}>
                <p className={NameTitleStyle}>Fecha en que se realizó la operación</p>
                <DateInput
                  updateDate={updateDate}
                  currentName="operationDate"
                  isValid={displayErrors && operationDate === '' ? false : 'true'}
                  disabled={loading}
                />
              </div>
              {
                displayErrors ?
                  <div className={errorContainer}>
                    {errorMessage}
                    <img
                      className={errorSvgCss}
                      src={errorSVG}
                      alt="error Icon"
                    />
                  </div>
                  : null
              }
              <AcceptButton
                content="CONFIRMAR Y AÑADIR"
                width="100%"
                height="34px"
                loading={loading}
                type="submit"
                className={primaryButton}
              />
            </div>
          </form>
      }
    </OptionsModalWrapper >
  )
}

export default AddModal