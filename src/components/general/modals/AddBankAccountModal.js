import React from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import styled from 'react-emotion'
import arrowIcon from '../../../assets/blackArrow.svg'
import OptionsModalWrapper from '../OptionsModalWrapper'
import errorSVG from '../../../assets/ic-info-red.svg'
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
const bankLogoCSS = css`
width: 104.4px;
    height: 88.5px;
object-fit: contain;
`
const component = css`
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      // color: #f6f7fa;
      margin-top: 10px;
    }
  `
const contentContainer = css`
width: 100%;
  display: flex;
  flex-direction: column;
  `
const addBankAccountModalInputsContainer = css`
  display: flex;
  flex-direction: column;
  margin: 5px;
  margin-left: 15px;
  margin-right: 15px;
  `
const addBankAccountModalInputContainer = css`
  display: flex;
  flex-direction: row;
  justify-content: center;
  input{
    color: #242656;
  }
  `
const CustomDropdownDark = styled('select')`
    height: 43.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #242656;
    appearance: none; 
    outline: none !important;
    option{
      color: #ffff;
      background-color: #242657;
    }
  `
const dropDownDiv = css`
    position: relative;
    display: flex;
    align-items:center;
  `
const firstComponent = css`
    margin-top: 0 !important;
  `
const DivDNI = css`
    width: 40px !important;
    border-bottom: 1px solid white;
    margin-right: 15px;
    position: relative;
    min-width: 38px;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const arrowContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    pointer-events: none;
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const addBankAccountModalShortInput = css`
width: 90%;
height: 50.6px;
margin: 4px;
  `
const addBankAccountButton = css`
width: 115px;
height: 38px;
border: none;
background: transparent;
margin: 10px;
cursor: pointer;
margin-top: 20px
  `
const addBankAccountButtonText = css`
    font-size: 14.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    -webkit-letter-spacing: 0.5px;
    -moz-letter-spacing: 0.5px;
    -ms-letter-spacing: 0.5px;
    letter-spacing: 0.5px;
    text-align: center;
    color: #8078ff;
  `
const relative = css`
  position: relative
  `
const spinnerMargin = css`
  margin-top: 20px
  `
const errorContainer = css`
  color: red;
  display: flex;
  justify-content: center;
  align-items: center; 
  font-size: 12px;
  margin-top: 15px;
`
const errorSvgCss = css`
  width: 14.7px;
  height: 13px;
  object-fit: contain;
  margin: 5px;
`
const AddBankAccountModal = ({
  modalShow,
  getBankLogo,
  onSubmit,
  onChange,
  accountNumber,
  idnumber,
  IdType,
  commerceName,
  // firstName,
  // secondFirstName,
  // lastName,
  // secondLastName,
  displayErrors,
  errorMessage,
  displayErrorsClass,
  width,
  show,
  onClose,
  isLoading,
  isAddCommerce = false,
}) => {
  const addBankAccountModalContainer = css`
  width: 100%;
  height: auto;
  min-height: ${isAddCommerce ? "" : "293px"};
  border-radius: 4.2px;
  box-shadow: 0 8px 8px 0 rgba(0, 0, 0, 0.15), 0 0 8px 0 rgba(0, 0, 0, 0.12);
  border: solid 0.5px transparent;
  background-image: linear-gradient(#f6f7fa, #f6f7fa), linear-gradient(to bottom, rgba(255, 255, 255, 0.2), rgba(255, 255, 255, 0.1) 5%, rgba(255, 255, 255, 0) 20%, rgba(255, 255, 255, 0));
  background-origin: border-box;
  background-clip: content-box, border-box;
    `
  return (
    <OptionsModalWrapper
      show={show}
      onClose={onClose}
      width={width}
    >
      <div
        className={`${contentContainer} ${component}`}>
        <div className={`
          ${addBankAccountModalContainer} 
          ${(displayErrors === true ? displayErrorsClass : '')}
          `}>
          <div className={addBankAccountModalInputsContainer}>
            <div className={addBankAccountModalInputContainer}>
              <img
                className={bankLogoCSS}
                src={getBankLogo}
                alt='bank-logo'
              />
            </div>
            <div className={addBankAccountModalInputContainer}>
              <input
                placeholder="Nro de cuenta"
                maxLength="19"
                className={`${displayErrors && accountNumber === '' ? displayErrorsClass : ''}`}
                style={{
                  color: "#242656",
                  textAlign: "center"
                }}
                type="number"
                onChange={onChange}
                value={accountNumber}
                name="accountNumber" />
            </div>
            {
              !isAddCommerce
                ? <div className={addBankAccountModalInputContainer}>
                  <div className='d-flex'>
                    <div
                      className={`
                  ${dropDownDiv} 
                  ${firstComponent} 
                  ${DivDNI} 
                  ${
                        displayErrors && IdType === ''
                          ?
                          displayErrorsClass
                          : ''
                        }
                  `}>
                      <CustomDropdownDark
                        name='IdType'
                        disabled={true}
                        onChange={onChange}
                        value={IdType}
                        id='contactIdType'>
                        <option value={IdType} defaultValue>{IdType}</option>
                        {IdType !== "E-" ? <option value="E-">E-</option> : null}
                        {IdType !== "V-" ? <option value="V-">V-</option> : null}
                        {IdType !== "J-" ? <option value="J-">J-</option> : null}
                        {IdType !== "G-" ? <option value="G-">G-</option> : null}
                      </CustomDropdownDark>
                      <div className={`${arrowContainer} ${firstComponent}`}>
                        <img src={arrowIcon} className={arrow} alt='arrow' />
                      </div>
                    </div>
                    <input
                      type="number"
                      placeholder="No. de identificación *"
                      name="idnumber"
                      disabled={true}
                      className={displayErrors && idnumber === '' ? displayErrorsClass : ''}
                      style={{
                        color: "#242656",
                        textAlign: "center"
                      }}
                      value={idnumber}
                      onChange={onChange}
                      maxLength="8"
                      pattern='^\d{7,8}$'
                    />
                  </div>
                </div>
                : null
            }
            {
              !isAddCommerce
                ? <div className={addBankAccountModalInputContainer}>
                  <div>
                    <input
                      type="text"
                      className={`${addBankAccountModalShortInput}`}
                      style={{
                        color: "#242656",
                        textAlign: "center"
                      }}
                      placeholder="Nombre"
                      disabled={true}
                      name="commerceName"
                      value={commerceName}
                      onChange={onChange}
                      maxLength="15"
                    />
                  </div>
                  {/* <div>
                  <input
                    type="text"
                    className={`${addBankAccountModalShortInput} ${displayErrors && lastName === '' ? displayErrorsClass : ''}`}
                    disabled={true}
                    placeholder="Apellido"
                    name="lastName"
                    value={lastName}
                    onChange={onChange}
                    maxLength="15"
                  />
                </div> */}
                </div>
                : null
            }
            {/* <div className={addBankAccountModalInputContainer}>
                <div>
                  <input
                    type="text"
                    className={`${addBankAccountModalShortInput} ${displayErrors && lastName === '' ? displayErrorsClass : ''}`}
                    placeholder="Apellido"
                    name="lastName"
                    value={lastName}
                    onChange={onChange}
                    maxLength="15"
                  />
                </div>
                <div>
                  <input
                    type="text"
                    className={`${addBankAccountModalShortInput} ${displayErrors && secondLastName === '' ? displayErrorsClass : ''}`}
                    placeholder="Segundo apellido"
                    name="secondLastName"
                    value={secondLastName}
                    onChange={onChange}
                    maxLength="15"
                  />
                </div>
              </div> */}
            {
              displayErrors ?
                <div className={errorContainer}>
                  {errorMessage}
                  <img
                    className={errorSvgCss}
                    src={errorSVG}
                    alt="error Icon"
                  />
                </div>
                : null
            }
            <div className={`${addBankAccountModalInputContainer} ${relative}`}>
              {
                !isLoading ?
                  <div>
                    <button
                      onClick={modalShow}
                      className={addBankAccountButton}>
                      <p className={addBankAccountButtonText}>
                        CANCELAR
                      </p>
                    </button>
                  </div>
                  : null
              }
              {
                !isLoading ?
                  <div>
                    <button
                      onClick={onSubmit}
                      className={addBankAccountButton}>
                      <p className={addBankAccountButtonText}>
                        AGREGAR
                      </p>
                    </button>
                  </div>
                  : null
              }
              {
                isLoading ?
                  <div
                    className={`${spinnerContainer} ${spinnerMargin}`}><Spinner color='blue' /></div>
                  : null
              }
            </div>
          </div>
        </div>
      </div>
    </OptionsModalWrapper>
  )
}
export default AddBankAccountModal