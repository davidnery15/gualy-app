import React from 'react'
import { css } from 'emotion'
import OptionsModalWrapper from '../OptionsModalWrapper'
import AcceptButton from '../AcceptButton'
import AmountInput from '../AmountInput'
import errorSVG from '../../../assets/ic-info-red.svg'
import FourPinsInput from '../FourPinInput'
import CreateSpecialPassModal from './CreateSpecialPassModal'
import ResetSpecialPassModal from './ResetSpecialPassModal'
import AutoFocusInput from '../AutoFocusInput'
const component = css`
    padding: 10px !important;
    posotion: relative;
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
      padding: 3px;
      margin-top: 10px;
    }
    input:-webkit-autofill, input:-webkit-autofill:hover{
      -webkit-text-fill-color: #f6f7fa !important;
     }
  `
const amountContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 65px;
    span{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.2px;
      text-align: center;
      color: #f6f7fa;
    }
  `
const senderPictureStyle = css`
    height: 96px;
    width: 96px;
    position: absolute;
    top: -48px;
    left: 0; 
    right: 0; 
    margin-left: auto; 
    margin-right: auto; 
    border-radius: 7px;
    background-color: #ffff;
  `
const userNameClass = css`
margin-bottom: 5px;
color: #8078ff !important;
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: 1.2px;
text-align: center;
`
const mainText = css`
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: 1.2px;
text-align: center;
color: #f6f7fa;
`
const displayRedBorders = css`
    border-color:red !important;
    border-bottom: 1px solid #757575;
`
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const primaryButton = css`
    margin: 20px 20px;
    width: 100%;
  `
export default ({
  loading,
  onClose,
  showModal,
  amount,
  userName,
  senderPicture,
  displayErrors,
  onChange,
  sendDescription,
  errorMessage,
  onSubmit,
  amountInputHandleChange,
  //security password
  firstNumber,
  secondNumber,
  thirdNumber,
  fourthNumber,
  onCreatePasswordClick,
  isCreatePasswordOpen,
  cancelCreatePasswordClick,
  onCreateSpecialPasswordClick,
  openCreatePasswordClick,
  hasSecurityPassword,
  //reset special password
  isResetPasswordOpen,
  onResetSpecialPasswordClick,
  cancelResetPasswordClick,
  openResetPassword,
  isSended,
  specialPasswordIsShow
}) => {
  return (
    <OptionsModalWrapper show={showModal} onClose={onClose} width="328px" height="auto">
      {
        isCreatePasswordOpen
          ? <CreateSpecialPassModal
            onChange={onChange}
            loading={loading}
            firstNumber={firstNumber}
            secondNumber={secondNumber}
            thirdNumber={thirdNumber}
            fourthNumber={fourthNumber}
            onCreatePasswordClick={onCreatePasswordClick}
            cancelCreatePasswordClick={cancelCreatePasswordClick}
            onCreateSpecialPasswordClick={onCreateSpecialPasswordClick}
            displayErrors={displayErrors}
            errorMessage={errorMessage}
          />
          : isResetPasswordOpen
          ?  <ResetSpecialPassModal
          loading={loading}
          onResetSpecialPasswordClick={onResetSpecialPasswordClick}
          cancelResetPasswordClick={cancelResetPasswordClick}
          displayErrors={displayErrors}
          errorMessage={errorMessage}
          isSended={isSended}
        />
         : <form
            onSubmit={onSubmit}
            noValidate
            autoComplete='off'
            className={`content-container ${component}`}>
            <img
              src={senderPicture}
              alt=""
              className={senderPictureStyle}
            />
            <div className={amountContainer}>
              <p className={mainText}>{` `}
                ¿CUÁNTO QUIERES ENVIAR A
              </p>
              <p
                className={userNameClass}
              >
                {userName ? userName.toUpperCase() : ""}?
          </p>
              <AmountInput
                amount={amount}
                disabled={loading}
                spanBorder={`${(displayErrors === true &&
                  !Number(amount.replace(/\./g, '').replace(',', '.')) > 0 ? displayRedBorders : ''
                    ? displayRedBorders : '')}`}
                onChange={amountInputHandleChange}
              />
              <AutoFocusInput
                placeholder="Descripción del pago"
                type="text"
                readOnly={loading}
                onChange={onChange}
                value={sendDescription}
                name="sendDescription"
                css={`argin-bottom: 10px`}
              />
              {
                !loading && specialPasswordIsShow
                  ? <FourPinsInput
                    firstNumber={firstNumber}
                    secondNumber={secondNumber}
                    thirdNumber={thirdNumber}
                    fourthNumber={fourthNumber}
                    onChange={onChange}
                    disabled={loading}
                    onCreatePasswordClick={onCreatePasswordClick}
                    openCreatePasswordClick={openCreatePasswordClick}
                    displayErrors={displayErrors}
                    hasSecurityPassword={hasSecurityPassword}
                    title="Contraseña Especial"
                    openResetPassword={openResetPassword}
                    canResetPass={true}
                    loading={loading}
                    canShowPassword={true}
                  />
                  : null
              }
              {
                displayErrors ?
                  <div className={errorContainer}>
                    {errorMessage}
                    <img
                      className={errorSvgCss}
                      src={errorSVG}
                      alt="error Icon"
                    />
                  </div>
                  : null
              }
              <AcceptButton
                content="ENVIAR PAGO"
                width="100%"
                height="34px"
                loading={loading}
                type="submit"
                className={primaryButton}
              />
            </div>
          </form>
      }
    </OptionsModalWrapper>
  )
}
