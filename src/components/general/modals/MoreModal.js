import React from 'react'
import { css } from 'emotion'

/* CSS */
  const modalContainer = css` 
    justify-content: space-around;
    align-items: center;
    position: absolute;
    right: 22px;
    z-index: 1000; 
    width: 129px !important;
    height: 46px !important; 
    border-radius: 1.4px;
    background-color: #1d1f4a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25), 0 10px 21px 0 rgba(0, 0, 0, 0.25);
  `
  const invisible = css` 
    display: none; 
  `
  const visibleModal = css`
    display: flex;
  `
  const responsiveModal = css`
    @media(min-width: 1300px) {
      display: none;
    }
  `

export default class Modal extends React.Component {
  render() {
    const { visible, responsive } = this.props
    return (
      <div 
      className={`
      ${modalContainer} 
      ${visible ? visibleModal : invisible} 
      ${responsive ? responsiveModal : null}
      `}>
        {this.props.children}
      </div>
    )
  }
}