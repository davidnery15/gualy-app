import React from 'react'
import AmountInput from '../AmountInput'
import QRscan from '../../../assets/icon-qr-scan.svg'
import QrReader from 'react-qr-reader'
import AcceptButton from '../AcceptButton'
import errorSVG from '../../../assets/ic-info-red.svg'
import { css } from 'emotion'
import FourPinsInput from '../FourPinInput'
import CreateSpecialPassModal from './CreateSpecialPassModal'
import ResetSpecialPassModal from './ResetSpecialPassModal'
import AutoFocusInput from '../AutoFocusInput'
const displayRedBorders = css`
    border-color:red !important;
    border-bottom: 1px solid #757575;
`
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-top: 15px;
  `
const amountContainer = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 20px;
    padding: 0 10px;
    span{
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.2px;
      text-align: center;
      color: #f6f7fa;
    }
    input:-webkit-autofill, input:-webkit-autofill:hover{
      -webkit-text-fill-color: #f6f7fa !important;
    }
  `
const component = css`
    padding: 10px !important;
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      color: #f6f7fa;
      padding: 3px;
      margin-top: 10px;
    }
  `
const qrText = css`
    font-size: 12px !important;
    font-weight: bold !important;
    font-style: normal !important;
    font-stretch: normal !important;
    line-height: normal !important;
    letter-spacing: 0.4px !important;
    text-align: center !important;
    color: #95fad8 !important;
    margin-top: 20px;
    cursor: pointer;
  `
const primaryButton = css`
    margin: auto;
    margin-top: 20px;
    margin-bottom: 20px;
    width: 100%;
  `
const primaryButton2 = css`
    margin: 20px 20px;
    margin-left: 29%;
  `
const SendModal = ({
  amount,
  sendDescription,
  sendEmail,
  onChange,
  onSubmit,
  loading,
  scan,
  openScan,
  displayErrors,
  deley,
  handleError,
  handleScan,
  errorMessage,
  amountInputHandleChange,
  //security password
  firstNumber,
  secondNumber,
  thirdNumber,
  fourthNumber,
  onCreatePasswordClick,
  isCreatePasswordOpen,
  cancelCreatePasswordClick,
  onCreateSpecialPasswordClick,
  openCreatePasswordClick,
  hasSecurityPassword,
  //reset special password
  isResetPasswordOpen,
  onResetSpecialPasswordClick,
  cancelResetPasswordClick,
  openResetPassword,
  isSended,
  specialPasswordIsShow
}) => (
    <div>
      {
        isCreatePasswordOpen
          ? <CreateSpecialPassModal
            onChange={onChange}
            loading={loading}
            firstNumber={firstNumber}
            secondNumber={secondNumber}
            thirdNumber={thirdNumber}
            fourthNumber={fourthNumber}
            onCreatePasswordClick={onCreatePasswordClick}
            cancelCreatePasswordClick={cancelCreatePasswordClick}
            onCreateSpecialPasswordClick={onCreateSpecialPasswordClick}
            displayErrors={displayErrors}
            errorMessage={errorMessage}
          />
          : isResetPasswordOpen
            ? <ResetSpecialPassModal
              loading={loading}
              onResetSpecialPasswordClick={onResetSpecialPasswordClick}
              cancelResetPasswordClick={cancelResetPasswordClick}
              displayErrors={displayErrors}
              errorMessage={errorMessage}
              isSended={isSended}
            />
            : !scan ?
              <form
                onSubmit={onSubmit}
                noValidate
                autoComplete="off"
                className={`content-container ${component}`}
              >
                <div className={amountContainer}>
                  <span>¿CUÁNTO&nbsp;QUIERES&nbsp;ENVIAR?</span>
                  <AmountInput
                    amount={amount}
                    disabled={loading}
                    spanBorder={`${(
                      displayErrors === true &&
                        !Number(amount.replace(/\./g, '').replace(',', '.')) > 0 ? displayRedBorders : ''
                    )}`}
                    onChange={amountInputHandleChange}
                  />
                  <AutoFocusInput
                    placeholder="Descripción del pago"
                    type="text"
                    readOnly={loading}
                    onChange={onChange}
                    value={sendDescription}
                    name="sendDescription"
                  />
                  <AutoFocusInput
                    placeholder="Email"
                    type="text"
                    readOnly={loading}
                    onChange={onChange}
                    className={displayErrors && !sendEmail ? displayRedBorders : ""}
                    value={sendEmail}
                    name="sendEmail"
                    required
                  />
                  {
                    !loading
                      ? <span
                        onClick={openScan}
                        className={qrText}>
                        O ESCANEA EL CÓDIGO QR
              <img css={`margin-bottom: 4px;`} src={QRscan} alt="qr" />
                      </span>
                      : null
                  }
                  {
                    !loading && specialPasswordIsShow
                      ? <FourPinsInput
                        firstNumber={firstNumber}
                        secondNumber={secondNumber}
                        thirdNumber={thirdNumber}
                        fourthNumber={fourthNumber}
                        onChange={onChange}
                        disabled={loading}
                        onCreatePasswordClick={onCreatePasswordClick}
                        openCreatePasswordClick={openCreatePasswordClick}
                        displayErrors={displayErrors}
                        hasSecurityPassword={hasSecurityPassword}
                        title="Contraseña Especial"
                        openResetPassword={openResetPassword}
                        canResetPass={true}
                        loading={loading}
                        canShowPassword={true}
                      />
                      : null
                  }
                  {
                    displayErrors ?
                      <div className={errorContainer}>
                        {errorMessage}
                        <img
                          className={errorSvgCss}
                          src={errorSVG}
                          alt="error Icon"
                        />
                      </div>
                      : null
                  }
                  <AcceptButton
                    content="ENVIAR PAGO"
                    width="100%"
                    height="34px"
                    loading={loading}
                    type="submit"
                    className={primaryButton}
                  />
                </div>
              </form>
              : <div>
                <QrReader
                  delay={deley}
                  onError={handleError}
                  onScan={handleScan}
                  style={{ width: '100%' }}
                />
                <button
                  onClick={openScan}
                  disabled={loading}
                  className={` ${primaryButton2} btn-accept`}
                >
                  CANCELAR
                </button>
              </div>
      }
    </div>
  )
export default SendModal