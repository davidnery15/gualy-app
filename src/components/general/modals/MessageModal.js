import React, { Component } from 'react'
import { css } from 'emotion'

import iconClose from './../../../assets/icon-close.svg'

/* CSS */
  const invisible = css`
    display: none !important;
  `
  const visibleModal = css`
    display: flex;
  `
  const modalComponent = css`
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-color: rgba(11, 11, 29, 0.85);
    z-index: 1000;
  `
  const modalContainer = css`
    width: 40%;
    max-width: 500px;
    min-width: 300px;
    border-radius: 5px;
    background-color: #2a2c6a; 
    box-shadow: 0 9px 18px 0 rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: column;
    position: relative;
    z-index: 1000;
  `

  const modalHeader = css`
    margin: 0px;
    padding: 18px 25px;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `
  const divisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
  const modalBody = css`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    height: 100%;
    padding: 18px 25px;
  `
  const approvedButton = css`
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #2a2d6a;
    border-radius: 100px;
    border: none;
    background-color: #95fad8;
    width:100%;
    height: 34.2px;
    margin-top: 18px;
    cursor:pointer;
    outline:none !important;
  `
  const closeButton = css`
    position: absolute;
    top: -23px;
    background: transparent;
    border: none;
    text-align: right;
    width: 25px;
    cursor: pointer;
    outline: none !important;
    width: 100%;
  `
  const messageContainer = css`
    width: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    height: 100%;
    h2{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
    }
  `

export default class MessageModal extends Component {
  render() {
    const { visible, onClick, title, message } = this.props
    return (
      <div className={`${modalComponent} ${(visible === true ? visibleModal : invisible)}`} onClick={onClick}>
        <div className={modalContainer}>
          <button className={closeButton} onClick={onClick}>
            <img src={iconClose} alt='icon close' />
          </button>
          <div className={modalHeader}>
            <h1> {title} </h1>
          </div>
          <div className={divisor}></div>
          <div className={modalBody}>
            <div className={messageContainer}>
              <h2>{message}</h2>
            </div>
            <button className={approvedButton} onClick={onClick}>ACEPTAR</button>
          </div>
        </div>
      </div>
    )
  }
}