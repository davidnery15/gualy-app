import React from 'react'
import { css } from 'emotion'
import OptionsModalWrapper from '../OptionsModalWrapper'
import reportProblemIcon from '../../../assets/report-problem-icon-red.svg'
import { BS2 } from '../../../config/currencies'
import moment from 'moment'
import { Link } from 'react-router-dom'
import CountUp from 'react-countup'
import defaultUserPicture from '../../../assets/accountCircle.svg'
const component = css`
    padding: 0px !important;
    posotion: relative;
    display: flex;
    flex-direction: column;
    margin-top: 30px;
    margin-bottom: 20px;
    a{
      text-decoration: none !important;
    }
    p{
      word-break: break-all;
    }
  `
const senderPictureStyle = css`
    height: 78px;
    width: 78px;
    border-radius: 7px;
  `
const mainText = css`
font-size: 18px;
font-weight: 900;
font-style: normal;
font-stretch: normal;
line-height: 1.1;
letter-spacing: normal;
`
const sectionChild = css`
  display: flex;
  justify-content: center;
  flex-direction: row;
  margin-top: 20px;
  cursor: pointer
  `
const reportProblemTitle = css`
margin-left: 9px;
margin-top: 14px;
color: #ff6061;
font-size: 14px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
@media(max-width: 320px){
  font-size: 12px;
}
  `
const opacityText = css`
  opacity: 0.7;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  `
const emailClass = css`
  font-size: 14px;
  font-weight: 400;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  `
const dateClass = css`
font-size: 14px;
font-weight: 400;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
  `
const currencyClass = css`
font-size: 17px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
margin-right: 12px;
margin-top: 10px;
  `
const amountClass = css`
  font-size: 26.3px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  `
const devisor = css`
height: 1px;
width: 100%;
padding: 0;
margin: 0;
opacity: 0.9;
background-color: #1d1f4a;
margin-bottom: 10px;
  `
const idTransactionClassDesktop = css`
  word-break: break-all;
  font-size: 11px;
  margin-left: 17px;
  `
const idClass = css`
  font-family: Montserrat;
  font-weight: 900;
  font-style: normal;
  font-stretch: normal;
  line-height: 0.98;
  letter-spacing: normal;
  color: #f6f7fa;
  margin-right: 6px;
    `
const dateFormatDivided = (dateFormat) => dateFormat ? dateFormat.split(' ') : []
const TransactionDetailsModal = ({
  onClose,
  showModal,
  transaction,
  isAdmin,
}) => {
  const isRejected = transaction.status === "Rejected" ? true : false
  return (
    <OptionsModalWrapper show={showModal} onClose={onClose} width="328px" height="auto">
      <div className={`content-container ${component}`}>
        <div css={`display: flex; flex-direction: row; margin-left: 17px;`}>
          <img
            src={!isRejected
              ? transaction.senderProfilePicture
              : transaction.operator
                ? transaction.operator.profilePicture
                : transaction.receiverProfilePicture}
            alt=""
            className={senderPictureStyle}
            onError={(e) => { e.target.onerror = null; e.target.src = defaultUserPicture }}
          />
          <div css={`display: flex;flex-direction: column;margin-left: 15px;`}>
            <p className={opacityText}>
              {isRejected
                ? "Rechazado por"
                : transaction.mode === "Deposit"
                  ? "Saldo añadido por"
                  : transaction.mode === "Withdraw"
                    ? "Saldo retirado por"
                    : "Pago enviado por"}
            </p>
            <p className={mainText}>
              {!isRejected
                ? transaction.senderUsername
                : transaction.operator
                  ? transaction.operator.name
                  : transaction.receiverUsername}
            </p>
            <p className={emailClass}>
              {!isRejected
                ? transaction.senderEmail
                : transaction.operator
                  ? transaction.operator.email
                  : transaction.receiverEmail}
            </p>
          </div>
        </div>
        {
          !isRejected
            ? <div css={`margin-left: 17px;`}>
              <p className={opacityText} css={`margin: 0;margin-top: 20px;margin-bottom: 4px;`}>
                {transaction.mode === "Withdraw"
                  ? "Cantidad retirada"
                  : transaction.transactionType === "Send" && transaction.mode !== "Deposit"
                    ? "Cantidad enviada"
                    : "Cantidad recibida"}
              </p>
              <div css={`display: flex`}>
                <p className={currencyClass}>{`${BS2}`}</p>
                <span className={amountClass}>
                  <CountUp
                    start={transaction.originalAmount || transaction.amount}
                    end={transaction.originalAmount || transaction.amount}
                    duration={0}
                    separator="."
                    decimals={2}
                    decimal=","
                  />
                </span>
              </div>
              {
                (transaction.transactionType === "Receive" && transaction.mode !== "Deposit") || transaction.mode === "Withdraw"
                  ? <p className={opacityText} css={`display:flex;margin: 0;margin-top: 4px;margin-bottom: 4px;`}>
                    {`Tarifa ${BS2} `}
                    <CountUp
                      start={transaction.fee || 0}
                      end={transaction.fee || 0}
                      duration={0}
                      separator="."
                      decimals={2}
                      decimal=","
                      css={`margin-left: 7px;`}
                    />
                  </p>
                  : null
              }
              {
                (transaction.transactionType === "Receive" && transaction.mode !== "Deposit") || transaction.mode === "Withdraw"
                  ? <p className={opacityText} css={`display:flex;margin: 0;margin-top: 4px;margin-bottom: 4px;`}>
                    {`TOTAL ${BS2} `}
                    <CountUp
                      start={transaction.amount}
                      end={transaction.amount}
                      duration={0}
                      separator="."
                      decimals={2}
                      decimal=","
                      css={`margin-left: 7px;`}
                    />
                  </p>
                  : null
              }
              <p className={dateClass} css={`margin-top: 14px;`}>
                {`${dateFormatDivided(transaction.dateFormat)[1]}
          ${dateFormatDivided(transaction.dateFormat)[0]}, 
          ${moment(transaction.timestamp).utc(-264).format('YYYY')} | ${moment(transaction.timestamp).utc(-264).format('h:mm a')}`}
              </p>
            </div>
            : <div css={`display: flex;flex-direction: column;margin-left: 17px;`}>
              <p className={opacityText} css={`margin: 0;margin-top: 20px;margin-bottom: 4px;`}>
                Razón
          </p>
              <p className={dateClass}>{transaction.reason || "Sin razón de rechazo"}</p>
              <p className={dateClass} css={`margin-top: 14px;`}>
                {`${dateFormatDivided(transaction.dateFormat)[1]}
          ${dateFormatDivided(transaction.dateFormat)[0]}, 
          ${moment(transaction.timestamp).utc(-264).format('YYYY')} | ${moment(transaction.timestamp).utc(-264).format('h:mm a')}`}
              </p>
            </div>
        }
        <div className={devisor} />
        {
          transaction.mode !== "Deposit"
            && transaction.mode !== "Withdraw"
            ? <div css={`display: flex;flex-direction: column;margin-left: 17px;`}>
              <p className={opacityText}>Descripción del pago</p>
              <p className={dateClass}>{transaction.description || "Sin descripción"}</p>
            </div>
            : null
        }
        <div className={idTransactionClassDesktop}>
          <span className={idClass}>ID:</span>
          {`${transaction.idTransaction}`}
        </div>
        {
          !isAdmin
            ? <Link to='/ayuda'>
              <div className={sectionChild}>
                <img
                  alt=""
                  src={reportProblemIcon}
                />
                <p className={reportProblemTitle}>REPORTAR UN PROBLEMA</p>
              </div>
            </Link>
            : null
        }
      </div>
    </OptionsModalWrapper>
  )
}
export default TransactionDetailsModal
