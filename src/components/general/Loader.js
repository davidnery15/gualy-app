import React from 'react'
import { css } from 'emotion'
const Loader = ({ color }) => {
  const generalLoader = css`
  :empty {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  :empty::after {
    content: "";
    width: 1.5em;
    height: 1.5em;
    border: transparent;
    border-left: 0.1em solid ${color || "#F6F7FA"};
    border-radius: 50%;
    -webkit-animation: load8 1.1s infinite linear;
    animation: load8 1.1s infinite linear;
  }
  `
  return (
    <div className={generalLoader} />
  )
}
export default Loader