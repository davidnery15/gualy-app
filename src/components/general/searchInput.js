import React from 'react'
import { css } from 'emotion'
import SearchIcon from '../../assets/ic-search.svg'
import Loader from '../general/Loader'

/* CSS */
const group = css`
  display: flex;
  position: relative;
  width: 100%;
  margin-left: 20px;
  margin-right: 20px;
  input{
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
      color: #f6f7fa
    }
    ::-moz-placeholder { /* Firefox 19+ */
      color: #f6f7fa
    }
    :-ms-input-placeholder { /* IE 10+ */
      color: #f6f7fa
    }
    :-moz-placeholder { /* Firefox 18- */
      color: #f6f7fa
    }
  }
`
const inputProp = css`
  width: 100%;
  min-width: 100px;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.68;
  letter-spacing: normal;
  text-align: left;
  color: #f6f7fa;
  padding-left: 35px; 
  border-bottom: 1px solid #ffffff;
  padding-bottom: 3px
`
const inputContainer = css`
  width: 100%;
  `
const searchInputClass = css`
  position: absolute;
  left: 2px;
  bottom: 11px;
  `
const loaderClass = css`
  position: absolute;
  left: 2px;
  bottom: 11px;
`
export default ({
  onChange,
  value,
  name,
  type,
  placeholder,
  className,
  onKeyPress,
  isSearchingData,
}) =>
  <div className={`${group} ${className}`}>
    {
      isSearchingData
        ? <span className={loaderClass}><Loader color="#f6f7fa" /></span>
    : <img className={searchInputClass} src={SearchIcon} alt="search icon" />
    }
    <div className={`${inputContainer}`}>
      <input
        className={`${inputProp}`}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        type={type}
        name={name}
        onKeyPress={onKeyPress}
      />
    </div>
  </div>
