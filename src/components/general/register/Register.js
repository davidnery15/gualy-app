import React, { Component } from 'react'
import { css } from 'emotion'
import { Card, Button, CardHeader, CardBody } from 'reactstrap'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Redirect } from 'react-router'
import logo from '../../../assets/logos/logo-gualy-dark-bg.svg'
import Loader from './../../general/Loader'
import iconVisibilityOn from './../../../assets/baseline-visibility-on.svg'
import iconVisibilityOff from './../../../assets/baseline-visibility-off.svg'
import email from './../../../assets/email.svg'
import errorSVG from './../../../assets/ic-info-red.svg'
import styled from 'react-emotion'
import { securityQuestionKeys } from './../../../constants/securityQuestionKeys'
import arrowIcon from './../../../assets/drop-down-arrow.svg'
import { ToastContainer, toast } from 'react-toastify'
import iconCamera from './../../../assets/icon-camera.svg'
import GoBackButton from '../../general/goBackButton'
import moment from 'moment'
import { emailsRegex, numbers, letters } from '../../../constants/regex'
import CropImageModal from '../modals/CropImageModal'
import TermsAndConditionsModal from '../modals/TermsAndConditionsModal'
import DateInput from '../DateInput'
import { postRequest } from '../../../utils/createAxiosRequest';
// import { customConsoleLog } from '../../../utils/customConsoleLog';
// import moment from 'moment'
const createObjectMapper = objectArray => objectArray.reduce(
  (acumulator, current) => ({
    ...acumulator,
    [current.customId]: current.questionKey
  }),
  { default: null }
)
/* CSS */
const loginCard = css`
    width: 548px;
    max-width: 548px;
    @media (max-width: 558px) {
      width: 95%;
      margin: 0 10px;
      margin-top: 40px;
    }
  `
const formsContainer = css`
  display: flex;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap
  `
const formChildContainer = css`
  width: 203px;
  margin: 20px;
  @media (max-width: 558px) {
    width: 95%;
    margin: 5px;
  }
  `
// const helpText = css`
//   margin-top: 20px;
//   margin-bottom: 5px;
//   justify-content: center;
//   align-self: center;
//   width: 196px;
//   max-width: 196px;
//   height: 56px;
//   font-family: Montserrat;
//   font-size: 11px;
//   font-weight: 300;
//   text-align: center;
//   color: #f6f7fa;
// `
const logoGualy = css`
    width: 155.7px;
    max-width: 155.7px;
    height: auto;
    margin-top: 28px;
  `
const hSeparator = css`
    width: 270px;
    min-height: 1px;
    opacity: 0.5;
    background-color: #1d1f4a;
    align-self: center;
    margin-top: 30px;
  `

// const anchorLink = css`
//     text-decoration: none;
//     color: #95fad8 !important;

//     &:active {
//       color: #95fad8;
//       text-decoration: none;
//     }

//     &:visited {
//       color: #95fad8;
//       text-decoration: none;
//     }

//     &:hover {
//       color: #95fad8;
//       text-decoration: none;
//     }
//   `
const buttonLogIn = css`
    cursor: pointer;
    position:relative;
    min-height: 48px;
  `
const responsive = css`
    @media(max-width: 420px) {
      align-items: flex-start !important;
    }
    input[type="date"]::-webkit-inner-spin-button,
    input[type="date"]::-webkit-calendar-picker-indicator {
    display: none;
    -webkit-appearance: none;
}
  `
const passwordContainer = css`
    position: relative;
  `
const visibilityOnCSS = css` 
    width: 25px; 
    height: 25px; 
    object-fit: contain; 
    cursor: pointer;
  `
const btnVisibility = css`
    background: transparent;
    border: none;
    position: absolute;
    right: 5px;
    top: 10px;
  `
const errorContainer = css`
    color: red;
    display: flex;
    justify-content: center;
    align-items: center; 
    font-size: 12px;
    margin-bottom: 15px;
  `
const errorSvgCss = css`
    width: 14.7px;
    height: 13px;
    object-fit: contain;
    margin: 5px;
  `
const marginTop = css`
  margin-top: 20px !important;
`
const dropDownDivContainer = css`
border-bottom: 1px solid white;
width: 100%;
div{
  margin-top: 0;
}
`
const dropDownDiv = css`
position: relative;
display: flex;
align-items:center;
`
const CustomDropdown = styled('select')`
height: 43.3px;
width:100%;
padding: 7px 4px;
background: transparent;
border: none;
cursor: pointer;
font-size: 15px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.68;
letter-spacing: normal;
text-align: left;
color: #ffffff;
appearance: none; 
outline: none !important;
option{
  background-color: #242657;
}
`
const dropdownQuestionPadding = css`
padding-right: 15px;
@media(max-width: 500px){
  option{
    font-size: 8px;
  }
}
`
const arrowContainer = css`
width: 15px !important;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
position: absolute;
right: 0;
top: 0;
pointer-events: none;
`
const arrowSpaceImg = css`
  right: -3px;
`
const arrow = css`
width: 10.4px;
height: 6.6px;
object-fit: contain;
`
const firstComponent = css`
margin-top: 0 !important;
`
const DivDNI = css`
    width: 40px !important;
    border-bottom: 1px solid white;
    margin-right: 15px;
    position: relative;
    min-width: 55px;
    padding-left: 5px;
    span{
      position: absolute;
      left:0;
      pointer-events: none;
    }
  `
const passwordInfo = css`
font-size: 8px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.13;
letter-spacing: normal;
text-align: left;
color: #8078ff;
  `
const uploadBtnWrapper = css`
    margin-top: 0 !important;
  `
const UploadBtn = styled('button')`
  width: 84.4px;
  height: 84.4px;
  color: #242656;
  display: flex;
  justify-content: center;
  align-items: center;
  outline: none !important;
  cursor: pointer;
  background-color: #95fad8;
  border: none;
  border-radius: 11.9px;
  box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
  position: relative;
  margin: auto;
  @media(max-width: 630px) {
    width: 72px;
    height: 72px;
  }
`
const specficError = css`
border: 3px solid red;
`
const iconCameraClass = css`
    width: 41px;
    height: 37.4px;
    margin-right: 5px;
    object-fit: contain;
    @media(max-width: 630px) {
      width: 35px;
      height: 32px;
    }
  `
const imagePreview = css`
    width: 84.4px;
    height: 84.4px;
    position: absolute;
    cursor: pointer;
    border-radius: 11.9px;
    object-fit: cover;
    border: 2px solid white;
  `
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const loginButtonContainer = css`
    margin: 0 20px;
    margin-bottom: 35px;
    margin-top: 30px;
  `
const registerButton = css`
width: 299px !important;
height: 51.8px;
margin: auto;
@media(max-width: 450px){
  width: 100% !important;
}
  `
const termsAndConditions = css`
font-size: 10px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: 1.4;
letter-spacing: normal;
text-align: center;
color: #8078ff;
cursor: pointer
  `
const beforeTermsAndConditions = css`
font-size: 10px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: 1.4;
letter-spacing: normal;
text-align: center;
margin-right: 3px;
  `
const termsAndConditionsInput = css`
    width: 18.5px;  
    height: 18.5px;
    opacity: 0.54;
    margin-right: 6px;
    cursor: pointer
  `
const displayErrors = css`
  input:invalid {
    border-color:red;
  }
  textarea:invalid {
    border-color:red;
  }
  input:invalid specficError 
`
const displayRedBorders = css`
    border-color:red;
`

const emailSendedImage = css`
width: 300px;
height: 112px;
margin-top: 25px;
margin-bottom: 25px;
@media(max-width: 400px){
  width: 90%;
}
`
const registerTextArea = css`
    color: #8078ff;
    font-size: 14px;
    padding: 10px 10px 10px 5px;
    display: block;
    width: 100%;
    resize: none;
    border: none;
    border-bottom: 1px solid #757575;
    background-color: transparent;
`
const goBackButtonClass = css`
  top: 10px;
  left: 10px
  position: absolute
  `
const registerContainer = css`
  display: flex;
  justify-content: center;
  align-items: center;
  height: auto;
  margin-bottom: 20px;
  margin-top: 20px
  `
const widthInfo = css`
 position: absolute;
 bottom: 2px;
 left: 0; 
 right: 0; 
 margin-left: auto; 
 margin-right: auto; 
 font-size: 8px;
 color: #242656;
 margin: 0;
 font-weight: 600;
 @media(max-width: 630px) {
  font-size: 8px;
  bottom: 1px;
}
  `

class Register extends Component {
  state = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    securityAnswer: '',
    address: '',
    contactIdType: '',
    contactIdNumber: '',
    profileImgURI: '',
    imageError: false,
    image: '',
    phone: '',
    bornDate: '',
    questionKey: '',
    loading: false,
    passwordVisibility: false,
    acceptTerms: false,
    displayErrors: false,
    modalIsOpen: false,
    redirectAfterRegister: false,
    registered: false,
    errorMessage: '',
    userImage: '',
    cropImageModal: false,
  }
  validate18years = (bornDate) => {
    const bornDateArray = bornDate.split('-')
    const eighteenYearsAgo = new Date().setFullYear(new Date().getFullYear() - 18)
    const minBornTimeStamp = new Date(eighteenYearsAgo).setHours(0, 0, 0, 0)// La fecha minima de nacimiento para un mayor de edad
    const bornTimeStamp = new Date(`${bornDateArray[0]}-${bornDateArray[1]}-${bornDateArray[2]}`).setHours(24, 0, 0, 0)
    if (bornTimeStamp > minBornTimeStamp) {
      return false //is not an adult
    } else {
      return bornDate ? true : false//is an adult
    }
  }
  componentDidMount = () => {
    this.setState({
      idToUidObjectMapper: createObjectMapper(securityQuestionKeys)
    })
  }

  handleInputChange = ({ target }) => {
    const value =
      target.name === 'phone'
        ? target.value.length < 11
          ? target.value !== '0'
            ? target.value
            : target.value.slice(0, -1)
          : target.value.slice(0, -1)
        : target.name === 'contactIdNumber'
          ? target.value.length < 10 ? target.value : target.value.slice(0, -1)
          : target.name === 'password'
            ? target.value.length < 16 ? target.value : target.value.slice(0, -1)
            : target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [target.name]: value
    })
  }

  toastRegisterId = null
  handleSubmit = async (event) => {
    event.preventDefault()
    this.toastRegisterId = toast('Registrando usuario...', { autoClose: false })
    if (this.state.firstName !== '' &&
      this.state.lastName !== '' &&
      this.state.contactIdType !== '' &&
      this.state.password !== '' &&
      this.state.contactIdNumber !== '' &&
      this.state.address !== '' &&
      this.state.email !== '' &&
      this.state.questionKey !== '' &&
      this.state.securityAnswer !== '' &&
      this.state.bornDate !== '' &&
      this.validate18years(this.state.bornDate) &&
      this.state.image !== '' &&
      this.state.acceptTerms === true &&
      this.state.phone.length === 10 &&
      this.state.password.length > 7 &&
      this.state.email.match(emailsRegex) &&
      this.state.password.match(numbers) &&
      this.state.password.match(letters)
    ) {
      this.setState({ loading: true })
      let { firstName,
        lastName,
        contactIdType,
        password,
        contactIdNumber,
        email, phone,
        questionKey,
        securityAnswer,
        bornDate,
        image } = this.state
      const phoneNumber = `+58${phone}`
      const data = {
        firstName,
        lastName,
        name: `${firstName} ${lastName}`,
        idType: contactIdType,
        idNumber: contactIdNumber,
        email,
        phone: phoneNumber,
        bornDate: moment(bornDate).format('DD-MM-YYYY'),
        publicKey: '-L-CGPS6Iuge1PFk755b',
        idCountryName: 'Cédula',
        password,
        profileImgURI: image, //base 64 image
        type: 'client',
        questionKey,
        securityAnswer,
        deviceUniqueToken: ''
      }
      // console.log("SENDED: ", data)
      try {
        let response = await postRequest('users/addUser', data)
        // console.log("RESPONSE: ", response)
        if (response.data.success) {
          document.getElementById('registerForm').reset();
          toast.update(this.toastRegisterId, {
            render: 'Su cuenta ha sido registrada exitosamente.',
            autoClose: 5000,
          })
          this.setState({
            loading: false,
            email: '',
            referredEmail: '',
            password: '',
            firstName: '',
            lastName: '',
            securityAnswer: '',
            contactIdType: '',
            contactIdNumber: '',
            profileImgURI: '',
            imageError: false,
            image: '',
            phone: '',
            bornDate: '',
            questionKey: '',
            passwordVisibility: false,
            acceptTerms: false,
            displayErrors: false,
            redirectAfterRegister: true,
            registered: true
          })
        } else {
          this.setState({
            loading: false,
            errorMessage: response.data.error.message
          })
          return toast.update(this.toastRegisterId, {
            render: response.data.error.message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        this.setState({
          loading: false,
        })
        console.error(error)
        this.setState({ errorMessage: 'Verifique su conexión y vuelva a intentarlo.' })
        return toast.update(this.toastRegisterId, {
          render: 'Verifique su conexión y vuelva a intentarlo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } else {
      this.setState({
        displayErrors: true
      })
      if (!event.target.profileImgURI.checkValidity()) {
        this.setState({
          imageError: true
        })
        if (this.state.image === '') {
          this.setState({ errorMessage: 'Debe subir una foto de perfil.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe subir una foto de perfil.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (!event.target.firstName.checkValidity()) {
        event.target.firstName.focus()
        if (event.target.firstName.value === '') {
          this.setState({ errorMessage: 'Debe colocar un nombre de contacto.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar un nombre de contacto.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (!event.target.lastName.checkValidity()) {
        event.target.lastName.focus()
        if (event.target.lastName.value === '') {
          this.setState({ errorMessage: 'Debe colocar un apellido de contacto.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar un apellido de contacto.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (!event.target.email.checkValidity()) {
        event.target.email.focus()
        if (event.target.email.value === '') {
          this.setState({ errorMessage: 'Debe colocar un email.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar un email.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (!this.state.email.match(emailsRegex)) {
        event.target.email.focus()
        this.setState({ errorMessage: 'Debe colocar un email valido.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar un email valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!event.target.contactIdNumber.checkValidity()) {
        event.target.contactIdNumber.focus()
        if (event.target.contactIdNumber.value === '') {
          this.setState({ errorMessage: 'Debe colocar un número de cédula.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar un número de cédula.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (this.state.contactIdType === '') {
        event.target.contactIdType.focus()
        this.setState({ errorMessage: 'Debe colocar colocar tipo de documento.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar colocar tipo de documento.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }

      if (!event.target.phone.checkValidity()) {
        event.target.phone.focus()
        if (event.target.phone.value === '') {
          this.setState({ errorMessage: 'Debe colocar un número de telefóno.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar un número de telefóno.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (this.state.phone.length !== 10) {
        event.target.phone.focus()
        this.setState({ errorMessage: 'Su número de telefóno debe contener 10 digitos, ej: 4164543456' })
        return toast.update(this.toastRegisterId, {
          render: 'Su número de telefóno debe contener 10 digitos, ej: 4164543456',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (this.state.bornDate === '') {
        this.setState({ errorMessage: 'Debe colocar una fecha de nacimiento.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una fecha de nacimiento.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.validate18years(this.state.bornDate)) {
        this.setState({ errorMessage: 'Debes ser mayor de edad para crear una cuenta Gualy.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debes ser mayor de edad para crear una cuenta Gualy.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!event.target.password.checkValidity()) {
        event.target.password.focus()
        if (event.target.password.value === '') {
          this.setState({ errorMessage: 'Debe colocar una contraseña.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar una contraseña.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (event.target.password.value.length <= 7) {
        event.target.password.focus()
        this.setState({ errorMessage: 'Debe colocar una contraseña entre 8 y 15 caracteres.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe colocar una contraseña entre 8 y 15 caracteres.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.password.match(numbers)) {
        event.target.password.focus()
        this.setState({ errorMessage: 'La contraseña debe contener un número entre (0-9).' })
        return toast.update(this.toastRegisterId, {
          render: 'La contraseña debe contener un número entre (0-9).',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.password.match(letters)) {
        event.target.password.focus()
        this.setState({ errorMessage: 'La contraseña debe contener una letra mayúscula.' })
        return toast.update(this.toastRegisterId, {
          render: 'La contraseña debe contener una letra mayúscula.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }

      if (!event.target.questionKey.checkValidity()) {
        event.target.questionKey.focus()
        if (event.target.questionKey.value === '') {
          this.setState({ errorMessage: 'Debe colocar una pregunta de seguridad.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar una pregunta de seguridad.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (!event.target.securityAnswer.checkValidity()) {
        event.target.securityAnswer.focus()
        if (event.target.securityAnswer.value === '') {
          this.setState({ errorMessage: 'Debe colocar una respuesta de seguridad.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar una respuesta de seguridad.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (!event.target.address.checkValidity()) {
        event.target.address.focus()
        if (event.target.address.value === '') {
          this.setState({ errorMessage: 'Debe colocar una dirección.' })
          return toast.update(this.toastRegisterId, {
            render: 'Debe colocar una dirección.',
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      }
      if (this.state.acceptTerms === false) {
        event.target.acceptTerms.focus()
        this.setState({ errorMessage: 'Debe aceptar términos y condiciones.' })
        return toast.update(this.toastRegisterId, {
          render: 'Debe aceptar términos y condiciones.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  handleVisibility = (e) => {
    e.preventDefault()
    this.setState({ passwordVisibility: !this.state.passwordVisibility })
  }
  renderError = (error, errorCode) =>
    error ? <p css={`color:red;`}>{errorCode}</p> : null

  getOptions = items => items.map(item => {
    return <option key={item.customId} value={item.questionKey}>{item.text}</option>
  })
  handleQuestionSelect = ({ target: { value }, target }) => {
    this.setState({
      [target.name]: value
    })
  }
  redirectUser = () => {
    const redirect = this.props.isLoggedIn ? <Redirect to="/" /> : null
    return redirect
  }
  onAccepted = () => {
    this.setState({ modalIsOpen: false })
  }
  showModal = () => {
    this.setState({ modalIsOpen: !this.state.modalIsOpen })
  }
  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0]
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader()
        //Read the contents of Image File.
        reader.readAsDataURL(file)
        reader.onload = (e) => {
          //Initiate the JavaScript Image object.
          let image = new Image()
          //Set the Base64 string return from FileReader as source.
          image.src = e.target.result
          image.onload = (e) => {
            //Validate the File Height and Width.
            // let height = e.path[0].height
            // let width = e.path[0].width
            // if (height < 100 || width < 100 || height > 800 || width > 800) {
            //   toast.error("Altura y anchura de la imagen debe ser mínimo de 100px y un máximo de 800px y preferiblemente cuadrada. ej: 200x200");
            //   return false
            // } else {
              this.setState({
                userImage: reader.result,
                imageError: false,
                cropImageModal: true
              })
            // }
          }
        }
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }
  closeCropImageModal = () => {
    this.setState({ cropImageModal: false })
  }
  onSave = (image, cancelCrop) => {
    this.setState({ image, cropImageModal: false })
    cancelCrop()
  }

  render() {
    const { loading, passwordVisibility } = this.state
    return (
      <div className={`${registerContainer} ${responsive}`}>
        <TermsAndConditionsModal
          onAccepted={this.onAccepted}
          show={this.state.modalIsOpen}
          onClose={this.showModal}
          width="90%"
          height="80%"
        />
        <CropImageModal
          show={this.state.cropImageModal}
          onClose={this.closeCropImageModal}
          image={this.state.userImage}
          onSave={this.onSave}
        />
        <Card className={`${loginCard} box-shadow justify-content-center`} fluid="true">
          {
            !loading
              ? <GoBackButton
                height="21px"
                width="21px"
                route='/'
                style={goBackButtonClass}
              />
              : null
          }
          <CardHeader className='text-center' css={`border-bottom: 0px solid transparent !important;`} fluid="true">
            <span css={`font-weight: bold; font-size: 24px;`}>{this.state.registered ? 'Bienvenido a' : 'Regístrate en'}</span>
            <br />
            <img src={logo} alt="Logo Gualy" className={`${logoGualy} img-fluid`} />
          </CardHeader>
          <div className={hSeparator} />
          <CardBody>
            <form noValidate autoComplete='off' onSubmit={this.handleSubmit} id="registerForm">
              {
                this.state.registered ?
                  <div css={`text-align: center`}>
                    <span css={`font-weight: bold; font-size: 24px;`}>Se te ha enviado un correo para verificar tu cuenta.</span>
                    <img alt="" className={emailSendedImage} src={email}></img>
                    <div className={loginButtonContainer}>
                      <NavLink to='/'>
                        <Button
                          color='primary'
                          size='lg'
                          onClick={this.goLogin}
                          className={`${buttonLogIn} ${registerButton}`}
                          block
                          disabled={(loading)}>
                          IR A LOGIN
                        </Button>
                      </NavLink>
                    </div>
                  </div>
                  :
                  <div className={`${(this.state.displayErrors === true ? displayErrors : '')}`}>
                    <div className={uploadBtnWrapper}>
                      <UploadBtn className={this.state.imageError ? specficError : ''}>
                        <img
                          src={this.state.image === '' ? iconCamera : this.state.image}
                          className={this.state.image === '' ? iconCameraClass : imagePreview}
                          alt="uploadPicture"
                        />
                        <input
                          className={inputFileHidden}
                          value={this.state.profileImgURI}
                          name='profileImgURI'
                          type='file'
                          accept="image/*"
                          onChange={this.encodeImageFileAsURL}
                          disabled={loading}
                          required
                        />
                        {
                          this.state.image === ''
                            ? <p className={widthInfo}>200x200</p>
                            : null
                        }
                      </UploadBtn>
                    </div>
                    <div className={formsContainer}>
                      <div className={formChildContainer}>
                        <div css={`padding-bottom:5px; font-size: 14px;`} className='group mb-1'>
                          <input
                            css={`font-size: 14px;`}
                            placeholder='Nombre *'
                            name='firstName'
                            onChange={this.handleInputChange}
                            value={this.state.firstName}
                            maxLength="40"
                            disabled={loading}
                            required
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <div css={`padding-bottom:5px;`} className='group mb-1'>
                          <input
                            css={`font-size: 14px;`}
                            placeholder='Apellido *'
                            name='lastName'
                            onChange={this.handleInputChange}
                            value={this.state.lastName}
                            maxLength="40"
                            disabled={loading}
                            required
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <div css={`padding-bottom:5px;`} className='group mb-1'>
                          <input
                            css={`font-size: 14px;`}
                            type='text'
                            placeholder='Correo electrónico *'
                            name='email'
                            onChange={this.handleInputChange}
                            value={this.state.email}
                            disabled={loading}
                            required
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <div className='d-flex'>
                          <div
                            className={`
                          ${dropDownDiv} 
                          ${firstComponent} 
                          ${DivDNI} 
                          ${this.state.displayErrors && this.state.contactIdType === '' ? displayRedBorders : ''}`}
                          >
                            {/* <span>C.I.&nbsp;</span> */}
                            <CustomDropdown
                              name='contactIdType'
                              onChange={this.handleInputChange}
                              value={this.state.contactIdType}
                              disabled={loading}
                            >
                              <option value="V-">CI-V</option>
                              <option value="E-">CI-E</option>
                              <option value="P-">P-</option>
                            </CustomDropdown>
                            <div className={`${arrowContainer} ${firstComponent} ${arrowSpaceImg}`}>
                              <img src={arrowIcon} className={arrow} alt='arrow' />
                            </div>
                          </div>
                          <div css={`position: relative`}>
                            <input
                              css={`font-size: 14px;`}
                              type="number"
                              placeholder="Cédula"
                              onChange={this.handleInputChange}
                              value={this.state.contactIdNumber}
                              name='contactIdNumber'
                              maxLength="8"
                              pattern='^\d{7,8}$'
                              disabled={loading}
                              required
                            />
                            <span className='highlight'></span>
                            <span className='bar'></span>
                          </div>
                        </div>
                        <div css={`padding-bottom:5px;position: relative`} className='group mb-1'>
                          <p css={`position: absolute; top: 10px;`}
                            >+58
                          </p>
                          <input
                            css={`padding-left: 40px; font-size: 14px;`}
                            placeholder='Teléfono *'
                            name='phone'
                            type="number"
                            onChange={this.handleInputChange}
                            value={this.state.phone}
                            disabled={loading}
                            required
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <div css={`padding-bottom:5px;position: relative`} className='group mb-1'>
                          <span className={beforeTermsAndConditions} >Fecha de nacimiento</span>
                          <DateInput
                            updateDate={({ day, month, year, currentName }) => {
                              if (day && month && year) {
                                this.setState({
                                  [currentName]: `${year}-${month}-${day}`
                                })
                              }
                            }}
                            currentName="bornDate"
                            isValid={this.state.displayErrors && this.state.bornDate === '' ? false : true}
                            disabled={loading}
                          />
                        </div>
                      </div>
                      <div className={formChildContainer}>
                        <div css={`padding-bottom:5px;`} className='group mb-1'>
                          <textarea
                            placeholder='Dirección *'
                            name='address'
                            className={registerTextArea}
                            onChange={this.handleInputChange}
                            value={this.state.address}
                            disabled={loading}
                            required
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <div css={`padding-bottom:5px;`} className={` ${passwordContainer}`}>
                          <input
                            css={`font-size: 14px;`}
                            type={passwordVisibility ? 'text' : 'password'}
                            placeholder='Contraseña *'
                            name='password'
                            onChange={this.handleInputChange}
                            value={this.state.password}
                            disabled={loading}
                            required
                          />
                          <button
                            className={btnVisibility}
                            onClick={this.handleVisibility}>
                            <img
                              className={visibilityOnCSS}
                              src={passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
                              alt="visibilidad"
                            />
                          </button>
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <p className={passwordInfo}>Debe contener un número (0-9), una letra mayúscula. Entre 8 y 15 caracteres.</p>
                        <div className='group mb-1'>
                          <div
                            css={`padding-bottom:5px;`}
                            className={`
                          ${marginTop} 
                          ${dropDownDivContainer} 
                          ${this.state.displayErrors && this.state.questionKey === '' ? displayRedBorders : ''}`}
                          >
                            <div className={dropDownDiv}>
                              <CustomDropdown
                                className={dropdownQuestionPadding}
                                value={this.state.questionKey}
                                name='questionKey'
                                onChange={this.handleQuestionSelect}
                                disabled={loading}
                                required
                              >
                                <option value="" disabled defaultValue>Pregunta de seguridad</option>
                                {
                                  this.getOptions(securityQuestionKeys)
                                }
                              </CustomDropdown>
                              <div className={arrowContainer}>
                                <img src={arrowIcon} className={arrow} alt="dropdown" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div css={`padding-bottom:5px;`} className='group mb-1'>
                          <input
                            css={`font-size: 14px;`}
                            type='text'
                            placeholder='Respuesta secreta *'
                            name='securityAnswer'
                            onChange={this.handleInputChange}
                            value={this.state.securityAnswer}
                            disabled={loading}
                            required
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                        <div
                          css={`padding-bottom:5px; display: flex; height: 20px; flex-direction: row`}
                          className={`group mb-1 ${this.state.displayErrors && this.state.acceptTerms === false ? displayRedBorders : ''}`}>
                          <input
                            type='checkbox'
                            name='acceptTerms'
                            className={termsAndConditionsInput}
                            onChange={this.handleInputChange}
                            value={this.state.acceptTerms}
                            disabled={loading}
                            required
                          />
                          <p className={beforeTermsAndConditions}>
                            {`Acepto los `}
                          </p>
                          <p onClick={!loading ? this.showModal : null} className={termsAndConditions}> términos y condiciones.</p>
                        </div>
                        <div css={`padding-bottom:5px;`} className='group mb-1'>
                          <input
                            css={`font-size: 14px;`}
                            type='text'
                            placeholder='Email de quien me refirió'
                            name='referredEmail'
                            onChange={this.handleInputChange}
                            value={this.state.referredEmail}
                            disabled={loading}
                          />
                          <span className='highlight'></span>
                          <span className='bar'></span>
                        </div>
                      </div>
                    </div>
                    {
                      this.state.displayErrors ?
                        <div className={errorContainer}>
                          {this.state.errorMessage}
                          <img
                            className={errorSvgCss}
                            src={errorSVG}
                            alt="error Icon"
                          />
                        </div>
                        : null
                    }
                    <div>
                      <Button
                        color='primary'
                        size='lg'
                        className={`${buttonLogIn} ${registerButton}`}
                        block
                        disabled={(loading)}>
                        {
                          loading ?
                            <Loader color={'#2a2c6a'} />
                            :
                            'REGISTRAR'
                        }
                      </Button>
                    </div>
                  </div>
              }
            </form>
          </CardBody>
        </Card>
        {
          this.redirectUser()
        }
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn,
    isFetching: state.general.isFetching,
    errorCode: state.general.errorCode
  }
}
export default connect(mapStateToProps)(Register)