import React from 'react'
import { css } from 'emotion'
import downloadIcon from '../../../assets/baseline-save.svg'
import SearchInput from '../../general/searchInput'
import Workbook from 'react-excel-workbook'
import DatePickerIcon from '../../../assets/ic-date-picker.svg'
import moment from 'moment'
import Loader from '../Loader'

const btnTransparent = css`
background: transparent;
border: none;
outline: none !important;
cursor: pointer;
margin-right: 8px;
margin-top: 11px;
`
const historyHeaderContainer = css`
display: flex;
flex-wrap: wrap;
justify-content: center;
@media(max-width: 430px) {
    margin-top: 10px;
}
`
const datePickerIcon = css`
cursor: pointer
@media(min-width: 365px){
margin-left:32px;
margin-right: 10px;
}
@media(max-width: 365px){
padding: 12px;
}
@media (min-width: 970px) {
  margin-left:10px;
}
`
const orderByDateTitle = css`
font-family: Montserrat;
font-size: 12.8px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
text-align: right;
color: #95fad8;
white-space: nowrap;
margin: 0;
@media (max-width: 970px) {
display: none;
}
`
const orderByAllTitle = css`
font-family: Montserrat;
font-size: 12.8px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
text-align: right;
color: #95fad8;
white-space: nowrap;
margin: 0;
@media(min-width: 365px){
margin-left:10px;
margin-right: 10px;
  }
@media(max-width: 365px){
padding: 3px;
  }
  @media (min-width: 970px) {
    margin-left:10px;
  }
`
const orderByDateContainer = css`
@media (min-width: 970px) {
margin-left: 20px;
}
`
const orderByMainContainer = css`
  position: relative;
`
const downloadIconClassUsers = css`
    width: 30px !important;
    height: 30px !important;
    cursor: pointer;
    margin-top: 5px;
    margin-right: 10px;
  `
const orderByContainer = css`
  width: 100px;
  height: 100px;
  position: absolute;
  background-color: #2a2c6a;
  border-radius: 5px;
  box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
  z-index: 2;
  top: 41px;
  display: flex;
  flex-direction: column;
  justify-content: center 
`
const downloadButtonsContainer = css`
display: grid !important;
text-align: center;
`
const firstOrderByContainer = css`
    height: 50%;
    cursor: pointer;
    width: 100%;
    p{
      margin: auto;
      font-family: Montserrat;
      font-size: 12.8px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
    }
  `
export default ({
  title,
  data,
  historyIsDateFilter,
  openDateRangeModal,
  updateSearch,
  search,
  onKeyPress,
  isSearchingData,
  historialHandleSwitch,
  balance,
  closeDateFilter,
  isDownloadSelectOpen,
  openDownloadBySelect,
  allUserTransactions,
  allUserTransactionsLoading,
}) => {
  return (
    <div className={historyHeaderContainer}>
      <div>
        <div className={orderByMainContainer}>
          {
            !allUserTransactionsLoading
              ? <img
                src={downloadIcon}
                alt=""
                className={downloadIconClassUsers}
                onClick={openDownloadBySelect}
              />
              : <Loader color="#f6f7fa" />
          }
          {isDownloadSelectOpen && !allUserTransactionsLoading ?
            <div className={`${orderByContainer} ${downloadButtonsContainer}`}>
              <Workbook filename={`${title}-TABLA-ACTUAL-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.xlsx`}
                element={
                  <div
                    className={`${firstOrderByContainer}`}>
                    <p>Tabla actual</p>
                  </div>
                }>
                <Workbook.Sheet data={data} name={`${moment().utc(-264).format('YYYY-MM-DD h:mm A')}`}>
                  <Workbook.Column label="Modo" value={
                    row => row.mode === 'Send'
                      ? 'Pago enviado'
                      : row.mode === 'Receive'
                        ? 'Pago recibido'
                        : row.mode === 'Withdraw'
                          ? 'Retiraste saldo'
                          : row.mode === 'Deposit'
                            ? 'Añadiste saldo'
                            : ''
                  } />
                  <Workbook.Column label="ID" value="idTransaction" />
                  <Workbook.Column label="Fecha" value="date" />
                  <Workbook.Column label="Correo" value={
                    row => row.mode === 'Send'
                      ? row.receiverEmail
                      : row.mode === 'Receive' || row.mode === 'Withdraw' || row.mode === 'Deposit'
                        ? row.senderEmail
                        : ''
                  } />
                  <Workbook.Column label="Descripción" value="description" />
                  <Workbook.Column label="Monto" value="amount" />
                </Workbook.Sheet>
              </Workbook>
              <Workbook filename={`${title}-TODOS-LOS-REGISTROS-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.xlsx`}
                element={
                  <div
                    className={`${firstOrderByContainer}`}>
                    <p>Todos los registros</p>
                  </div>
                }>
                <Workbook.Sheet data={allUserTransactions} name={`${moment().utc(-264).format('YYYY-MM-DD h:mm A')}`}>
                  <Workbook.Column label="Modo" value={
                    row => row.mode === 'Send'
                      ? 'Pago enviado'
                      : row.mode === 'Receive'
                        ? 'Pago recibido'
                        : row.mode === 'Withdraw'
                          ? 'Retiraste saldo'
                          : row.mode === 'Deposit'
                            ? 'Añadiste saldo'
                            : ''
                  } />
                  <Workbook.Column label="ID" value="idTransaction" />
                  <Workbook.Column label="Fecha" value="date" />
                  <Workbook.Column label="Correo" value={
                    row => row.mode === 'Send'
                      ? row.receiverEmail
                      : row.mode === 'Receive' || row.mode === 'Withdraw' || row.mode === 'Deposit'
                        ? row.senderEmail
                        : ''
                  } />
                  <Workbook.Column label="Descripción" value="description" />
                  <Workbook.Column label="Monto" value="amount" />
                </Workbook.Sheet>
              </Workbook>
            </div>
            : null}
        </div>
        {
          !historyIsDateFilter
            ? <div className={orderByDateContainer}>
              <h6
                className={orderByDateTitle}
                onClick={openDateRangeModal}>
                Filtrar por fecha
            </h6>
              <img
                className={datePickerIcon}
                src={DatePickerIcon}
                onClick={openDateRangeModal}
                alt="date picker icon"
              />
            </div>
            : <div onClick={closeDateFilter} className={orderByDateContainer}>
              <h6 className={orderByAllTitle}>
                Todos
          </h6>
            </div>
        }
        <SearchInput
          onChange={updateSearch}
          value={search}
          onKeyPress={onKeyPress}
          placeholder="Buscar..."
          name="search"
          type="text"
          isSearchingData={isSearchingData}
        />
      </div>
      <div>
        <button className={btnTransparent} onClick={historialHandleSwitch({ balance: 'in' })}>
          <h2 className={balance === 'in' ? 'text-accept' : 'text-non-active'}>Ingresos</h2>
        </button>
        <button className={btnTransparent} onClick={historialHandleSwitch({ balance: 'out' })}>
          <h2 className={balance === 'out' ? 'text-accept' : 'text-non-active'}>Egresos</h2>
        </button>
        <button className={btnTransparent} onClick={historialHandleSwitch({ balance: 'all' })}>
          <h2 className={balance === 'all' ? 'text-accept' : 'text-non-active'}>Todas</h2>
        </button>
      </div>
    </div>
  )
}

