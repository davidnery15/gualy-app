import React from 'react'
import { css } from 'emotion'
import approvedIcon from './../../../assets/ic-info-turquoise.svg'
import rejectedIcon from './../../../assets/ic-info-red.svg'
import pendingIcon from './../../../assets/icon-edit-yellow.svg'

/* CSS */
  const statusContainer = css`
    display: flex;
    justify-content: left;
  `
  const approvedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #95fad8;
    display: flex;
  `
  const rejectedText = css`
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ff6061;
    display: flex;
  `
  const pendingText = css`
    display: flex;
    font-size: 11.8px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #f2ff4e;
    cursor:pointer;
  `

export default class Status extends React.Component {
  state = ({
    status: 'pending'
  })

  render() {
    const { info, onClick, className } = this.props
    return (
      <div className={className} onClick={onClick}>
        <div className={statusContainer}>
          {
            info === "Approved" ?
              <span className={approvedText}> Aprobado&nbsp;&nbsp;<img src={approvedIcon} alt="Aprobado" /></span> :
              info === "Rejected" ?
                <span className={rejectedText}>Rechazado&nbsp;&nbsp;<img src={rejectedIcon} alt="Rechazado" /></span> :
                <span className={pendingText}>Pendiente&nbsp;&nbsp;<img src={pendingIcon} alt="Pendiente" /></span>
          }
        </div>
      </div>
    );
  }
}