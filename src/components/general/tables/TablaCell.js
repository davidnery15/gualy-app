import React from 'react'
import CountUp from 'react-countup'
import { BS } from '../../../config/currencies.js'
import { css } from 'emotion'
import AcceptButton from '../../general/AcceptButton.js'
import RejectButton from '../../general/RejectButton.js'
import Status from './Status'
import getBankLogo from '../../../utils/getBank'
import defaultUserPicture from '../../../assets/accountCircle.svg'
import moment from 'moment'
const dateFormatDivided = (dateFormat) => dateFormat
  ? dateFormat.split(' ')
  : []
const PendingTextStyle = css`
font-size: 18px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 0.76;
letter-spacing: normal;
text-align: right;
overflow: visible !important;
color: #f2ff4e;
text-align: left;
white-space: nowrap;
margin-bottom: 4px;
@media(max-width: 800px){
  text-align: left
}
  `
// const PendingTextStyleAdmin = css`
// font-size: 18px;
// font-weight: bold;
// font-style: normal;
// font-stretch: normal;
// line-height: 0.76;
// letter-spacing: normal;
// text-align: right;
// overflow: visible !important;
// color: #f2ff4e;
// text-align: left;
// white-space: nowrap;
// margin-bottom: 4px;
// @media(max-width: 800px){
//   text-align: left
// }
// @media (max-width: 500px){
//       font-size: 13px;
// }
//   `
const buttonsContainer = css`
display: flex;
width: auto !important;
justify-content: center;
  flex-direction: column;
`
const requestButtonAccept = css`
margin-bottom: 10px;
margin-top: 10px;
`
const requestButtonReject = css`
margin-bottom: 10px;
margin-top: 10px;
@media(max-width: 1230px){
  margin-left: 0px;
}
`
const dateDay = css`
    font-size: 20px;
    margin: 0;
  `
const dateMonth = css`
    font-size: 14px;
    margin: 0;
  `
const cellContainer = css`
display: flex;
justify-content: center;
flex-direction: row;
  `

const imgContainerTransparent = css`
    width: 60.3px;
    height: 60.4px;
    border-radius: 3.3px;
    background-color: transparent;
    padding: 10px 3px;
    display: flex;
    flex-direction: column;
    padding-left: 7px;
    padding-top: 0;
    @media(max-width: 950px) {
      padding-top: 6px;
    }
  `

const imgContainer = css`
    width: 60.3px;
    height: 60.4px;
    border-radius: 3.3px;
    box-shadow: 0 7px 7px 0 rgba(0,0,0,0.15), 0 0 7px 0 rgba(0,0,0,0.12);
    border: solid 0.5px transparent;
    background-color: white;
    padding: 10px 3px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    @media(min-width: 501px) and (max-width:1200px){
      margin: auto;
    }
  `

const imgContainerHistory = css`
    width: 60.3px;
    height: 60.4px;
    border-radius: 3.3px;
    box-shadow: 0 7px 7px 0 rgba(0,0,0,0.15), 0 0 7px 0 rgba(0,0,0,0.12);
    border: solid 0.5px transparent;
    background-color: white;
    padding: 10px 3px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    @media(min-width: 501px){
      margin: auto;
    }
  `
const bankLogoCSS = css`
    width: 56.4px;
    height: 37.5px;
    object-fit: contain;
  `
const receiverUsernameCLass = css`
white-space: pre-wrap;
text-overflow: ellipsis;
display: flex;
flex-direction: column;
  `
const nameText = css`
  font-family: Montserrat;
  font-size: 16px;
  font-weight: 900;
  font-style: normal;
  font-stretch: normal;
  line-height: 0.98;
  letter-spacing: normal;
  color: #f6f7fa; 
  @media(max-width: 500px){
    font-size: 13px;
  }
  `

export const cellRenderer = ({
  type,
  sendLoading,
  rejectLoading,
  openSendMoneyModal,
  mobileMediaWidth,
  //transactions
  onCloseShowProcessWithdrawModal,
  onHistoryTransactionClick
}) => ({
  cellData, //any 
  columnData, //any 
  dataKey, //string 
  rowData, //any 
  rowIndex, //number 
}) => {
    //pending requests

    const datetd = css`
      text-align: center;
      vertical-align: -webkit-baseline-middle;
      vertical-align: top;
      span {
        font-weight: 300;
        font-style: normal;
        font-stretch: normal;
        letter-spacing: normal;
      }
      @media(max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 7px;
        }
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 34%;
        }
    `
    const secondDatetd = css`
      text-align: center;
      vertical-align: -webkit-baseline-middle;
      vertical-align: top;
      display: flex;
      flex-direction: column;
      text-align: center;
      span {
        font-weight: 300;
        font-style: normal;
        font-stretch: normal;
        letter-spacing: normal;
      }
      @media(max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 30px;
      text-align: right;
        }
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 42%;
        }
    `
    const processedDatetd = css`
      text-align: center;
      vertical-align: -webkit-baseline-middle;
      vertical-align: top;
      span {
        font-weight: 300;
        font-style: normal;
        font-stretch: normal;
        letter-spacing: normal;
      }
      @media(max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 58px;
        }
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 53%;
        }
    `
    const secondProcessedDatetd = css`
      text-align: center;
      vertical-align: -webkit-baseline-middle;
      vertical-align: top;
      display: flex;
      flex-direction: column;
      text-align: center;
      span {
        font-weight: 300;
        font-style: normal;
        font-stretch: normal;
        letter-spacing: normal;
      }
      @media(max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 80px;
      text-align: right;
        }
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
      width: 53px;
      position: absolute;
      right: 20px;
      top: 61%;
        }
    `
    const subTitleDate = css`
      text-align: center;
      vertical-align: -webkit-baseline-middle;
      vertical-align: top;
      display: none;
      span {
        font-weight: 300;
        font-style: normal;
        font-stretch: normal;
        letter-spacing: normal;
      }
      @media(max-width: ${mobileMediaWidth}){
        width: 46px;
        position: absolute;
        right: 42px;
        top: 6px;
        display: initial;
        font-weight: 400 !important;
        }
      @media(max-width: 500px){
          font-size: 10px;
          right: 39px;
          top: 10px;
        }
      @media(max-width: 360px){
        right: 31px;
        top: 0px;
        }
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
      width: 46px;
      position: absolute;
      right: 43px;
      top: 4px;
      display: initial;
      font-weight: 400 !important;
        }
    `
    const imgContainerDestinyBank = css`
      width: 60.3px;
      height: 60.4px;
      border-radius: 3.3px;
      box-shadow: 0 7px 7px 0 rgba(0,0,0,0.15), 0 0 7px 0 rgba(0,0,0,0.12);
      border: solid 0.5px transparent;
      background-color: white;
      padding: 10px 3px;
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      @media(min-width: ${mobileMediaWidth}){
        margin: auto;
      }
    `
    //   const mainSpan = css`
    // display: flex;
    // justify-content: center;
    // flex-direction: column;
    // width: 45px;
    // // @media(max-width: ${mobileMediaWidth}){
    // //   width: auto;
    // //   flex-direction: row;
    // //   }
    // `
    const userNameContainerNameImage = css`
  display: flex;
  align-items: center;
  width: 100%;
  @media(min-width: 501px) and (max-width: 700px){
    flex-direction: column !important;
  }
  @media(max-width: 500px){
    position: absolute;
    top: 2px;
    left: 10px;
  }
  @media(max-width: 400px){
    width: 200px;
  }
    `
    const userNameContainerNameImage2 = css`
  display: flex;
  align-items: center;
  width: 100%;
  @media(min-width: 501px) and (max-width: 700px){
    flex-direction: column !important;
  }
  @media(max-width: 500px){
    position: absolute;
    top: 2px;
    left: 10px;
  }
  @media(max-width: 400px){
    width: 200px;
  }
    `
    const userNameContainerNameImageUser = css`
  display: flex;
  align-items: center;
  width: 100%;
  @media(min-width: ${mobileMediaWidth}){
    flex-direction: column;
  }
  @media(max-width: 500px){
    position: absolute;
    top: 2px;
    left: 10px;
  }
  @media(max-width: 400px){
    width: 200px;
  }
  @media(max-width: ${mobileMediaWidth}){
    margin-bottom: 15px;
  }
    `
    const userNameContainerNameImageOrigin = css`
  display: flex;
  align-items: center;
  width: 100%;
  @media(min-width: ${mobileMediaWidth}){
    flex-direction: column;
  }
  @media(max-width: 500px){
    position: absolute;
    top: 2px;
    left: 10px;
  }
  @media(max-width: 400px){
    width: 200px;
  }
  @media(max-width: ${mobileMediaWidth}){
    margin-bottom: 15px;
  }
    `
    const userNameContainerNameImageDestiny = css`
  display: flex;
  align-items: center;
  width: 100%;
  @media(min-width: ${mobileMediaWidth}){
    flex-direction: column;
  }
  @media(max-width: 500px){
    position: absolute;
    top: 81px;
    left: 10px;
  }
  @media(max-width: 400px){
    width: 200px;
  }
  @media(max-width: ${mobileMediaWidth}){
    margin-bottom: 15px;
  }
    `
    const depositWithdraTitle = css`
    margin-top: 6px;
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #f6f7fa;
      @media(max-width: 500px){
        position: absolute;
        top: 66px;
        left: 10px;
        }
    `
    const depositWithdraTitleHistory = css`
      font-size: 18px;
      font-weight: bold;
      margin-top: 6px; 
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #f6f7fa;
      @media(max-width: 500px){
        position: absolute;
        top: 80px;
        left: 10px;
        }
      @media(min-width: 500px){
        margin-top: 12px;
        }
    `
    const positiveAmount = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #95fad8;
      @media(max-width: 500px){
        position: absolute;
        top: 150px;
        left: 10px;
        }
    `
    const RMpositiveAmount = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #95fad8;
      @media(max-width: 500px){
        position: absolute;
        top: 137px;
        left: 10px;
        }
    `
    const positiveAmountAdmin = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #95fad8;
      @media(max-width: 500px){
        position: absolute;
        top: 206px;
        left: 68px;
        }
    `
    const positiveAmountAdminStatus = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #95fad8;
      @media(max-width: 500px){
        position: absolute;
        top: 181px;
        left: 68px;
        }
    `
    const positiveAmountAdminStatusWithdraw = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #95fad8;
      @media(max-width: 500px){
        position: absolute;
        top: 180px;
        left: 68px;
        }
    `
    const depositWithdrawStatusButtonWithdraw = css`
      @media(max-width: 500px){
        position: absolute;
        top: 150px;
        left: 68px;
        }
    `
    const depositWithdrawStatusButtonDeposit = css`
      @media(max-width: 500px){
        position: absolute;
        top: 174px;
        left: 68px;
        }
    `
    const pendingAmount = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #f2ff4e;
      @media(max-width: 500px){
      position: absolute;
      top: 105px;
      left: 10px;
      }
    `
    const pendingAmountPendingWithdrawReceive = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #f2ff4e;
      @media(max-width: 500px){
      position: absolute;
      top: 66px;
      left: 10px;
      }
    `
    const pendingAmountRejectRequest = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #f2ff4e;
      @media(max-width: 500px){
      position: absolute;
      top: 74px;
      left: 10px;
      }
      // @media(max-width: 800px){
      //   margin-right: 33px;
      // }
    `
    const pendingAmountRejected = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #f2ff4e;
      @media(max-width: 500px){
      position: absolute;
      top: 110px;
      left: 10px;
      }
    `
    const negativeAmount = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #ff6061;
      @media(max-width: 500px){
        position: absolute;
        top: 150px;
        left: 10px;
        }
    `
    const RMnegativeAmount = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #ff6061;
      @media(max-width: 500px){
        position: absolute;
        top: 137px;
        left: 10px;
        }
    `
    const negativeAmountAdmin = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #ff6061;
      @media(max-width: 500px){
        position: absolute;
        top: 206px;
        left: 68px;
        }
    `
    const negativeAmountAdminStatus = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #ff6061;
      @media(max-width: 500px){
        position: absolute;
        top: 181px;
        left: 68px;
        }
    `
    const negativeAmountAdminStatusWithdraw = css`
      font-size: 18px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.76;
      letter-spacing: normal;
      text-align: right;
      overflow: visible !important;
      color: #ff6061;
      @media(max-width: 500px){
        position: absolute;
        top: 180px;
        left: 68px;
        }
    `
    const depositWIthdrawsNameAndImage = css`
  @media(max-width: ${mobileMediaWidth}){
    position: absolute;
    top: 0;
    display: flex;
    left: 10px;
    width: 200px;
  }
  @media(min-width: 1200px){
    display: flex
  }
    `
    const depositWithdrawsNameAndImageDestinyBank = css`
      display: flex;
      @media(min-width: ${mobileMediaWidth}){
        flex-direction: column
      }
  @media(max-width: 500px){
    position: absolute;
    top: 73px;
    display: flex;
    left: 68px;
    width: 200px;
  }
  @media(max-width: ${mobileMediaWidth}){
    margin-bottom: 15px;
  }
    `
    const depositWIthdrawsNameAndImageHistory = css`
  @media(max-width: 500px){
    position: absolute;
    top: 0;
    display: flex;
    left: 10px;
    width: 200px;
  }
  @media(min-width: 500px) and (max-width: 1200px){
    display: flex;
    flex-direction: column
  }
    `
    const userBankAccountNumber = css`
  text-overflow: ellipsis;
  word-break: break-all;
  @media(max-width: ${mobileMediaWidth}){
    margin-top: 20px;
    margin-left: 6px;
    font-size: 80% !important; 
    width: 81px;
  }
  @media(min-width: 1200px){
    margin-top: 21px;
    margin-left: 8px;
  }
    `
    const pendingRequestEmailDesktop = css`
    font-family: Montserrat;
    font-size: 12px;
    font-weight: normal !important;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.98;
    letter-spacing: normal;
    color: #f6f7fa;
    word-break: break-all;
    margin-top: 8px;
    @media(max-width: 700px){
      margin-top: 6px;
      margin-bottom: 6px;
    }
    `
    const pendingRequestEmailDesktopUser = css`
    font-family: Montserrat;
    font-size: 12px;
    font-weight: normal !important;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.98;
    letter-spacing: normal;
    color: #f6f7fa;
    word-break: break-all;
    margin-top: 8px;
    @media(max-width: 700px){
      margin-top: 6px;
      margin-bottom: 6px;
    }
    // @media(max-width: 500px){
    //   word-break: normal;
    // }
    `
    const emailHistory = css`
    font-family: Montserrat;
    font-size: 12px;
    font-weight: normal !important;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.98;
    letter-spacing: normal;
    color: #f6f7fa;
    word-break: break-all;
    margin-top: 8px;
    @media(max-width: 500px){
      position: absolute;
      top: 56px;
      left: 0px;
    }
    @media(max-width: 700px){
      margin-top: 6px;
      margin-bottom: 6px;
    }
    `
    const idTransactionClassDesktop = css`
    word-break: break-all;
    font-size: 11px;
    @media(max-width: 500px){
      position: absolute;
      top: 117px;
      font-size: 13px;
      left: 10px;
    }
    @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
      position: absolute;
      top: 195px !important;
      font-size: 13px;
    }
    `
    const RMidTransactionClassDesktop = css`
    word-break: break-all;
    font-size: 11px;
    @media(max-width: ${mobileMediaWidth}){
      display: none;
    }
    `
    const historydescription = css`
    word-break: break-all;
    @media(max-width: 500px){
      position: absolute;
      top: 89px;
      font-size: 13px;
      left: 10px;
    }
    `
    const feeAmount = css`
    word-break: break-all;
    @media(max-width: 500px){
      position: absolute;
      top: 110px;
      font-size: 13px;
      left: 10px;
    }
    `
    const RMamount = css`
    word-break: break-all;
    @media(max-width: 500px){
      position: absolute;
      top: 90px;
      font-size: 13px;
      left: 10px;
    }
    `
    const RMhistorydescription = css`
    word-break: break-all;
    font-size: 14px;
    @media(max-width: ${mobileMediaWidth}){
      display: none;
    }
    `
    const idClass = css`
      font-family: Montserrat;
      font-weight: 900;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.98;
      letter-spacing: normal;
      color: #f6f7fa;
      margin-right: 6px;
      display: none;
      
      @media(max-width: ${mobileMediaWidth}){
        display: initial
      }
        `
    const descriptionClass = css`
      @media(max-width: 500px){
      position: absolute;
      top: 140px;
      left: 68px;
      width: 170px;
      overflow: auto;
      height: 36px;
      }
    `
    const bankReference = css`
      @media(max-width: 500px){
        position: absolute;
        top: 144px;
        left: 68px;
      }
    `
    const cursorPointer = css`
     cursor: pointer
    `
    const recentsMovementsUser = css`
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
        margin-top: 70px;
      }
    `
    const recentsMovementsUserWithdrawsAndDeposits = css`
      @media(min-width: 500px) and (max-width: ${mobileMediaWidth}){
        margin-top: 70px;
      }
    `
    const extraStyleLarge = css`
    font-size: 13px;
    height: 21px;
    @media(max-width: 500px){
      height: 13px;
    }
  `
    const extraStyleSmall = css`
  font-size: 10px
  `
    const fee = rowData.fee ? rowData.fee : 0
    const originalAmount = rowData.originalAmount
      ? rowData.originalAmount
      : rowData.mode === 'Receive' || rowData.mode === 'Deposit'
        ? rowData.amount + fee
        : rowData.amount - fee
        const isClickableCell = type === "history" ||
        type === "recentsMovements" ||
        type === "withdraws" ||
        type === "transactions" ||
        type === "deposits"
        const processedTimestamp = rowData.processedTimestamp || rowData.timestamp
    return <div
      className={`
        ${cellContainer} 
        ${type === "history" ? cursorPointer : ""}
        `}
      onClick={isClickableCell && rowData.status !== "Pending"
        ? onHistoryTransactionClick({ transaction: rowData })
        : null}
    >
      {/*---------------- CLIENT / COMMERCES TABLE CELLS ----------------*/}
      {
        /* DATE */
        dataKey === 'date'
          ? <div className={datetd}>
            <span style={{ marginRight: "4px" }} className={dateMonth}>
              {dateFormatDivided(rowData.dateFormat)[0]}
            </span>
            <span className={dateDay}>
              {dateFormatDivided(rowData.dateFormat)[1]}
            </span>
          </div>
          : null
      }
      {
        /* ADMIN DATE */
        dataKey === 'adminDate'
          ? <div>
            <div className={datetd}>
              {
                type === "withdraws" || type === "deposits"
                  ? <span className={subTitleDate}>
                    F.S
            </span>
                  : null
              }
              <span style={{ marginRight: "4px" }} className={dateMonth}>
                {dateFormatDivided(rowData.dateFormat)[0]}
              </span>
              <span className={dateDay}>
                {dateFormatDivided(rowData.dateFormat)[1]}
              </span>
            </div>
            <div className={secondDatetd}>
              <span
                className={`${dateDay} ${extraStyleLarge}`}>
                {moment(rowData.timestamp).utc(-264).format('YYYY')}
              </span>
              <span
                className={`${dateDay} ${extraStyleSmall}`}>
                {moment(rowData.timestamp).utc(-264).format('h:mm a')}
              </span>
            </div>
          </div>
          : null
      }
      {
        /* PROCESSED DATE */
        dataKey === 'processedDate'
          ? <div>
            <div className={processedDatetd}>
              {
                type === "withdraws" || type === "deposits"
                  ? <span className={subTitleDate}>
                    F.P
            </span>
                  : null
              }
              <span style={{ marginRight: "4px" }} className={dateMonth}>
                {dateFormatDivided(rowData.processedDateFormat || rowData.dateFormat)[0]}
              </span>
              <span className={dateDay}>
                {dateFormatDivided(rowData.processedDateFormat || rowData.dateFormat)[1]}
              </span>
            </div>
            <div className={secondProcessedDatetd}>
              <span
                className={`${dateDay} ${extraStyleLarge}`}>
                {moment(processedTimestamp).utc(-264).format('YYYY')}
              </span>
              <span
                className={`${dateDay} ${extraStyleSmall}`}>
                {moment(processedTimestamp).utc(-264).format('h:mm a')}
              </span>
            </div>
          </div>
          : null
      }
      {
        /* USER IMAGE,NAME,EMAIL,ACCOUNT */
        dataKey === 'userImageNameAccount'
          ? rowData.mode === "Withdraw" || rowData.mode === 'Deposit'
            ? <div
              className={`
              ${type === 'history' || type === 'recentsMovements'
                  ? depositWIthdrawsNameAndImageHistory
                  : depositWIthdrawsNameAndImage}
              ${type === 'recentsMovements' ? recentsMovementsUserWithdrawsAndDeposits : ""}
              `}>
              <div
                className={type === 'history' || type === 'recentsMovements'
                  ? imgContainerHistory
                  : imgContainer}
              >
                <img
                  className={bankLogoCSS}
                  src={getBankLogo(rowData.gualyBankAccount || rowData.userBankAccount)}
                  alt='Imagen de Perfil'
                />
              </div>
              <span className={`${nameText} ${userBankAccountNumber}`}>
                {rowData.gualyBankAccount || rowData.userBankAccount}
              </span>
            </div>
            : <div className={`
              ${rowData.mode === 'Send'
                ? userNameContainerNameImage2
                : userNameContainerNameImage} 
              ${type === 'recentsMovements' ? recentsMovementsUser : ""}
              `}>
              <div className={imgContainerTransparent}>
                <img
                  className='oval-image'
                  src={rowData.mode === "Send"
                    ? rowData.receiverProfilePicture
                    : rowData.senderProfilePicture}
                  onError={(e) => { e.target.onerror = null; e.target.src = defaultUserPicture }}
                  alt=''
                />
              </div>
              <span className={`${nameText} ${receiverUsernameCLass}`}>
                {
                  type === 'pendingRequest' || type === 'rejectedRequests'
                    ? <div css={`text-align: left;display: flex; flex-direction: column`}>
                      <span className={PendingTextStyle}>
                        {rowData.mode === 'Send' ? "Recibida de" : "Enviada a"}
                      </span>
                      <span>
                        {rowData.mode === 'Send'
                          ? rowData.receiverUsername
                          : rowData.senderUsername}
                      </span>
                      <span className={pendingRequestEmailDesktop}>
                        {rowData.mode === 'Send' ? rowData.receiverEmail : rowData.senderEmail}
                      </span>
                    </div>
                    : <div
                      css={`text-align: left;display: flex; flex-direction: column`}
                    >
                      <span>
                        {rowData.mode === 'Send' ?
                          rowData.receiverUsername
                          : rowData.senderUsername}
                      </span>
                      {
                        type === 'history' || type === 'recentsMovements'
                          ? <span className={emailHistory}>
                            {rowData.mode === 'Send'
                              ? rowData.receiverEmail
                              : rowData.senderEmail}
                          </span>
                          : null
                      }
                    </div>
                }
              </span>
            </div>
          : null
      }
      {
        /* 
        STATUS AND  AMOUNT FROM 
        PENDING REQUESTS, 
        PENDING WITHDRAWS, 
        REJECT PENDING REQUESTS 
        */
        dataKey === 'thirdColumn'
          ? type === 'pendingRequest' && rowData.mode === 'Send'
            ? <span className={pendingAmount}
              css={`text-align: left !important;`}
            >
              <CountUp
                start={rowData.amount}
                end={rowData.amount}
                duration={0}
                separator="."
                decimals={2}
                decimal=","
                prefix={`${BS}`}
              />
            </span>
            : rowData.status === 'Pending'
              ? <span css={`margin-top: 6px;`}
                className={pendingAmountPendingWithdrawReceive}
              >
                Pendiente
            </span>
              : <span className={pendingAmountRejectRequest}>
                Rechazado
            </span>
          : null
      }
      {
        /* RECENT MOVEMENTS THIRD COLUMN */
        dataKey === 'recentMovementsThirdColumn' && type === 'recentsMovements'
          ? rowData.mode === 'Deposit'
            ? <span
              className={type === 'history'
                ? depositWithdraTitleHistory
                : depositWithdraTitle}
            >
              Deposito
            </span>
            : rowData.mode === 'Withdraw'
              ? <span
                css={`margin-top: 6px;`}
                className={type === 'history'
                  ? depositWithdraTitleHistory
                  : depositWithdraTitle}
              >
                Retirado
              </span>
              : <div
                className={type === 'recentsMovements'
                  ? RMhistorydescription
                  : historydescription}
              >
                {rowData.description || "Sin descripción."}
              </div>
          : null
      }
      {
        /* HISTORY DESCRIPTION */
        dataKey === 'historyDescription'
          ? rowData.mode === 'Deposit'
            ? <span
              className={type === 'history'
                ? depositWithdraTitleHistory
                : depositWithdraTitle}
            >
              Deposito
            </span>
            : rowData.mode === 'Withdraw'
              ? <span
                css={`margin-top: 6px;`}
                className={type === 'history'
                  ? depositWithdraTitleHistory
                  : depositWithdraTitle}>
                Retirado
              </span>
              : <div className={historydescription}>
                {rowData.description || "Sin descripción."}
              </div>
          : null
      }
      {
        /* PENDING REQUEST BUTONS */
        dataKey === 'pendingRequestButtons' ?
          <div>
            {
              type === 'pendingRequest' && rowData.mode === "Send"
                ? <div className={`${buttonsContainer}`}>
                  <AcceptButton
                    className={requestButtonAccept}
                    loading={sendLoading}
                    content="ENVIAR PAGO"
                    onClick={openSendMoneyModal({ info: rowData, isReject: false })}
                    width="200px"
                    height="30px"
                  />
                  <RejectButton
                    className={requestButtonReject}
                    onClick={openSendMoneyModal({ info: rowData, isReject: true })}
                    width="200px"
                    content="RECHAZAR"
                    height="30px"
                    loading={rejectLoading}
                  />
                </div>
                : <span className={pendingAmountRejected}
                  css={`text-align: left !important;`}
                >
                  <CountUp
                    start={rowData.amount}
                    end={rowData.amount}
                    duration={0}
                    separator="."
                    decimals={2}
                    decimal=","
                    prefix={`${BS}`}
                  />
                </span>
            }
          </div>
          : null
      }
      {
        /* STATUS AMOUNT */
        dataKey === 'statusAmount' ?
          <span className={pendingAmount}
            css={`text-align: left !important;`}
          >
            <CountUp
              start={rowData.amount}
              end={rowData.amount}
              duration={0}
              separator="."
              decimals={2}
              decimal=","
              prefix={`${BS}`}
            />
          </span>
          : null
      }
      {
        /* RECENT MOVEMENTS AMOUNT */
        dataKey === 'recentMovementsAmounts'
          ? <div>
            <span
              className={RMamount}
              css={`text-align: left !important;`}
            >
              <span className={idClass}>Recibido: </span>
              <CountUp
                start={originalAmount}
                end={originalAmount}
                duration={0}
                separator="."
                decimals={2}
                decimal=","
                prefix={BS}
              />
            </span>
          </div>
          : null
      }
      {
        /* RECENT FEE AMOUNT */
        dataKey === 'recentMovementsFeeAmounts'
          ? <div>
            <span
              className={feeAmount}
              css={`text-align: left !important;`}
            >
              <span className={idClass}>Tarifa:</span>
              <CountUp
                start={fee}
                end={fee}
                duration={0}
                separator="."
                decimals={2}
                decimal=","
                prefix={BS}
              />
            </span>
          </div>
          : null
      }
      {
        /* RECENT MOVEMENTS RECEIVE AMOUNT */
        dataKey === 'recentMovementsReceiveAmounts'
          ? <div>
            <span
              className={
                rowData.mode === 'Receive' || rowData.mode === 'Deposit'
                  ? RMpositiveAmount
                  : RMnegativeAmount}
              css={`text-align: left !important;`}
            >
              <CountUp
                start={rowData.amount}
                end={rowData.amount}
                duration={0}
                separator="."
                decimals={2}
                decimal=","
                prefix={
                  rowData.mode === 'Withdraw' ||
                    rowData.mode === 'Send'
                    ? `-${BS}`
                    : `+${BS}` //Receive and Deposit
                }
              />
            </span>
          </div>
          : null
      }
      {
        /* HISTORY AMOUNT */
        dataKey === 'historyAmounts'
          ? <div>
            <span className={
              rowData.mode === 'Receive' || rowData.mode === 'Deposit'
                ? positiveAmount
                : negativeAmount}
              css={`text-align: left !important;`}
            >
              <CountUp
                start={rowData.amount}
                end={rowData.amount}
                duration={0}
                separator="."
                decimals={2}
                decimal=","
                prefix={
                  rowData.mode === 'Withdraw' ||
                    rowData.mode === 'Send'
                    ? `-${BS}`
                    : `+${BS}` //Receive and Deposit
                }
              />
            </span>
          </div>
          : null
      }
      {
        /* TRANSACTION ID */
        dataKey === 'transactionId' ?
          <div className={idTransactionClassDesktop}>
            <span className={idClass}>ID</span>
            {`${rowData.idTransaction}`}
          </div>
          : null
      }
      {
        /* RECENT MOVEMENTS TRANSACTION ID */
        dataKey === 'recentMovementsTransactionId' ?
          <div className={RMidTransactionClassDesktop}>
            <span className={idClass}>ID:</span>
            {`${rowData.idTransaction}`}
          </div>
          : null
      }
      {/*---------------- ADMIN TABLES CELLS ----------------*/}
      {
        /* ORIGIN ACCOUNT */
        dataKey === 'originAccount' ?
          <div className={userNameContainerNameImageOrigin}>
            <div className={imgContainerTransparent}>
              <img
                className='oval-image'
                src={rowData.senderProfilePicture}
                onError={(e) => { e.target.onerror = null; e.target.src = defaultUserPicture }}
                alt=''
              />
            </div>
            <span className={`${nameText} ${receiverUsernameCLass}`}>
              <div css={`text-align: left;display: flex; flex-direction: column`}>
                <span>{rowData.senderUsername}</span>
                <span className={pendingRequestEmailDesktopUser}>
                  {rowData.senderEmail}
                </span>
              </div>
            </span>
          </div>
          : null
      }
      {
        /* DESTINY ACCOUNT */
        dataKey === 'destinyAccount' ?
          <div className={userNameContainerNameImageDestiny}>
            <div className={imgContainerTransparent}>
              <img
                className='oval-image'
                src={rowData.receiverProfilePicture}
                onError={(e) => { e.target.onerror = null; e.target.src = defaultUserPicture }}
                alt=''
              />
            </div>
            <span className={`${nameText} ${receiverUsernameCLass}`}>
              <div css={`text-align: left;display: flex; flex-direction: column`}>
                <span>{rowData.receiverUsername}</span>
                <span className={pendingRequestEmailDesktopUser}>
                  {rowData.receiverEmail}
                </span>
              </div>
            </span>
          </div>
          : null
      }
      {
        /* CONCEPT */
        dataKey === 'concept'
          ? <div className={descriptionClass}>
            {rowData.description || 'Sin descripción'}
          </div>
          : null
      }
      {
        /* TRANSACTIONS STATUS */
        dataKey === 'transactionStatus'
          ? type === 'transactions'
            ? rowData.status === 'Rejected'
              ? <p className={negativeAmountAdminStatus}>
                Rechazado
          </p>
              : <p className={positiveAmountAdminStatus}>
                Aprobado
              </p>
            : rowData.status === 'Pending'
              ? <Status
                className={type === 'deposits'
                  ? depositWithdrawStatusButtonDeposit
                  : depositWithdrawStatusButtonWithdraw}
                onClick={onCloseShowProcessWithdrawModal({ info: rowData })}
                info={rowData.status}
              />
              : <Status
                className={type === 'deposits'
                  ? depositWithdrawStatusButtonDeposit
                  : depositWithdrawStatusButtonWithdraw}
                info={rowData.status}
              />
          : null
      }
      {
        /* DESTINY BANK */
        dataKey === 'destinyBank'
          ? <div className={depositWithdrawsNameAndImageDestinyBank}>
            <div className={imgContainerDestinyBank}>
              <img
                className={bankLogoCSS}
                src={getBankLogo(rowData.gualyBankAccount || rowData.userBankAccount)}
                alt='Imagen de Perfil'
              />
            </div>
            <span
              className={`${nameText} ${userBankAccountNumber}`}>
              {rowData.gualyBankAccount || rowData.userBankAccount}
            </span>
          </div>
          : null
      }
      {
        /* BANK REFERENCE */
        dataKey === 'bankReference' ?
          <div className={bankReference}>{rowData.bankReference}</div>
          : null
      }
      {
        /* TRANSACTIONS AMOUNT */
        dataKey === 'transactionsAmount'
          ? <span className={

            rowData.status === 'Rejected'
              ? type === 'withdraws'
                ? negativeAmountAdminStatusWithdraw
                : negativeAmountAdmin
              : type === 'withdraws'
                ? positiveAmountAdminStatusWithdraw
                : positiveAmountAdmin}>
            <CountUp
              start={type === 'withdraws'
                ? rowData.withdrawAmount || rowData.amount
                : rowData.amount}
              end={type === 'withdraws'
                ? rowData.withdrawAmount || rowData.amount
                : rowData.amount}
              duration={0}
              separator="."
              decimals={2}
              decimal=","
              prefix={`${BS}`}
            />
          </span>
          : null
      }
      {
        /* USER */
        dataKey === 'user'
          ? <div className={userNameContainerNameImageUser}>
            <div className={imgContainerTransparent}>
              <img
                className='oval-image'
                src={rowData.senderProfilePicture}
                onError={(e) => { e.target.onerror = null; e.target.src = defaultUserPicture }}
                alt=''
              />
            </div>
            <span className={`${nameText} ${receiverUsernameCLass}`}>
              <div css={`text-align: left;display: flex; flex-direction: column`}>
                <span>{rowData.senderUsername}</span>
                <span className={pendingRequestEmailDesktopUser}>
                  {rowData.senderEmail}
                </span>
              </div>
            </span>
          </div>
          : null
      }
    </div>
  }
