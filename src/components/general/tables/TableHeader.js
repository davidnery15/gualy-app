import React from 'react'
import { css } from 'emotion'
import Input from '../Input'
import icFilter from './../../../assets/ic-filter.svg'
import icSort from './../../../assets/ic-sort.svg'
import icAddStoreIcon from './../../../assets/ic-add-store.svg'
import Workbook from 'react-excel-workbook'
import downloadIcon from './../../../assets/baseline-save.svg'
import moment from 'moment'
import rechargeIcon from './../../../assets/baseline-autorenew.svg'
import Loader from '../Loader'
/* CSS */
const tableHeader = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0px;
    padding: 20px 25px;
    @media (max-width: 767px) {
      padding: 0  22px 25px 22px;
      width:100%;
      flex-direction: column;
    }
  `
const titleContainer = css`
    width: 40%;
    @media (max-width: 767px) {
      display: none;
    }
  `
const mobileTitleContainer = css`
    display: none;
    @media (max-width: 767px) {
      display: flex;
    }
  `
const tableHeaderTitle = css`
    font-family: Montserrat;
    font-size: 13px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 1.1px;
    margin: 4px 0;
    padding-top: 7px;
    width: 100%;
    @media (max-width: 767px) {
      padding-top: 15px;
    }
  `
const inputContainer = css`
    width: 60%;
    display: flex;
    justify-content: flex-end;
    @media (max-width: 767px) {
      width: 100%;
      justify-content: center;
    }
  `
const iconContainer = css`
    display: flex;
    justify-content: flex-end;
    width: 100%;
    div{
      display: flex;
      align-items:center;
      h6{
        margin: 0 32px 0 0;
        font-family: Montserrat;
        font-size: 12.8px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: right;
        color: #95fad8;
        white-space: nowrap;
        @media (max-width: 970px) {
          display: none;
        }
      }
      img{
        width: 18px;
        height: 18px;
        object-fit: contain;
        margin: 0 32px 0 0;
      }
    }
    @media(max-width: 365px){
      img{
         margin: 8px !important;
     }
    }
    @media (max-width: 970px) {
      width: 100%;
    }
    @media (max-width: 767px) {
      justify-content: flex-start;
    }
  `
const orderByContainer = css`
  width: 100px;
  height: 100px;
  position: absolute;
  background-color: #2a2c6a;
  border-radius: 5px;
  box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
  z-index: 2;
  top: 41px;
  display: flex;
  flex-direction: column;
  justify-content: center 
`
const orderByMainContainer = css`
  position: relative;
`
const firstOrderByContainer = css`
    height: 50%;
    cursor: pointer;
    width: 100%;
    p{
      margin: auto;
      font-family: Montserrat;
      font-size: 12.8px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
    }
  `
const secondOrderByContainer = css`
    height: 50%;
    cursor: pointer;
    width: 100%;
    p{
      margin: auto;
      font-family: Montserrat;
      font-size: 12.8px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
    }
  `
const cursor = css`
    cursor: pointer
  `
const darkBackground = css`
  border: 2px #95fad8 solid;
  border-radius: 12px;
  `
const downloadIconClass = css`
    width: 30px;
    height: 30px;
    cursor: pointer;
    margin-top: 5px;
    margin-right: 20px;
  `
const downloadIconClassUsers = css`
    width: 30px !important;
    height: 30px !important;
    cursor: pointer;
    margin-top: 5px;
    margin-right: 20px;
  `
const downloadButtonsContainer = css`
display: grid !important;
`
const TableHeader = ({
  title,
  updateSearch,
  search,
  placeholder,
  icon,
  filter,
  store,
  isOrderBy,
  orderByOlders,
  orderByNewers,
  toggleOrderBy,
  reverseNewers,
  isFilterBy,
  filterByStatus,
  filterText,
  toggleFilterBy,
  isDownload = false,
  downloadTitle,
  downloadColumns,
  downloadColumnsAll,
  isDownloadSelectOpen,
  allUsersLoading,
  showOrderByButton = true,
  showFilterByButton = true,
  onKeyPress,
  recharge = false,
  onRechargeClick,

  dateRangePickerIsOpen,
  dateRangeFilterHandleSelect,
  ranges,
  searchDateRange,
  openDateRangeModal,
  isBackButtonShow,
  goBackButtonClick,
  isSearchingData,
  idAdmin,
  openDownloadBySelect,
}) => {
  const status = filterText === "all"
    ? "(Todos)"
    : filterText === "Approved"
      ? "(Aprobados)"
      : filterText === "Pending"
        ? "(Pendientes)"
        : filterText === "Rejected"
          ? "(Rechazados)"
          : "(Todos)"
  const isOrderName = reverseNewers ? "(Mas nuevos)" : "(Mas viejos)"
  return (
    <div className={tableHeader}>
      <div className={titleContainer}>
        <h3 className={tableHeaderTitle}>{title}</h3>
      </div>
      <div className={mobileTitleContainer}>
        <h3 className={tableHeaderTitle}>{title}</h3>
      </div>
      <div className={`${inputContainer}`}>
        {filter ?
          (
            <div className={iconContainer}>
              {recharge
                ? <div>
                  <img
                    src={rechargeIcon}
                    onClick={onRechargeClick}
                    css={`cursor: pointer; height: 30px`}
                    alt="recharge Icon"
                  />
                </div>
                : null}
              {
                store ?
                  <div>
                    <h6>Agregar&nbsp;comercio</h6>
                    <img
                      css={`@media(max-width: 970px){margin: 0px 32px 0 0 !important;}`}
                      src={icAddStoreIcon}
                      alt="Add Icon"
                    />
                  </div> : null
              }
              {
                showOrderByButton
                  ? <div className={orderByMainContainer}>
                    <h6 className={cursor} onClick={toggleOrderBy}>{`Ordenar por ${isOrderName}`}</h6>
                    <img className={cursor} onClick={toggleOrderBy} src={icSort} alt="Filter Icon" />
                    {
                      isOrderBy ?
                        <div className={orderByContainer}>
                          <div onClick={orderByNewers} className={`${reverseNewers ? darkBackground : ''} ${firstOrderByContainer}`}>
                            <p>Mas nuevos</p>
                          </div>
                          <div onClick={orderByOlders} className={`${!reverseNewers ? darkBackground : ''} ${secondOrderByContainer}`}>
                            <p>Mas viejos</p>
                          </div>
                        </div>
                        : null
                    }
                  </div>
                  : null
              }
              {
                showFilterByButton
                  ? <div className={orderByMainContainer}>
                    <h6 className={cursor} onClick={toggleFilterBy}>{`Filtrar por ${status}`}</h6>
                    <img className={cursor} onClick={toggleFilterBy} src={icFilter} alt="Sort Icon" />
                    {
                      isFilterBy ?
                        <div className={orderByContainer}>
                          <div
                            onClick={filterByStatus({ text: 'all' })}
                            className={`${filterText === 'all' ? darkBackground : ''} 
                      ${firstOrderByContainer}`}
                          >
                            <p>Todos</p>
                          </div>
                          <div
                            onClick={filterByStatus({ text: 'Approved' })}
                            className={`${filterText === 'Approved' ? darkBackground : ''} 
                      ${firstOrderByContainer}`}
                          >
                            <p>Aprobados</p>
                          </div>
                          <div
                            onClick={filterByStatus({ text: 'Rejected' })}
                            className={`${filterText === 'Rejected' ? darkBackground : ''} 
                      ${secondOrderByContainer}`}
                          >
                            <p>Rechazados</p>
                          </div>
                          <div
                            onClick={filterByStatus({ text: 'Pending' })}
                            className={`
                            ${filterText === 'Pending' ? darkBackground : ''} 
                      ${secondOrderByContainer}
                      `}
                          >
                            <p>Pendientes</p>
                          </div>
                        </div>
                        : null
                    }
                  </div>
                  : null
              }
              {isDownload
                ? <div>
                  {idAdmin
                    ? <div className={orderByMainContainer}>
                      {
                        !allUsersLoading
                          ? <img
                            src={downloadIcon}
                            alt=""
                            className={downloadIconClassUsers}
                            onClick={openDownloadBySelect}
                          />
                          : <Loader color="#f6f7fa" />
                      }
                      {isDownloadSelectOpen && !allUsersLoading ?
                        <div className={`${orderByContainer} ${downloadButtonsContainer}`}>
                          <Workbook
                            filename={`${downloadTitle}-TABLA-ACTUAL-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.xlsx`}
                            element={
                              <div
                                className={`${firstOrderByContainer}`}>
                                <p>Tabla actual</p>
                              </div>
                            }>
                            {downloadColumns}
                          </Workbook>
                          <Workbook
                            filename={`${downloadTitle}-TODOS-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.xlsx`}
                            element={
                              <div
                                className={`${firstOrderByContainer}`}>
                                <p>Todos</p>
                              </div>
                            }>
                            {downloadColumnsAll}
                          </Workbook>
                        </div>
                        : null}
                    </div>
                    : <Workbook
                      filename={`${downloadTitle}-${moment().utc(-264).format('YYYY-MM-DD h:mm A')}.xlsx`}
                      element={
                        <img
                          src={downloadIcon}
                          alt=""
                          className={downloadIconClass}
                        />
                      }>
                      {downloadColumns}
                    </Workbook>}
                </div>
                : null}
            </div>
          ) : null}
        <Input
          icon={icon}
          updateSearch={updateSearch}
          search={search}
          placeholder={placeholder}
          extraImg={(store ? true : false)}
          onKeyPress={onKeyPress}
          dateRangePickerIsOpen={dateRangePickerIsOpen}
          dateRangeFilterHandleSelect={dateRangeFilterHandleSelect}
          ranges={ranges}
          searchDateRange={searchDateRange}
          openDateRangeModal={openDateRangeModal}
          isBackButtonShow={isBackButtonShow}
          goBackButtonClick={goBackButtonClick}
          isSearchingData={isSearchingData}
        />
      </div>
    </div >
  )
}
export default TableHeader