import React from 'react'
import { css } from 'emotion'
import TableHeader from './TableHeader'
import { NavLink } from 'react-router-dom'
import Spinner from 'react-spinkit'
import GoBackButton from '../goBackButton'
import { Column, Table, AutoSizer } from 'react-virtualized'
import 'react-virtualized/styles.css'
import moment from 'moment'
import arrowRightIcon from './../../../assets/arrow-right.svg'
import { postRequest } from '../../../utils/createAxiosRequest'
import throttle from 'lodash.throttle'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { toast } from 'react-toastify'
import { cellRenderer } from './TablaCell'
import HistoryHeader from './HistoryHeader'
const separator = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
    margin-top: 23px;
  `
const adminSeparator = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const spinnerContainer = css`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 300px;
    width: 100%;
    margin: auto;
  `
const goBackButton = css`
  position: absolute;
  top: 21px;
  left: 21px;
  border: none;
  background: transparent;
  height: 21px;
  width: 21px;
  cursor: pointer;
    `
const tableClass = css`
text-align: center;
margin-top: 20px;
@media(max-width: 400px){
    text-align: left;
    }
    `
const columnClass = css`
text-align: center;
white-space: normal;
    `
const header = css`
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    /* padding: 34px 25px; */
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
    @media(max-width: 430px) {
      flex-direction: column;
    }
  `
const arrowRightCSS = css`
    width: 10.3px;
    height: 16px;
    object-fit: contain;
    margin-left: 5px;
  `
const noRegistersText = css`
  opacity: 0.5;
  text-align: center
`


export default class extends React.Component {
  state = {
    search: '',
    dateFilter: false,
    datePickerIsOpen: false,
    selectedDate: moment(),
    isOrderBy: false,
    reverseNewers: true,
    filterText: 'all',
    isFilterBy: false,
    matchMedia: false,
    dateRangePickerIsOpen: false,
    selection: {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection',
    },
    isDateFilter: false,
    itemsIndex: 10,
    isSearchingData: false,
    filteredTransactions: {},
    isBackButtonShow: false,
  }
  updateSearch = (event) => {
    this.setState({
      search: event.target.value,
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  toggleOrderBy = () => {
    this.setState({
      isOrderBy: !this.state.isOrderBy,
    })
  }
  toggleFilterBy = () => {
    this.setState({
      isFilterBy: !this.state.isFilterBy,
    })
  }
  orderByNewers = () => {
    this.setState({
      reverseNewers: true,
      dateFilter: false,
      isOrderBy: false,
      isDateFilter: false
    })
  }
  orderByOlders = () => {
    this.setState({
      reverseNewers: false,
      dateFilter: false,
      isOrderBy: false,
      isDateFilter: false
    })
  }
  filterByStatus = ({ text }) => () => {
    this.setState({
      filterText: text,
      reverseNewers: true,
      dateFilter: false,
      isOrderBy: false,
      isFilterBy: false,
      isDateFilter: false
    })
  }
  heightHandle = ({ tableType }) => {
    const mq = window.matchMedia(`(max-width: ${this.props.mobileMediaWidth})`)
    const mqMobile = window.matchMedia(`(max-width: 500px)`)
    const height =
      (tableType === 'transactions' || tableType === 'deposits' || tableType === 'withdraws') &&
        mqMobile.matches
        ? 265
        : mq.matches
          ? 300
          : 150
    return tableType
      ? tableType === "history"
        ? 200
        : tableType === "pendingRequest"
          ? 230
          : tableType === 'rejectedRequests'
            ? 180
            : tableType === 'recentsMovements'
              ? 200
              : height
      : height
  }
  openDateRangeModal = () => {
    this.setState({
      dateRangePickerIsOpen: !this.state.dateRangePickerIsOpen,
      isDateFilter: false,
      selection: {
        ...this.state.selection,
        startDate: new Date(),
        endDate: new Date(),
      }
    })
  }
  searchDateRange = () => {
    this.setState({
      isDateFilter: true,
      dateRangePickerIsOpen: false,
      search: "",
    }, () => {
      this.throtled()
    })
  }
  handleSelect = (ranges) => {
    this.setState({
      selection: {
        ...this.state.selection,
        startDate: ranges.selection.startDate,
        endDate: ranges.selection.endDate,
      }
    })
  }
  onScroll = ({ clientHeight, scrollHeight, scrollTop }) => {
    // customConsoleLog("type= ", this.props.type)
    if (
      (scrollHeight - scrollTop === clientHeight)
      && this.props.databaseRequest
    ) {
      this.props.databaseRequest(this.props.type)
    }
  }
  throtled = throttle(async () => {
    if (this.state.search || this.state.isDateFilter) {
      this.setState({ isSearchingData: true })
      const data = this.state.isDateFilter
        ? {
          adminID: this.props.userKey,
          startDate: moment(this.state.selection.startDate).format('YYYY-MM-DD'),
          finishDate: moment(this.state.selection.endDate).format('YYYY-MM-DD'),
        }
        : {
          adminID: this.props.userKey,
          query: this.state.search,
        }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest(`transactions/adminFilter?mode=${this.state.isDateFilter ? "date" : "fields"}`,
          data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success && resp.data.data.message.success) {
          this.setState({
            filteredTransactions: Object.values(resp.data.data.message.message),
            isSearchingData: false
          })
        } else {
          this.setState({
            filteredTransactions: "",
            isSearchingData: false
          })
        }
      } catch (error) {
        this.setState({ isSearchingData: false })
        customConsoleLog(error)
        toast.error("Verifique su conexión y vuelva a intentarlo.")
      }
    }
  }, 1000)
  goBackButtonClick = () => {
    this.setState({
      isDateFilter: false,
      search: "",
      filteredTransactions: "",
    })
  }
  render() {
    const mqDisabledHeader = window.matchMedia(`(max-width: ${this.props.mobileMediaWidth})`)
    const isTableLarge = window.matchMedia(`(min-width: 1440px)`).matches
    const {
      columns = [],
      data = [],
      height = 400,
      isThereBackButton = false,
      backButtonRoute,
      title = 'Inserte un titulo aqui',
      loading,
      minHeight,
      className,
      type,
      maxWidth = isTableLarge ? "1300px" : "1200px",
      disableHeader = true,
      //pending requests 
      sendLoading,
      rejectLoading,
      openSendMoneyModal,
      mobileMediaWidth = "500px",
      //history 
      goHistorialButtonIsShown = true,
      historialHandleSwitch,
      balance,
      onHistoryTransactionClick,
      historyIsDateFilter,
      closeDateFilter,
      onKeyPress,
      openDateRangeModal,
      // tableHeader 
      filter,
      placeholder,
      icon,
      //transactions 
      onCloseShowProcessWithdrawModal,
      //rechargeIcon
      onRechargeClick,
      isSearchingData = false,
      //download
      isDownloadSelectOpen,
      openDownloadBySelect,
      allUserTransactions,
      allUserTransactionsLoading,
    } = this.props
    const {
      selection,
      search,
      filterText,
      reverseNewers,
      isOrderBy,
      isFilterBy,
      dateRangePickerIsOpen,
      filteredTransactions,
      isDateFilter,
    } = this.state
    const tableContainer = css`
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    display: -ms-flexbox;
    display: flex;
    flex-direction: column;
    max-width: ${maxWidth};
    margin: auto;
    margin-top: 42px;
    margin-bottom: 40px;
    border-radius: 5px;
    @media(max-width: 1230px){
      margin: 0 15px;
      margin-top: 40px;
    }
    @media(min-width: 1230px) and (max-width: ${maxWidth}){
      margin-left: 12px
    }
  `
    const tableContainerHistorial = css`
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    display: -ms-flexbox;
    display: flex;
    flex-direction: column;
    max-width: ${maxWidth};
    margin: auto;
    margin-top: 50px;
    margin-bottom: 40px;
    @media(max-width: 1230px){
      margin: 0 25px;
      margin-top: 40px;
      margin-bottom: 40px !important;
    }
    }
  `
    const rowClassName = css`
text-align: center;
margin-top: 5px;
@media(max-width: ${mobileMediaWidth}){
  flex-direction: column;
}
@media(min-width: ${mobileMediaWidth}){
    :hover{
      transition: background-color 0.3s ease;
      background-color: ${type === "history" || type === "recentsMovements" ? "#242657" : ""} 
    }
}
    `
    const cardTableBottom = css`
    position: absolute;
    bottom: -1px;
    width: 100%;
    height: 100px;;
    background-image: linear-gradient(to bottom, rgba(41, 43, 105, 0), rgba(41, 43, 105, 0.5) 49%, #2a2c6a);
    pointer-events: none;
    border-radius: 5px;
  `
    const isClickableCell = type === "history" ||
      type === "recentsMovements" ||
      type === "withdraws" ||
      type === "transactions" ||
      type === "deposits"
    const cellClass = css`
@media(min-width: ${mobileMediaWidth}){
  cursor: ${isClickableCell ? "pointer" : ""}
  transition: ${isClickableCell ? "all 0.3s ease;" : ""}
  : hover{
  -webkit - transform: ${isClickableCell ? "scale(1.08);" : ""}
  -ms - transform: ${isClickableCell ? "scale(1.08);" : ""}
  transform: ${ isClickableCell ? "scale(1.08);" : ""}
}
}
`
    const transactionsHandled = search || isDateFilter ? filteredTransactions : data
    const transactionsOrdered =
      type !== "history"
        ? transactionsHandled && transactionsHandled.length > 0
          ? filterText !== 'all'
            ? reverseNewers
              ? transactionsHandled.filter(
                trans => trans.status === filterText
              ).sort((a, b) => b.timestamp - a.timestamp)
              : transactionsHandled.filter(
                trans => trans.status === filterText
              ).sort((a, b) => a.timestamp - b.timestamp)
            : reverseNewers
              ? transactionsHandled.sort((a, b) => b.timestamp - a.timestamp)
              : transactionsHandled.sort((a, b) => a.timestamp - b.timestamp)
          : !search && !isDateFilter
            ? data
            : null
        : data
    return (
      <div className={`
${ type === 'history' ? tableContainerHistorial : tableContainer}
${ className}
`}
        style={{ minHeight: `${minHeight} ` }}
      >
        {isThereBackButton ?
          <GoBackButton
            height="20px"
            width="20px"
            style={goBackButton}
            route={backButtonRoute}
          />
          : null}
        {type === 'transactions' || type === 'withdraws' || type === 'deposits'
          ? <TableHeader
            title={title}
            reverseNewers={reverseNewers}
            isOrderBy={isOrderBy}
            orderByOlders={this.orderByOlders}
            orderByNewers={this.orderByNewers}
            toggleOrderBy={this.toggleOrderBy}
            filter={filter}
            updateSearch={this.updateSearch}
            search={search}
            placeholder={placeholder}
            icon={icon}
            isFilterBy={isFilterBy}
            filterByStatus={this.filterByStatus}
            filterText={filterText}
            toggleFilterBy={this.toggleFilterBy}
            dateRangePickerIsOpen={dateRangePickerIsOpen}
            dateRangeFilterHandleSelect={this.handleSelect}
            ranges={selection}
            searchDateRange={this.searchDateRange}
            openDateRangeModal={this.openDateRangeModal}
            recharge={!this.state.search && !isDateFilter}
            onRechargeClick={onRechargeClick}
            isSearchingData={this.state.isSearchingData}
            isBackButtonShow={this.state.search || isDateFilter}
            goBackButtonClick={this.goBackButtonClick}
          />
          : <div className={header}>
            <h1> {title} </h1>
            {type === "history"
              ? <HistoryHeader
                title={title}
                data={data}
                historyIsDateFilter={historyIsDateFilter}
                openDateRangeModal={openDateRangeModal}
                updateSearch={this.props.updateSearch}
                search={this.props.search}
                onKeyPress={onKeyPress}
                isSearchingData={isSearchingData}
                historialHandleSwitch={historialHandleSwitch}
                balance={balance}
                closeDateFilter={closeDateFilter}
                isDownloadSelectOpen={isDownloadSelectOpen}
                openDownloadBySelect={openDownloadBySelect}
                allUserTransactions={allUserTransactions}
                allUserTransactionsLoading={allUserTransactionsLoading}
              />
              : goHistorialButtonIsShown
                ? <div>
                  <NavLink to='/historial'>
                    <h2 className='text-accept'>Ver historial completo</h2>
                  </NavLink>
                  <img
                    className={arrowRightCSS}
                    src={arrowRightIcon}
                    alt="arrow right"
                  />
                </div>
                : null
            }
          </div>}
        <div className={
          type === 'transactions' ||
            type === 'withdraws' ||
            type === 'deposits'
            ? adminSeparator
            : separator} />
        {transactionsOrdered &&
          transactionsOrdered.length > 0 &&
          !this.state.isSearchingData &&
          !isSearchingData
          ? <div css={`position: relative`}>
            <AutoSizer disableHeight>
              {({ width }) => (
                <Table
                  headerHeight={20}
                  height={height}
                  width={width}
                  rowHeight={this.heightHandle({ tableType: type })}
                  rowCount={transactionsOrdered.length}
                  rowGetter={({ index }) => transactionsOrdered[index]}
                  overscanRowCount={10}
                  headerClassName={columnClass}
                  disableHeader={mqDisabledHeader.matches ? true : disableHeader}
                  className={tableClass}
                  rowClassName={rowClassName}
                  onScroll={this.onScroll}
                >
                  {columns.length > 0
                    ? columns.map(({ label, key }) =>
                      <Column
                        label={label}
                        dataKey={key}
                        key={key}
                        className={cellClass}
                        cellRenderer={cellRenderer({
                          type: type,
                          //pending requests
                          sendLoading: sendLoading,
                          rejectLoading: rejectLoading,
                          openSendMoneyModal: openSendMoneyModal,
                          mobileMediaWidth: mobileMediaWidth,
                          onCloseShowProcessWithdrawModal: onCloseShowProcessWithdrawModal,
                          onHistoryTransactionClick: onHistoryTransactionClick,
                        })}
                        width={width / columns.length}
                      />
                    ) : null}
                </Table>)}
            </AutoSizer>
            <div className={cardTableBottom}></div>
          </div>
          : <div className={spinnerContainer}>
            {loading || isSearchingData || this.state.isSearchingData
              ? <Spinner color='white' />
              : <h1 className={noRegistersText}>No hay registros</h1>}
          </div>
        }
      </div>
    )
  }
}