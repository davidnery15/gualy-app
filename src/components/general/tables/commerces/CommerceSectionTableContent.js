import React from 'react'
import { css } from 'emotion'
import ubicationIcon from '../../../../assets/ubicationIcon.svg'
import computerIcon from '../../../../assets/computer.svg'
const commercesContainer = css`
display: flex;
flex-wrap: wrap;
justify-content: center;
`
const hd = css`
flex-wrap: wrap;
justify-content: center;
display: flex;
div{
  width: 152px
}
@media(max-width:767px){
  display: none
}
@media(max-width: 380px){
  div{
    width: 111px;
  }
}
`
const mobile = css`
flex-wrap: wrap;
display: none;
justify-content: center;
div{
  width: 152px
}
@media(max-width:767px){
  display: flex
}
@media(max-width:399px){
  display: flex;
  display-flex: column;
}
@media(max-width: 380px){
  div{
    width: 111px;
  }
}
`
const singleCommerceContainerSmall = css`
height: 243px;
border-radius: 5.5px;
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
display: flex;
flex-direction: column;
margin: 10px;
cursor: pointer;
@media(max-width: 380px){
  height: 200px;
}
`
const imageContainer = css`
height: 60%;
width: 100%;
`
const infoContainer = css`
height: 40%;
width: 100%;
display: flex;
display-flex: column;
text-align: left
`
const imageClass = css`
height: 100%;
width: 100%;
background: #ffff;
border-top-right-radius: 7.9px;
border-top-left-radius: 7.9px;
`
const commerceNameText = css`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.17;
  letter-spacing: normal;
  color: #f6f7fa;
  margin: 10px;
  @media(max-width: 380px){
    font-size: 10px;
  }
`
const commercePlaceText = css`
font-family: Montserrat;
font-size: 12.3px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
color: #f6f7fa;
margin-left: 11px;
@media(max-width: 380px){
  font-size: 8px;
}
`
const ubicationIconClass = css`
margin-right: 4px
`
const CommercesContent = ({
  commerces,
  onCommerceClick,
}) => {
  return <div className={commercesContainer}>
    <div className={hd}>
      {
        commerces && commerces.length > 0
          ? commerces.map((commerce, index) => {
            return <div
              onClick={onCommerceClick({ selectedCommerce: commerce })}
              key={index}
              className={`
            ${singleCommerceContainerSmall} 
          `}>
              <div className={imageContainer}>
                <img
                  alt=""
                  className={imageClass}
                  src={commerce.profilePicture}
                />
              </div>
              <div className={infoContainer}>
                <p className={commerceNameText}>
                  {commerce.name}
                </p>
              </div>
              {
                commerce.commerceType === "digitalCommerce"
                  ? <p className={commercePlaceText}>
                    <img alt="" className={ubicationIconClass} src={computerIcon} />
                    Comercio digital
                  </p>
                  : null
              }
              {
                commerce.commerceType !== "digitalCommerce"
                  ? <p className={commercePlaceText}>
                    <img alt="" className={ubicationIconClass} src={ubicationIcon} />
                    {commerce.city ? `En ${commerce.city}` : 'En Maracaibo'}
                  </p>
                  : null
              }
            </div>
          })
          : null
      }
    </div>
    <div className={mobile}>
      {
        commerces && commerces.length > 0
          ? commerces.map((commerce, index) => {
            return <div
              onClick={onCommerceClick({ selectedCommerce: commerce })}
              key={index}
              className={singleCommerceContainerSmall}>
              <div className={imageContainer}>
                <img
                  alt=""
                  className={imageClass}
                  src={commerce.profilePicture}
                />
              </div>
              <div className={infoContainer}>
                <p className={commerceNameText}>
                  {commerce.name}
                </p>
              </div>
              {
                commerce.commerceType === "digitalCommerce"
                  ? <p className={commercePlaceText}>
                    <img alt="" className={ubicationIconClass} src={computerIcon} />
                    Comercio digital 
                  </p>
                  : null
              }
              {
                commerce.commerceType !== "digitalCommerce"
                  ? <p className={commercePlaceText}>
                    <img alt="" className={ubicationIconClass} src={ubicationIcon} />
                    {commerce.city ? `En ${commerce.city}` : 'En Maracaibo'}
                  </p>
                  : null
              }
            </div>
          })
          : null
      }
    </div>
  </div>
}
export default CommercesContent