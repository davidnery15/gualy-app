import React from 'react'
import { css } from 'emotion'
import Spinner from 'react-spinkit'
import DashboardCommercesTableContent from './DashboardCommercesTableContent'
import CommerceSectionTableContent from './CommerceSectionTableContent'
import arrowRightIcon from './../../../../assets/arrow-right.svg'
import { NavLink } from 'react-router-dom'
import SearchInput from '../../../general/searchInput'
import throttle from 'lodash.throttle'
import { customConsoleLog } from '../../../../utils/customConsoleLog'
import { postRequest } from '../../../../utils/createAxiosRequest'
import { toast } from 'react-toastify'

/* CSS */
const component = css`
    margin: 50px;
    border-radius: 5px;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    background-color: #2a2c6a;
    max-width: 1200px;
    margin: auto;
    margin-top: 50px;
    @media (min-width: 1230px) {
      width: 100%;
      margin-bottom: 40px;
    }
    @media (max-width: 1230px) {
      margin: 0 15px;
      margin-top: 40px;
      margin-bottom: 40px;
    }
  `
const tableDivisor = css`
    height: 1px;
    width: 100%;
    padding: 0;
    margin: 0;
    opacity: 0.9;
    background-color: #1d1f4a;
  `
const commercesSectionCardTableComponent = css`
height: 70vh;
padding-left: 32px;
padding-right: 32px;
position: relative;
overflow-x: hidden;
min-height: 400px;
/* Track */
  ::-webkit-scrollbar-track {
    background: #f6f7fa;
    border: 3px solid transparent;
    background-clip: content-box;
  }
/* Handle */
  ::-webkit-scrollbar-thumb {
    border: 1px;
    background-color: #95fad8;
    border-radius: 2px;
  }
::-webkit-scrollbar {
    width: 7px;
}
@media (max-width: 750px) {
  padding-left: 0;
  padding-right: 0;
  height: 100%;
  max-height: 383px;
}
  `
const cardTableBottom = css`
    position: sticky;
    bottom: -1px;
    width: 100%;
    height: 100px;
    background-image: linear-gradient(to bottom, rgba(41, 43, 105, 0), rgba(41, 43, 105, 0.5) 49%, #2a2c6a);
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
  `
const arrowRightCSS = css`
    width: 10.3px;
    height: 16px;
    object-fit: contain;
    margin-left: 5px;
  `
const header = css`
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    /* padding: 34px 25px; */
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
    @media(max-width: 430px) {
      flex-direction: column;
    }
  `
const header2 = css`
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 15px 25px;
    justify-content: space-between;
    align-items: center;
    /* padding: 34px 25px; */
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
    @media(max-width: 430px) {
      flex-direction: column;
    }
  `
const cardTable = css`
    border-radius: 5px;
    position: relative;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: column;
    max-width: 1200px;
    margin: auto;
    height: 345px;
    margin-top: 50px;
    @media(max-width: 1230px) {
        margin: 0 15px;
        margin-top: 40px;
    }
  `
const dashboardCardTableComponent = css`
overflow-x: hidden;
    height: 536px;
    // padding: 0 20px;
    position: relative;
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f6f7fa;
      border: 3px solid transparent;
      background-clip: content-box;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border: 1px;
      background-color: #95fad8;
      border-radius: 2px;
    }
    ::-webkit-scrollbar {
      width: 7px;
    }
    table{
      width: 100%;
    }
    @media(max-width: 400px) {
      padding: 0 5px;
    }
    @media(min-width: 1023px) {
      overflow-y: hidden;
    }
    @media(min-width: 1440px) {
      height: 444px;
    }
  `
const searchInputClass = css`
  width: 240px;
  max-width: 300px;
  `
const orizontalDivisor = css`
  height: 1px;
  width: 100%;
  padding: 0;
  margin: 0;
  opacity: 0.9;
  background-color: #1d1f4a;
  margin-top: 23px;
  margin-bottom: 15px;
  `
const noRegistersText = css`
  opacity: 0.5;
  text-align: center;
`
const btnTransparent = css`
background: transparent;
border: none;
outline: none !important;
cursor: pointer;
margin-right: 8px;
margin-top: 11px;
`
const commercesSectionFiltersContainer = css`
display: flex;
flex-wrap: wrap;
justify-content: center;
`
export default class UsersCardTable extends React.Component {
  state = ({
    search: '',
    filteredUsers: '',
    isSearchingData: false,
    commerceTypeWitch: "all",
  })
  updateSearch = (event) => {
    this.setState({
      search: event.target.value.substr(0, 120),
    })
    this.throtled()
  }
  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.throtled()
    }
  }
  onScroll = () => {
    if (
      (this.desktopTable.scrollHeight - this.desktopTable.scrollTop === this.desktopTable.clientHeight)
      && this.props.databaseRequestCommerces
    ) {
      this.props.databaseRequestCommerces("commerces")
    }
  }
  throtled = throttle(async () => {
    if (this.state.search) {
      this.setState({ isSearchingData: true })
      const data = {
        value: this.state.search,
      }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest('users/filter', data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success) {
          this.setState({ isSearchingData: false })
          if (Object.values(resp.data.data.message).length === 0) {
            // toast.error("No se encontraron resultados.")
            this.setState({
              filteredUsers: Object.values(resp.data.data.message)
            })
          } else {
            this.setState({
              filteredUsers: Object.values(resp.data.data.message)
            })
          }
        } else {
          this.setState({ isSearchingData: false })
        }
      } catch (error) {
        this.setState({ isSearchingData: false })
        customConsoleLog(error)
        toast.error("Verifique su conexión y vuelva a intentarlo.")
      }
    }
  }, 1000)
  commerceTypeWitchHandle = ({ commerceTypeWitch }) => (e) => {
    e.preventDefault()
    this.setState({ commerceTypeWitch: commerceTypeWitch })
  }
  render() {
    const {
      commerces,
      title,
      loading,
      onCommerceClick,
      type,
      className,
    } = this.props
    const {
      filteredUsers,
      search,
      isSearchingData,
      commerceTypeWitch,
    } = this.state
    const commercesFiltered = !filteredUsers || !search
      ? commerces
      : type === 'commercesSection'
        ? filteredUsers.filter(commerce => commerce.type === "commerce")
        : commerces
    const finalCommercesFiltering = commerceTypeWitch === "all"
      ? commercesFiltered
      : commerceTypeWitch === "digitals"
        ? commercesFiltered.filter(commerce => commerce.commerceType === "digitalCommerce")
        : commercesFiltered.filter(commerce => commerce.commerceType !== "digitalCommerce")
    return (
      <div className={`
      ${type === 'commercesSection' ? component : ''} 
      ${type === 'dashboardCommerces' ? className : ''} 
      ${type === 'dashboardCommerces' ? cardTable : ''}
      `}>
        <div className={type === 'commercesSection' ? header2 : header}>
          <h1> {title} </h1>
          {
            type === 'commercesSection'
              ? <div className={commercesSectionFiltersContainer}>
                <SearchInput
                  onChange={this.updateSearch}
                  value={search}
                  placeholder="Buscar..."
                  name="search"
                  type="text"
                  className={searchInputClass}
                  onKeyPress={this.onKeyPress}
                  isSearchingData={isSearchingData}
                />
                <div>
                  <button className={btnTransparent} onClick={this.commerceTypeWitchHandle({ commerceTypeWitch: 'physical' })}>
                    <h2 className={commerceTypeWitch === 'physical' ? 'text-accept' : 'text-non-active'}>Físicos</h2>
                  </button>
                  <button className={btnTransparent} onClick={this.commerceTypeWitchHandle({ commerceTypeWitch: 'digitals' })}>
                    <h2 className={commerceTypeWitch === 'digitals' ? 'text-accept' : 'text-non-active'}>Digitales</h2>
                  </button>
                  <button className={btnTransparent} onClick={this.commerceTypeWitchHandle({ commerceTypeWitch: 'all' })}>
                    <h2 className={commerceTypeWitch === 'all' ? 'text-accept' : 'text-non-active'}>Todos</h2>
                  </button>
                </div>
              </div>
              : type === 'dashboardCommerces'
                ? <div>
                  <NavLink to='/directorio-comercios'>
                    <h2 className='text-accept'>Ver directorio</h2>
                  </NavLink>
                  <img
                    className={arrowRightCSS}
                    src={arrowRightIcon}
                    alt="arrow right"
                  />
                </div>
                : null
          }
        </div>
        <div className={type === 'commercesSection' ? tableDivisor : orizontalDivisor}></div>
        <div
          ref={ref => this.desktopTable = ref}
          onScroll={this.onScroll}
          className={type === 'commercesSection'
            ? commercesSectionCardTableComponent
            : dashboardCardTableComponent}>
          {
            loading || isSearchingData ?
              (<div className={spinnerContainer}> <Spinner color='white' /> </div>)
              : finalCommercesFiltering && finalCommercesFiltering.length > 0
                ? type === 'commercesSection'
                  ? <CommerceSectionTableContent
                    onCommerceClick={onCommerceClick}
                    commerces={finalCommercesFiltering}
                  />
                  : <DashboardCommercesTableContent
                    onCommerceClick={onCommerceClick}
                    commerces={finalCommercesFiltering}
                  />
                : <div className={spinnerContainer}>
                  <h1 className={noRegistersText}>No hay registros</h1>
                </div>
          }
          {
            type === 'commercesSection'
              ? <div className={cardTableBottom}></div>
              : null
          }
        </div>
      </div>
    )
  }
}