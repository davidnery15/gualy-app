import React from 'react'
import { css } from 'emotion'
import { BS } from '../../config/currencies.js'
import CurrencyInput from 'react-currency-input'
/**CSS */
const inputContainerSmall = css`
    position: relative;
    overflow: hidden;
      opacity: 0.65;
      font-size: 20.5px !important;
      font-weight: normal !important;
      font-style: normal !important;
      font-stretch: normal !important;
      line-height: normal !important;
      letter-spacing: normal !important;
      text-align: center !important;
      color: #95fad8 !important;
  `
const inputHidden = css`
    position: relative;
    overflow: hidden;
      opacity: 0.65;
      font-size: 39.5px !important;
      font-weight: normal !important;
      font-style: normal !important;
      font-stretch: normal !important;
      line-height: normal !important;
      letter-spacing: normal !important;
      text-align: center !important;
      color: #95fad8 !important;
      padding-left: 97px !important
  `
const currencyClass = css`
      position: absolute;
      overflow: hidden;
      opacity: 0.65;
      font-size: 39.5px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
      bottom: 4px;
      padding-left: 46px;
  `
const currencyClassSmall = css`
      position: absolute;
      overflow: hidden;
      opacity: 0.65;
      font-size: 25.5px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #95fad8;
      bottom: 0;
  `
const amountImputContainer = css`
      position: relative;
      width: 100%;
      margin-bottom: 15px;
      margin-top: 15px;
      display: flex;
      flex-direction: row;
  `
const AmountInput = ({ amount = '', onChange, spanBorder, disabled }) => (
  <div className={`${amountImputContainer}`}>
    <div className={String(amount).length > 4 ? currencyClassSmall : currencyClass}>{BS}</div>
    <CurrencyInput
      className={`${String(amount).length > 4 ? inputContainerSmall : inputHidden} ${spanBorder}`}
      inputMode='numeric'
      disabled={disabled}
      decimalSeparator=","
      thousandSeparator="."
      onChange={onChange}
      value={amount}
      maxLength={13}
      autoFocus={false}
    />
  </div>
)
export default AmountInput