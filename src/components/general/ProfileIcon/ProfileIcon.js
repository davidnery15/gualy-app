import React, { Component } from 'react'
import { css } from 'emotion'
import moment from 'moment'
// eslint-disable-next-line
import es from 'moment/locale/es'

import ProfileModal from './ProfileModal'
import { auth, db } from './../../../firebase'

/**CSS */
  const flexLi = css`
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.1s ease-in-out;
    border-bottom: solid 4px transparent;
  `
  const anchorLink = css`
    padding: 9px 10px;
    color: white;
    font-weight: 300;
    font-size: 14px;
    letter-spacing: 1.6px;
    outline: transparent;
    cursor:pointer;
    position: relative;
    &:active {
      color: white;
      text-decoration: none;
    }
    &:visited {
      color: white;
      text-decoration: none;
    }
    &:hover {
      color: white;
      text-decoration: none;
    }
  `
  const activeLink = css`
    width: 100%;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold !important;
    letter-spacing: 1.2px;
    color: #8078ff !important;
    fill: #8078ff;
  `
  const navItemLogoSVG = css`
    margin-right: 5px;  
    @media (max-width: 1284px) {
      transition: all 0.5s;
    }
  `
  const navItemLogo = css`
    fill: currentColor; 
  `
  
export default class ProfileIcon extends Component {
  state = {
    active: false,
    activeOnClick: false,
    loading: true,
    userData: {}
  }

  componentDidMount = () => {
    auth.onAuthStateChanged((user) => {
      const adminUid = user ? user.uid : null
      const displayName = user ? user.displayName : null
      const lastSignInTime = moment( user ? user.metadata.lastSignInTime : '').utc(-264).format('DD-MM-YYYY h:mm a')
      const userEmail = user ? user.email : null
      const usersRef = db.ref('users').orderByChild('userKey').equalTo( user ? adminUid.toString() : '')
      let userData = {}
      usersRef.on('value', async (snapshot) => {
        const data = snapshot.val() ? Object.values(snapshot.val()) : null
        let { type } = data ? data[0] : ''
        if (data) {
          switch (type) {
            case 'admin':
              type = 'Administrador'
              break;
            case 'support':
              type = 'Soporte'
              break;
            case 'cashier':
              type = 'Cajero'
              break;
            case 'marketing':
              type = 'Mercadeo'
              break;
            case 'commerce':
              type = 'Comercio'
              break;
            default:
              type = 'Cliente'
              break;
          }
          userData = {
            profilePicture: data[0].profilePicture,
            displayName: displayName,
            email: userEmail,
            lastSignInTime: lastSignInTime,
            type,
            userId: adminUid,
          }
          this.setState({
            loading: false,
            userData
          })
        }
      })
    })
  }

  // componentWillMount() {
  //   document.addEventListener('mousedown', this.handleClick, false)
  // }

  // componentWillUnmount() {
  //   document.removeEventListener('mousedown', this.handleClick, false)
  // }

  // handleClick = (e) => {
  //   if (this.node.contains(e.target)) {
  //     this.setState(prevState => ({
  //       activeOnClick: !prevState.activeOnClick
  //     }))
  //     return
  //   } else {
  //     this.handleClickOutside()
  //   }
  // }

  // handleClickOutside = () => {
  //   this.setState({
  //     activeOnClick: false
  //   });
  // }

  // mouseOut = (e) => {
  //   this.setState({
  //     active: false
  //   });
  // }

  // mouseOver = () => {
  //   this.setState({
  //     active: true
  //   });
  // }

  render() {
    const { active, activeOnClick, userData, loading } = this.state
    return(
      <div ref={node=> this.node = node} className={`${flexLi} ${activeOnClick ? css`border-bottom: solid 4px #8078ff;` : ''}`}>
        <div className={`${anchorLink} ${this.state.active ? activeLink : ''} ${this.state.activeOnClick ? activeLink : ''}`}>
          {
            loading ? 
            <svg 
              className={`${navItemLogoSVG}`} width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.396 18.396" >
              <path className={navItemLogo} id="ic_account_circle_24px" fill='#f6f7fa' d="M11.2 2a9.2 9.2 0 1 0 9.2 9.2A9.2 9.2 0 0 0 11.2 2zm0 2.759a2.759 2.759 0 1 1-2.761 2.76 2.756 2.756 0 0 1 2.761-2.76zm0 13.061a6.623 6.623 0 0 1-5.519-2.962c.028-1.83 3.679-2.833 5.519-2.833s5.491 1 5.519 2.833a6.623 6.623 0 0 1-5.519 2.963z" transform="translate(-2 -2)" />
            </svg>
            : 
              <div css={`margin-top: -8px`}>
                <img
                  src={userData.profilePicture} 
                  alt="profile"
                  css={`width:22px; height: 22px;border-radius: 50%;
                        object-fit: cover;`}
                />
            </div>
          }
          {
            (active || activeOnClick) ? <ProfileModal data={userData} loading={loading}/> : null
          }
        </div>
      </div>
    )
  }
}