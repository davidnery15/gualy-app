import React from 'react'
import { css } from 'emotion' 

import Loader from './../Loader'

/*CSS*/
  const component = css`
    width: 186px;
    border-radius: 5px;
    min-height: 100px;
    /* -webkit-filter: blur(11px);
    filter: blur(11px); */
    background-color: #232553;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    /* box-shadow: 0 0px 10px 0 rgba(255,255,255,0.75); */
    position: absolute;
    right: -70px;
    z-index: 1;
    display: flex;
    flex-direction: column;
    padding: 10px 9px;
    top: 94px;
    @media(max-width: 835px){
      top: 89px;
    }
  `
  const userContainer = css`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    span{
      font-family: Montserrat;
      font-size: 14px;
      font-weight: 900;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.19;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    }
    img{
      width: 50px !important;
      height: 50px !important;
      border-radius: 50%;
      object-fit: cover;
      margin-right: 10px;
    }
  `
  const textContainer = css`
    width: 100%;
    margin-top: 10px;
    margin-left: 10px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.3;
    letter-spacing: normal;
    text-align: left;
    color: #f6f7fa;
  `
  
const ProfileModal = ({ data, loading }) => {
  return(
    <div className={component}>
      {
        loading ? 
          <Loader color='white' />
          :
          (<div>
            <div className={userContainer}>
              <img src={data.profilePicture} alt="user" />
              <span>{data.displayName}</span>
            </div>
            <div className={textContainer}><span>{data.email}</span></div>
            <div className={textContainer}><span>{data.lastSignInTime}</span></div>
            <div className={textContainer}><span css={`font-weight: bold !important;`}>{data.type}</span></div>
          </div>)
      }
    </div>
  )
} 

export default ProfileModal