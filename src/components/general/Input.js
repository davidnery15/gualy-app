import React from 'react'
import SearchIcon from '../../assets/ic-search.svg'
import DatePickerIcon from '../../assets/ic-date-picker.svg'
import { css } from 'emotion'
import OptionsModalWrapper from '../general/OptionsModalWrapper'
import DateRangeModal from './modals/DateRangeModal'
import GoBackButton from '../general/goBackButton'
import Loader from '../general/Loader'
/* CSS */
const group = css`
    display: flex;
    position: relative;
    width: 100%;
  `
const datePickerIcon = css`
cursor: pointer
@media(min-width: 365px){
  margin-left:32px;
    }
  `
const inputProp = css`
    width: 100%;
    min-width: 100px;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #f6f7fa;
    padding-left: 35px; 
  `
const inputContainer = css`
    width: 100%;
  `
const extraImgContent = css`
    @media(max-width: 475px){
      justify-content: flex-end;
    }
  `
const datePickerContainer = css`
  display: flex;
  flex-direction: row;
  margin: auto;
  img {
    margin-left: 10px
  }
`
const loaderClass = css`
width: 40px;
position: absolute;
bottom: 11px;
left: -6px;
`
const searchIconClass = css`
position: absolute;
bottom: 14px;
left: 5px;
`

export default ({
  extraImg,
  placeholder,
  updateSearch,
  search,
  type,
  id,
  name,
  onKeyPress,
  icon,
  dateRangePickerIsOpen,
  dateRangeFilterHandleSelect,
  ranges,
  searchDateRange,
  openDateRangeModal,
  isBackButtonShow,
  goBackButtonClick,
  isSearchingData,
}) =>
  <div className={`${group} ${(extraImg ? extraImgContent : null)}`}>
    <div className={`${inputContainer} ${(extraImg ? extraImg : null)}`}>
      {
        isSearchingData
          ? <div className={loaderClass}><Loader color={'#f6f7fa'} /></div>
          : <img className={searchIconClass} src={SearchIcon} alt="search icon" />
      }
      <input
        className={`${inputProp} ${(extraImg ? extraImg : null)}`}
        placeholder={placeholder ? `${placeholder}` : ''}
        onChange={updateSearch}
        value={search}
        type={type ? `${type}` : ''}
        id={id ? `${id}` : ''}
        name={name ? `${name}` : ''}
        onKeyPress={onKeyPress ? onKeyPress : null}
      />
      <span className='highlight'></span>
      <span className='bar'></span>
    </div>
    <div className={datePickerContainer}>
      {
        icon ?
          <img
            className={datePickerIcon}
            src={DatePickerIcon}
            onClick={openDateRangeModal}
            alt="date picker icon"
          /> : null
      }
      {
        isBackButtonShow
          ? <GoBackButton
            height="21px"
            width="21px"
            style={datePickerIcon}
            onClick={goBackButtonClick}
          />
          : null
      }
    </div>
    <OptionsModalWrapper
      show={dateRangePickerIsOpen}
      onClose={openDateRangeModal}
      width={310}
    >
      <DateRangeModal
        onChange={dateRangeFilterHandleSelect}
        moveRangeOnFirstSelection={false}
        ranges={ranges}
        onSearchClick={searchDateRange}
      />
    </OptionsModalWrapper>
  </div>
