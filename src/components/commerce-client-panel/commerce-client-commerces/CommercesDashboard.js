import React, { Component } from 'react'
import { css } from 'emotion'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import CommerceClientHeader from '../commerce-client-header/CommerceClientHeader'
import CommercesTable from '../../general/tables/commerces/CommerceTable'
import { db } from '../../../firebase'
import SendMoneyToCommerceModal from '../../general/modals/SendMoneyToCommerceModal'
import { ToastContainer, toast } from 'react-toastify'
import moment from 'moment'
import { BS2 } from '../../../config/currencies.js'
import { postRequest } from '../../../utils/createAxiosRequest'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { userLogout } from '../../../redux/actions/general'
import {
  getCommerces
} from '../../../redux/actions/general'
const container = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
  `
const scrollDataRequestedRange = 10

class CommercesDashboard extends Component {
  state = {
    sendAmount: '',
    displayErrors: false,
    sendShow: false,
    sendDescription: '',
    sendEmail: '',
    errorMessage: '',
    userData: {},
    selectedCommerce: '',
    hasSecurityPassword: false,
    //special password
    firstNumber: '',
    secondNumber: '',
    thirdNumber: '',
    fourthNumber: '',
    isCreatePasswordOpen: false,
    //reset special password
    isResetPasswordOpen: false,
    isSended: false,
    commercesIndex: 20,
    isUserBlocked: false
  }
  amountInputHandleChange = ({ name }) => (maskedvalue) => {
    this.setState({ [name]: maskedvalue })
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  componentDidMount() {
    const usersRef = db.ref(`users/${this.props.userInCollectionData.userKey}/amount`)
    let userData = {}
    usersRef.on('value', (snapshot) => {
      userData = {
        amount: snapshot.val(),
      }
      this.setState({
        userData
      })
    })
    const commercesIndex = this.state.commercesIndex
    this.props.dispatch(getCommerces({ commercesIndex }))
    if (this.props.userInCollectionData) {
      const securityPasswordRef = db.ref(`users/${this.props.userInCollectionData.userKey}/hasSecurityPassword`)
      securityPasswordRef.on('value', async (snapshot) => {
        this.setState({
          hasSecurityPassword: snapshot.val()
        })
      })
      const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
      isUserBlockRef.once('value', snapshot => {
        this.setState({
          isUserBlocked: snapshot.val(),
        })
      })
    }
  }
  databaseRequestCommerces = () => {
    customConsoleLog("databaseRequestCommerces")
    this.setState({ commercesIndex: this.state.commercesIndex + scrollDataRequestedRange }, () => {
      const commercesIndex = this.state.commercesIndex
      this.props.dispatch(getCommerces({ commercesIndex }))
    })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
      if (this.props.userInCollectionData.type) {
        const redirect =
          this.props.userInCollectionData.type === 'client'
            ? null
            : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  selectCommerce = config => () => {
    // "commerce selected: ", config.selectedCommerce)
    const selectedCommerce = config.selectedCommerce
    this.setState({ selectedCommerce: selectedCommerce })
    this.sendShow()
  }
  sendShow = () => {
    return !this.state.sendLoading
      ? this.setState({
        sendDescription: '',
        sendEmail: '',
        sendAmount: '',
        sendShow: this.state.sendLoading ? true : !this.state.sendShow,
        displayErrors: false,
      })
      : null
  }
  toastLoaderSendId = null
  sendPayment = async (event) => {
    event.preventDefault();
    this.toastLoaderSendId = toast('Enviando dinero...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    const { sendAmount, sendDescription } = this.state
    const trasactionAmount = Number(sendAmount.replace(/\./g, '').replace(',', '.'))
    if (
      trasactionAmount > 0 &&
      trasactionAmount <= this.state.userData.amount &&
      (
        this.state.hasSecurityPassword
          ? this.state.firstNumber &&
          this.state.secondNumber &&
          this.state.thirdNumber &&
          this.state.fourthNumber
          : true
      )
    ) {
      const data = {
        receiverEmail: this.state.selectedCommerce.email,
        senderUid: this.props.userInCollectionData.userKey,
        date: moment().format('YYYY-MM-DD'),
        description: sendDescription.trim(),
        currency: BS2,
        amount: trasactionAmount,
        time: moment().format('LTS'),
        securityPassword:
          this.state.firstNumber +
          this.state.secondNumber +
          this.state.thirdNumber +
          this.state.fourthNumber
      }
      customConsoleLog("SEND:", data)
      try {
        let response = await postRequest('transactions/sendMoney', data)
        customConsoleLog('sendPayment response', response)
        if (response.data.success) {
          this.setState({
            sendLoading: false,
            displayErrors: false
          })
          this.sendShow()
          toast.update(this.toastLoaderSendId, {
            render: 'Dinero enviado exitosamente',
            autoClose: 5000,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, errorMessage: message, displayErrors: true })
          return toast.update(this.toastLoaderSendId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderSendId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ sendLoading: false, displayErrors: true })
      if (!trasactionAmount > 0) {
        this.setState({ errorMessage: 'Debe colocar un monto.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un monto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (trasactionAmount > this.state.userData.amount) {
        this.setState({ errorMessage: 'Saldo insuficiente.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Saldo insuficiente.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if ((!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) &&
        this.state.hasSecurityPassword) {
        this.setState({ errorMessage: 'Contraseña de seguridad incompleta.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Contraseña de seguridad incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  cancelCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: false,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  openCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: true,
      sendDescription: '',
      sendEmail: '',
      sendAmount: '',
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  onCreateSpecialPasswordClick = async (event) => {
    event.preventDefault()
    this.createPasswordToaster = toast('Creando contraseña...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    if (
      this.state.firstNumber &&
      this.state.secondNumber &&
      this.state.thirdNumber &&
      this.state.fourthNumber
    ) {
      try {
        const data = {
          securityPassword:
            this.state.firstNumber +
            this.state.secondNumber +
            this.state.thirdNumber +
            this.state.fourthNumber,
          uid: this.props.userInCollectionData.userKey
        }
        customConsoleLog("SENDED: ", data)
        let response = await postRequest('security/createSecurityPassword', data)
        customConsoleLog('addMoney response', response)
        if (response.data.success) {
          toast.update(this.createPasswordToaster, {
            render: 'Tu contraseña especial ha sido creada exitosamente.',
            autoClose: 5000,
          })
          this.setState({
            sendLoading: false,
            firstNumber: '',
            secondNumber: '',
            thirdNumber: '',
            fourthNumber: '',
            isCreatePasswordOpen: false,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, displayErrors: true, errorMessage: message })
          return toast.update(this.createPasswordToaster, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.createPasswordToaster, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        sendLoading: false
      })
      if (!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.createPasswordToaster, {
          render: 'Contraseña especial incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  //RESET SPECIAL PASSWORD
  onResetSpecialPasswordClick = async () => {
    this.setState({
      displayErrors: false,
      errorMessage: ""
    })
    this.resetSpecialPassToast = toast('Restableciendo contraseña especial...', { autoClose: false })
    const data = {
      mode: 'secretPasswordToken',
      uid: this.props.userInCollectionData.userKey,
    }
    customConsoleLog("SENDED: ", data)
    try {
      this.setState({ sendLoading: true })
      let response = await postRequest('security/generatePasswordTokenByEmail', data)
      customConsoleLog("RESPOSNE: ", response)
      if (response.data.success) {
        this.setState({
          sendLoading: false,
          sent: true,
          isSended: true
        })
        toast.update(this.resetSpecialPassToast, {
          render: "Casi listo, Ahora ingresa a tu correo y sigue las instrucciones...",
          autoClose: 5000
        })
      } else {
        this.setState({
          sendLoading: false,
          displayErrors: true,
          errorMessage: `${response.data.error.message}`
        })
      }
    } catch (error) {
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.update(this.resetSpecialPassToast, {
        render: errorMessage,
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
      this.setState({
        sendLoading: false,
        displayErrors: true,
        errorMessage,
      })
      customConsoleLog('catch err', error)
    }
  }
  openResetPassword = () => {
    this.setState({
      isResetPasswordOpen: true,
      sendDescription: '',
      sendEmail: '',
      sendAmount: '',
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  cancelResetPasswordClick = () => {
    this.setState({
      isResetPasswordOpen: false,
      isSended: false,
      displayErrors: false,
      errorMessage: '',
    })
  }
  render() {
    customConsoleLog("commercesIndex: ", this.state.commercesIndex)
    customConsoleLog("commerces: ", this.props.commerces)
    return (
      <div className={container}>
        <CommerceClientHeader />
        <CommercesTable
          commerces={this.props.commerces}
          title="COMERCIOS AFILIADOS"
          loading={this.props.loadingCommerces}
          onCommerceClick={this.selectCommerce}
          type='commercesSection'
          databaseRequestCommerces={this.databaseRequestCommerces}
        />
        <SendMoneyToCommerceModal
          amount={this.state.sendAmount}
          displayErrors={this.state.displayErrors}
          sendDescription={this.state.sendDescription}
          onChange={this.handleInputChange}
          onSubmit={this.sendPayment}
          loading={this.state.sendLoading}
          errorMessage={this.state.errorMessage}
          onClose={this.sendShow}
          showModal={this.state.sendShow}
          userName={this.state.selectedCommerce ? this.state.selectedCommerce.commerceName : ''}
          senderPicture={this.state.selectedCommerce ? this.state.selectedCommerce.profilePicture : ''}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'sendAmount' })}
          //special password
          firstNumber={this.state.firstNumber}
          secondNumber={this.state.secondNumber}
          thirdNumber={this.state.thirdNumber}
          fourthNumber={this.state.fourthNumber}
          isCreatePasswordOpen={this.state.isCreatePasswordOpen}
          cancelCreatePasswordClick={this.cancelCreatePasswordClick}
          onCreateSpecialPasswordClick={this.onCreateSpecialPasswordClick}
          openCreatePasswordClick={this.openCreatePasswordClick}
          hasSecurityPassword={this.state.hasSecurityPassword}
          //reset special password
          isResetPasswordOpen={this.state.isResetPasswordOpen}
          onResetSpecialPasswordClick={this.onResetSpecialPasswordClick}
          cancelResetPasswordClick={this.cancelResetPasswordClick}
          openResetPassword={this.openResetPassword}
          isSended={this.state.isSended}
        />
        {
          this.redirect()
        }
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn,
    //GET COMMERCES
    commerces: state.general.commerces,
    loadingCommerces: state.loadingCommerces,
  }
}
export default connect(mapStateToProps)(CommercesDashboard)