import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import { db } from '../../../firebase'
import CommerceHeader from './../commerce-client-header/CommerceClientHeader'
import Table from '../../general/tables/Table'
import OptionsModalWrapper from '../../general/OptionsModalWrapper'
import DateRangeModal from '../../general/modals/DateRangeModal'
import TransactionDetailsModal from '../../general/modals/transactionDetailsModal'
import moment from 'moment'
import { userLogout, getApprovedHistoryTransactions } from '../../../redux/actions/general'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { ToastContainer, toast } from 'react-toastify'
import { postRequest } from '../../../utils/createAxiosRequest'
import throttle from 'lodash.throttle'
const historyColumns = [
  { label: 'ID', key: 'transactionId' },
  { label: 'Fecha', key: 'date' },
  { label: 'Usuario', key: 'userImageNameAccount' },
  { label: 'Descripción', key: 'historyDescription' },
  { label: 'Monto', key: 'historyAmounts' },
]
const scrollDataRequestedRange = 10
class CommerceHistory extends Component {
  state = {
    balance: 'in',
    dateRangePickerIsOpen: false,
    transactionDetailModalIsOpen: false,
    selectedTransaction: "",
    selection: {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection',
    },
    isDateFilter: false,
    itemsIndex: 20,
    isUserBlocked: false,
    search: '',
    searchMode: "fields",
    filteredTransactions: "",
    isSearchingData: false,
    isDownloadSelectOpen: false,
    allUserTransactions: "",
    allUserTransactionsLoading: false,
  }
  historialHandleSwitch = ({ balance }) => () => {
    this.setState({
      balance: balance
    })
  }
  componentDidMount = () => {
    const userKey = this.props.userInCollectionData.userKey
    const transactionHistorylimitIndex = this.state.itemsIndex
    if (this.props.userInCollectionData) {
      const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
      isUserBlockRef.once('value', snapshot => {
        this.setState({
          isUserBlocked: snapshot.val(),
        })
      })
      this.props.dispatch(getApprovedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
    }
  }
  historyTransactionsRequest = () => {
    customConsoleLog("----get new history transactions----")
    this.setState({ itemsIndex: this.state.itemsIndex + scrollDataRequestedRange }, () => {
      const transactionHistorylimitIndex = this.state.itemsIndex
      const userKey = this.props.userInCollectionData.userKey
      this.props.dispatch(getApprovedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
    })
  }

  redirectUser = () => {
    if (this.props.isLoggedIn) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
      if (this.props.userInCollectionData.type) {
        const redirect = (
          this.props.userInCollectionData.type === 'commerce' ||
          this.props.userInCollectionData.type === 'client'
        )
          ? null
          : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  openDateRangeModal = () => {
    this.setState({
      dateRangePickerIsOpen: !this.state.dateRangePickerIsOpen,
      isDateFilter: false,
      selection: {
        ...this.state.selection,
        startDate: new Date(),
        endDate: new Date(),
      }
    })
  }
  searchDateRange = () => {
    this.setState({
      isDateFilter: true,
      dateRangePickerIsOpen: false,
      search: "",
    }, () => {
      this.throtled()
    })
  }
  handleSelect = (ranges) => {
    this.setState({
      selection: {
        ...this.state.selection,
        startDate: ranges.selection.startDate,
        endDate: ranges.selection.endDate,
      }
    })
  }
  //HISTORY MODAL
  closeTransactionDetailsModal = () => {
    this.setState({
      transactionDetailModalIsOpen: false,
      selectedTransaction: ""
    })
  }
  onHistoryTransactionClick = ({ transaction }) => () => {
    this.setState({
      selectedTransaction: transaction,
      transactionDetailModalIsOpen: true
    })
  }
  throtled = throttle(async () => {
    if (this.state.search || this.state.isDateFilter) {
      this.setState({ isSearchingData: true })
      const data = this.state.isDateFilter
        ? {
          uid: this.props.userInCollectionData.userKey,
          startDate: moment(this.state.selection.startDate).format('YYYY-MM-DD'),
          finishDate: moment(this.state.selection.endDate).format('YYYY-MM-DD'),
        }
        : {
          uid: this.props.userInCollectionData.userKey,
          query: this.state.search,
        }
      customConsoleLog("SENDED: ", data)
      try {
        let resp = await postRequest(`transactions/filter?mode=${this.state.isDateFilter ? "date" : "fields"}`,
          data)
        customConsoleLog("RESPONSE: ", resp)
        if (resp.data.success && resp.data.data.message.success) {
          this.setState({
            filteredTransactions: Object.values(resp.data.data.message.message),
            isSearchingData: false
          })
        } else {
          this.setState({
            filteredTransactions: "",
            isSearchingData: false
          })
        }
      } catch (error) {
        this.setState({ isSearchingData: false })
        customConsoleLog(error)
        toast.error("Verifique su conexión y vuelva a intentarlo.")
      }
    }
  }, 1000)
  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.throtled()
    }
  }
  updateSearch = (event) => {
    this.setState({
      search: event.target.value.substr(0, 120),
      isDateFilter: false,
    }, () => {
      this.throtled()
    })
  }
  closeDateFilter = () => {
    this.setState({
      isDateFilter: false,
      filteredTransactions: "",
    })
  }
  openDownloadBySelect = () => {
    const userKey = this.props.userInCollectionData.userKey
    if(this.state.isDownloadSelectOpen){
      this.setState({ isDownloadSelectOpen: false })
    } else {
      this.setState({ allUserTransactionsLoading: true }, () => {
        const userApprovedTransactions = db
        .ref(`transactionHistory/${userKey}`)
        .orderByChild("status")
        .equalTo("Approved")
        userApprovedTransactions.once('value', (snapshot) => {
          const transactions = Object.values(snapshot.val() || {})
          this.setState({
            allUserTransactions: transactions.sort((a, b) => b.timestamp - a.timestamp),
            allUserTransactionsLoading: false,
            isDownloadSelectOpen: true,
          })
        })
      })
    }
  }
  render() {
    // console.log("this.state.isDateFilter: ", this.state.isDateFilter)
    // console.log("filteredTransactions: ", this.state.filteredTransactions)
    const {
      filteredTransactions,
      search,
      isDateFilter,
    } = this.state
    const transactionsFilteredByEndpoint = search ||
      isDateFilter ||
      (filteredTransactions.length > 0 && search) ||
      (filteredTransactions.length > 0 && isDateFilter)
      ? filteredTransactions
      : this.props.approvedTransactionsHistory
    const finalFilteredTransactions = Object.values(transactionsFilteredByEndpoint || {}).filter(
      transaction => this.state.balance === 'in'
        ? transaction.mode === 'Receive' || transaction.mode === 'Deposit'
        : this.state.balance === 'out'
          ? transaction.mode === 'Send' || transaction.mode === 'Withdraw'
          : transaction.mode === 'Receive' ||
          transaction.mode === 'Deposit' ||
          transaction.mode === 'Send' ||
          transaction.mode === 'Withdraw'
    )
    return (
      <div>
        <CommerceHeader />
        <Table
          title="HISTORIAL DE TRANSACCIONES"
          columns={historyColumns}
          height={650}
          minHeight="621px" //height + 81px
          mobileMediaWidth="1100px"
          data={finalFilteredTransactions}
          isThereBackButton={false}
          loading={this.props.loadingApprovedTransactions}
          disableHeader={false}
          type="history"
          //table filter input
          onKeyPress={this.onKeyPress}
          updateSearch={this.updateSearch}
          isSearchingData={this.state.isSearchingData}
          historyIsDateFilter={isDateFilter}
          closeDateFilter={this.closeDateFilter}
          //history
          historialHandleSwitch={this.historialHandleSwitch}
          balance={this.state.balance}
          //date range
          openDateRangeModal={this.openDateRangeModal}
          //transaction detail modal
          onHistoryTransactionClick={this.onHistoryTransactionClick}
          //scrolling
          databaseRequest={this.historyTransactionsRequest}
          //download excel
          isDownloadSelectOpen={this.state.isDownloadSelectOpen}
          openDownloadBySelect={this.openDownloadBySelect}
          allUserTransactions={this.state.allUserTransactions}
          allUserTransactionsLoading={this.state.allUserTransactionsLoading}
        />
        <OptionsModalWrapper
          show={this.state.dateRangePickerIsOpen}
          onClose={this.openDateRangeModal}
          width={310}
        >
          <DateRangeModal
            onChange={this.handleSelect}
            moveRangeOnFirstSelection={false}
            ranges={this.state.selection}
            onSearchClick={this.searchDateRange}
          />
        </OptionsModalWrapper>
        <TransactionDetailsModal
          onClose={this.closeTransactionDetailsModal}
          showModal={this.state.transactionDetailModalIsOpen}
          transaction={this.state.selectedTransaction}
        />
        {
          this.redirectUser()
        }
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    isLoggedIn: state.general.isLoggedIn,
    //HISTORY TRANSACTIONS
    approvedTransactionsHistory: state.general.approvedTransactionsHistory,
    loadingApprovedTransactions: state.general.loadingApprovedTransactions,
  }
}

export default connect(mapStateToProps)(CommerceHistory)