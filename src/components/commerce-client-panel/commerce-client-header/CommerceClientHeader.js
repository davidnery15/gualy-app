import React, { Component } from 'react'
import { NavLink, Route, Link } from 'react-router-dom'
import { Navbar, Nav, NavItem } from 'reactstrap'
import { css } from 'emotion'
import { userLogout, getPendingHistoryTransactions } from '../../../redux/actions/general'
import { connect } from 'react-redux'
import moment from 'moment'
// eslint-disable-next-line
// import es from 'moment/locale/es'
import AddModal from '../../general/modals/AddModal'
import { db } from '../../../firebase'
import logo from './../../../assets/logos/logo-gualy-dark-bg.png'
import ProfileIcon from '../../general/ProfileIcon/ProfileIcon'
import WithdrawModal from '../../general/modals/WithdrawModal'
import { toast } from 'react-toastify'
import { BS2 } from '../../../config/currencies'
import iconReject from '../../../assets/help-icons/icon-bank-blue.svg'
import { banks, getBankName } from '../../../constants/bankLogos'
import { postRequest } from '../../../utils/createAxiosRequest'
import { customConsoleLog } from '../../../utils/customConsoleLog'
/* CSS */
const gualyNavbar = css`
    height: 100%;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    color: white;
    transition: all 0.2s;
    padding: 0 !important;
  `
const navbarBrandImage = css`
    margin: 0;
    img {
      height: 50px;
      width: 140px;
      margin: 0 55px;
    }
    @media (max-width: 500px) {
      & {
        img {
          width: 106.7px;
          height: 37px;
          margin: 0 0 0 15px;
        }
      }
    }
    @media (max-width: 347px) {
      margin: auto;
      margin-top: 15px;
    }
  `
const anchorLink = css`
    padding: 41px 10px;
    color: white;
    font-weight: 300;
    font-size: 14px;
    letter-spacing: 1.6px;
    outline: transparent;
    cursor:pointer;
    &:active {
      color: white;
      text-decoration: none;
    }
    &:visited {
      color: white;
      text-decoration: none;
    }
    &:hover {
      color: white;
      text-decoration: none;
    }
    @media (max-width: 1253px) {
      & {
        padding: 35px 20px;
      }
    }
    @media (max-width: 835px) {
      & {
        padding: 21px;
      }
    }
  `
const activeLink = css`
    width: 100%;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold !important;
    letter-spacing: 1.2px;
    color: #8078ff !important;
    fill: #8078ff;
  `
const activeLinkText = css`
  display: none;
  /* @media (max-width: 1130px) {
    display: inline;
  } */
  `
const flexLi = css`
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.1s ease-in-out;
    border-bottom: solid 4px transparent;
  `
const navItemLogoSVG = css`
    @media (max-width: 1284px) {
      transition: all 0.5s;
    }
  `
const navItemLogoSVG2 = css`
margin-bottom: 11px;
    @media (max-width: 1284px) {
      transition: all 0.5s;
    }
  `
const navItemLogo = css`
    fill: currentColor; 
  `
const navItemText = css`
    @media (max-width: 1130px) {
      display: none;
    }
  `
const leftSeparator = css`
    width: 1.2px;
    height: 100px;
    opacity: 0.5;
    background-color: #1d1f4a;
    margin-right: 30px;
    padding: 0;
    @media (max-width: 866px) {
      display: none;
      transition: all 0.5s;
    }
  `
const rightSeparator = css`
    width: 1.2px;
    height: 100%;
    opacity: 0.5;
    margin-right: 30px;
    @media (max-width: 386px) {
      margin-right: 10px;
      transition: all 0.5s;
    }
    @media (max-width: 330px) {
      margin-right: 2px;
      transition: all 0.5s;
    }
  `
const horizontalSeparator = css`
    display: none;
    
    @media (max-width: 866px) {
      width: 100%;
      height: 1.2px;
      opacity: 0.5;
      display: inline
      transition: all 0.5s;
      border-top: solid 1px #292929;
    }
  `
const NavItemContent = css`
    display: flex;
    justify-content: center;
    align-items: center;
  `
const navItemLogoSVGMargin = css`
    margin-right: 5px;
  `
const greenBackground = css`
    border-radius: 100px;
    background-color: #95fad8;
    border: none;
    padding-top: 3px;
    cursor: pointer;
  `
const rotate = css`
transform:          rotate(180deg);
-ms-transform:      rotate(180deg);
-moz-transform:     rotate(180deg);
-webkit-transform:  rotate(180deg);
-o-transform:       rotate(180deg);
  `
// const imageRoundStyle = css`
// width: 30px;
// height: 30px;
// border-radius: 50%;
// `
// const mainProfile = css`
// @media(min-width: 1130px) {
//   margin-right: 5px !important
// }
// `
const bankCard = css`
    margin: 0 5px;
    width: 104.3px;
    height: 104.4px;
    border-radius: 3.3px;
    box-shadow: 0 7px 7px 0 rgba(0,0,0,0.15), 0 0 7px 0 rgba(0,0,0,0.12);
    border: solid 0.5px transparent;
    background-color: white;
    cursor: pointer;
    padding: 10px 3px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: relative;
    outline: none !important;
    div{
      padding: 0 5px;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
      color: #242656;
      text-align: left;
      width: 104px;
      direction: rtl;
    }
    span {
      font-size: 9.9px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: normal;
      text-align: left;
    }
  `
const cssChange = css`
  -webkit-box-shadow: inset 0px 0px 0px 10px #20c997 !important;
  -moz-box-shadow: inset 0px 0px 0px 10px #20c997 !important;
  box-shadow: inset 0px 0px 0px 5px #20c997 !important;
  background-color: white !important;
  border: 1px solid !important;
`
const bankCardHoverOrFocus = css({
  ':hover,:focus': cssChange
})

// const inputHidden = css`
//     width: 100%;
//     height: 100%;
//     position: absolute;
//     opacity: 0;
//   `
const bankLogoCSS = css`
  width: 56.4px;
  height: 37.5px;
  object-fit: contain;
`
// const inputContainer = css`
//     position: relative;
//     padding: 0 !important;
//     margin: 20px;
//     overflow: hidden;
//     span {
//       opacity: 0.65;
//       font-size: 39.5px;
//       font-weight: normal;
//       font-style: normal;
//       font-stretch: normal;
//       line-height: normal;
//       letter-spacing: normal;
//       text-align: center;
//       color: #95fad8;
//     }
//   `
const ActiveNavItem = ({ children, to, title, ...props }) => (
  <Route
    path={to}
    {...props}
    children={({ match }) => (
      <NavItem className={`${flexLi} ${match ? css`border-bottom: solid 4px #8078ff;` : ""}`}>
        <Link className={`${anchorLink} ${match ? activeLink : ""}`} to={to}>
          <div className={NavItemContent}>
            {children}
            {match ? <span className={activeLinkText}>{title}</span> : null}
            <span className={navItemText}>{title}</span>
          </div>
        </Link>
      </NavItem>
    )}
  />
)

class FullHeader extends Component {
  state = {
    active: false,
    activeOnClick: false,
    show: false,
    showAddModal: false,
    userBankAccounts: [],
    //add modal
    requestShowAddModal: false,
    amount: '',
    AddModalCompanyName: 'Gualy Payment',
    AddModalCompanyRif: 'J-410269367',
    requestLoadingAddModal: false,
    paymentMethod: '',
    companyAccountNumbers: [],
    companyAccountNumber: '',
    displayErrors: false,
    isNextSeccion: false,
    userBank: '',
    transferNumber: '',
    operationDate: '',
    //withdraw modal
    selectedBankAccount: null,
    isLoadingWithdraw: false,
    withdrawAmount: '',
    errorMessage: ''
  }
  componentDidMount = () => {
    const bankAccountsRef = db.ref('systemSettings').child('bankAccounts')
    bankAccountsRef.on('value', async (snapshot) => {
      const data = Object.values(snapshot.val())
      this.setState({
        companyAccountNumbers: data
      })
    })
    if (Object.values(this.props.userInCollectionData || {}).length !== 0) {
      const userRef = db.ref('users').child(this.props.userInCollectionData.userKey).child('bankAccount')
      userRef.on('value', async (snapshot) => {
        const data = Object.values(snapshot.val() || {})
        this.setState({
          userBankAccounts: data
        })
      })
    }
  }
  showModal = () => {
    return !this.state.isLoadingWithdraw
      ? this.setState({
        show: !this.state.show,
        withdrawAmount: '',
        selectedBankAccount: ''
      })
      : null
  }
  ShowAddModal = () => {
    return !this.state.requestLoadingAddModal
      ? this.setState({
        requestShowAddModal: !this.state.requestShowAddModal,
        amount: '',
        isNextSeccion: false,
        companyAccountNumber: '',
        paymentMethod: '',
        displayErrors: false,
        userBank: '',
        transferNumber: '',
        operationDate: ''
      })
      : null
  }
  nextSeccion = (event) => {
    event.preventDefault();
    const transactionAmount = Number(this.state.amount.replace(/\./g, '').replace(',', '.'))
    if (
      transactionAmount > 0 &&
      this.state.paymentMethod !== '' &&
      this.state.companyAccountNumber !== ''
    ) {
      this.setState({ isNextSeccion: true })
    } else {
      this.setState({
        displayErrors: true
      })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe elegir un monto.' })
        return toast.error('Debe elegir un monto.')
      }
      if (event.target.paymentMethod.value === '') {
        this.setState({ errorMessage: 'Debe elegir un metodo de pago.' })
        event.target.paymentMethod.focus()
        return toast.error('Debe elegir un metodo de pago.')
      }
      if (event.target.companyAccountNumber.value === '') {
        this.setState({ errorMessage: 'Debe elegir un numero de cuenta.' })
        event.target.companyAccountNumber.focus()
        return toast.error('Debe elegir un numero de cuenta.')
      }
    }
  }
  amountInputHandleChange = ({ name }) => (maskedvalue) => {
    this.setState({ [name]: maskedvalue })
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  handleAccountNumberSelect = ({ target: { value }, target }) => {
    this.setState({
      [target.name]: value
    })
  }
  // handlepaymentMethodSelect = () => { }
  getCompanyAccountNumbers = items => items.map((item, i) => {
    if (item.enable) {
      return <option key={i} value={item.account}>{item.account}</option>
    } else {
      return null
    }
  })
  onAddPayment = async (event) => {
    event.preventDefault();
    this.toastLoaderAddId = toast('Añadiendo dinero...', { autoClose: false })
    this.setState({ requestLoadingAddModal: true, displayErrors: false, errorMessage: '' })
    const transactionAmount = Number(this.state.amount.replace(/\./g, '').replace(',', '.'))
    if (
      this.state.transferNumber.trim() !== '' &&
      this.state.operationDate !== '' &&
      this.state.selectedBankAccount !== ''
    ) {
      try {
        const data = {
          amount: transactionAmount,
          currency: BS2,
          date: moment(this.state.operationDate).format('DD-MM-YYYY'),
          dateFormat: moment(this.state.operationDate).format('MMMM DD'),
          senderUid: this.props.userInCollectionData.userKey,
          time: moment(this.state.operationDate).format('LTS'),
          bankReference: this.state.transferNumber.trim(),
          userBankAccount: this.state.selectedBankAccount,
          gualyBankAccount: this.state.companyAccountNumber
        }
        customConsoleLog("SENDED: ", data)
        let response = await postRequest('transactions/addMoneyToGualyByBankAccount', data)
        customConsoleLog('RESPONSE', response)
        if (response.data.success) {
          toast.update(this.toastLoaderAddId, {
            render: 'Tu solicitud ha sido enviada.',
            autoClose: 5000,
          })
          this.setState({
            requestLoadingAddModal: false,
            amount: '',
            isNextSeccion: false,
            companyAccountNumber: '',
            paymentMethod: '',
            displayErrors: false,
            userBank: '',
            transferNumber: '',
            operationDate: '',
            requestShowAddModal: false
          })
        } else {
          const { message } = response.data.error
          this.setState({ requestLoadingAddModal: false, displayErrors: true, errorMessage: message })
          return toast.update(this.toastLoaderAddId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderAddId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          requestLoadingAddModal: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        requestLoadingAddModal: false
      })
      if (!this.state.selectedBankAccount) {
        this.setState({ errorMessage: 'Selecciona tu cuenta bancaria desde donde se realizó la operación.' })
        return toast.update(this.toastLoaderAddId, {
          render: 'Selecciona tu cuenta bancaria desde donde se realizó la operación.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.transferNumber.value.trim() === '') {
        event.target.transferNumber.focus()
        this.setState({ errorMessage: 'Debe elegir un numero de transferencia o depósito.' })
        return toast.update(this.toastLoaderAddId, {
          render: 'Debe elegir un numero de transferencia o depósito.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (this.state.operationDate === '') {
        this.setState({ errorMessage: 'Debe elegir la fecha en la que se realizó la operación.' })
        return toast.update(this.toastLoaderAddId, {
          render: 'Debe elegir la fecha en la que se realizó la operación.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }

  getBankLogo = (bankAccountNumber) => {
    const bankId = bankAccountNumber.slice(0, 4)
    const filteredLogo = banks.filter(item => item.id === bankId)[0]
    const filteredLogoPhoto = filteredLogo ? filteredLogo.photo : iconReject
    return filteredLogoPhoto
  }

  getBankAccount = (selectedBankAccount, e) => {
    e.preventDefault()
    this.setState({ selectedBankAccount })
  }
  renderBankCards = bankAccounts => {
    if (bankAccounts) {
      return Object.values(bankAccounts).map((bankAccount, i) =>
        <button
          key={i}
          className={`${bankCard} ${bankCardHoverOrFocus} ${bankAccount.bankAccountID === this.state.selectedBankAccount ? cssChange : ''}`}
          onClick={(e) => this.getBankAccount(bankAccount.bankAccountID, e)}>
          <img className={bankLogoCSS} src={this.getBankLogo(bankAccount.bankAccountNumber)} alt='bank-logo' />
          <div css={`width: 80px !important; direction: ltr !important;position: absolute;bottom: 24px;`}>
            <span css={`font-weight: normal !important`}>Cuenta No.</span>
          </div>
          <div css={`width: 98px !important;`}>
            <span>{bankAccount.bankAccountNumber}</span>
          </div>
        </button>
      )
    } else {
      return <h1 css={`font-size: 25px`}>No hay cuentas registradas</h1>
    }
  }
  renderBankCardsToAddMoney = bankAccounts => {
    if (bankAccounts) {
      return Object.values(bankAccounts).map((bankAccount, i) =>
        <button
          key={i}
          className={`
        ${bankCard} ${bankCardHoverOrFocus} 
        ${
            `${getBankName((bankAccount.bankAccountNumber || '0000').slice(0, 4))} ${bankAccount.bankAccountNumber}`
              === this.state.selectedBankAccount ? cssChange : ''}
         `}
          onClick={(e) => this.getBankAccount(
            `${getBankName((bankAccount.bankAccountNumber || '0000').slice(0, 4))} ${bankAccount.bankAccountNumber}`
            , e)}>
          <img className={bankLogoCSS} src={this.getBankLogo(bankAccount.bankAccountNumber)} alt='bank-logo' />
          <div css={`width: 80px !important; direction: ltr !important;position: absolute;bottom: 24px;`}>
            <span css={`font-weight: normal !important`}>Cuenta No.</span>
          </div>
          <div css={`width: 98px !important;`}>
            <span>{bankAccount.bankAccountNumber}</span>
          </div>
        </button>
      )
    } else {
      return <h1 css={`font-size: 25px`}>No hay cuentas registradas</h1>
    }
  }
  toastLoaderId = null
  onWithdraw = async (e) => {
    e.preventDefault()
    this.setState({ isLoadingWithdraw: true, displayErrors: false, errorMessage: '' })
    this.toastLoaderId = toast('Cargando retiro...', { autoClose: false })
    const { selectedBankAccount, withdrawAmount } = this.state
    const transactionAmount = Number(withdrawAmount.replace(/\./g, '').replace(',', '.'))
    // console.log("withdrawAmount: ", transactionAmount)
    if (transactionAmount > 0) {
      if (selectedBankAccount) {
        const data = {
          amount: transactionAmount,
          currency: BS2,
          date: moment().format('YYYY-MM-DD'),
          time: moment().format('LTS'),
          uid: this.props.userInCollectionData.userKey,
          userBankAccount: selectedBankAccount
        }
        // console.log('SENDED ', data)
        try {
          let response = await postRequest('transactions/withDrawMoneyFromGualy', data)
          if (response.data.success) {
            const transactionHistorylimitIndex = 10
            const userKey = this.props.userInCollectionData.userKey
            this.props.dispatch(getPendingHistoryTransactions({ userKey, transactionHistorylimitIndex }))
            document.getElementById('withdrawForm').reset()
            this.setState({ isLoadingWithdraw: false, withdrawAmount: '', selectedBankAccount: '' })
            toast.update(this.toastLoaderId, {
              render: response.data.data.message,
              autoClose: 5000
            })
            setTimeout(() => {
              this.showModal()
            }, 1000);
          } else {
            this.setState({ isLoadingWithdraw: false, displayErrors: true, errorMessage: response.data.error.message })
            toast.update(this.toastLoaderId, {
              render: response.data.error.message,
              type: toast.TYPE.ERROR,
              autoClose: 5000
            })
          }
          // console.log('onWithdraw response', response)
        } catch (error) {
          const errorMessage = ({
            '401': 'Su sesión ha expirado',
            'default': 'Verifique su conexión y vuelva a intentarlo.'
          })[error.response ? error.response.status || 'default' : 'default']
          toast.update(this.toastLoaderId, {
            render: errorMessage,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
          this.setState({
            isLoadingWithdraw: false,
            errorMessage,
            displayErrors: true
          })
        }
      } else {
        this.setState({ isLoadingWithdraw: false, errorMessage: 'Debe seleccionar una cuenta.', displayErrors: true })
        return toast.update(this.toastLoaderId, {
          render: 'Debe seleccionar una cuenta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    } else {
      this.setState({ isLoadingWithdraw: false, errorMessage: 'Debe colocar un monto a retirar.', displayErrors: true })
      return toast.update(this.toastLoaderId, {
        render: 'Debe colocar un monto a retirar.',
        type: toast.TYPE.ERROR,
        autoClose: 5000
      })
    }
  }
  render() {
    const { show } = this.state
    return (
      <Navbar className={gualyNavbar}>
        <WithdrawModal
          amount={this.state.withdrawAmount}
          isLoading={this.state.isLoadingWithdraw}
          userBankAccounts={this.state.userBankAccounts}
          onSubmit={this.onWithdraw}
          banksCards={this.renderBankCards(this.state.userBankAccounts)}
          handleInputChange={this.handleInputChange}
          show={show}
          onClose={this.showModal}
          errorMessage={this.state.errorMessage}
          displayErrors={this.state.displayErrors}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'withdrawAmount' })}
        />
        <AddModal
          idFormName="addModal"
          extraIdFormName="nextSeccion"
          showModal={this.state.requestShowAddModal}
          loading={this.state.requestLoadingAddModal}
          onClose={this.ShowAddModal}
          onSubmit={this.onAddPayment}
          onChange={this.handleInputChange}
          handleAccountNumberSelect={this.handleAccountNumberSelect}
          paymentMethod={this.state.paymentMethod}
          companyAccountNumbers={this.state.companyAccountNumbers}
          getCompanyAccountNumbers={this.getCompanyAccountNumbers}
          companyAccountNumber={this.state.companyAccountNumber}
          rifNumber={this.state.AddModalCompanyRif}
          displayErrors={this.state.displayErrors}
          companyName={this.state.AddModalCompanyName}
          amount={this.state.amount}
          nextSeccion={this.nextSeccion}
          isNextSeccion={this.state.isNextSeccion}
          userBank={this.state.userBank}
          transferNumber={this.state.transferNumber}
          operationDate={this.state.operationDate}
          errorMessage={this.state.errorMessage}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'amount' })}
          banksCards={this.renderBankCardsToAddMoney(this.state.userBankAccounts)}
          userBankAccounts={this.state.userBankAccounts}
          updateDate={({ day, month, year, currentName }) => {
            if (day && month && year) {
              this.setState({
                [currentName]: `${year}-${month}-${day}`
              })
            }
          }}
        />
        <NavLink className={navbarBrandImage} exact={true} to='/'>
          <img src={logo} alt={'Gualy'} />
        </NavLink>
        <div className={leftSeparator}></div>
        <Nav className='mr-auto' css={`
            display: flex;
            padding-top: 10px !important;
            
            @media (max-width: 866px) {
            display: none;
            }
        `} >
          <ActiveNavItem exact={true} to='/dashboard' title="DASHBOARD">
            <svg
              className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`}
              xmlns="http://www.w3.org/2000/svg"
              width="17"
              height="18"
              viewBox="0 0 17 18">
              <path
                className={navItemLogo}
                fill="#F6F7FA"
                fillRule="evenodd"
                d="M5.91 10.75h4.73v7.093h5.91V8.386L8.275 0 0 8.386v9.457h5.91z"
              />
            </svg>
          </ActiveNavItem>
          <ActiveNavItem to='/historial' title="HISTORIAL">
            <svg
              className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`}
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              viewBox="0 0 18 18"
            >
              <path
                className={navItemLogo}
                fill="#F6F7FA"
                fillRule="evenodd"
                d="M8.961 0C4.01 0 0 4.019 0 8.97c0 4.951 4.01 8.97 8.961 8.97 4.96 0 8.979-4.019 8.979-8.97C17.94 4.019 13.921 0 8.961 0zm.009 16.146A7.174 7.174 0 0 1 1.794 8.97 7.174 7.174 0 0 1 8.97 1.794a7.174 7.174 0 0 1 7.176 7.176 7.174 7.174 0 0 1-7.176 7.176zm.449-11.661H8.073v5.382l4.71 2.826.672-1.104-4.036-2.395V4.485z"
              />
            </svg>
          </ActiveNavItem>
          {
            this.props.userInCollectionData
              ? this.props.userInCollectionData.type === 'client'
                ? <ActiveNavItem to='/directorio-comercios' title="COMERCIOS">
                  <svg
                    width="18"
                    height="18"
                    viewBox="0 0 16 18"
                    className={`${navItemLogoSVG} ${navItemLogoSVGMargin}`}
                  >
                    <defs></defs>
                    <g id="Historial-de-pagos" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="User_Historial_Mobile-Portrait" className={navItemLogo} transform="translate(-272.000000, -103.000000)" fill="#F6F7FA">
                        <g id="Header">
                          <g id="location_white" transform="translate(270.000000, 102.000000)">
                            <path d="M4.94834574,3.3733111 C7.79582701,0.542540845 12.4118802,0.541918436 15.2599876,3.3733111 C18.1074689,6.20408136 18.1080949,10.7930444 15.2599876,13.624437 L10.1041667,18.75 L4.94834574,13.624437 C2.10086447,10.7936668 2.10023839,6.20470377 4.94834574,3.3733111 Z M8.22932269,6.63503299 C7.19598189,7.66230943 7.19387496,9.33334414 8.22932269,10.3627151 C9.2626635,11.3899916 10.9435629,11.3920862 11.9790106,10.3627151 C13.0123514,9.33543871 13.0144584,7.66440399 11.9790106,6.63503299 C10.9456698,5.60775655 9.26477043,5.60566199 8.22932269,6.63503299 Z" id="Combined-Shape"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                </ActiveNavItem>
                : null
              : null
          }
          {/* <ActiveNavItem to='/perfil' title="PERFIL">
          { this.props.userInCollectionData ? <img className={`${imageRoundStyle} ${mainProfile}`} src={this.props.userInCollectionData ? this.props.userInCollectionData.profilePicture : null} alt={'Perfil'} /> : null}
          </ActiveNavItem> */}
          {
            this.props.userInCollectionData
              ? this.props.userInCollectionData.type !== 'client'
                ? < div className={flexLi}>
                  <div className={anchorLink}>
                    <button onClick={this.showModal} className={`btn-accept`}>
                      RETIRAR SALDO
                    </button>
                  </div>
                </div>
                : null
              : null
          }
          {
            this.props.userInCollectionData
              ? this.props.userInCollectionData.type !== 'client' && this.props.userInCollectionData.type !== 'commerce'
                ? < div className={flexLi}>
                  <div className={anchorLink}>
                    <button onClick={this.ShowAddModal} className={`btn-accept`}>
                      AÑADIR SALDO
              </button>
                  </div>
                </div>
                : null
              : null
          }

        </Nav>

        <Nav className='ml-auto' css={`padding-top: 10px !important;`}>
          {/* Search icon */}
          {/* <NavItem className={flexLi}>
            <Link className={anchorLink} to='/'>
              <svg className={navItemLogoSVG} xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                <path className={navItemLogo} fill="#F6F7FA" fillRule="evenodd" d="M12.497 11.314h-.8l-.3-.308c1-1.132 1.6-2.675 1.6-4.32 0-3.703-2.9-6.686-6.498-6.686C2.899 0 0 2.983 0 6.686s2.9 6.685 6.499 6.685c1.6 0 3.099-.617 4.199-1.645l.3.308v.823L15.995 18l1.5-1.543-4.999-5.143zm-5.998 0c-2.5 0-4.5-2.057-4.5-4.628 0-2.572 2-4.629 4.5-4.629 2.499 0 4.498 2.057 4.498 4.629 0 2.571-2 4.628-4.498 4.628z" />
              </svg>
            </Link>
          </NavItem> */}
          {/* Help section */}
          <ActiveNavItem to='/ayuda'>
            <svg className={navItemLogoSVG2} width="22px" height="22px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" >
              <g id="Perfil" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="User_Perfil_Mobile-Portrait" transform="translate(-230.000000, -34.000000)" fill="#F6F7FA">
                  <g id="Header">
                    <g id="User_topbar" transform="translate(181.000000, 29.000000)">
                      <path className={navItemLogo} d="M57.1,19.4 L58.9,19.4 L58.9,17.6 L57.1,17.6 L57.1,19.4 L57.1,19.4 Z M58,5 C53.032,5 49,9.032 49,14 C49,18.968 53.032,23 58,23 C62.968,23 67,18.968 67,14 C67,9.032 62.968,5 58,5 L58,5 Z M58,21.2 C54.031,21.2 50.8,17.969 50.8,14 C50.8,10.031 54.031,6.8 58,6.8 C61.969,6.8 65.2,10.031 65.2,14 C65.2,17.969 61.969,21.2 58,21.2 L58,21.2 Z M58,8.6 C56.011,8.6 54.4,10.211 54.4,12.2 L56.2,12.2 C56.2,11.21 57.01,10.4 58,10.4 C58.99,10.4 59.8,11.21 59.8,12.2 C59.8,14 57.1,13.775 57.1,16.7 L58.9,16.7 C58.9,14.675 61.6,14.45 61.6,12.2 C61.6,10.211 59.989,8.6 58,8.6 L58,8.6 Z" id="icon_help"></path>
                    </g>
                  </g>
                </g>
              </g>
            </svg>
          </ActiveNavItem>
          {/* Settings icon / Perfil icon */}
          <ActiveNavItem to='/perfil'>
            <ProfileIcon />
          </ActiveNavItem>
          {/* signOut icon */}
          <NavItem className={flexLi}>
            <Link to='/' className={anchorLink} onClick={() => this.props.dispatch(userLogout())}>
              <svg
                className={navItemLogoSVG}
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="18"
                viewBox="0 0 22 18">
                <path
                  className={navItemLogo}
                  fill="#F6F7FA"
                  fillRule="evenodd"
                  d="M13.542 11.836H4.378V5.66h9.164V.133l8.513 8.684-8.513 8.684v-5.665zm-3.466 4.025H2.335c-.145 0-.32-.195-.32-.493V2.476c0-.298.175-.493.32-.493h7.741V0H2.335C1.016 0 0 1.136 0 2.476v12.892c0 1.34 1.015 2.476 2.335 2.476h7.741v-1.983z"
                />
              </svg>
            </Link>
          </NavItem>
          <div className={rightSeparator}>
          </div>
        </Nav>
        <div className={horizontalSeparator}></div>
        <Navbar css={`
            display: none;
            @media (max-width: 866px) {
              padding: 0;
              display: flex;
              justify-content: center;
              align-items: center;
              width: 100%;
            }
        `} >
          <Nav >
            <ActiveNavItem exact={true} to='/dashboard'>
              <svg
                className={navItemLogoSVG}
                xmlns="http://www.w3.org/2000/svg"
                width="17"
                height="18"
                viewBox="0 0 17 18">
                <path
                  className={navItemLogo}
                  fill="#F6F7FA"
                  fillRule="evenodd"
                  d="M5.91 10.75h4.73v7.093h5.91V8.386L8.275 0 0 8.386v9.457h5.91z"
                />
              </svg>
            </ActiveNavItem>
            <ActiveNavItem to='/historial'>
              <svg
                className={navItemLogoSVG}
                xmlns="http://www.w3.org/2000/svg"
                width="18"
                height="18"
                viewBox="0 0 18 18"
              >
                <path
                  className={navItemLogo}
                  fill="#F6F7FA"
                  fillRule="evenodd"
                  d="M8.961 0C4.01 0 0 4.019 0 8.97c0 4.951 4.01 8.97 8.961 8.97 4.96 0 8.979-4.019 8.979-8.97C17.94 4.019 13.921 0 8.961 0zm.009 16.146A7.174 7.174 0 0 1 1.794 8.97 7.174 7.174 0 0 1 8.97 1.794a7.174 7.174 0 0 1 7.176 7.176 7.174 7.174 0 0 1-7.176 7.176zm.449-11.661H8.073v5.382l4.71 2.826.672-1.104-4.036-2.395V4.485z"
                />
              </svg>
            </ActiveNavItem>
            {
              this.props.userInCollectionData
                ? this.props.userInCollectionData.type === 'client'
                  ? <ActiveNavItem to='/directorio-comercios'>
                    <svg
                      className={`${navItemLogoSVG}`}
                      width="18"
                      height="18"
                      viewBox="0 0 16 18"
                    >
                      <defs></defs>
                      <g id="Historial-de-pagos" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <g id="User_Historial_Mobile-Portrait" className={navItemLogo} transform="translate(-272.000000, -103.000000)" fill="#F6F7FA">
                          <g id="Header">
                            <g id="location_white" transform="translate(270.000000, 102.000000)">
                              <path d="M4.94834574,3.3733111 C7.79582701,0.542540845 12.4118802,0.541918436 15.2599876,3.3733111 C18.1074689,6.20408136 18.1080949,10.7930444 15.2599876,13.624437 L10.1041667,18.75 L4.94834574,13.624437 C2.10086447,10.7936668 2.10023839,6.20470377 4.94834574,3.3733111 Z M8.22932269,6.63503299 C7.19598189,7.66230943 7.19387496,9.33334414 8.22932269,10.3627151 C9.2626635,11.3899916 10.9435629,11.3920862 11.9790106,10.3627151 C13.0123514,9.33543871 13.0144584,7.66440399 11.9790106,6.63503299 C10.9456698,5.60775655 9.26477043,5.60566199 8.22932269,6.63503299 Z" id="Combined-Shape"></path>
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                  </ActiveNavItem>
                  : null
                : null
            }
            {/* <ActiveNavItem to='/perfil'>
            {this.props.userInCollectionData ? <img className={imageRoundStyle} src={this.props.userInCollectionData ? this.props.userInCollectionData.profilePicture : null} alt={'Perfil'} /> : "Perfil"}
            </ActiveNavItem> */}
            {
              this.props.userInCollectionData
                ? this.props.userInCollectionData.type !== 'client'
                  ? <div className={flexLi}>
                    <div className={anchorLink}>
                      <button className={`${greenBackground} ${rotate}`} onClick={this.showModal} >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="18"
                          viewBox="0 0 18 20">
                          <path
                            fill="#2a2c6a"
                            fillRule="evenodd"
                            d="M15.725 7.95v10.007H1.95V7.95H0v11.957h17.675V7.95z M8.379 9.002V8.28H9.38v.732c.699.17 1.05.698 1.073 1.273h-.739c-.02-.418-.24-.702-.835-.702-.565 0-.902.254-.902.618 0 .318.244.522 1.002.719.759.197 1.571.521 1.571 1.47 0 .685-.518 1.063-1.17 1.186v.719H8.38v-.725c-.642-.137-1.19-.548-1.23-1.28h.735c.037.394.308.702.996.702.739 0 .902-.368.902-.599 0-.31-.167-.604-1.002-.805-.932-.224-1.57-.608-1.57-1.38 0-.645.52-1.066 1.169-1.206m-4.303 2.286a4.762 4.762 0 1 0 5.737-4.662V3.735l1.975 1.975 1.38-1.38L8.838 0l-4.33 4.33 1.38 1.38 1.974-1.975v2.892a4.763 4.763 0 0 0-3.786 4.66"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                  : null
                : null
            }
            {
              this.props.userInCollectionData
                ? this.props.userInCollectionData.type !== 'client' && this.props.userInCollectionData.type !== 'commerce'
                  ? <div className={flexLi}>
                    <div className={anchorLink}>
                      <button className={greenBackground} onClick={this.ShowAddModal} >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="18"
                          viewBox="0 0 18 20">
                          <path
                            fill="#2a2c6a"
                            fillRule="evenodd"
                            d="M15.725 7.95v10.007H1.95V7.95H0v11.957h17.675V7.95z M8.379 9.002V8.28H9.38v.732c.699.17 1.05.698 1.073 1.273h-.739c-.02-.418-.24-.702-.835-.702-.565 0-.902.254-.902.618 0 .318.244.522 1.002.719.759.197 1.571.521 1.571 1.47 0 .685-.518 1.063-1.17 1.186v.719H8.38v-.725c-.642-.137-1.19-.548-1.23-1.28h.735c.037.394.308.702.996.702.739 0 .902-.368.902-.599 0-.31-.167-.604-1.002-.805-.932-.224-1.57-.608-1.57-1.38 0-.645.52-1.066 1.169-1.206m-4.303 2.286a4.762 4.762 0 1 0 5.737-4.662V3.735l1.975 1.975 1.38-1.38L8.838 0l-4.33 4.33 1.38 1.38 1.974-1.975v2.892a4.763 4.763 0 0 0-3.786 4.66"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                  : null
                : null
            }
          </Nav>
        </Navbar>
      </Navbar >
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(FullHeader)