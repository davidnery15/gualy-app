import React, { Component } from 'react'
import { css } from 'emotion'
import moment from 'moment'
import CountUp from 'react-countup'
import { ToastContainer, toast } from 'react-toastify'
import { style } from "react-toastify"
import { db } from './../../../../firebase'
import RequestModal from '../../../general/modals/RequestModal'
import OptionsModalWrapper from '../../../general/OptionsModalWrapper'
import { BS, BS2 } from '../../../../config/currencies.js'
import Spinner from 'react-spinkit'
import SendModal from '../../../general/modals/SendModal'
import { postRequest } from '../../../../utils/createAxiosRequest'
import { emailsRegex } from '../../../../constants/regex'
import { customConsoleLog } from '../../../../utils/customConsoleLog'
style({
  colorProgressDefault: "#95fad8",
  fontFamily: "Montserrat",
});
const welcomeBody = css`
    display: flex;
    flex-direction: column;
    width: auto;
    h1{
      margin: 0;
      font-size: 24px;
      text-align: left;
    }
    @media(max-width: 800px) {
      padding: 18px 25px 8px 25px;
    }
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 26px;
    left: 0;
  `
const divisor = css`
    width: 55px;
    border: 1px solid;
    margin-top: 10px;
  `
const cardData = css`
    width: 100%;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    flex-wrap: wrap;
    margin-top: 12px;
    height: 100%;
    h3{
      font-family: Montserrat;
      font-size: 16px;
      text-align: left;
      margin: 0;
    }
    p{
      margin: 0 !important;
      margin-top: 5px;
      opacity: 0.65;
      font-family: Montserrat;
      font-size: 11px;
      text-align: left;
      color: #f6f7fa;
    }
    @media(max-width: 800px) {
      flex-direction: row;
    }
  `
const controlBody = css`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    width: 100%;
    justify-content: center;
    @media(max-width: 1048px) {
      flex-direction: column
      justify-content: center;
    }
    @media(max-width: 800px) {
      margin-top: 10px;
      background-color: rgba(0, 0, 0, 0.25);
      border-top: 1px solid black; 
      padding: 8px 25px 18px 25px;
    }
  `
const body = css`
    display: flex;
    flex-direction: row;
    justify-content: center;
    @media(max-width: 1048px) {
      flex-direction: column;
    }
    @media(max-width: 800px) {
      margin-top: 10px;
      flex-direction: row;
      width: 100%;
      justify-content: space-between;
    }
    @media(max-width: 800px) {
      flex-direction: column;
      align-items:center;
    }
  `
const buttonBody = css`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width:337px;
    @media(max-width: 1048px) {
      margin-top: 10px;
    }
    @media(max-width: 800px) {
      margin-top: 0;
    }
    @media(max-width: 800px) {
      width:100%;
      margin-top: 10px;
      flex-direction: column;
    }
  `
const balanceDiv = css`
    display: flex;
    min-height: 65px;
    flex-direction: column;
    justify-content: center;
    position: relative;
    h3{
      font-family: Montserrat;
      font-size: 14px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      opacity: 0.65;
      margin: 0;
    }
    h2 {
      font-family: Montserrat;
      font-size: 32px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #8078ff;
      margin: 0;
    }
    @media(max-width: 800px) {
      align-self: flex-start;
    }
    @media(max-width: 800px) {
      align-self: center;
    }
    @media(max-width: 400px) {
      h2 {
        font-size: 100%;
      }
    }
  `
const divisorBalance = css`
    border: solid 1px #f6f7fa;
    margin: 0 20px;
    opacity: 0.65;
    height: 40px;
    @media(max-width: 1048px) {
      display: none;
    }
    @media(max-width: 800px) {
      display: inherit;
      align-self: center;
    }
    @media(max-width: 800px) {
      display: none;
    }
  `
const responsive = css`
    @media(max-width: 800px) {
      flex-direction: column;
      padding: 0 !important;
    }
  `
const responsiveTransparentBtn = css`
    @media(max-width: 800px) {
      margin-top: 0;
    }
    @media(max-width: 800px) {
      margin-top: 10px !important;
    }
  `

const displayErrors = css`
  input:invalid {
    border-color:red;
  }
  textarea:invalid {
    border-color:red;
  }
`


export default class CommerceWelcome extends Component {
  state = {
    requestLoading: false,
    requestShow: false,
    requestAmount: '',
    requestDescription: '',
    requestEmail: '',
    sendEmail: '',
    sendDescription: '',
    sendAmount: '',
    requestQR: false,
    sendLoading: false,
    sendShow: false,
    scan: false,
    delay: 300,
    displayErrors: false,
    errorMessage: '',
    //special password
    firstNumber: '',
    secondNumber: '',
    thirdNumber: '',
    fourthNumber: '',
    isCreatePasswordOpen: false,
    //reset special password
    isResetPasswordOpen: false,
    isSended: false,
  }
  openScan = () => {
    this.setState({ scan: !this.state.scan })
  }
  handleScan = (data) => {
    if (data) {
      const [, userKey, newMount, description] = data.split('-')
      // customConsoleLog("userKey:", userKey)
      // customConsoleLog("newMount:", newMount)
      // customConsoleLog("description:", description)
      db.ref(`users/${userKey}`).once('value', (snap) => {
        if (snap.val()) {
          let user = snap.val();
          // customConsoleLog("snap: ", user)
          this.setState({
            sendEmail: user.email,
            sendAmount: newMount ? newMount : '',
            sendDescription: description ? description : '',
            scan: false,
          })
        }
      });
    }
  }
  handleError = (err) => {
    console.error(err)
  }
  amountInputHandleChange = ({ name }) => (maskedvalue) => {
    this.setState({ [name]: maskedvalue })
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  toastLoaderRequestId = null
  toastLoaderSendId = null
  sendPayment = async (event) => {
    event.preventDefault();
    this.toastLoaderSendId = toast('Enviando dinero...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    const { sendAmount, sendEmail, sendDescription } = this.state
    const transactionAmount = Number(sendAmount.replace(/\./g, '').replace(',', '.'))
    // customConsoleLog("sendAmount: ", transactionAmount)
    if (
      transactionAmount > 0 &&
      sendEmail.trim() &&
      sendEmail.trim().match(emailsRegex) &&
      transactionAmount <= this.props.userAmount &&
      (
        this.props.hasSecurityPassword
          ? this.state.firstNumber &&
          this.state.secondNumber &&
          this.state.thirdNumber &&
          this.state.fourthNumber
          : true
      )
    ) {
      const data = {
        receiverEmail: sendEmail.trim(),
        senderUid: this.props.data.userKey,
        date: moment().format('YYYY-MM-DD'),
        description: sendDescription.trim(),
        currency: BS2,
        amount: transactionAmount,
        time: moment().format('LTS'),
        securityPassword:
          this.state.firstNumber +
          this.state.secondNumber +
          this.state.thirdNumber +
          this.state.fourthNumber,
      }
      customConsoleLog("SEND:", data)
      try {
        let response = await postRequest('transactions/sendMoney', data)
        customConsoleLog('sendPayment response', response)
        if (response.data.success) {
          this.props.historyTransactionsRequest()
          this.setState({
            sendLoading: false,
            displayErrors: false,
            firstNumber: '',
            secondNumber: '',
            thirdNumber: '',
            fourthNumber: '',
          })
          this.sendShow()
          toast.update(this.toastLoaderSendId, {
            render: 'Dinero enviado exitosamente',
            autoClose: 5000,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, errorMessage: message, displayErrors: true })
          return toast.update(this.toastLoaderSendId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderSendId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ sendLoading: false, displayErrors: true })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe colocar un monto.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un monto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (transactionAmount > this.props.userAmount) {
        this.setState({ errorMessage: 'Saldo insuficiente.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Saldo insuficiente.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!sendEmail.trim()) {
        this.setState({ errorMessage: 'Debe colocar un correo.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un correo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!sendEmail.trim().match(emailsRegex)) {
        this.setState({ errorMessage: 'Debe colocar un correo valido.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un correo valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Contraseña especial incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  requestPayment = async (e) => {
    e.preventDefault()
    this.toastLoaderRequestId = toast('Enviando solicitud...', { autoClose: false })
    this.setState({ requestLoading: true, displayErrors: false, errorMessage: '' })
    const { requestAmount, requestDescription, requestEmail } = this.state
    const transactionAmount = Number(requestAmount.replace(/\./g, '').replace(',', '.'))
    if (
      transactionAmount > 0 &&
      requestEmail.trim() &&
      requestEmail.trim().match(emailsRegex)
    ) {
      const data = {
        senderEmail: requestEmail.trim(),
        receiverUid: this.props.data.userKey,
        date: moment().format('YYYY-MM-DD'),
        description: requestDescription.trim(),
        currency: BS2,
        source: 'moneyRequest',
        amount: transactionAmount,
        time: moment().format('LTS')
      }
      customConsoleLog('SENDED', data)
      try {
        let response = await postRequest('transactions/makeMoneyRequest', data)
        customConsoleLog('requestPayment response', response)
        if (response.data.success) {
          this.props.moneyRequestTransactionsRequest()
          this.setState({
            requestAmount: '',
            requestDescription: '',
            requestEmail: '',
            requestLoading: false,
            displayErrors: false
          })
          document.getElementById('requestPaymentForm').reset()
          toast.update(this.toastLoaderRequestId, {
            render: 'Solicitud enviada exitosamente',
            autoClose: 5000,
          })
          setTimeout(() => {
            this.requestShow()
          }, 1000);
        } else {
          const { message } = response.data.error
          this.setState({ requestLoading: false, displayErrors: true, errorMessage: message })
          return toast.update(this.toastLoaderRequestId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderRequestId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          requestLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ requestLoading: false, displayErrors: true })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe colocar un monto.' })
        return toast.update(this.toastLoaderRequestId, {
          render: 'Debe colocar un monto a retirar.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!requestEmail.trim()) {
        this.setState({ errorMessage: 'Debe colocar un correo.' })
        return toast.update(this.toastLoaderRequestId, {
          render: 'Debe colocar un correo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!requestEmail.trim().match(emailsRegex)) {
        this.setState({ errorMessage: 'Debe colocar un correo valido.' })
        return toast.update(this.toastLoaderRequestId, {
          render: 'Debe colocar un correo valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  sendShow = () => {
    return !this.state.sendLoading
      ? this.setState({
        scan: false,
        sendDescription: '',
        sendEmail: '',
        sendAmount: '',
        sendShow: this.state.sendLoading ? true : !this.state.sendShow,
        displayErrors: false,
        isCreatePasswordOpen: false
      })
      : null
  }
  requestShow = () => {
    return !this.state.requestLoading
      ? this.setState({
        requestShow: !this.state.requestShow,
        requestQR: false,
        requestAmount: '',
        requestDescription: '',
        requestEmail: '',
        displayErrors: false
      })
      : null
  }
  onRequestQR = e => {
    e.preventDefault()
    customConsoleLog('onRequestQR')
    this.setState({ requestQR: !this.state.requestQR })
  }
  cancelCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: false,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  openCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: true,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  onCreateSpecialPasswordClick = async (event) => {
    event.preventDefault()
    this.createPasswordToaster = toast('Creando contraseña...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    if (
      this.state.firstNumber &&
      this.state.secondNumber &&
      this.state.thirdNumber &&
      this.state.fourthNumber
    ) {
      try {
        const data = {
          securityPassword:
            this.state.firstNumber +
            this.state.secondNumber +
            this.state.thirdNumber +
            this.state.fourthNumber,
          uid: this.props.data.userKey
        }
        customConsoleLog("SENDED: ", data)
        let response = await postRequest('security/createSecurityPassword', data)
        customConsoleLog('addMoney response', response)
        if (response.data.success) {
          toast.update(this.createPasswordToaster, {
            render: 'Tu contraseña especial ha sido creada exitosamente.',
            autoClose: 5000,
          })
          this.setState({
            sendLoading: false,
            firstNumber: '',
            secondNumber: '',
            thirdNumber: '',
            fourthNumber: '',
            isCreatePasswordOpen: false,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, displayErrors: true, errorMessage: message })
          return toast.update(this.createPasswordToaster, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        customConsoleLog("error: ", error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.createPasswordToaster, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        sendLoading: false
      })
      if (!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.createPasswordToaster, {
          render: 'Contraseña especial incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  //RESET SPECIAL PASSWORD
  onResetSpecialPasswordClick = async () => {
    this.setState({
      displayErrors: false,
      errorMessage: ""
    })
    this.resetSpecialPassToast = toast('Restableciendo contraseña especial...', { autoClose: false })
    const data = {
      mode: 'secretPasswordToken',
      uid: this.props.data.userKey,
    }
    customConsoleLog("SENDED: ", data)
    try {
      this.setState({ sendLoading: true })
      let response = await postRequest('security/generatePasswordTokenByEmail', data)
      customConsoleLog("RESPOSNE: ", response)
      if (response.data.success) {
        this.setState({
          sendLoading: false,
          sent: true,
          isSended: true
        })
        toast.update(this.resetSpecialPassToast, {
          render: "Casi listo, Ahora ingresa a tu correo y sigue las instrucciones...",
          autoClose: 5000
        })
      } else {
        this.setState({
          sendLoading: false,
          displayErrors: true,
          errorMessage: `${response.data.error.message}`
        })
      }
    } catch (error) {
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.update(this.resetSpecialPassToast, {
        render: errorMessage,
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
      this.setState({
        sendLoading: false,
        displayErrors: true,
        errorMessage,
      })
      customConsoleLog('catch err', error)
    }
  }
  openResetPassword = () => {
    this.setState({
      isResetPasswordOpen: true,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  cancelResetPasswordClick = () => {
    this.setState({
      isResetPasswordOpen: false,
      isSended: false,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  render() {
    const {
      requestShow,
      requestAmount,
      requestDescription,
      requestEmail,
      sendShow,
      sendAmount,
      sendDescription,
      sendEmail,
      requestLoading,
      sendLoading,
      scan
    } = this.state
    const requestData = {
      amount: requestAmount,
      description: requestDescription,
      email: requestEmail,
      userKey: this.props.data ? this.props.data.userKey : "",
      formNames: {
        idFormName: 'requestPaymentForm',
        amountName: 'requestAmount',
        descriptionName: 'requestDescription',
        emailName: 'requestEmail'
      }
    }
    return (
      <div className={`
      ${(this.state.displayErrors === true ? displayErrors : '')} 
      ${responsive} component-card
      `}
      >
        <div className={welcomeBody}>
          <h1>¡Bienvenido a <b>{this.props.data ? this.props.data.commerceName : 'Gualy'}</b>!</h1>
          <div className={divisor}></div>
          <div className={cardData}>
            <h3>Has iniciado sesión como <b>{`${this.props.data ? this.props.data.firstName : '...'} ${this.props.data ? this.props.data.lastName : '...'}`}</b></h3>
            <p>Tú último inicio de sesión fue el {this.props.lastSignInTime ? this.props.lastSignInTime : '...'}</p>
          </div>
        </div>
        <div className={controlBody}>
          <div className={body}>
            <div className={balanceDiv}>
              <h3>Balance Actual</h3>
              <h2>
                {
                  this.props.userAmount || this.props.userAmount === 0 ?
                    <CountUp
                      start={0}
                      end={this.props.userAmount}
                      duration={0.5}
                      separator="."
                      decimals={2}
                      decimal=","
                      prefix={BS}
                      redraw={!(requestShow || sendShow)}
                    />
                    :
                    null
                }
              </h2>
              {
                !this.props.userAmount && this.props.userAmount !== 0 ?
                  <div className={spinnerContainer}> <Spinner color='white' /></div>
                  : null
              }
            </div>
            <div className={buttonBody}>
              <div className={divisorBalance}></div>
              {/* <NavLink to='/comercios'> */}
              <button className='btn-accept' onClick={this.requestShow}>SOLICITAR&nbsp;PAGO</button>
              {/* </NavLink> */}
              {/* <NavLink to='/notificaciones'> */}
              <button
                className={`${responsiveTransparentBtn} btn-accept-transparent`}
                onClick={this.sendShow}>
                ENVIAR&nbsp;PAGO
                    </button>
              {/* </NavLink> */}
            </div>
          </div>
        </div>
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
        <RequestModal
          showModal={requestShow}
          loading={requestLoading}
          data={requestData}
          displayErrors={this.state.displayErrors}
          onClose={this.requestShow}
          onSubmit={this.requestPayment}
          onChange={this.handleInputChange}
          requestQR={this.state.requestQR}
          onChangeQR={this.onRequestQR}
          errorMessage={this.state.errorMessage}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'requestAmount' })}
        />
        <OptionsModalWrapper show={sendShow} onClose={this.sendShow} width="310px">
          <SendModal
            name='sendAmount'
            amount={sendAmount}
            displayErrors={this.state.displayErrors}
            sendDescription={sendDescription}
            sendEmail={sendEmail}
            scan={scan}
            onChange={this.handleInputChange}
            openScan={this.openScan}
            onSubmit={this.sendPayment}
            loading={sendLoading}
            deley={this.state.deley}
            handleError={this.handleError}
            handleScan={this.handleScan}
            errorMessage={this.state.errorMessage}
            amountInputHandleChange={this.amountInputHandleChange({ name: 'sendAmount' })}
            //special password
            firstNumber={this.state.firstNumber}
            secondNumber={this.state.secondNumber}
            thirdNumber={this.state.thirdNumber}
            fourthNumber={this.state.fourthNumber}
            isCreatePasswordOpen={this.state.isCreatePasswordOpen}
            cancelCreatePasswordClick={this.cancelCreatePasswordClick}
            onCreateSpecialPasswordClick={this.onCreateSpecialPasswordClick}
            openCreatePasswordClick={this.openCreatePasswordClick}
            hasSecurityPassword={this.props.hasSecurityPassword}
            //reset special password
            isResetPasswordOpen={this.state.isResetPasswordOpen}
            onResetSpecialPasswordClick={this.onResetSpecialPasswordClick}
            cancelResetPasswordClick={this.cancelResetPasswordClick}
            openResetPassword={this.openResetPassword}
            isSended={this.state.isSended}
            specialPasswordIsShow={true}
          />
        </OptionsModalWrapper>
      </div>
    )
  }
}