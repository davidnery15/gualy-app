import React, { Component } from 'react'
import { css } from 'emotion'
import moment from 'moment'
import { ToastContainer, toast } from 'react-toastify'
import { style } from "react-toastify"
import { db } from './../../../../firebase'
import RequestModal from '../../../general/modals/RequestModal'
import OptionsModalWrapper from '../../../general/OptionsModalWrapper'
import { BS2 } from '../../../../config/currencies.js'
//ADD MONEY AND WITHDRAW
import AddModal from '../../../general/modals/AddModal'
import WithdrawModal from '../../../general/modals/WithdrawModal'
import iconReject from './../../../../assets/help-icons/icon-bank-blue.svg'
import { banks, getBankName } from '../../../../constants/bankLogos'
import SendModal from '../../../general/modals/SendModal'
import ProfileCard from './ProfileCard'
import IwantCard from './IwantCard'
import CommercesTable from '../../../general/tables/commerces/CommerceTable'
import SendMoneyToCommerceModal from '../../../general/modals/SendMoneyToCommerceModal'
import { postRequest } from '../../../../utils/createAxiosRequest'
import Table from '../../../general/tables/Table'
import { emailsRegex } from '../../../../constants/regex'
import { customConsoleLog } from '../../../../utils/customConsoleLog'

style({
  colorProgressDefault: "#95fad8",
  fontFamily: "Montserrat",
});

const displayErrors = css`
  input:invalid {
    border-color:red;
  }
  textarea:invalid {
    border-color:red;
  }
`
const mainContainer = css`
    display: flex;
    justify-content: center;
    flex-direction: row;
    @media(max-width: 1200px){
      flex-direction: column;
    }
`
//ADD MONEY AND WITHDRAW
const bankCard = css`
    margin: 0 5px;
    width: 104.3px;
    height: 104.4px;
    border-radius: 3.3px;
    box-shadow: 0 7px 7px 0 rgba(0,0,0,0.15), 0 0 7px 0 rgba(0,0,0,0.12);
    border: solid 0.5px transparent;
    background-color: white;
    cursor: pointer;
    padding: 10px 3px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: relative;
    outline: none !important;
    div{
      padding: 0 5px;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
      color: #242656;
      text-align: left;
      width: 104px;
      direction: rtl;
    }
    span {
      font-size: 9.9px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: normal;
      text-align: left;
    }
  `
const cssChange = css`
    -webkit-box-shadow: inset 0px 0px 0px 10px #20c997 !important;
    -moz-box-shadow: inset 0px 0px 0px 10px #20c997 !important;
    box-shadow: inset 0px 0px 0px 5px #20c997 !important;
    background-color: white !important;
    border: 1px solid !important;
  `
const bankCardHoverOrFocus = css({
  ':hover,:focus': cssChange
})
const bankLogoCSS = css`
  width: 56.4px;
  height: 37.5px;
  object-fit: contain;
`
const isIwantShow = css`
  @media(min-width: 1440px){
    display: none !important;
  }
`
const secondHdCardsContainer = css`
display: none;
@media(min-width: 1440px){
  display: flex;
  flex-direction: column;
  width: 930px;
}
`
const firstHdCardsContainer = css`
display: none;
@media(min-width: 1440px){
  display: flex;
  flex-direction: column;
}
`
const commercesHdClass = css`
width: 360px !important;
height: 610px !important;
margin-top: 25px;
margin-right: 30px;
`
const DestopMobileCommercesClass = css`
@media(min-width: 1440px) {
  display: none !important
}
  `
const HDrecentTable = css`
margin-left: 0 !important;
margin-right: 0 !important;
display: none !important;
margin-bottom: 0 !important;
  @media(min-width: 1440px){
    display: flex !important;
  }
  `


export default class ClientWelcome extends Component {
  state = {
    requestLoading: false,
    requestShow: false,
    requestAmount: '',
    requestDescription: '',
    requestEmail: '',
    sendEmail: '',
    sendDescription: '',
    sendAmount: '',
    sendToCommerceAmount: '',
    requestQR: false,
    sendToCommerceLoading: false,
    sendLoading: false,
    sendToCommerceShow: false,
    sendShow: false,
    scan: false,
    delay: 300,
    displayErrors: false,
    //ADD MONEY AND WITHDRAW
    active: false,
    show: false,
    showAddModal: false,
    userBankAccounts: [],
    //add modal
    requestShowAddModal: false,
    amount: '',
    AddModalCompanyName: 'Gualy Payment',
    AddModalCompanyRif: 'J-410269367',
    requestLoadingAddModal: false,
    paymentMethod: 'Transfer',
    companyAccountNumbers: [],
    companyAccountNumber: '',
    isNextSeccion: false,
    userBank: '',
    transferNumber: '',
    operationDate: '',
    //withdraw modal
    selectedBankAccount: null,
    isLoadingWithdraw: false,
    withdrawAmount: '',
    errorMessage: '',
    selectedCommerce: '',
    //special password
    firstNumber: '',
    secondNumber: '',
    thirdNumber: '',
    fourthNumber: '',
    isCreatePasswordOpen: false,
    //reset special password
    isResetPasswordOpen: false,
    isSended: false,
  }
  componentDidMount = () => {
    //ADD MONEY AND WITHDRAW
    const bankAccountsRef = db.ref('systemSettings').child('bankAccounts')
    bankAccountsRef.on('value', async (snapshot) => {
      const data = Object.values(snapshot.val())
      this.setState({
        companyAccountNumbers: data
      })
    })
    if (Object.values(this.props.data || {}).length !== 0) {
      const usersRef = db.ref('users').child(this.props.data.userKey).child('bankAccount')
      usersRef.on('value', async (snapshot) => {
        const data = Object.values(snapshot.val() || {})
        this.setState({
          userBankAccounts: data
        })
      })
    }
  }

  openScan = () => {
    this.setState({ scan: !this.state.scan })
  }
  handleScan = (data) => {
    if (data) {
      const [, userKey, newMount, description] = data.split('-')
      if (userKey) {
        // "userKey:", userKey)
        // "newMount:", newMount)
        // "description:", description)
        db.ref(`users/${userKey}`).once('value', (snap) => {
          customConsoleLog("snap.val(): ", snap.val())
          if (snap.val()) {
            let user = snap.val();
            // "snap: ", user)
            this.setState({
              sendEmail: user.email,
              sendAmount: newMount ? newMount : '',
              sendDescription: description ? description : '',
              scan: false,
              displayErrors: false,
              errorMessage: ""
            })
          } else {
            this.openScan()
            this.setState({
              displayErrors: true,
              errorMessage: "Codigo QR incorrecto.",
              sendEmail: "",
              sendAmount: "",
              sendDescription: "",
            })
          }
        });
      } else {
        this.openScan()
        this.setState({ displayErrors: true, errorMessage: "Codigo QR incorrecto." })
      }
    }
  }
  handleError = (err) => {
    console.error(err)
  }
  amountInputHandleChange = ({ name }) => (maskedvalue) => {
    this.setState({ [name]: maskedvalue })
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  toastLoaderRequestId = null
  toastLoaderSendId = null
  sendPayment = async (event) => {
    event.preventDefault();
    this.toastLoaderSendId = toast('Enviando dinero...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    const { sendAmount, sendEmail, sendDescription } = this.state
    const transactionAmount = Number(sendAmount.replace(/\./g, '').replace(',', '.'))
    // 'transactionAmount: ', transactionAmount)
    if (
      (transactionAmount > 0 && transactionAmount !== 0) &&
      sendEmail.trim() &&
      sendEmail.trim().match(emailsRegex) &&
      transactionAmount <= this.props.userAmount &&
      (
        this.props.hasSecurityPassword
          ? this.state.firstNumber &&
          this.state.secondNumber &&
          this.state.thirdNumber &&
          this.state.fourthNumber
          : true
      )
    ) {
      const data = {
        receiverEmail: sendEmail.trim(),
        senderUid: this.props.data.userKey,
        date: moment().format('YYYY-MM-DD'),
        description: sendDescription.trim(),
        currency: BS2,
        amount: transactionAmount,
        time: moment().format('LTS'),
        securityPassword:
          this.state.firstNumber +
          this.state.secondNumber +
          this.state.thirdNumber +
          this.state.fourthNumber
      }
      customConsoleLog("SENDED:", data)
      try {
        let response = await postRequest('transactions/sendMoney', data)
        customConsoleLog('RESPONSE', response)
        if (response.data.success) {
          this.props.approvedHistoryTransactionsRequest()
          this.props.moneyRequestTransactionsRequest()
          this.setState({
            sendLoading: false,
            displayErrors: false
          })
          this.sendShow()
          toast.update(this.toastLoaderSendId, {
            render: 'Dinero enviado exitosamente',
            autoClose: 5000,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, errorMessage: message, displayErrors: true })
          return toast.update(this.toastLoaderSendId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        customConsoleLog("sendPayment error: ", error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderSendId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ sendLoading: false, displayErrors: true })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe colocar un monto.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un monto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (transactionAmount > this.props.userAmount) {
        this.setState({ errorMessage: 'Saldo insuficiente.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Saldo insuficiente.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!sendEmail.trim()) {
        this.setState({ errorMessage: 'Debe colocar un correo.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un correo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!sendEmail.trim().match(emailsRegex)) {
        this.setState({ errorMessage: 'Debe colocar un correo valido.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar un correo valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if ((!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) &&
        this.props.hasSecurityPassword) {
        this.setState({ errorMessage: 'Contraseña de seguridad incompleta.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Contraseña de seguridad incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  sendToCommerceShowToastId = null
  sendToCommerce = async (event) => {
    event.preventDefault();
    this.sendToCommerceShowToastId = toast('Enviando dinero...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    const { sendToCommerceAmount, sendDescription, selectedCommerce } = this.state
    const transactionAmount = Number(sendToCommerceAmount.replace(/\./g, '').replace(',', '.'))
    if (
      transactionAmount > 0 &&
      transactionAmount / 100 <= this.props.userAmount &&
      (
        this.props.hasSecurityPassword
          ? this.state.firstNumber &&
          this.state.secondNumber &&
          this.state.thirdNumber &&
          this.state.fourthNumber
          : true
      )
    ) {
      const data = {
        receiverEmail: selectedCommerce.email,
        senderUid: this.props.data.userKey,
        date: moment().format('YYYY-MM-DD'),
        description: sendDescription.trim(),
        currency: BS2,
        amount: transactionAmount,
        time: moment().format('LTS'),
        securityPassword:
          this.state.firstNumber +
          this.state.secondNumber +
          this.state.thirdNumber +
          this.state.fourthNumber
      }
      customConsoleLog("SENDED:", data)
      try {
        let response = await postRequest('transactions/sendMoney', data)
        customConsoleLog('sendPayment response', response)
        if (response.data.success) {
          this.props.approvedHistoryTransactionsRequest()
          this.setState({
            sendLoading: false,
            displayErrors: false
          })
          this.sendToCommerceShow()
          toast.update(this.sendToCommerceShowToastId, {
            render: 'Dinero enviado exitosamente',
            autoClose: 5000,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, errorMessage: message, displayErrors: true })
          return toast.update(this.sendToCommerceShowToastId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        customConsoleLog(error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.sendToCommerceShowToastId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ sendLoading: false, displayErrors: true })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe colocar un monto.' })
        return toast.update(this.sendToCommerceShowToastId, {
          render: 'Debe colocar un monto.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (transactionAmount / 100 > this.props.userAmount) {
        this.setState({ errorMessage: 'Saldo insuficiente.' })
        return toast.update(this.sendToCommerceShowToastId, {
          render: 'Saldo insuficiente.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if ((!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) &&
        this.props.hasSecurityPassword) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.sendToCommerceShowToastId, {
          render: 'Contraseña de seguridad incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  requestPayment = async (e) => {
    e.preventDefault()
    this.toastLoaderRequestId = toast('Enviando solicitud...', { autoClose: false })
    this.setState({ requestLoading: true, displayErrors: false, errorMessage: '' })
    const { requestAmount, requestDescription, requestEmail } = this.state
    const transactionAmount = Number(requestAmount.replace(/\./g, '').replace(',', '.'))
    if (
      transactionAmount > 0 &&
      requestEmail.trim() &&
      requestEmail.trim().match(emailsRegex)
    ) {
      const data = {
        senderEmail: requestEmail.trim(),
        receiverUid: this.props.data.userKey,
        date: moment().format('YYYY-MM-DD'),
        description: requestDescription.trim(),
        currency: BS2,
        source: 'moneyRequest',
        amount: transactionAmount,
        time: moment().format('LTS')
      }
      customConsoleLog('SENDED', data)
      try {
        let response = await postRequest('transactions/makeMoneyRequest', data)
        customConsoleLog('RESPONSE', response)
        if (response.data.success) {
          this.props.moneyRequestTransactionsRequest()
          this.setState({
            requestAmount: '',
            requestDescription: '',
            requestEmail: '',
            requestLoading: false,
            displayErrors: false
          })
          document.getElementById('requestPaymentForm').reset()
          toast.update(this.toastLoaderRequestId, {
            render: 'Solicitud enviada exitosamente',
            autoClose: 5000,
          })
          setTimeout(() => {
            this.requestShow()
          }, 1000);
        } else {
          const { message } = response.data.error
          this.setState({ requestLoading: false, displayErrors: true, errorMessage: message })
          return toast.update(this.toastLoaderRequestId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        customConsoleLog('error', error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderRequestId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          requestLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ requestLoading: false, displayErrors: true })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe colocar un monto.' })
        return toast.update(this.toastLoaderRequestId, {
          render: 'Debe colocar un monto a solicitar.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!requestEmail.trim()) {
        this.setState({ errorMessage: 'Debe colocar un correo.' })
        return toast.update(this.toastLoaderRequestId, {
          render: 'Debe colocar un correo.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!requestEmail.trim().match(emailsRegex)) {
        this.setState({ errorMessage: 'Debe colocar un correo valido.' })
        return toast.update(this.toastLoaderRequestId, {
          render: 'Debe colocar un correo valido.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  sendShow = () => {
    return !this.state.sendLoading
      ? this.setState({
        scan: false,
        sendDescription: '',
        sendEmail: '',
        sendAmount: '',
        firstNumber: '',
        secondNumber: '',
        thirdNumber: '',
        fourthNumber: '',
        sendShow: this.state.sendLoading ? true : !this.state.sendShow,
        displayErrors: false,
        isCreatePasswordOpen: false,
        isResetPasswordOpen: false,
        isSended: false
      })
      : null
  }
  sendToCommerceShow = () => {
    return !this.state.sendToCommerceLoading
      ? this.setState({
        sendDescription: '',
        sendToCommerceAmount: '',
        sendToCommerceShow: this.state.sendToCommerceLoading ? true : !this.state.sendToCommerceShow,
        displayErrors: false,
        errorMessage: '',
        firstNumber: '',
        secondNumber: '',
        thirdNumber: '',
        fourthNumber: '',
      })
      : null
  }
  selectCommerce = config => () => {
    const selectedCommerce = config.selectedCommerce
    this.setState({ selectedCommerce: selectedCommerce })
    this.sendToCommerceShow()
  }
  requestShow = () => {
    return !this.state.requestLoading
      ? this.setState({
        requestShow: !this.state.requestShow,
        requestQR: false,
        requestAmount: '',
        requestDescription: '',
        requestEmail: '',
        displayErrors: false
      })
      : null
  }
  onRequestQR = e => {
    e.preventDefault()
    // 'onRequestQR')
    this.setState({ requestQR: !this.state.requestQR })
  }
  // ADD MONEY AND WITHDRAW
  showModal = () => {
    return !this.state.isLoadingWithdraw
      ? this.setState({
        show: !this.state.show,
        withdrawAmount: '',
        selectedBankAccount: ''
      })
      : null
  }
  ShowAddModal = () => {
    return !this.state.requestLoadingAddModal
      ? this.setState({
        requestShowAddModal: !this.state.requestShowAddModal,
        amount: '',
        isNextSeccion: false,
        companyAccountNumber: '',
        paymentMethod: 'Transfer',
        displayErrors: false,
        userBank: '',
        transferNumber: '',
        operationDate: ''
      })
      : null
  }
  nextSeccion = (event) => {
    event.preventDefault();
    const transactionAmount = Number(this.state.amount.replace(/\./g, '').replace(',', '.'))
    // "transactionAmount nextSection: ", transactionAmount)
    if (
      transactionAmount > 0 &&
      this.state.paymentMethod !== '' &&
      this.state.companyAccountNumber !== ''
    ) {
      this.setState({ isNextSeccion: true })
    } else {
      this.setState({
        displayErrors: true
      })
      if (!transactionAmount > 0) {
        this.setState({ errorMessage: 'Debe elegir un monto.' })
        return toast.error('Debe elegir un monto.')
      }
      if (event.target.paymentMethod.value === '') {
        this.setState({ errorMessage: 'Debe elegir un metodo de pago.' })
        event.target.paymentMethod.focus()
        return toast.error('Debe elegir un metodo de pago.')
      }
      if (event.target.companyAccountNumber.value === '') {
        this.setState({ errorMessage: 'Debe elegir un numero de cuenta.' })
        event.target.companyAccountNumber.focus()
        return toast.error('Debe elegir un numero de cuenta.')
      }
    }
  }
  handleAccountNumberSelect = ({ target: { value }, target }) => {
    this.setState({
      [target.name]: value
    })
  }
  // handlepaymentMethodSelect = () => { }
  getCompanyAccountNumbers = items => items.map((item, i) => {
    if (item.enable) {
      return <option key={i} value={item.account}>{item.account} {item.name}</option>
    } else {
      return null
    }
  })

  onAddPayment = async (event) => {
    event.preventDefault();
    this.toastLoaderAddId = toast('Añadiendo dinero...', { autoClose: false })
    this.setState({ requestLoadingAddModal: true, displayErrors: false, errorMessage: '' })
    const transactionAmount = Number(this.state.amount.replace(/\./g, '').replace(',', '.'))
    if (
      this.state.transferNumber.trim() !== '' &&
      this.state.operationDate !== '' &&
      this.state.selectedBankAccount !== ''
    ) {
      try {
        const data = {
          amount: transactionAmount,
          currency: BS2,
          date: moment(this.state.operationDate).format('DD-MM-YYYY'),
          dateFormat: moment(this.state.operationDate).format('MMMM DD'),
          senderUid: this.props.data.userKey,
          time: moment(this.state.operationDate).format('LTS'),
          bankReference: this.state.transferNumber.trim(),
          userBankAccount: this.state.selectedBankAccount,
          gualyBankAccount: this.state.companyAccountNumber
        }
        customConsoleLog("SENDED: ", data)
        let response = await postRequest('transactions/addMoneyToGualyByBankAccount', data)
        customConsoleLog('RESPONSE', response)
        if (response.data.success) {
          toast.update(this.toastLoaderAddId, {
            render: 'Tu solicitud ha sido enviada.',
            autoClose: 5000,
          })
          this.setState({
            requestLoadingAddModal: false,
            amount: '',
            isNextSeccion: false,
            companyAccountNumber: '',
            paymentMethod: '',
            displayErrors: false,
            userBank: '',
            transferNumber: '',
            operationDate: '',
            requestShowAddModal: false
          })
        } else {
          const { message } = response.data.error
          this.setState({ requestLoadingAddModal: false, displayErrors: true, errorMessage: message })
          return toast.update(this.toastLoaderAddId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderAddId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          requestLoadingAddModal: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        requestLoadingAddModal: false
      })
      if (!this.state.selectedBankAccount) {
        this.setState({ errorMessage: 'Selecciona tu cuenta bancaria desde donde se realizó la operación.' })
        return toast.update(this.toastLoaderAddId, {
          render: 'Selecciona tu cuenta bancaria desde donde se realizó la operación.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (event.target.transferNumber.value.trim() === '') {
        event.target.transferNumber.focus()
        this.setState({ errorMessage: 'Debe elegir un numero de transferencia o depósito.' })
        return toast.update(this.toastLoaderAddId, {
          render: 'Debe elegir un numero de transferencia o depósito.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (this.state.operationDate === '') {
        this.setState({ errorMessage: 'Debe elegir la fecha en la que se realizó la operación.' })
        return toast.update(this.toastLoaderAddId, {
          render: 'Debe elegir la fecha en la que se realizó la operación.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  getBankLogo = (bankAccountNumber) => {
    const bankId = bankAccountNumber.slice(0, 4)
    const filteredLogo = banks.filter(item => item.id === bankId)[0]
    const filteredLogoPhoto = filteredLogo ? filteredLogo.photo : iconReject
    return filteredLogoPhoto
  }
  onCreateSpecialPasswordClick = async (event) => {
    event.preventDefault()
    this.createPasswordToaster = toast('Creando contraseña...', { autoClose: false })
    this.setState({
      sendLoading: true,
      sendToCommerceLoading: true,
      displayErrors: false,
      errorMessage: ''
    })
    if (
      this.state.firstNumber &&
      this.state.secondNumber &&
      this.state.thirdNumber &&
      this.state.fourthNumber
    ) {
      try {
        const data = {
          securityPassword:
            this.state.firstNumber +
            this.state.secondNumber +
            this.state.thirdNumber +
            this.state.fourthNumber,
          uid: this.props.data.userKey
        }
        customConsoleLog("SENDED: ", data)
        let response = await postRequest('security/createSecurityPassword', data)
        customConsoleLog('RESPONSE', response)
        if (response.data.success) {
          toast.update(this.createPasswordToaster, {
            render: 'Tu contraseña especial ha sido creada exitosamente.',
            autoClose: 5000,
          })
          this.setState({
            sendLoading: false,
            sendToCommerceLoading: false,
            firstNumber: '',
            secondNumber: '',
            thirdNumber: '',
            fourthNumber: '',
            isCreatePasswordOpen: false,
          })
        } else {
          const { message } = response.data.error
          this.setState({
            sendLoading: false,
            sendToCommerceLoading: false,
            displayErrors: true,
            errorMessage: message
          })
          return toast.update(this.createPasswordToaster, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.createPasswordToaster, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          sendToCommerceLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        sendLoading: false,
        sendToCommerceLoading: false,
      })
      if (!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.createPasswordToaster, {
          render: 'Contraseña especial incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  cancelCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: false,
      displayErrors: false,
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  openCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: true,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
      sendDescription: '',
      sendEmail: '',
      sendAmount: '',
      sendToCommerceAmount: '',
    })
  }
  getBankAccount = (selectedBankAccount, e) => {
    e.preventDefault()
    this.setState({ selectedBankAccount })
  }
  renderBankCards = bankAccounts => {
    if (bankAccounts) {
      return Object.values(bankAccounts).map((bankAccount, i) =>
        <button
          key={i}
          className={`${bankCard} ${bankCardHoverOrFocus} ${bankAccount.bankAccountID === this.state.selectedBankAccount ? cssChange : ''}`}
          onClick={(e) => this.getBankAccount(bankAccount.bankAccountID, e)}>
          <img className={bankLogoCSS} src={this.getBankLogo(bankAccount.bankAccountNumber)} alt='bank-logo' />
          <div css={`width: 80px !important; direction: ltr !important;position: absolute;bottom: 24px;`}>
            <span css={`font-weight: normal !important`}>Cuenta No.</span>
          </div>
          <div css={`width: 98px !important;`}>
            <span>{bankAccount.bankAccountNumber}</span>
          </div>
        </button>
      )
    } else {
      return <h1 css={`font-size: 25px`}>No hay cuentas registradas</h1>
    }
  }
  renderBankCardsToAddMoney = bankAccounts => {
    if (bankAccounts) {
      return Object.values(bankAccounts).map((bankAccount, i) =>
        <button
          key={i}
          className={`
        ${bankCard} ${bankCardHoverOrFocus} 
        ${
            `${getBankName((bankAccount.bankAccountNumber || '0000').slice(0, 4))} ${bankAccount.bankAccountNumber}`
              === this.state.selectedBankAccount ? cssChange : ''}
         `}
          onClick={(e) => this.getBankAccount(
            `${getBankName((bankAccount.bankAccountNumber || '0000').slice(0, 4))} ${bankAccount.bankAccountNumber}`
            , e)}>
          <img className={bankLogoCSS} src={this.getBankLogo(bankAccount.bankAccountNumber)} alt='bank-logo' />
          <div css={`width: 80px !important; direction: ltr !important;position: absolute;bottom: 24px;`}>
            <span css={`font-weight: normal !important`}>Cuenta No.</span>
          </div>
          <div css={`width: 98px !important;`}>
            <span>{bankAccount.bankAccountNumber}</span>
          </div>
        </button>
      )
    } else {
      return <h1 css={`font-size: 25px`}>No hay cuentas registradas</h1>
    }
  }
  toastLoaderId = null
  onWithdraw = async (e) => {
    e.preventDefault()
    this.setState({ isLoadingWithdraw: true, displayErrors: false, errorMessage: '' })
    this.toastLoaderId = toast('Cargando retiro...', { autoClose: false })
    const { selectedBankAccount, withdrawAmount } = this.state
    const transactionAmount = Number(withdrawAmount.replace(/\./g, '').replace(',', '.'))
    if (transactionAmount > 0) {
      if (selectedBankAccount) {
        const data = {
          amount: transactionAmount,
          currency: BS2,
          date: moment().format('YYYY-MM-DD'),
          time: moment().format('LTS'),
          uid: this.props.data.userKey,
          userBankAccount: selectedBankAccount
        }
        customConsoleLog('SENDED: ', data)
        try {
          let response = await postRequest('transactions/withDrawMoneyFromGualy', data)
          customConsoleLog('withdraw response: ', response)
          if (response.data.success) {
            this.props.pendingHistoryTransactionsRequest()
            document.getElementById('withdrawForm').reset()
            this.setState({ isLoadingWithdraw: false, withdrawAmount: '', selectedBankAccount: '' })
            toast.update(this.toastLoaderId, {
              render: response.data.data.message,
              autoClose: 5000
            })
            setTimeout(() => {
              this.showModal()
            }, 1000);
          } else {
            this.setState({ isLoadingWithdraw: false, displayErrors: true, errorMessage: response.data.error.message })
            toast.update(this.toastLoaderId, {
              render: response.data.error.message,
              type: toast.TYPE.ERROR,
              autoClose: 5000
            })
          }
        } catch (error) {
          const errorMessage = ({
            '401': 'Su sesión ha expirado',
            'default': 'Verifique su conexión y vuelva a intentarlo.'
          })[error.response ? error.response.status || 'default' : 'default']
          toast.update(this.toastLoaderId, {
            render: errorMessage,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
          this.setState({
            isLoadingWithdraw: false,
            errorMessage,
            displayErrors: true
          })
        }
      } else {
        this.setState({ isLoadingWithdraw: false, errorMessage: 'Debe seleccionar una cuenta.', displayErrors: true })
        return toast.update(this.toastLoaderId, {
          render: 'Debe seleccionar una cuenta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    } else {
      this.setState({ isLoadingWithdraw: false, errorMessage: 'Debe colocar un monto a retirar.', displayErrors: true })
      return toast.update(this.toastLoaderId, {
        render: 'Debe colocar un monto a retirar.',
        type: toast.TYPE.ERROR,
        autoClose: 5000
      })
    }
  }
  //RESET SPECIAL PASSWORD
  onResetSpecialPasswordClick = async () => {
    this.setState({
      displayErrors: false,
      errorMessage: ""
    })
    this.resetSpecialPassToast = toast('Restableciendo contraseña especial...', { autoClose: false })
    const data = {
      mode: 'secretPasswordToken',
      uid: this.props.data.userKey,
    }
    customConsoleLog("SENDED: ", data)
    try {
      this.setState({
        sendLoading: true,
        sendToCommerceLoading: true,
        displayErrors: false,
        errorMessage: ""
      })
      let response = await postRequest('security/generatePasswordTokenByEmail', data)
      customConsoleLog("RESPOSNE: ", response)
      if (response.data.success) {
        this.setState({
          sendLoading: false,
          sendToCommerceLoading: false,
          sent: true,
          isSended: true
        })
        toast.update(this.resetSpecialPassToast, {
          render: "Casi listo, Ahora ingresa a tu correo y sigue las instrucciones...",
          autoClose: 5000
        })
      } else {
        this.setState({
          sendLoading: false,
          sendToCommerceLoading: false,
          displayErrors: true,
          errorMessage: `${response.data.error.message}`
        })
      }
    } catch (error) {
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.update(this.resetSpecialPassToast, {
        render: errorMessage,
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
      this.setState({
        sendLoading: false,
        sendToCommerceLoading: false,
        displayErrors: true,
        errorMessage,
      })
      customConsoleLog('catch err', error)
    }
  }
  openResetPassword = () => {
    this.setState({
      isResetPasswordOpen: true,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
      sendDescription: '',
      sendEmail: '',
      sendAmount: '',
      sendToCommerceAmount: '',
    })
  }
  cancelResetPasswordClick = () => {
    this.setState({
      isResetPasswordOpen: false,
      isSended: false,
      displayErrors: false
    })
  }
  render() {
    const {
      requestShow,
      requestAmount,
      requestDescription,
      requestEmail,
      sendShow,
      sendAmount,
      sendDescription,
      sendEmail,
      requestLoading,
      sendLoading,
      scan
    } = this.state
    const {
      titleForRecentMovements,
      dataForRecentMovements,
      loadingForRecentMovements,
      titleCommerces,
      commerces,
      loadingCommerces,
      type,
      columns,
      databaseRequestCommerces,
      onHistoryTransactionClick,
    } = this.props
    const requestData = {
      amount: requestAmount,
      description: requestDescription,
      email: requestEmail,
      userKey: this.props.data ? this.props.data.userKey : "",
      formNames: {
        idFormName: 'requestPaymentForm',
        amountName: 'requestAmount',
        descriptionName: 'requestDescription',
        emailName: 'requestEmail'
      }
    }
    return (
      <div>
        <div
          className={`
      ${(this.state.displayErrors === true ? displayErrors : '')} 
      ${mainContainer}
      `}
        >
          {/* --------DESKTOP AND MOBILE VIEW START-------- */}
          <ProfileCard
            requestShow={this.requestShow}
            sendShow={this.sendShow}
            amount={this.props.userAmount}
            ShowAddModal={this.ShowAddModal}
            showWithdrawModal={this.showModal}
            userData={this.props.data}
            lastSignInTime={this.props.lastSignInTime}
            className={isIwantShow}
          />
          <IwantCard
            className={isIwantShow}
            requestShow={this.requestShow}
            sendShow={this.sendShow}
          />
          {/* --------DESKTOP AND MOBILE VIEW ENDS-------- */}
          {/* --------HD VIEW START--------*/}
          <div className={firstHdCardsContainer}>
            <ProfileCard
              requestShow={this.requestShow}
              sendShow={this.sendShow}
              amount={this.props.userAmount}
              ShowAddModal={this.ShowAddModal}
              showWithdrawModal={this.showModal}
              userData={this.props.data}
              lastSignInTime={this.props.lastSignInTime}
            />
            <CommercesTable
              title={titleCommerces}
              commerces={commerces}
              loading={loadingCommerces}
              onCommerceClick={this.selectCommerce}
              className={commercesHdClass}
              type='dashboardCommerces'
              databaseRequestCommerces={databaseRequestCommerces}
            />
          </div>
          <div className={secondHdCardsContainer}>
            <IwantCard
              requestShow={this.requestShow}
              sendShow={this.sendShow}
            />
            <Table
              title={titleForRecentMovements}
              columns={columns}
              height={675}
              data={dataForRecentMovements}
              isThereBackButton={false}
              minHeight="796px" //height + 81px
              maxWidth="910px"
              loading={loadingForRecentMovements}
              className={HDrecentTable}
              //table header
              type={type}
              databaseRequest={this.props.approvedHistoryTransactionsRequest}
              disableHeader={false}
              onHistoryTransactionClick={onHistoryTransactionClick}
            />
          </div>
          {/* --------HD VIEW ENDS-------- */}
          <ToastContainer
            className={{
              fontSize: "15px"
            }} />
        </div>
        <Table
          title={titleForRecentMovements}
          columns={columns}
          height={450}
          data={dataForRecentMovements}
          isThereBackButton={false}
          className={DestopMobileCommercesClass}
          minHeight="531px" //height + 81px
          mobileMediaWidth="1100px"
          loading={loadingForRecentMovements}
          //table header
          type={type}
          databaseRequest={this.props.approvedHistoryTransactionsRequest}
          disableHeader={false}
          onHistoryTransactionClick={onHistoryTransactionClick}
        />
        <CommercesTable
          title={titleCommerces}
          commerces={commerces}
          loading={loadingCommerces}
          onCommerceClick={this.selectCommerce}
          className={DestopMobileCommercesClass}
          type='dashboardCommerces'
          databaseRequestCommerces={databaseRequestCommerces}
        />
        {/* --------MODALS-------- */}
        <RequestModal
          showModal={requestShow}
          loading={requestLoading}
          data={requestData}
          displayErrors={this.state.displayErrors}
          onClose={this.requestShow}
          onSubmit={this.requestPayment}
          onChange={this.handleInputChange}
          requestQR={this.state.requestQR}
          onChangeQR={this.onRequestQR}
          errorMessage={this.state.errorMessage}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'requestAmount' })}
        />
        <OptionsModalWrapper show={sendShow} onClose={this.sendShow} width="310px">
          <SendModal
            name='sendAmount'
            amount={sendAmount}
            displayErrors={this.state.displayErrors}
            sendDescription={sendDescription}
            sendEmail={sendEmail}
            scan={scan}
            onChange={this.handleInputChange}
            openScan={this.openScan}
            onSubmit={this.sendPayment}
            loading={sendLoading}
            deley={this.state.deley}
            handleError={this.handleError}
            handleScan={this.handleScan}
            errorMessage={this.state.errorMessage}
            amountInputHandleChange={this.amountInputHandleChange({ name: 'sendAmount' })}
            //special password
            firstNumber={this.state.firstNumber}
            secondNumber={this.state.secondNumber}
            thirdNumber={this.state.thirdNumber}
            fourthNumber={this.state.fourthNumber}
            isCreatePasswordOpen={this.state.isCreatePasswordOpen}
            cancelCreatePasswordClick={this.cancelCreatePasswordClick}
            onCreateSpecialPasswordClick={this.onCreateSpecialPasswordClick}
            openCreatePasswordClick={this.openCreatePasswordClick}
            hasSecurityPassword={this.props.hasSecurityPassword}
            //reset special password
            isResetPasswordOpen={this.state.isResetPasswordOpen}
            onResetSpecialPasswordClick={this.onResetSpecialPasswordClick}
            cancelResetPasswordClick={this.cancelResetPasswordClick}
            openResetPassword={this.openResetPassword}
            isSended={this.state.isSended}
          />
        </OptionsModalWrapper>
        <SendMoneyToCommerceModal
          name='sendToCommerceAmount'
          amount={this.state.sendToCommerceAmount}
          displayErrors={this.state.displayErrors}
          sendDescription={this.state.sendDescription}
          onChange={this.handleInputChange}
          onSubmit={this.sendToCommerce}
          loading={this.state.sendToCommerceLoading}
          errorMessage={this.state.errorMessage}
          onClose={this.sendToCommerceShow}
          showModal={this.state.sendToCommerceShow}
          userName={this.state.selectedCommerce ? this.state.selectedCommerce.commerceName : ''}
          senderPicture={this.state.selectedCommerce ? this.state.selectedCommerce.profilePicture : ''}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'sendToCommerceAmount' })}
          //special password
          firstNumber={this.state.firstNumber}
          secondNumber={this.state.secondNumber}
          thirdNumber={this.state.thirdNumber}
          fourthNumber={this.state.fourthNumber}
          isCreatePasswordOpen={this.state.isCreatePasswordOpen}
          cancelCreatePasswordClick={this.cancelCreatePasswordClick}
          onCreateSpecialPasswordClick={this.onCreateSpecialPasswordClick}
          openCreatePasswordClick={this.openCreatePasswordClick}
          hasSecurityPassword={this.props.hasSecurityPassword}
          //reset special password
          isResetPasswordOpen={this.state.isResetPasswordOpen}
          onResetSpecialPasswordClick={this.onResetSpecialPasswordClick}
          cancelResetPasswordClick={this.cancelResetPasswordClick}
          openResetPassword={this.openResetPassword}
          isSended={this.state.isSended}
        />
        <WithdrawModal
          amount={this.state.withdrawAmount}
          isLoading={this.state.isLoadingWithdraw}
          userBankAccounts={this.state.userBankAccounts}
          onSubmit={this.onWithdraw}
          banksCards={this.renderBankCards(this.state.userBankAccounts)}
          handleInputChange={this.handleInputChange}
          show={this.state.show}
          onClose={this.showModal}
          errorMessage={this.state.errorMessage}
          displayErrors={this.state.displayErrors}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'withdrawAmount' })}
        />
        <AddModal
          idFormName="addModal"
          extraIdFormName="nextSeccion"
          showModal={this.state.requestShowAddModal}
          loading={this.state.requestLoadingAddModal}
          onClose={this.ShowAddModal}
          onSubmit={this.onAddPayment}
          onChange={this.handleInputChange}
          handleAccountNumberSelect={this.handleAccountNumberSelect}
          paymentMethod={this.state.paymentMethod}
          companyAccountNumbers={this.state.companyAccountNumbers}
          getCompanyAccountNumbers={this.getCompanyAccountNumbers}
          companyAccountNumber={this.state.companyAccountNumber}
          rifNumber={this.state.AddModalCompanyRif}
          displayErrors={this.state.displayErrors}
          companyName={this.state.AddModalCompanyName}
          amount={this.state.amount}
          nextSeccion={this.nextSeccion}
          isNextSeccion={this.state.isNextSeccion}
          userBank={this.state.userBank}
          transferNumber={this.state.transferNumber}
          operationDate={this.state.operationDate}
          errorMessage={this.state.errorMessage}
          amountInputHandleChange={this.amountInputHandleChange({ name: 'amount' })}
          banksCards={this.renderBankCardsToAddMoney(this.state.userBankAccounts)}
          userBankAccounts={this.state.userBankAccounts}
          updateDate={({ day, month, year, currentName }) => {
            if (day && month && year) {
              this.setState({
                [currentName]: `${year}-${month}-${day}`
              })
            }
          }}
        />
      </div>
    )
  }
}