import React from 'react'
import { css } from 'emotion'
import CountUp from 'react-countup'
import Spinner from 'react-spinkit'
import AcceptButton from '../../../general/AcceptButton'
import userPicture from './../../../../assets/accountCircle.svg'
import editIcon from '../../../../assets/icon-edit.svg'
import { NavLink } from 'react-router-dom'
import { BS } from '../../../../config/currencies.js'
import defaultUserPicture from './../../../../assets/accountCircle.svg'

const proileRoundImage = css`
width: 72px;
height: 72px;
border-radius: 50%;
margin-left: 20px;
margin-bottom: auto;
margin-right: 15px;
object-fit: cover;
@media(max-width: 500px){
  margin: auto;
  margin-right: 15px;
}
  `
const horizontalDivisor = css`
width: 100%;
padding: 0;
margin: 0;
opacity: 0.9;
margin-top: 20px;
border: 0.5px solid #1d1f4a;
@media(max-width: 800px){
  display: none
}
  `
const editButtonContainer2 = css`
  display: flex;
  flex-direction: row;
  position: absolute;
  right: 13px;
  cursor: pointer;
  top: 10px;
  right: 10px;
  @media(max-width: 500px){
    display: none
  }
  `
const editButtonContainerMovil = css`
  display: none;
  flex-direction: row;
  right: 13px;
  cursor: pointer;
  @media(max-width: 500px){
    display: flex
  }
  `
const editButtonText = css`
font-family: Montserrat;
font-size: 12.8px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
letter-spacing: normal;
text-align: right;
color: #95fad8;
margin-right: 6px;
margin-top: 14px
  `
const editIconStyle2 = css`
cursor: pointer;
width: 16px;
height: 16px;
margin-top: 14px;
  `
const noDecoration = css`
text-decoration: none;
  `
const firstContainer = css`
    border-radius: 5px;
    background-color: #2a2c6a;
    -webkit-box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    width: 45%;
    height: 355px;
    max-width: 600px;
    max-height: 355px;
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin: 17px;
    margin-top: 50px;
    position: relative;
    @media(max-width: 1200px){
      width: 600PX;
      max-width: 600PX;
      margin-top: 25px;
      margin-bottom: 0;
      margin-left: 25px;
      margin-right: 25px;
      align-self: center;
    }
    @media(max-width: 650px){
      width: 91%;
      max-width: 94%;
      margin-top: 25px;
      margin-bottom: 0;
      margin-left: 25px;
      margin-right: 25px;
      align-self: center;
      height: auto;
      max-height: max-content;
    }
    @media(min-width: 1440px){
      width: 360px;
      height: 350px;
      margin-right: 30px;
    }
`
const responsiveTransparentBtn = css`
    @media(max-width: 800px) {
      margin-top: 0;
    }
    @media(max-width: 800px) {
      margin-top: 10px !important;
    }
    @media(min-width: 1440px) {
      margin-top: 10px !important;
    }
  `
const welcomeBody = css`
    display: flex;
    flex-direction: row;
    width: 90%;
    height: 20%;
    margin: 35px 20px 20px 20px;
    h1{
      margin: 0;
      font-size: 24px;
      text-align: left;
    }
    @media(max-width: 500px) {
      margin-left: 15px;
      height: auto;
      margin-bottom: 0;
    }
    @media(max-width: 370px) {
      // height: 200px;
    }
    // @media(min-width: 1440px) {
    //   height: 30%
    // }
  `
const spinnerContainer = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 26px;
    left: 0;
  `
const carduserData = css`
    width: 100%;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    flex-wrap: wrap;
    margin-top: 12px;
    height: auto;
    h3{
      font-family: Montserrat;
      font-size: 16px;
      text-align: left;
      margin: 0;
    }
  `
const carduserDataText = css`
      margin: 0 !important;
      margin-top: 5px;
      padding-right: 10px;
      opacity: 0.65;
      font-family: Montserrat;
      font-size: 11px;
      text-align: left;
      color: #f6f7fa;
      @media(min-width: 1440px){
        padding-right: 79px;
      }
      @media(min-width: 500px){
        width: 300px;
      }
  `
const controlBody = css`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    width: 100%;
    height: 80%;
    justify-content: center;
    @media(max-width: 1048px) {
      flex-direction: column
      justify-content: center;
    }
    @media(max-width: 800px) {
      margin-top: 10px;
      background-color: rgba(0, 0, 0, 0.25);
      border-top: 1px solid black; 
      // padding: 8px 25px 18px 25px;
    }
    @media(max-width: 500px) {
      height: auto
    }
    @media(min-width: 1440px) {
      height: 70%
    }
  `
const body = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    @media(max-width: 1200px) {
      margin-top: 10px;
      width: 100%;
      justify-content: space-between;
      align-items:center;
    }
  `
const userNameFirstName = css`
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    width: 170px;
    @media(min-width: 700px) and (max-width: 1440px) {
      width: 400px;
    }
  `
const userNameLastName = css`
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    width: 170px;
    @media(min-width: 700px) and (max-width: 1440px) {
      width: 400px;
    }
  `
const isHd = css`
    @media(min-width: 1440px) {
      width: 90% !important;
    }
  `
const buttonBody = css`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width:337px;
    margin: 25px;
    @media(max-width: 1048px) {
      margin-top: 10px;
    }
    @media(max-width: 800px) {
      margin-top: 0;
    }
    @media(max-width: 800px) {
      width:100%;
      margin-top: 10px;
      flex-direction: column;
    }
    @media(min-width: 1440px) {
      flex-direction: column;
    }
  `
const balanceDiv = css`
    display: flex;
    min-height: 65px;
    flex-direction: column;
    justify-content: center;
    position: relative;
    h3{
      font-family: Montserrat;
      font-size: 14px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #f6f7fa;
      opacity: 0.65;
      margin: 0;
    }
    h2 {
      font-family: Montserrat;
      font-size: 32px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #8078ff;
      margin: 0;
    }
    @media(max-width: 800px) {
      align-self: flex-start;
    }
    @media(max-width: 800px) {
      align-self: center;
    }
    @media(max-width: 500px) {
      h2 {
        font-size: 100%;
      }
    }
    @media(min-width: 1440px) {
      margin-top: 20px;
      margin-left: 25px;
    }
  `
const ProfileCard = ({
  requestShow,
  sendShow,
  amount,
  ShowAddModal,
  showWithdrawModal,
  userData,
  lastSignInTime,
  className
}) => (
    <div className={`${firstContainer} ${className}`}>
      <NavLink className={noDecoration} to="/perfil">
        <div
          className={editButtonContainer2}
          onClick={this.edit}
        >
          <p className={editButtonText}>Editar</p>
          <img
            src={editIcon}
            alt="EditIcon"
            className={editIconStyle2}>
          </img>
        </div>
      </NavLink>
      <div className={welcomeBody}>
        <div>
          <img
            src={
              userData
                ?
                userData.profilePicture
                :
                userPicture
            }
            className={proileRoundImage}
            alt="profileRoundImage"
            onError={(e) => { e.target.onerror = null; e.target.src = defaultUserPicture }}
            />
        </div>
        <div>
          <h1 className={userNameFirstName}>{userData ? userData.firstName : ""}</h1>
          <h1 className={userNameLastName}>{userData ? userData.lastName : ""}</h1>
          <div className={carduserData}>
            <p className={carduserDataText}>
              Tú último inicio de sesión fue el {lastSignInTime ? lastSignInTime : '...'}
            </p>
            <NavLink className={noDecoration} to="/perfil">
              <div
                css={`height: auto;`}
                className={editButtonContainerMovil}
                onClick={this.edit}
              >
                <p className={editButtonText}>Editar</p>
                <img
                  src={editIcon}
                  alt="EditIcon"
                  className={editIconStyle2}>
                </img>
              </div>
            </NavLink>
          </div>
        </div>
      </div>
      <div className={horizontalDivisor}></div>
      <div className={controlBody}>
        <div className={body}>
          <div className={balanceDiv}>
            <h3>Balance Actual</h3>
            {
              amount || amount === 0 ?
                <h2>
                  <CountUp
                    start={amount}
                    end={amount}
                    duration={0.5}
                    separator="."
                    decimals={2}
                    decimal=","
                    prefix={BS}
                    redraw={!(requestShow || sendShow)}
                  />
                </h2>
                :
                null
            }
            {
              !amount && amount !== 0 ?
                <div className={spinnerContainer}> <Spinner color='white' /></div>
                : null
            }
          </div>
          <div className={buttonBody}>
            <AcceptButton
              width="171px"
              height="34px"
              onClick={ShowAddModal}
              content="AÑADIR&nbsp;SALDO"
              className={isHd}
            />
            <button
              onClick={showWithdrawModal}
              css={`height: 34px;width: 171px`}
              className={`${responsiveTransparentBtn} btn-accept-transparent`}
            >
              RETIRAR&nbsp;SALDO
          </button>
          </div>
        </div>
      </div>
    </div>
  )
export default ProfileCard