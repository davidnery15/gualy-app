

import React from 'react'
import { css } from 'emotion'
import sendMoneyIcon from './../../../../assets/profile/icon-send-payment.svg'
import askForPaymentIcon from './../../../../assets/profile/icon-ask-for-payment.svg'
const secondContainer = css`
    border-radius: 5px;
    background-color: #2a2c6a;
    -webkit-box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
    width: 45%;
    height: 355px;
    max-width: 600px;
    max-height: 355px;
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin: 17px;
    margin-top: 50px;
    @media(max-width: 1200px){
      width: 600PX;
      max-width: 600PX;
      margin-top: 25px;
      margin-bottom: 0;
      margin-left: 25px;
      margin-right: 25px;
      align-self: center;
    }
    @media(max-width: 650px){
      width: 91%;
      max-width: 94%;
      margin-top: 25px;
      margin-bottom: 0;
      margin-left: 25px;
      margin-right: 25px;
      align-self: center;
    }
    @media(min-width: 1440px){
      width: 910px;
      height: 164px;
      max-height: 164px;
      max-width: 910px;
      flex-direction: row;
      margin-left: 0;
      margin-right: 0;
      margin-bottom: 0;
    }
`
const iWantContainer = css`
height: 20%;
margin-top: 20px;
margin-left: 20px;
@media(min-width: 1440px){
  width: 30%;
  height: 100%;
  margin: 0;
  h1{
    text-align: center;
    margin-top: 62px;
  }
}
  `
const sendRequestButtonsContainer = css`
height: 80%;
display: flex;
justify-content: center;
flex-direction: column;
@media(min-width: 1440px){
  width: 70%;
  height: 100%;
  flex-direction: row;
  align-items: center;
}
  `
const mainButtonContainer = css`
width: 100%;
height: 50%;
display: flex;
flex-direction: row;
justify-content: left;
  `
const buttonContainerMain = css`
width: auto;
height: auto;
display: flex;
flex-direction: row;
margin: auto;
margin-left: 20px;
cursor: pointer
  `
const buttonContainer = css`
width: 64.4px;
height: 64.4px;
border-radius: 65.8px;
border: solid 1.3px #95fad8;
background-color: rgba(255, 255, 255, 0.2);
display: flex;
justify-content: center;
align-items: center;
  `
const buttonText = css`
  font-family: Montserrat;
  font-size: 16px;
  font-weight: 900;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: 1px;
  text-align: center;
  margin-top: 20px;
  margin-left: 20px;
  `
const sendMoneyIconClass = css`
width: 31px;
height: 31px;
  `
const isDivisorShow = css`
@media(min-width: 1440px){
display: none
}
  `
const horizontalDivisor = css`
height: 1px;
width: 100%;
padding: 0;
margin: 0;
opacity: 0.9;
background-color: #1d1f4a;
margin-top: 25px;
  `
const ProfileCard = ({
  requestShow,
  sendShow,
  className
}) => (
    <div className={`${secondContainer} ${className}`}>
      <div className={iWantContainer}>
        <h1>QUIERO...</h1>
      </div>
      <div className={`${isDivisorShow} ${horizontalDivisor}`}></div>
      <div className={sendRequestButtonsContainer}>
        <div className={mainButtonContainer}>
          <div onClick={sendShow} className={buttonContainerMain}>
            <div className={buttonContainer}>
              <img
                src={sendMoneyIcon}
                className={sendMoneyIconClass}
                alt=""
              />
            </div>
            <p className={buttonText}>ENVIAR PAGO</p>
          </div>
        </div>
        <div className={mainButtonContainer}>
          <div onClick={requestShow} className={buttonContainerMain}>
            <div className={buttonContainer}>
              <img
                src={askForPaymentIcon}
                className={sendMoneyIconClass}
                alt=""
              />
            </div>
            <p className={buttonText}>SOLICITAR PAGO</p>
          </div>
        </div>
      </div>
    </div>
  )
export default ProfileCard