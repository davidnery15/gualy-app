import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import CommerceHeader from './../../commerce-client-header/CommerceClientHeader'
import { css } from 'emotion'
// import { ToastContainer, toast } from 'react-toastify'
import ReactToPrint from "react-to-print"
import verticalTriangle from '../../../../assets/prints/triangulo-vertical.svg'
import horizontalTriangle from '../../../../assets/prints/triangulo-horizontal.svg'
import target from '../../../../assets/prints/presentacional-target.svg'
import printableIcon from '../../../../assets/prints/icon-button-clear.svg'
import { QRCode } from 'react-qr-svg'
import GoBackButton from '../../../general/goBackButton'
import { db } from './../../../../firebase'
import { userLogout } from '../../../../redux/actions/general'
const mainProfileContainer = css`
display: flex; 
flex-direction: column;
position: relative;
justify-content: center;
@media(max-width: 980px){
  flex-direction: column 
}
`
const banksDataContainer = css`
margin: auto;
margin-top: 50px;
margin-bottom: 0px;
border-radius: 5px;
background-color: #2a2c6a;
-webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
padding-top: 21px;
display: -ms-flexbox;
display: flex;
height: auto;;
position: relative;
max-width: 1353px;
width: auto;
flex-direction: column;
height: auto;
max-width: 90%;
// overflow: auto;
@media(max-width: 580px){
  margin-left: 5%;
  margin-right: 5%;
}
  `
const header = css`
  position: absolute;
  top: -6px;
  left: 33px;
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
  `
const headerPresentationalTarget = css`
    position: absolute;
    left: 33px;
    top: 0px;
    margin: 0px;
    display: flex;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    h1{
      margin: 0px;
      font-size: 10px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
  `
const editButtonContainerAdd = css`
 display: flex;
 flex-direction: row;
 position: absolute;
 right: 20px;
 top: 14px;
 cursor: pointer
 `
const addIconStyle = css`
width: 17.6px;
  height: 17.5px;
  `

const verticalTriangleContainer = css`
  width: 1058px;
  height: 794px;
  background-image: url("${verticalTriangle}");
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  background-color: #fff;
  position: relative;
  `
const horizontalTriangleContainer = css`
  width: 794px;
  height: 1054px;
  background-image: url("${horizontalTriangle}");
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  background-color: #fff;
  position: relative;
  `
const presentacionalTarget = css`
width: 287px;
height: 312px;
  background-image: url("${target}");
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  background-color: #fff;
  position: relative;
  `
const firstVerticalLogo = css`
    top: 38px;
    left: 70px; 
    position: absolute;
    width: 200px;
  height: 200px;
  `
const secondVerticalLogo = css`
    position: absolute;
    width: 200px;
    height: 200px;
    top: 38px;
    right: 123px;
  `
const qrCode = css`
    width: 114px;
    height: 114px;
    color: black
    position: absolute;
    top: 409px;
    left: 118px;
  `
const qrCode2 = css`
    width: 114px;
    height: 114px;
    position: absolute;
    top: 409px;
    right: 170px;
    color: black
  `
const verticalTriangleTitles = css`
font-size:20.19px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
    font-weight: bold;
  `
const verticalTriangleTitlesEmail = css`
font-size: 13px;
color:#2b2d6a
font-family:Montserrat-Medium, Montserrat;
font-weight: bold;
  `

const companyTitle = css`
position: absolute;
top: 255px
right: 70px;
max-width: 310px;
text-align: center;
font-size: 100%;
width: 310px;
  `
const companyTitleSmall = css`
position: absolute;
top: 255px
right: 70px;
max-width: 310px;
text-align: center;
font-size: 70%;
width: 310px;
  `
const companyTitle2Small = css`
position: absolute;
top: 255px
left: 17px;
max-width: 310px;
text-align: center;
font-size: 70%;
width: 310px;
  `
const companyTitle2 = css`
position: absolute;
top: 255px
left: 17px;
max-width: 310px;
text-align: center;
font-size: 100%;
width: 310px;
  `
const companyRif = css`
position: absolute;
    top: 277px;
    right: 70px;
    max-width: 310px;
    text-align: center;
    font-size: 100%;
    width: 310px;
  `
const companyRif2 = css`
position: absolute;
    top: 277px;
    left: 17px;
    max-width: 310px;
    text-align: center;
    font-size: 100%;
    width: 310px;
  `
const companyEmail = css`
position: absolute;
top: 545px;
left: 15px;
width: 310px;
text-align: center;

  `
const companyEmailSmall = css`
position: absolute;
top: 545px;
left: 15px;
width: 310px;
text-align: center;
font-size: 60%
  `
const companyEmail2 = css`
position: absolute;
top: 545px;
right: 69px;
width: 310px;
text-align: center
  `
const companyEmail2Small = css`
position: absolute;
top: 545px;
right: 69px;
width: 310px;
text-align: center;
font-size: 60%
  `
const userQRHorizontal = css`
width: 114px;
height: 114px;
color: black;
position: absolute;
top: 605px;
right: 56px;
  `
const userQRHorizontal2 = css`
width: 114px;
height: 114px;
color: black;
position: absolute;
top: 329px;
left: 56px;
/* Safari */
-webkit-transform: rotate(-180deg);

/* Firefox */
-moz-transform: rotate(-180deg);

/* IE */
-ms-transform: rotate(-180deg);

/* Opera */
-o-transform: rotate(-180deg);
  `
const scroller = css`
position: relative;
overflow: auto
  `
const companyTitleHorizontal = css`
position: absolute;
right: 0;
width: 589px;
top: 194px;
text-align: center;
    /* Safari */
-webkit-transform: rotate(-180deg);

/* Firefox */
-moz-transform: rotate(-180deg);

/* IE */
-ms-transform: rotate(-180deg);

/* Opera */
-o-transform: rotate(-180deg);
  `
const companyTitle2Horizontal = css`
position: absolute;
left: 0;
width: 589px;
bottom: 194px;
text-align: center;
  `
const companyRifHorizontal = css`
position: absolute;
    right: 0;
width: 589px;
top: 170px;
text-align: center;
    /* Safari */
-webkit-transform: rotate(-180deg);

/* Firefox */
-moz-transform: rotate(-180deg);

/* IE */
-ms-transform: rotate(-180deg);

/* Opera */
-o-transform: rotate(-180deg);
  `
const companyRif2Horizontal = css`
position: absolute;
    left: 0;
width: 589px;
bottom: 170px;
text-align: center;
  `
const firstHorizontalLogo = css`
  top: 241px;
  right: 177px; 
  position: absolute;
  width: 230px;
  height: 230px;
  /* Safari */
  -webkit-transform: rotate(-180deg);
  
  /* Firefox */
  -moz-transform: rotate(-180deg);
  
  /* IE */
  -ms-transform: rotate(-180deg);
  
  /* Opera */
  -o-transform: rotate(-180deg);
`
const secondHorizontalLogo = css`
bottom: 241px;
left: 177px;
position: absolute;
width: 230px;
height: 230px;
`
const companyEmailHorizontal = css`
bottom: 728px;
left: 8px;
width: 207px;
text-align: center;
position: absolute;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
const companyEmailHorizontalSmall = css`
bottom: 728px;
font-size: 60%;
width: 207px;
text-align: center;
left: 8px;
    position: absolute;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
const companyEmail2Horizontal = css`
top: 728px;
right: 8px;
width: 207px;
text-align: center;
    position: absolute;
`
const companyEmail2HorizontalSmall = css`
top: 728px;
font-size: 60%;
width: 207px;
text-align: center;
right: 8px;
    position: absolute;
`
const companyTextHorizontal = css`
top: 558px;
right: 49px;
position: absolute;
font-size:10.68px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
font-weight: bold;
`
const companyTextHorizontal2 = css`
top: 577px;
right: 59px;
position: absolute;
font-size:10.68px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
font-weight: bold;
`
const userQTarget = css`
width: 114px;
    height: 114px;
    color: black
    position: absolute;
    top: 21px;
    right: 155px;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
const userQTarget2 = css`
width: 114px;
    height: 114px;
    color: black
    position: absolute;
    top: 177px;
    left: 155px;
`
const clientFirstName = css`
    position: absolute;
    top: 222px;
    left: 4px;
    width: 148px;
    text-align: center;
`
const clientLastName = css`
    position: absolute;
    top: 236px;
    left: 4px;
    width: 148px;
    text-align: center;
`
const clientEmail = css`
    position: absolute;
    top: 282px;
    left: 4px;
    width: 145px;
    text-align: center;
`
const clientEmailStyle = css`
font-size: 8px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
    font-weight: bold;
`
const clientEmailStyleSmall = css`
white-space: nowrap;
text-overflow: ellipsis;
overflow: hidden;
font-size: 6px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
font-weight: bold;
`
const clientFirstNameStyle = css`
white-space: nowrap;
text-overflow: ellipsis;
overflow: hidden;
font-size: 17px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
font-weight: bold;
`
const clientFirstNameStyleSmall = css`
white-space: nowrap;
text-overflow: ellipsis;
overflow: hidden;
font-size: 10px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
    font-weight: bold;
`
const clientText = css`
    position: absolute;
    top: 266px;
    left: 4px;
    width: 148px;
    text-align: center;
`
const clientTextStyle = css`
font-size: 12px;
color:#2b2d6a;
font-family:Montserrat-Medium, Montserrat;
    font-weight: bold;
`

const clientFirstName2 = css`
    position: absolute;
    bottom: 222px;
    right: 4px;
    width: 148px;
    text-align: center;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
const clientLastName2 = css`
    position: absolute;
    bottom: 236px;
    right: 4px;
    width: 148px;
    text-align: center;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
const clientEmail2 = css`
    position: absolute;
    bottom: 282px;
    right: 4px;
    width: 145px;
    text-align: center;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
const clientText2 = css`
    position: absolute;
    bottom: 266px;
    right: 4px;
    width: 148px;
    text-align: center;
    /* Safari */
    -webkit-transform: rotate(-180deg);
    
    /* Firefox */
    -moz-transform: rotate(-180deg);
    
    /* IE */
    -ms-transform: rotate(-180deg);
    
    /* Opera */
    -o-transform: rotate(-180deg);
`
// const verticalTriangleSubTitle = css`
// font-size:15.03px;
// color:#2b2d6a;
// font-family:Montserrat-Medium, Montserrat;
// stroke:#e6e7e8;
//     stroke-miterlimit:10;
//   `
// const verticalTriangleText = css`
// font-size:10.68px;
// color:#2b2d6a;
// font-family:Montserrat-Medium, Montserrat;
//   `
// const firstTitle = css`
// position: absolute;
// top: 191px;
// left: 408px;
//   `
// const secondTitle = css`
// position: absolute;
// top: 229px;
// left: 408px;
//   `
// const thirdTitle = css`
// position: absolute;
// top: 269px;
// left: 408px;
//   `
// const forthTitle = css`
// position: absolute;
// top: 287px;
// left: 408px;
//   `
const goBackButtonClass = css`
  top: 10px;
  left: 10px
  position: absolute
  `
class VerticalTriangle extends Component {
  render() {
    return (
      <div className={scroller}>
        <div className={verticalTriangleContainer}>
          <img className={firstVerticalLogo} src={this.props.profilePicture} alt="" />
          <img className={secondVerticalLogo} src={this.props.profilePicture} alt="" />
          <QRCode
            bgColor="#FFFFFF"
            fgColor="black"
            level="L"
            className={qrCode}
            value={this.props.userQR}
          />
          <QRCode
            bgColor="#FFFFFF"
            fgColor="black"
            level="L"
            className={qrCode2}
            value={this.props.userQR}
          />
          <h1 className={`${verticalTriangleTitles} ${this.props.companyName.length > 32 ? companyTitleSmall : companyTitle}`}>{this.props.companyName}</h1>
          <h1 className={`${verticalTriangleTitles} ${this.props.companyName.length > 32 ? companyTitle2Small : companyTitle2}`}>{this.props.companyName}</h1>
          <h1 className={`${verticalTriangleTitles} ${companyRif}`}>{`${this.props.companyRifType} ${this.props.companyRif}`}</h1>
          <h1 className={`${verticalTriangleTitles} ${companyRif2}`}>{`${this.props.companyRifType} ${this.props.companyRif}`}</h1>
          <h1 className={`${verticalTriangleTitlesEmail} ${this.props.companyEmail.length > 32 ? companyEmailSmall : companyEmail}`}>{this.props.companyEmail}</h1>
          <h1 className={`${verticalTriangleTitlesEmail} ${this.props.companyEmail.length > 32 ? companyEmail2Small : companyEmail2}`}>{this.props.companyEmail}</h1>
          {/* <h1 className={`${verticalTriangleSubTitle} ${firstTitle}`}>Envía y recibe dinero</h1>
        <h1 className={`${verticalTriangleSubTitle} ${secondTitle}`}>Paga tus compras</h1>
        <h1 className={`${verticalTriangleSubTitle} ${thirdTitle}`}>Usando solo el e-mail</h1>
        <h1 className={`${verticalTriangleSubTitle} ${forthTitle}`}>o código QR</h1> */}

        </div>
      </div>
    )
  }
}
class HorizontalTriangle extends Component {
  render() {
    return (
      <div className={scroller}>
        <div className={horizontalTriangleContainer}>
          <QRCode
            bgColor="#FFFFFF"
            fgColor="black"
            level="L"
            className={userQRHorizontal}
            value={this.props.userQR}
          />
          <QRCode
            bgColor="#FFFFFF"
            fgColor="black"
            level="L"
            className={userQRHorizontal2}
            value={this.props.userQR}
          />
          <h1 className={`${verticalTriangleTitles} ${companyTitleHorizontal}`}>{this.props.companyName}</h1>
          <h1 className={`${verticalTriangleTitles} ${companyTitle2Horizontal}`}>{this.props.companyName}</h1>
          <h1 className={`${verticalTriangleTitles} ${companyRifHorizontal}`}>{`${this.props.companyRifType} ${this.props.companyRif}`}</h1>
          <h1 className={`${verticalTriangleTitles} ${companyRif2Horizontal}`}>{`${this.props.companyRifType} ${this.props.companyRif}`}</h1>
          <img className={firstHorizontalLogo} src={this.props.profilePicture} alt="" />
          <img className={secondHorizontalLogo} src={this.props.profilePicture} alt="" />
          <h1 className={`${verticalTriangleTitlesEmail} ${this.props.companyEmail.length > 17 ? companyEmailHorizontalSmall : companyEmailHorizontal}`}>{this.props.companyEmail}</h1>
          <h1 className={`${verticalTriangleTitlesEmail} ${this.props.companyEmail.length > 17 ? companyEmail2HorizontalSmall : companyEmail2Horizontal}`}>{this.props.companyEmail}</h1>
          <p className={`${verticalTriangleTitles} ${companyTextHorizontal}`}>Para enviarnos un pago,</p>
          <p className={`${verticalTriangleTitles} ${companyTextHorizontal2}`}>escanea nuestro QR</p>
        </div>
      </div>
    )
  }
}
class PresentacionalTarget extends Component {
  render() {
    return (
      <div className={scroller}>
        <div className={presentacionalTarget}>
          <QRCode
            bgColor="#FFFFFF"
            fgColor="black"
            level="L"
            className={userQTarget}
            value={this.props.userQR}
          />
          <QRCode
            bgColor="#FFFFFF"
            fgColor="black"
            level="L"
            className={userQTarget2}
            value={this.props.userQR}
          />
          <h1 className={`${this.props.firstName.length > 15 || this.props.lastName.length > 15 ? clientFirstNameStyleSmall : clientFirstNameStyle} ${clientFirstName}`}>{this.props.firstName}</h1>
          <h1 className={`${this.props.firstName.length > 15 || this.props.lastName.length > 15 ? clientFirstNameStyleSmall : clientFirstNameStyle} ${clientLastName}`}>{this.props.lastName}</h1>
          <h1 className={`${this.props.companyEmail.length > 27 ? clientEmailStyleSmall : clientEmailStyle} ${clientEmail}`}>{this.props.companyEmail}</h1>
          <h1 className={`${clientTextStyle} ${clientText}`}>Escanea mi QR</h1>
          <h1 className={`${this.props.firstName.length > 15 || this.props.lastName.length > 15 ? clientFirstNameStyleSmall : clientFirstNameStyle} ${clientFirstName2}`}>{this.props.firstName}</h1>
          <h1 className={`${this.props.firstName.length > 15 || this.props.lastName.length > 15 ? clientFirstNameStyleSmall : clientFirstNameStyle} ${clientLastName2}`}>{this.props.lastName}</h1>
          <h1 className={`${this.props.companyEmail.length > 27 ? clientEmailStyleSmall : clientEmailStyle} ${clientEmail2}`}>{this.props.companyEmail}</h1>
          <h1 className={`${clientTextStyle} ${clientText2}`}>Escanea mi QR</h1>
        </div>
      </div>
    )
  }
}
class Printable extends Component {
  state = {
    isUserBlocked: false
  }
  componentDidMount = () => {
    const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
    isUserBlockRef.once('value', snapshot => {
      this.setState({
        isUserBlocked: snapshot.val(),
      })
    })
  }
  redirectUser = () => {
    if (this.props.isLoggedIn) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
      if (this.props.userInCollectionData.type) {
        const redirect = (this.props.userInCollectionData.type === 'commerce' || this.props.userInCollectionData.type === 'client') ? null : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }

  render() {
    // console.log("props: ", this.props.userInCollectionData)
    return (
      <div>
        <CommerceHeader />
        {
          this.props.userInCollectionData ?
            <div className={mainProfileContainer}>
              {
                this.props.userInCollectionData.type === 'commerce' ?
                  <div className={banksDataContainer}>
                    <GoBackButton
                      height="21px"
                      width="21px"
                      route='/perfil'
                      style={goBackButtonClass}
                    />
                    <div className={header}>
                      <h1>TRIANGULO VERTICAL</h1>
                    </div>
                    <ReactToPrint
                      trigger={() =>
                        <div className={editButtonContainerAdd}>
                          <img src={printableIcon} alt="EditIcon" className={addIconStyle}></img>
                        </div>
                      }
                      content={() => this.componentRef}
                    />
                    <div className='horizontal-divisor'></div>
                    <VerticalTriangle
                      profilePicture={this.props.userInCollectionData.profilePicture}
                      userQR={`userQR-${this.props.userInCollectionData.userKey}`}
                      companyName={this.props.userInCollectionData.name}
                      companyRif={this.props.userInCollectionData.dni.id}
                      companyRifType={this.props.userInCollectionData.dni.type}
                      companyEmail={this.props.userInCollectionData.email}
                      ref={el => (this.componentRef = el)} />
                  </div>
                  : null
              }
              {
                this.props.userInCollectionData.type === 'commerce' ?
                  <div className={banksDataContainer}>
                    <GoBackButton
                      height="21px"
                      width="21px"
                      route='/perfil'
                      style={goBackButtonClass}
                    />
                    <div className={header}>
                      <h1>TRIANGULO HORIZONTAL</h1>
                    </div>
                    <div onClick={this.print} className={editButtonContainerAdd}>
                      <img src={printableIcon} alt="EditIcon" className={addIconStyle}></img>
                    </div>
                    <div className='horizontal-divisor'></div>
                    <ReactToPrint
                      trigger={() =>
                        <div className={editButtonContainerAdd}>
                          <img src={printableIcon} alt="EditIcon" className={addIconStyle}></img>
                        </div>
                      }
                      content={() => this.componentRef2}
                    />
                    <HorizontalTriangle
                      profilePicture={this.props.userInCollectionData.profilePicture}
                      userQR={`userQR-${this.props.userInCollectionData.userKey}`}
                      companyName={this.props.userInCollectionData.name}
                      companyRif={this.props.userInCollectionData.dni.id}
                      companyRifType={this.props.userInCollectionData.dni.type}
                      companyEmail={this.props.userInCollectionData.email}
                      ref={el => (this.componentRef2 = el)} />
                  </div>
                  : null
              }
              {
                this.props.userInCollectionData.type === 'client' ?
                  <div className={banksDataContainer}>
                    <GoBackButton
                      height="21px"
                      width="21px"
                      route='/perfil'
                      style={goBackButtonClass}
                    />
                    <div className={headerPresentationalTarget}>
                      <h1 css={`margin: -10px !important`}>TARJETA DE PRESENTACIÓN</h1>
                    </div>
                    <div onClick={this.print} className={editButtonContainerAdd}>
                      <img src={printableIcon} alt="EditIcon" className={addIconStyle}></img>
                    </div>
                    <div className='horizontal-divisor'></div>
                    <ReactToPrint
                      trigger={() =>
                        <div className={editButtonContainerAdd}>
                          <img src={printableIcon} alt="EditIcon" className={addIconStyle}></img>
                        </div>
                      }
                      content={() => this.componentRef3}
                    />
                    <PresentacionalTarget
                      profilePicture={this.props.userInCollectionData.profilePicture}
                      userQR={`userQR-${this.props.userInCollectionData.userKey}`}
                      companyName={this.props.userInCollectionData.name}
                      firstName={this.props.userInCollectionData.firstName}
                      lastName={this.props.userInCollectionData.lastName}
                      companyRif={this.props.userInCollectionData.dni.id}
                      companyRifType={this.props.userInCollectionData.dni.type}
                      companyEmail={this.props.userInCollectionData.email}
                      ref={el => (this.componentRef3 = el)} />
                  </div>
                  : null
              }
              <div css={`margin-bottom: 50px;`}></div>

              {/* <ToastContainer
            className={{
              fontSize: "15px"
            }} /> */}
            </div>
            : null
        }
        {
          this.redirectUser()
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    isLoggedIn: state.general.isLoggedIn
  }
}

export default connect(mapStateToProps)(Printable)