import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import moment from 'moment'
import { toast } from 'react-toastify'
import { db, auth } from '../../../firebase'
import CommerceHeader from './../commerce-client-header/CommerceClientHeader'
import CommerceWelcome from './commerce-client-welcome/CommerceWelcome'
import ClientWelcome from './commerce-client-welcome/ClientWelcome'
import SendMoneyModal from '../../general/modals/SendMoneyModal'
import Table from '../../general/tables/Table'
import { postRequest } from '../../../utils/createAxiosRequest'
import { customConsoleLog } from '../../../utils/customConsoleLog'
import { requestPermission } from '../../../push-notification'
import TransactionDetailsModal from '../../general/modals/transactionDetailsModal'

import {
  userLogout,
  getApprovedHistoryTransactions,
  getRejectedHistoryTransactions,
  getPendingHistoryTransactions,
  getMoneyRequestTransactions,
  getCommerces
} from '../../../redux/actions/general'

const pendingRequestColumns = [
  { label: 'Fecha', key: 'date' },
  { label: 'Usuario', key: 'userImageNameAccount' },
  { label: 'Monto / Estado', key: 'thirdColumn' },
  { label: 'Monto / Acción', key: 'pendingRequestButtons' },
]
const pendingWithdraws = [
  { label: 'Fecha', key: 'date' },
  { label: 'Usuario', key: 'userImageNameAccount' },
  { label: 'Estado', key: 'thirdColumn' },
  { label: 'Monto', key: 'statusAmount' },
]
const rejectPendingRequestsColumns = [
  { label: 'Fecha', key: 'date' },
  { label: 'Usuario', key: 'userImageNameAccount' },
  { label: 'Estado', key: 'thirdColumn' },
  { label: 'Monto', key: 'statusAmount' },
]
const recentMovementsColumns = [
  { label: 'USUARIO', key: 'userImageNameAccount' },
  { label: 'FECHA', key: 'date' },
  { label: 'DESCRIPCIÓN', key: 'recentMovementsThirdColumn' },
  { label: 'ID', key: 'recentMovementsTransactionId' },
  // { label: 'Correo', key: 'recentMovementsThirdColumn' },
  { label: 'CANTIDAD RECIBIDA', key: 'recentMovementsAmounts' },
  { label: 'TARIFA', key: 'recentMovementsFeeAmounts' },
  { label: 'PAGO RECIBIDO', key: 'recentMovementsReceiveAmounts' },
]

const scrollDataRequestedRange = 10
class CommerceDashboard extends Component {
  state = {
    commerces: {},
    lastSignInTime: null,
    sendLoading: false,
    rejectLoading: false,
    loadingCommerces: true,
    sendModalIsOpen: false,
    isReject: false,
    selectedRequest: [],
    userAmount: '',
    isUserBlocked: false,
    //special password
    isCreatePasswordOpen: false,
    displayErrors: false,
    errorMessage: '',
    firstNumber: '',
    secondNumber: '',
    thirdNumber: '',
    fourthNumber: '',
    hasSecurityPassword: false,
    //reset special password
    isResetPasswordOpen: false,
    isSended: false,
    //SCROLLING
    recentsMovementsIndex: 10,
    pendingRequestIndex: 10,
    commercesIndex: 10,
    //transaction detail modal
    transactionDetailModalIsOpen: false,
    selectedTransaction: "",
  }
  componentDidMount = () => {
    //PUSH NOTIFICATIONS
    if (this.props.isLoggedIn) {
      requestPermission()
    }
    auth.onAuthStateChanged(user => {
      if (user) {
        const lastSignInTime = moment(user.metadata.lastSignInTime).utc(-264).format('DD-MM-YYYY h:mm a')
        const transactionHistorylimitIndex = this.state.recentsMovementsIndex
        const moneyRequestTransactionslimitIndex = this.state.pendingRequestIndex
        this.setState({ lastSignInTime: lastSignInTime })
        const userKey = this.props.userInCollectionData.userKey
        if (this.props.userInCollectionData) {
          this.props.dispatch(getApprovedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
          this.props.dispatch(getRejectedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
          this.props.dispatch(getPendingHistoryTransactions({ userKey, transactionHistorylimitIndex }))
          this.props.dispatch(getMoneyRequestTransactions({ userKey, moneyRequestTransactionslimitIndex }))
        }
      }
    })
    if (this.props.userInCollectionData && this.props.userInCollectionData.userKey) {
      const usersRef = db.ref(`users/${this.props.userInCollectionData.userKey}/amount`)
      usersRef.on('value', async (snapshot) => {
        this.setState({
          userAmount: snapshot.val(),
        })
      })
      const hasSecurityPasswordRef = db.ref(`users/${this.props.userInCollectionData.userKey}/hasSecurityPassword`)
      hasSecurityPasswordRef.on('value', async (snapshot) => {
        this.setState({
          hasSecurityPassword: snapshot.val(),
        })
      })
      const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
      isUserBlockRef.once('value', snapshot => {
        this.setState({
          isUserBlocked: snapshot.val(),
        })
      })
    }
    const commercesIndex = this.state.commercesIndex
    this.props.dispatch(getCommerces({ commercesIndex }))
  }
  databaseRequestCommerces = () => {
    customConsoleLog("databaseRequestCommerces")
    this.setState({ commercesIndex: this.state.commercesIndex + scrollDataRequestedRange }, () => {
      const commercesIndex = this.state.commercesIndex
      this.props.dispatch(getCommerces({ commercesIndex }))
    })
  }
  approvedHistoryTransactionsRequest = () => {
    customConsoleLog("----get new approved history transactions----")
    this.setState({ recentsMovementsIndex: this.state.recentsMovementsIndex + scrollDataRequestedRange }, () => {
      const transactionHistorylimitIndex = this.state.recentsMovementsIndex
      const userKey = this.props.userInCollectionData.userKey
      this.props.dispatch(getApprovedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
    })
  }
  rejectedHistoryTransactionsRequest = () => {
    customConsoleLog("----get new rejected history transactions----")
    this.setState({ recentsMovementsIndex: this.state.recentsMovementsIndex + scrollDataRequestedRange }, () => {
      const transactionHistorylimitIndex = this.state.recentsMovementsIndex
      const userKey = this.props.userInCollectionData.userKey
      this.props.dispatch(getRejectedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
    })
  }
  pendingHistoryTransactionsRequest = () => {
    customConsoleLog("----get new pending history transactions----")
    this.setState({ recentsMovementsIndex: this.state.recentsMovementsIndex + scrollDataRequestedRange }, () => {
      const transactionHistorylimitIndex = this.state.recentsMovementsIndex
      const userKey = this.props.userInCollectionData.userKey
      this.props.dispatch(getPendingHistoryTransactions({ userKey, transactionHistorylimitIndex }))
    })
  }
  moneyRequestTransactionsRequest = () => {
    customConsoleLog("----get new money requests transactions----")
    this.setState({ pendingRequestIndex: this.state.pendingRequestIndex + scrollDataRequestedRange }, () => {
      const moneyRequestTransactionslimitIndex = this.state.pendingRequestIndex
      const userKey = this.props.userInCollectionData.userKey
      this.props.dispatch(getMoneyRequestTransactions({ userKey, moneyRequestTransactionslimitIndex }))
    })
  }
  openSendMoneyModal = config => () => {
    this.setState({
      sendModalIsOpen: !this.state.sendModalIsOpe,
      selectedRequest: config.info,
      isReject: config.isReject
    })
  }
  closeSendMoneyModal = () => {
    this.setState({
      sendModalIsOpen: this.state.sendLoading ? true : false,
      isCreatePasswordOpen: false,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  toastLoaderId = null
  sendPayment = async (event) => {
    event.preventDefault()
    this.toastLoaderSendId = toast('Enviando dinero...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    if (
      this.state.selectedRequest.amount <= this.state.userAmount &&
      (
        this.state.hasSecurityPassword
          ? this.state.firstNumber &&
          this.state.secondNumber &&
          this.state.thirdNumber &&
          this.state.fourthNumber
          : true
      )
    ) {
      const data = {
        uid: this.props.userInCollectionData.userKey,
        transactionID: this.state.selectedRequest.idTransaction,
        status: 'Approved',
        securityPassword:
          this.state.firstNumber +
          this.state.secondNumber +
          this.state.thirdNumber +
          this.state.fourthNumber
      }
      customConsoleLog("SENDED:", data)
      try {
        let response = await postRequest('transactions/processMoneyRequest', data)
        customConsoleLog('sendPayment response', response)
        if (response.data.success) {
          this.approvedHistoryTransactionsRequest()
          this.moneyRequestTransactionsRequest()
          this.setState({
            sendLoading: false,
            displayErrors: false,
            sendModalIsOpen: false,
          })
          toast.update(this.toastLoaderSendId, {
            render: 'Dinero enviado exitosamente',
            autoClose: 5000,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, errorMessage: message, displayErrors: true })
          return toast.update(this.toastLoaderSendId, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        customConsoleLog("sendPayment error: ", error)
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderSendId, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({ sendLoading: false, displayErrors: true })
      if (this.state.selectedRequest.amount > this.state.userAmount) {
        this.setState({ errorMessage: 'Saldo insuficiente.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Saldo insuficiente.',
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
      if ((!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) &&
        this.state.hasSecurityPassword) {
        this.setState({ errorMessage: 'Contraseña de seguridad incompleta.' })
        return toast.update(this.toastLoaderSendId, {
          render: 'Contraseña de seguridad incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  rejectPayment = async (event) => {
    event.preventDefault()
    this.toastLoaderSendId = toast('Rechazando pago...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    const data = {
      uid: this.props.userInCollectionData.userKey,
      transactionID: this.state.selectedRequest.idTransaction,
      status: 'Rejected',
    }
    customConsoleLog("SENDED:", data)
    try {
      let response = await postRequest('transactions/processMoneyRequest', data)
      customConsoleLog('sendPayment response', response)
      if (response.data.success) {
        this.rejectedHistoryTransactionsRequest()
        this.moneyRequestTransactionsRequest()
        this.setState({
          sendLoading: false,
          displayErrors: false,
          sendModalIsOpen: false
        })
        toast.update(this.toastLoaderSendId, {
          render: 'Pago rechazado con exito.',
          autoClose: 5000,
        })
      } else {
        const { message } = response.data.error
        this.setState({ sendLoading: false, errorMessage: message, displayErrors: true })
        return toast.update(this.toastLoaderSendId, {
          render: message,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    } catch (error) {
      customConsoleLog("sendPayment error: ", error)
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.update(this.toastLoaderSendId, {
        render: errorMessage,
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
      this.setState({
        sendLoading: false,
        errorMessage,
        displayErrors: true
      })
    }
  }
  redirectCommerce = () => {
    if (this.props.isLoggedIn) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
      if (this.props.userInCollectionData) {
        const redirect = (this.props.userInCollectionData.type === 'commerce'
          || this.props.userInCollectionData.type === 'client')
          ? null
          : <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  openCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: true,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  onCreateSpecialPasswordClick = async (event) => {
    event.preventDefault()
    this.createPasswordToaster = toast('Creando contraseña...', { autoClose: false })
    this.setState({ sendLoading: true, displayErrors: false, errorMessage: '' })
    if (
      this.state.firstNumber &&
      this.state.secondNumber &&
      this.state.thirdNumber &&
      this.state.fourthNumber
    ) {
      try {
        const data = {
          securityPassword:
            this.state.firstNumber +
            this.state.secondNumber +
            this.state.thirdNumber +
            this.state.fourthNumber,
          uid: this.props.userInCollectionData.userKey
        }
        customConsoleLog("SENDED: ", data)
        let response = await postRequest('security/createSecurityPassword', data)
        customConsoleLog('CreateSpecialPassword response', response)
        if (response.data.success) {
          toast.update(this.createPasswordToaster, {
            render: 'Tu contraseña especial ha sido creada exitosamente.',
            autoClose: 5000,
          })
          this.setState({
            sendLoading: false,
            firstNumber: '',
            secondNumber: '',
            thirdNumber: '',
            fourthNumber: '',
            isCreatePasswordOpen: false,
            hasSecurityPassword: true,
          })
        } else {
          const { message } = response.data.error
          this.setState({ sendLoading: false, displayErrors: true, errorMessage: message })
          return toast.update(this.createPasswordToaster, {
            render: message,
            type: toast.TYPE.ERROR,
            autoClose: 5000,
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.createPasswordToaster, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          sendLoading: false,
          errorMessage,
          displayErrors: true
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        sendLoading: false
      })
      if (!this.state.firstNumber ||
        !this.state.secondNumber ||
        !this.state.thirdNumber ||
        !this.state.fourthNumber) {
        this.setState({ errorMessage: 'Contraseña especial incompleta.' })
        return toast.update(this.createPasswordToaster, {
          render: 'Contraseña especial incompleta.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
    }
  }
  cancelCreatePasswordClick = () => {
    this.setState({
      isCreatePasswordOpen: false,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  //RESET SPECIAL PASSWORD
  onResetSpecialPasswordClick = async () => {
    this.setState({
      displayErrors: false,
      errorMessage: ""
    })
    this.resetSpecialPassToast = toast('Restableciendo contraseña especial...', { autoClose: false })
    const data = {
      mode: 'secretPasswordToken',
      uid: this.props.userInCollectionData.userKey,
    }
    customConsoleLog("SENDED: ", data)
    try {
      this.setState({ sendLoading: true })
      let response = await postRequest('security/generatePasswordTokenByEmail', data)
      customConsoleLog("RESPOSNE: ", response)
      if (response.data.success) {
        this.setState({
          sendLoading: false,
          sent: true,
          isSended: true
        })
        toast.update(this.resetSpecialPassToast, {
          render: "Casi listo, Ahora ingresa a tu correo y sigue las instrucciones...",
          autoClose: 5000
        })
      } else {
        this.setState({
          sendLoading: false,
          displayErrors: true,
          errorMessage: `${response.data.error.message}`
        })
      }
    } catch (error) {
      const errorMessage = ({
        '401': 'Su sesión ha expirado',
        'default': 'Verifique su conexión y vuelva a intentarlo.'
      })[error.response ? error.response.status || 'default' : 'default']
      toast.update(this.resetSpecialPassToast, {
        render: errorMessage,
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
      this.setState({
        sendLoading: false,
        displayErrors: true,
        errorMessage,
      })
      customConsoleLog('catch err', error)
    }
  }
  openResetPassword = () => {
    this.setState({
      isResetPasswordOpen: true,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  cancelResetPasswordClick = () => {
    this.setState({
      isResetPasswordOpen: false,
      isSended: false,
      displayErrors: false,
      errorMessage: '',
      firstNumber: '',
      secondNumber: '',
      thirdNumber: '',
      fourthNumber: '',
    })
  }
  //HISTORY MODAL
  closeTransactionDetailsModal = () => {
    this.setState({
      transactionDetailModalIsOpen: false,
      selectedTransaction: ""
    })
  }
  onHistoryTransactionClick = ({ transaction }) => () => {
    this.setState({
      selectedTransaction: transaction,
      transactionDetailModalIsOpen: true
    })
  }
  render() {
    //APPROVED TRANSACTIONS
    const approvedTransactions = this.props.approvedTransactionsHistory
    const loadingApprovedTransactions = this.props.loadingApprovedTransactions
    //REJECT TRANSACTIONS
    const rejectedTransactions = this.props.rejectedTransactionsHistory
    const loadingRejectedTransactions = this.props.loadingRejectedTransactions
    //PENDING TRANSACTIONS
    const pendingTransactions = this.props.pendingTransactionsHistory
    const loadingPendingTransactions = this.props.loadingPendingTransactions
    //REJECT MONEY REQUEST
    const rejectMoneyRequests = rejectedTransactions
      ? rejectedTransactions.filter(
        transaction =>
          transaction.mode !== 'Withdraw' &&
          transaction.mode !== 'Deposit')
      : null
    //MONEY REQUEST
    const loadingMoneyRequest = this.props.loadingMoneyRequest
    //PENDING MONEY REQUESTS
    const pendingMoneyRequest = this.props.moneyRequests
      ? Object.values(this.props.moneyRequests).filter(
        transaction =>
          transaction.status === 'Pending' &&
          (transaction.mode === 'Send' || transaction.mode === 'Receive') &&
          transaction.mode !== 'Deposit'
      )
      : null
    //COMMERCES
    const commerces = this.props.commerces
    const loadingCommerces = this.props.loadingCommerces
    // customConsoleLog("isUserBlocked: ", this.state.isUserBlocked)
    // customConsoleLog("recentsMovementsIndex: ", this.state.recentsMovementsIndex)
    // customConsoleLog("pendingRequestIndex: ", this.state.pendingRequestIndex)
    // customConsoleLog("commercesIndex: ", this.state.commercesIndex)
    // customConsoleLog("approvedTransactions: ", approvedTransactions)
    // customConsoleLog("rejectedTransactions: ", rejectedTransactions)
    // customConsoleLog("pendingTransactions: ", pendingTransactions)
    // customConsoleLog("commerces: ", this.state.commerces)
    // customConsoleLog("dataMoneyRequests: ", this.state.dataMoneyRequests)
    // customConsoleLog("rejectMoneyRequests: ", rejectMoneyRequests)

    return (
      <div>
        <CommerceHeader />
        {
          this.props.userInCollectionData
            ? this.props.userInCollectionData.type === "client"
              ? <ClientWelcome
                lastSignInTime={this.state.lastSignInTime}
                data={this.props.userInCollectionData}
                userAmount={this.state.userAmount}
                //recent movement tables
                titleForRecentMovements="MOVIMIENTOS RECIENTES"
                dataForRecentMovements={approvedTransactions}
                loadingForRecentMovements={loadingApprovedTransactions}
                type="recentsMovements"
                columns={recentMovementsColumns}
                onHistoryTransactionClick={this.onHistoryTransactionClick}
                //commerces table
                titleCommerces="NUEVOS COMERCIOS"
                commerces={commerces}
                loadingCommerces={loadingCommerces}
                hasSecurityPassword={this.state.hasSecurityPassword}
                databaseRequestCommerces={this.databaseRequestCommerces}
                //requests
                approvedHistoryTransactionsRequest={this.approvedHistoryTransactionsRequest}
                pendingHistoryTransactionsRequest={this.pendingHistoryTransactionsRequest}
                moneyRequestTransactionsRequest={this.moneyRequestTransactionsRequest}
              />
              : <CommerceWelcome
                userAmount={this.state.userAmount}
                lastSignInTime={this.state.lastSignInTime}
                data={this.props.userInCollectionData}
                hasSecurityPassword={this.state.hasSecurityPassword}
                historyTransactionsRequest={this.approvedHistoryTransactionsRequest}
                moneyRequestTransactionsRequest={this.moneyRequestTransactionsRequest}
              />
            : null
        }
        {
          this.props.userInCollectionData
            ? this.props.userInCollectionData.type === 'commerce'
              ?
              <Table
                title="MOVIMIENTOS RECIENTES"
                columns={recentMovementsColumns}
                height={450}
                minHeight="531px" //height + 81px
                mobileMediaWidth="1100px"
                data={approvedTransactions}
                isThereBackButton={false}
                loading={loadingApprovedTransactions}
                //table header
                type="recentsMovements"
                databaseRequest={this.approvedHistoryTransactionsRequest}
                disableHeader={false}
                onHistoryTransactionClick={this.onHistoryTransactionClick}
              />
              : null
            : null
        }
        {
          this.props.userInCollectionData
            ? <Table
              title="SOLICITUDES DE PAGO PENDIENTES"
              columns={pendingRequestColumns}
              height={450}
              minHeight="531px" //height + 81px
              data={pendingMoneyRequest}
              isThereBackButton={false}
              loading={loadingMoneyRequest}
              mobileMediaWidth="800px"
              //table header
              type="pendingRequest"
              //pending requests
              sendLoading={this.state.sendLoading}
              rejectLoading={this.state.rejectLoading}
              openSendMoneyModal={this.openSendMoneyModal}
              goHistorialButtonIsShown={false}
              databaseRequest={this.moneyRequestTransactionsRequest}
            />
            : null
        }
        {
          this.props.userInCollectionData
            ? this.props.userInCollectionData.type === 'commerce' ||
              this.props.userInCollectionData.type === 'client'
              ? <Table
                title="SOLICITUDES DE PAGO RECHAZADAS"
                columns={rejectPendingRequestsColumns}
                height={450}
                minHeight="531px" //height + 81px
                data={rejectMoneyRequests}
                isThereBackButton={false}
                mobileMediaWidth="800px"
                loading={loadingRejectedTransactions}
                //table header
                type="rejectedRequests"
                goHistorialButtonIsShown={false}
                //requests
                databaseRequest={this.rejectedHistoryTransactionsRequest}
              />
              : null
            : null
        }
        <Table
          title="RETIROS PENDIENTES"
          columns={pendingWithdraws}
          height={450}
          minHeight="531px" //height + 81px
          data={pendingTransactions
            ? pendingTransactions.filter(transaction =>
              transaction.mode === 'Withdraw')
            : null}
          isThereBackButton={false}
          //table header
          loading={loadingPendingTransactions}
          type="pendingWithdraw"
          goHistorialButtonIsShown={false}
          databaseRequest={this.pendingHistoryTransactionsRequest}
        />
        <TransactionDetailsModal
          onClose={this.closeTransactionDetailsModal}
          showModal={this.state.transactionDetailModalIsOpen}
          transaction={this.state.selectedTransaction}
        />
        <SendMoneyModal
          description={this.state.selectedRequest.description}
          senderPicture={this.state.selectedRequest.receiverProfilePicture}
          loading={this.state.sendLoading}
          onClose={this.closeSendMoneyModal}
          showModal={this.state.sendModalIsOpen}
          amount={this.state.selectedRequest.amount}
          userName={this.state.selectedRequest.receiverUsername}
          onSendMoney={this.sendPayment}
          onRejectMoney={this.rejectPayment}
          isReject={this.state.isReject}
          //special password
          firstNumber={this.state.firstNumber}
          secondNumber={this.state.secondNumber}
          thirdNumber={this.state.thirdNumber}
          fourthNumber={this.state.fourthNumber}
          onChange={this.handleInputChange}
          displayErrors={this.state.displayErrors}
          errorMessage={this.state.errorMessage}
          isCreatePasswordOpen={this.state.isCreatePasswordOpen}
          openCreatePasswordClick={this.openCreatePasswordClick}
          onCreateSpecialPasswordClick={this.onCreateSpecialPasswordClick}
          hasSecurityPassword={this.state.hasSecurityPassword}
          cancelCreatePasswordClick={this.cancelCreatePasswordClick}
          //reset special password
          isResetPasswordOpen={this.state.isResetPasswordOpen}
          onResetSpecialPasswordClick={this.onResetSpecialPasswordClick}
          cancelResetPasswordClick={this.cancelResetPasswordClick}
          openResetPassword={this.openResetPassword}
          isSended={this.state.isSended}
          specialPasswordIsShow={
            this.props.userInCollectionData
              ? this.props.userInCollectionData.type === 'commerce'
                ? true
                : false
              : false
          }
        />
        <div css={`margin-bottom: 50px;`}></div>
        {
          this.redirectCommerce()
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn,
    //HISTORY TRANSACTIONS
    //approved
    approvedTransactionsHistory: state.general.approvedTransactionsHistory,
    loadingApprovedTransactions: state.general.loadingApprovedTransactions,
    //rejected
    rejectedTransactionsHistory: state.general.rejectedTransactionsHistory,
    loadingRejectedTransactions: state.general.loadingRejectedTransactions,
    //pending
    pendingTransactionsHistory: state.general.pendingTransactionsHistory,
    loadingPendingTransactions: state.general.loadingPendingTransactions,
    //MONEY REQUEST TRABSACTIONS
    moneyRequests: state.general.moneyRequests,
    loadingMoneyRequest: state.generalloadingMoneyRequest,
    //GET COMMERCES
    commerces: state.general.commerces,
    loadingCommerces: state.loadingCommerces,

  }
}
export default connect(mapStateToProps)(CommerceDashboard)