import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css } from 'emotion'
import callIcon from '../../../../assets/ic-call.svg'
import sendCommentsIcon from '../../../../assets/send-comments-icon.svg'
import reportProblemIcon from '../../../../assets/report-problem-icon.svg'
import sendMoneyIcon from '../../../../assets/help-icons/ic-send-payment.svg'
import requestPeymentIcon from '../../../../assets/help-icons/ic-request-payment.svg'
import addFoundsIcon from '../../../../assets/help-icons/icon-add-funds.svg'
import withdrawIcon from '../../../../assets/help-icons/icon-withdraw.svg'
import paymentMethodIcon from '../../../../assets/help-icons/icon-credit-card.svg'
import banksAccountsIcon from '../../../../assets/help-icons/icon-bank.svg'
import myProfileIcon from '../../../../assets/help-icons/icon-profile.svg'
import commercesIcon from '../../../../assets/help-icons/icon-stores.svg'
import reportProblemIconRed from '../../../../assets/report-problem-icon-red.svg'
import CommerceHeader from './../../commerce-client-header/CommerceClientHeader'
import wsBussinesIcon from '../../../../assets/wsBussines.svg'
import arrowBack from '../../../../assets/arrowBack.svg'
import AcceptButton from '../../../general/AcceptButton'
import { ToastContainer, toast } from 'react-toastify'
import styled from 'react-emotion'
import iconCamera from '../../../../assets/icon-camera.svg'
import moment from 'moment'
import { Redirect } from 'react-router'
import { postRequest } from '../../../../utils/createAxiosRequest'
import { db } from './../../../../firebase'
import { userLogout } from '../../../../redux/actions/general'

const range = n => Array.from(Array(n), (_, i) => i)
const header = css`
    position: relative;
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    /* padding: 34px 25px; */
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center;
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
    @media(max-width: 430px) {
      flex-direction: column;
      div{
        margin-top: 10px;
      }
    }
  `
const cardTableComponent = css`
    height: auto;
    padding: 0 20px;
    position: relative;
    @media(max-width: 400px) {
      padding: 0 5px;
    }
  `
const sectionsContainer = css`
    position: relative; 
    height: auto;
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: row;
    @media(max-width: 1200px){
      flex-direction: column
    }
  `
const reportProblemSectionsContainer = css`
    position: relative; 
    height: auto;
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
  `
const section = css`
  width: 50%;
  height: auto;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  @media(max-width: 1200px){
    width: 100%;
  }
  `
const sectionChild = css`
  display: flex;
  justify-content: left;
  flex-direction: row;
  margin: 20px;
  cursor: pointer
  `
const sectionChildTitle = css`
  font-family: Montserrat;
  font-size: 24px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #95fad8;
  margin-left: 20px;
  margin-top: 20px;
  @media(max-width: 400px){
    font-size: 20px;
  }
  @media(max-width: 360px){
    font-size: 10px;
  }
  `
const questionsTitleText = css`
font-family: Montserrat;
font-size: 18px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.01;
letter-spacing: 0.8px;
color: #f6f7fa;
  `
const questionText = css`
font-family: Montserrat;
font-size: 14px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
color: #f6f7fa;
cursor: pointer
  `
const margin = css`
margin-top: 20px
  `
const answersContainer = css`
padding: 10px;
border: 1px white solid;
border-radius: 20px;
  `
const questionContainer = css`
margin-top: 15px;
margin-bottom 15px;
  `
const wsBussinesIconClass = css`
width: 34px;
height: 34px;
  `
const goBackButton = css`
position: absolute;
top: 21px;
left: 21px;
border: none;
background: transparent;
height: 21px;
width: 21px;
cursor: pointer;
  `
const reportProblemIconRedClass = css`
width: 22px;
height: 19px;
margin-left: 38px;
  `
const sendMessageIconClass = css`
width: 22px;
height: 19px;
margin-left: 25px;
  `
const secionTitleText = css`
margin-left: 38px !important;
  `
const reportProblemTitle = css`
margin-left: 9px !important;
color: #ff6061 !important;
@media(max-width: 380px){
  font-size: 10px !important
}
  `
const categorySecondTitle = css`
  margin-left: 9px !important;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: normal;
  text-align: left;
  color: #95fad8;

  `
const supportMargin = css`
margin-top: 30px;
margin-left: 30px;
@media(max-width: 600px){
  margin-top: 15px;
margin-left: 0;
}
  `
const whatsappBussinesContainer = css`
display: flex;
margin-bottom: 20px;
  `
// const whatsappBussinesText = css`
// margin-top: 7px;
// margin-left: 13px;
//   `
const writeUsText = css`
font-family: Montserrat;
font-size: 15.7px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
line-height: 1.5;
letter-spacing: normal;
text-align: left;
color: #f6f7fa;
  `
const writeUsTextContainer = css`
margin-top: 20px;
margin-bottom: 20px;
  `
const registerTextArea = css`
    color: #8078ff;
    font-size: 18px;
    padding: 10px 10px 10px 5px;
    display: block;
    width: 100%;
    resize: none;
    border: none;
    border-bottom: 1px solid #757575;
    background-color: transparent;
`
const SendMessageButton = css`
margin: 11px;
@media(max-width: 380px){
  width: 90% !important;
  height: 46.8px !important;
}
`
const sendMessageButtonContainer = css`
text-align: right;
@media(max-width: 1200px){
  text-align: center;
}
`
const reportProblemButtonContainer = css`
  text-align: center;
`
const displayErrors = css`
  input:invalid {
    border-color:red;
  }
  textarea:invalid {
    border-color:red;
  }
  input:invalid specficError 
`
const specficError = css`
border: 3px solid red;
`
const sendMessageForm = css`
margin-bottom: 20px;
display: flex;
flex-direction: column;
input{
  @media(max-width: 350px){
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
      font-size: 11px
    }
    ::-moz-placeholder { /* Firefox 19+ */
      font-size: 11px
    }
    :-ms-input-placeholder { /* IE 10+ */
      font-size: 11px
    }
    :-moz-placeholder { /* Firefox 18- */
      font-size: 11px
    }
  }
}
`
const smallCardTable = css`
max-width: 660px;
border-radius: 5px;
background-color: #2a2c6a;
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
display: flex;
flex-direction: column;
margin: auto;
margin-top: 50px;
margin-bottom: 50px;
@media(max-width: 730px) {
  margin-left: 25px;
  margin-right: 25px;
  margin-top: 40px;
}
`
const reportProblemAttachText = css`
  font-family: Montserrat;
  font-size: 18px;
  font-weight: 900;
  font-style: normal;
  font-stretch: normal;
  line-height: 0.87;
  letter-spacing: normal;
  text-align: left;
  color: #f6f7fa;
  margin-bottom: 10px;
`
const UploadBtn = styled('button')`
  width: 192px;
  height: 190.5px;
  color: #242656;
  display: flex;
  justify-content: center;
  align-items: center;
  outline: none !important;
  cursor: pointer;
  background-color: #95fad8;
  border: none;
  border-radius: 11.9px;
  box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
  position: relative;
  margin: auto;
  @media(max-width: 630px) {
    width: 72px;
    height: 72px;
  }
`
const uploadBtnWrapper = css`
    margin-top: 15px;
    margin-bottom: 20px;
  `
const iconCameraClass = css`
    width: 41px;
    height: 37.4px;
    object-fit: contain;
    @media(max-width: 630px) {
      width: 35px;
      height: 32px;
    }
  `
const imagePreview = css`
    width: 100%;
    height: 100%;
    position: absolute;
    cursor: pointer;
    border-radius: 11.9px;
    object-fit: cover;
    border: 2px solid white;
  `
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const optionalText = css`
    opacity: 0.5;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.29;
    letter-spacing: normal;
    text-align: left;
    color: #f6f7fa;
    margin-bottom: 20px
  `
const categoryTitle = css`
    font-size: 20px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1;
    letter-spacing: 0.9px;
    text-align: left;
    color: #f6f7fa;
    margin-top: 20px;
    margin-bottom: 20px;
  `
const categoryButtonsContainer = css`
    width: 100%;
    display: flex;
    flex-direction: column
  `
const categoryContainer = css`
    width: 100%;
    display: flex;
    flex-direction: row;
    position: relative;
    padding-left: 10px;
    padding-bottom: 15px;
    padding-top: 15px;
    cursor: pointer
  `
const categoryContainerReadOnly = css`
    width: 100%;
    display: flex;
    flex-direction: row;
    position: relative;
    padding-left: 10px;
    padding-bottom: 15px;
    padding-top: 15px;
  `
const categoryDivisor = css`
    height: 1.4px;
    opacity: 0.5;
    background-color: #1d1f4a;
    position: absolute;
    bottom: 0;
    width: 100%;
  `
const linkCallButton = css`
    margin-left: 20px;
    cursor: pointer
  `
const tableCard = css`
    border-radius: 5px;
    background-color: #2a2c6a;
    box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: column;
    max-width: 1200px;
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
    @media(max-width: 1230px) {
      margin: 0 25px;
      margin-top: 40px;
      margin-bottom: 50px;
    }
  `
class History extends Component {
  state = {
    actives: [],
    isHelpSupportOpen: true,
    isSupportOpen: false,
    IsReportProblemOpen: false,
    IsSendMesaggeOpen: false,
    sendMessageLoading: false,
    message: '',
    email: '',
    name: '',
    imageError: false,
    problemImage: '',
    problemPictureURI: '',
    problemMessage: '',
    reportProblemLoading: false,
    sendMesaggeFirstSectionIsOpen: true,
    category: '',
    sendCommentLoading: false,
    opinion: '',
    commentImage: '',
    commentImageURI: '',
    selectedCategory: '',
    numberIsShowed: false,
    isUserBlocked: false
  }
  componentDidMount = () => {
    const numberOfQuestions = 14
    this.setState({
      actives: range(numberOfQuestions).map(() => ({
        active: false
      }))
    })
    const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
    isUserBlockRef.once('value', snapshot => {
      this.setState({
        isUserBlocked: snapshot.val(),
      })
    })
  }
  showNumber = () => {
    this.setState({ numberIsShowed: !this.state.numberIsShowed })
  }
  goBack = () => {
    this.setState({
      isHelpSupportOpen: true,
      isSupportOpen: false,
      IsReportProblemOpen: false,
      IsSendMesaggeOpen: false,
      sendMessageLoading: false,
      message: '',
      email: '',
      name: '',
      imageError: false,
      problemImage: '',
      problemPictureURI: '',
      problemMessage: '',
      reportProblemLoading: false,
      displayErrors: false,
      sendMesaggeFirstSectionIsOpen: true,
      category: '',
      opinion: '',
      commentImage: '',
      commentImageURI: '',
      selectedCategory: ''
    })
  }
  goToSendMessageFirstSection = () => {
    this.setState({
      sendMesaggeFirstSectionIsOpen: true,
    })
  }
  openSuppport = () => {
    this.setState({
      isHelpSupportOpen: false,
      isSupportOpen: true,
      IsReportProblemOpen: false,
      IsSendMesaggeOpen: false
    })
  }
  openSendMessage = () => {
    this.setState({
      isHelpSupportOpen: false,
      isSupportOpen: false,
      IsReportProblemOpen: false,
      IsSendMesaggeOpen: true
    })
  }
  openReportProblem = () => {
    this.setState({
      isHelpSupportOpen: false,
      isSupportOpen: false,
      IsReportProblemOpen: true,
      IsSendMesaggeOpen: false
    })
  }
  handleInputChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    })
  }
  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0];
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            problemImage: reader.result,
            imageError: false
          })
        }
        reader.readAsDataURL(file);
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }
  encodeImageFileAsURLComments = (element) => {
    const file = element.target.files[0];
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            commentImage: reader.result,
            imageError: false
          })
        }
        reader.readAsDataURL(file);
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }
  toastLoaderId = null
  onSendMessage = (event) => {
    event.preventDefault()
    this.setState({ sendMessageLoading: true })
    this.toastLoaderId = toast('Enviando mensaje...', { autoClose: false })
    if (this.state.message !== '') {
      const newMessageKey = db.ref(`messagesFromUsers`).push().key
      db.ref(`messagesFromUsers/${newMessageKey}`).set({
        name: this.props.userInCollectionData.name,
        email: this.props.userInCollectionData.email,
        message: this.state.message,
        messageKey: newMessageKey,
        userKey: this.props.userInCollectionData.userKey
      })
      this.setState({
        sendMessageLoading: false,
        message: '',
        displayErrors: false
      })
      toast.update(this.toastLoaderId, {
        render: "Mensaje enviado exitosamente.",
        autoClose: 5000
      })
    } else {
      this.setState({
        displayErrors: true,
        sendMessageLoading: false
      })
      if (this.state.message === '') {
        event.target.message.focus()
        return toast.update(this.toastLoaderId, {
          render: "Debe colocar un mensaje.",
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    }
  }
  toastLoaderId2 = null
  onReportProblem = async (event) => {
    event.preventDefault()
    this.setState({ reportProblemLoading: true })
    this.toastLoaderId2 = toast('Enviando mensaje...', { autoClose: false })
    if (
      this.state.problemMessage.trim() !== ''
    ) {
      const data = {
        uid: this.props.userInCollectionData.userKey,
        category: 'COMMENTS',
        description: '',
        message: this.state.problemMessage.trim(),
        date: moment().format('YYYY-MM-DD'),
        time: moment().format('LTS'),
        attachments: this.state.problemImage ? [{ base64Img: this.state.problemImage }] : ''
      }
      // console.log('SENDED', data)
      try {
        let response = await postRequest('makeIssue', data)
        // console.log('makeIssue response', response)
        if (response.data.success) {
          this.setState({
            reportProblemLoading: false,
            opinion: '',
            problemImage: '',
            problemPictureURI: '',
            problemMessage: '',
            displayErrors: false,
            imageError: false,
            IsReportProblemOpen: false,
            isHelpSupportOpen: true
          })
          toast.update(this.toastLoaderId2, {
            render: "Mensaje enviado exitosamente.",
            autoClose: 5000
          })
        } else {
          this.setState({ reportProblemLoading: false })
          toast.update(this.toastLoaderId2, {
            render: "Error interno del servidor.",
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderId2, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          reportProblemLoading: false,
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        reportProblemLoading: false
      })
      if (this.state.problemMessage.trim() === '') {
        event.target.problemMessage.focus()
        return toast.update(this.toastLoaderId2, {
          render: "Debe colocar la descripcion del problema.",
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    }
  }
  toastLoaderId3 = null
  onSendComment = async (event) => {
    event.preventDefault()
    this.setState({ sendCommentLoading: true })
    this.toastLoaderId3 = toast('Enviando mensaje...', { autoClose: false })
    if (
      this.state.opinion.trim() !== ''
    ) {
      const data = {
        uid: this.props.userInCollectionData.userKey,
        category: this.state.category,
        description: '',
        message: this.state.opinion.trim(),
        date: moment().format('YYYY-MM-DD'),
        time: moment().format('LTS'),
        attachments: this.state.commentImage ? [{ base64Img: this.state.commentImage }] : ''
      }
      // console.log('SENDED: ', data)
      try {
        let response = await postRequest('makeIssue', data)
        // console.log('makeIssue response', response)
        if (response.data.success) {
          this.setState({
            sendCommentLoading: false,
            opinion: '',
            commentImage: '',
            commentImageURI: '',
            displayErrors: false,
            imageError: false,
            category: '',
            selectedCategory: '',
            sendMesaggeFirstSectionIsOpen: false,
            isHelpSupportOpen: true
          })
          toast.update(this.toastLoaderId3, {
            render: "Mensaje enviado exitosamente.",
            autoClose: 5000
          })
        } else {
          this.setState({ reportProblemLoading: false })
          toast.update(this.toastLoaderId3, {
            render: "Error interno del servidor.",
            type: toast.TYPE.ERROR,
            autoClose: 5000
          })
        }
      } catch (error) {
        const errorMessage = ({
          '401': 'Su sesión ha expirado',
          'default': 'Verifique su conexión y vuelva a intentarlo.'
        })[error.response ? error.response.status || 'default' : 'default']
        toast.update(this.toastLoaderId3, {
          render: errorMessage,
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
        this.setState({
          reportProblemLoading: false,
        })
      }
    } else {
      this.setState({
        displayErrors: true,
        sendCommentLoading: false
      })
      if (this.state.opinion.trim() === '') {
        event.target.opinion.focus()
        return toast.update(this.toastLoaderId3, {
          render: "Debe colocar un comentario.",
          type: toast.TYPE.ERROR,
          autoClose: 5000
        })
      }
    }
  }
  goSecondSectionSendMessage = config => () => {
    const categoryCode = config.categoryCode
    this.setState({
      category: categoryCode,
      sendMesaggeFirstSectionIsOpen: false,
      selectedCategory: config.categoryName
    })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
    }
    if (!this.props.isLoggedIn) {
      return <Redirect to="/" />
    }
  }
  returnQuestion = config => {
    const index = config.index
    switch (index) {
      case 0:
        return (
          <div
            className={answersContainer}
          >
            <p>
              Gualy es una Billetera Digital que puede ser usada por personas naturales y jurídicas (comercios).
          </p>
          </div>
        )
      case 1:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>Tener una cuenta en Gualy es muy sencillo, sólo debes seguir los siguientes pasos:</p>
            <p><strong>Si eres persona natural:</strong></p>
            <p>1) Descarga la Gualy en Google Play o App Store.</p>
            <p>2) Pulsa el botón REGISTRO.</p>
            <p>3) Ingresa tus datos, correo electrónico y número de teléfono.</p>
            <p>4) Coloca tu foto de perfil, tomando una foto con tu dispositivo o seleccionando una de tu galería.</p>
            <p>5) Coloca tu contraseña y selecciona una pregunta secreta de seguridad.</p>
            <p>6) Acepta los términos y condiciones, así como también las políticas de privacidad.</p>
            <p>Así, ya cuentas con tu perfil en Gualy, donde puedes tener tu dinero en un solo lugar.</p>
            <p>Además de tu correo electrónico, cuentas con un Código QR único que también será tu identificador para enviar y solicitar pagos.</p>
            <p><strong>Si eres persona jurídica (comercio):</strong></p>
            <p>1) Realiza el pre-registro a través de la <a href="https://gualy.com/afilia-tu-comercio/">https://gualy.com/afilia-tu-comercio/</a></p>
            <p>2) En breve uno de nuestros especialistas de negocios se comunicará contigo para coordinar el registro de tu empresa.</p>
          </div>
        )
      case 2:
        return (
          <div
            className={answersContainer}
          >
            <p>1) Presiona BALANCE desde la pantalla principal.</p>
            <p>2) Luego pulsa AÑADIR.</p>
            <p>3) Ingresa el monto que deseas añadir.</p>
            <p>4) Elige cómo deseas añadir, por ahora solo está disponible la opción de TRANSFERENCIA.</p>
            <p>5) Toma nota de los datos de la cuenta bancaria de Gualy a la que prefieres transferir.</p>
            <p>6) Ingresa a tu banco y realiza una transferencia con los datos de la cuenta bancaria de Gualy que observaste en la App.</p>
            <p>7) Toma nota del número de transferencia.</p>
            <p>8) Luego vuelve a la App y pulsa ENTENDIDO, QUIERO REPORTAR EL PAGO.</p>
            <p>9) Coloca el Banco al que hiciste la transferencia, el número de la transferencia y la fecha. Verifica todos los datos y si están correctos haz clic en CONFIRMAR Y AÑADIR, si deseas hacer alguna corrección presiona VOLVER.</p>
            <p>10) En breve un agente de atención Gualy recibirá tu reporte de pago, verificará la transferencia en la cuenta bancaria de Gualy y si todo está correcto aprobará tu ingreso de saldo, de lo contrario se comunicará contigo para resolver el problema.</p>
            <p><strong>-Para registrar tarjetas de crédito:</strong></p>
            <p style={{ textAlign: "justify" }}>Ve a tu perfil y en la sección de Métodos de Pago, ingresa los datos de tu tarjeta de crédito y pulsa AGREGAR.</p>
            <p style={{ textAlign: "justify" }}>Las tarjetas de crédito registradas pueden ser de diferentes bancos y sus datos se guardarán cifrados en nuestra app.</p>
            <p style={{ textAlign: "justify" }}>Con ellas puedes agregar dinero a tu cuenta Gualy.</p>
            <p><strong>&nbsp;</strong><strong>-Para registrar cuentas bancarias:</strong></p>
            <p style={{ textAlign: "justify" }}>Ve a tu perfil y en la sección de Métodos de Pago, ingresa los datos de tu cuenta bancaria y pulsa AGREGAR.</p>
            <p style={{ textAlign: "justify" }}>Las cuentas bancarias registradas pueden ser de diferentes bancos y sus datos se guardarán cifrados en nuestra app.</p>
            <p style={{ textAlign: "justify" }}>Con ellas puedes ingresar dinero a tu cuenta Gualy.</p>
          </div>
        )
      case 3:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textalign: "justify" }}>En Gualy cuentas con distintas maneras para enviar y solicitar pagos:</p>
            <p><strong>-Para enviar un pago:</strong></p>
            <p style={{ textalign: "justify" }}>Ingresa el correo electrónico o escanea el Código QR del usuario que recibirá el dinero y pulsa ENVIAR PAGO.</p>
            <p><strong>-Para solicitar un pago:</strong></p>
            <p style={{ textalign: "justify" }}>Ingresa el correo electrónico del usuario que recibirá el dinero o genera un Código QR por el monto solicitado y pulsa SOLICITAR PAGO.</p>
          </div>
        )
      case 4:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>Los pagos a usuarios y comercios se realizan en segundos.</p>
          </div>
        )
      case 5:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              Al recibir un pago puedes utilizar de inmediato tu dinero para pagar a otros usuarios o comercios en sus cuentas Gualy.
          </p>
          </div>
        )
      case 6:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              Recibirás notificaciones de cada transacción y en la sección Historial puedes ver el detalle, teniendo el control de todos tus movimientos.
        </p>
            <p style={{ textAlign: "justify" }}>
              Además, como comercio afiliado puedes observar el registro de cada acción realizada por los administradores que agregues a tu cuenta Gualy Comercio, asignando los permisos que consideres pertinentes para cada uno.
        </p>
          </div>
        )
      case 7:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              La tarifa por los servicios de recaudación de Gualy será un % de cada pago que hayas recibido a través de su plataforma.
        </p>
            <p style={{ textAlign: "justify" }}>
              Las tarifas para personas naturales estarán publicadas en <a href="http://www.gualy.com">www.gualy.com</a> en la sección de comisiones.
            </p>
            <p style={{ textAlign: "justify" }}>
              Las tarifas para personas jurídicas (comercios) debe ser consultada al correo info@gualy.com ya que dependerá del tipo de actividad económica y volumen de ventas.
        </p>
          </div>
        )
      case 8:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              Puedes solicitar en cualquier momento el traslado de saldo que tienes disponible en tu cuenta Gualy a cualquiera de las cuentas bancarias propias que tengas registradas en tu perfil.
      </p>
            <p style={{ textAlign: "justify" }}>
              Toma en cuenta que las transferencias pueden tardar hasta 72 horas en hacerse efectivas en tu cuenta bancaria, este tiempo dependerá de la plataforma bancaria.
      </p>
          </div>
        )
      case 9:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              No, Gualy trabaja con transacciones en la moneda local venezolana (VEF / VES).
    </p>
          </div>
        )
      case 10:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              En la sección de Ajustes puedes modificar o actualizar tu información de perfil en cualquier momento.
  </p>
          </div>
        )
      case 11:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              En el inicio debes pulsar OLVIDÉ MI CONTRASEÑA y recibirás un correo para crear una nueva contraseña.
  </p>
          </div>
        )
      case 12:
        return (
          <div
            className={answersContainer}
          >
            <p style={{ textAlign: "justify" }}>
              Si necesitas AYUDA puedes solicitar soporte técnico, enviar comentario o reportar un problema en la sección Ayuda de la aplicación. También puedes comunicarte con nuestro Help Center a través del correo soporte@gualy.com, el número telefónico 0261-8085657 y el chat en <a href="http://www.gualy.com">www.gualy.com</a>.
          </p>
          </div>
        )
      case 13:
        return (
          <div
            className={answersContainer}
          >
            <p>Envía a soporte@gualy.com los siguientes documentos en JPG, PNG o PDF:</p>
            <p>1) Documento de identidad: envía la foto de tu cédula de identidad o pasaporte vigente.</p>
            <p>2) Dirección:</p>
            <p>– Envía uno de estos documentos donde se observe la dirección. Puede ser RIF, factura de servicio público o privado o estado de cuenta bancaria.</p>
            <p>– Debe haber sido emitido con fecha menor a 6 meses.</p>
            <p>– Debe estar a tu nombre.</p>
            <p>– La dirección colocada en tu perfil debe estar igual al documento.</p>
            <p>3) Para verificar tu cuenta bancaria, agrega un soporte bancario, puede ser un cheque inutilizado, referencia bancaria o estado de cuenta donde puedan leerse los 20 dígitos de tu cuenta bancaria.</p>
          </div>
        )
      default:
        return null
    }
  }
  openQuestion = config => () => {
    const index = config.index
    this.setState({
      actives: {
        ...this.state.actives,
        [index]: {
          ...this.state.actives[index],
          active: !this.state.actives[index].active
        }
      }
    })
  }
  render() {
    return (
      <div>
        <CommerceHeader />
        <div className={`${this.state.IsReportProblemOpen || this.state.IsSendMesaggeOpen ? smallCardTable : tableCard} ${(this.state.displayErrors === true ? displayErrors : '')}`}>
          <div className={header}>
            {
              (this.state.isSupportOpen || this.state.IsReportProblemOpen || this.state.sendMesaggeFirstSectionIsOpen)
                && !this.state.isHelpSupportOpen
                ? <img
                  alt=""
                  onClick={this.goBack}
                  src={arrowBack}
                  className={`${goBackButton}`}
                />
                :
                !this.state.sendMesaggeFirstSectionIsOpen ?
                  <img
                    alt=""
                    onClick={this.goToSendMessageFirstSection}
                    src={arrowBack}
                    className={`${goBackButton}`}
                  />
                  : null
            }
            {
              this.state.isSupportOpen
                ? <h1 className={secionTitleText}>
                  SOPORTE
                </h1>
                : this.state.IsReportProblemOpen
                  ? <div>
                    <img
                      className={reportProblemIconRedClass}
                      src={reportProblemIconRed}
                      alt=""
                    />
                    <h1 className={reportProblemTitle}> REPORTAR UN PROBLEMA </h1>
                  </div>
                  : this.state.IsSendMesaggeOpen
                    ? <h1 className={secionTitleText}>
                      ENVIAR COMENTARIOS
                    </h1>
                    : this.state.isHelpSupportOpen
                      ? <h1 className={secionTitleText}>
                        AYUDA Y SOPORTE
                      </h1>
                      : null
            }
          </div>
          <div className='horizontal-divisor'></div>
          <div className={cardTableComponent}>
            {
              this.state.isHelpSupportOpen ?
                <div className={sectionsContainer}>
                  <div className={section}>
                    <div onClick={this.openSuppport} className={sectionChild}>
                      <img
                        alt=""
                        src={callIcon}
                      />
                      <p className={sectionChildTitle}>SOPORTE</p>
                    </div>
                    <div onClick={this.openSendMessage} className={sectionChild}>
                      <img
                        alt=""
                        src={sendCommentsIcon}
                      />
                      <p className={sectionChildTitle}>ENVIAR COMENTARIOS</p>
                    </div>
                    <div onClick={this.openReportProblem} className={sectionChild}>
                      <img
                        alt=""
                        src={reportProblemIcon}
                      />
                      <p className={sectionChildTitle}>REPORTAR UN PROBLEMA</p>
                    </div>
                  </div>
                  <div className={`${section} ${margin}`}>
                    <p
                      className={questionsTitleText}
                    >
                      Preguntas frecuentes
                </p>
                    <div className={questionContainer}>
                      {/* QUESTION 0 */}
                      <p
                        onClick={this.openQuestion({ index: 0 })}
                        className={questionText}
                      >
                        ¿Quiénes pueden usar Gualy?
                </p>
                      {
                        this.state.actives[0]
                          ? this.state.actives[0].active
                            ? this.returnQuestion({ index: 0 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 1 */}
                      <p
                        onClick={this.openQuestion({ index: 1 })}
                        className={questionText}
                      >
                        ¿Qué debo hacer para tener una cuenta en Gualy?
                </p>
                      {
                        this.state.actives[1]
                          ? this.state.actives[1].active
                            ? this.returnQuestion({ index: 1 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 2 */}
                      <p
                        onClick={this.openQuestion({ index: 2 })}
                        className={questionText}
                      >
                        ¿Cómo agrego dinero en Gualy?
                </p>
                      {
                        this.state.actives[2]
                          ? this.state.actives[2].active
                            ? this.returnQuestion({ index: 2 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 3 */}
                      <p
                        onClick={this.openQuestion({ index: 3 })}
                        className={questionText}
                      >
                        ¿Cómo envío y solicito un pago?
                </p>
                      {
                        this.state.actives[3]
                          ? this.state.actives[3].active
                            ? this.returnQuestion({ index: 3 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 4 */}
                      <p
                        onClick={this.openQuestion({ index: 4 })}
                        className={questionText}
                      >
                        ¿Cuánto tiempo toma pagar con Gualy?
                </p>
                      {
                        this.state.actives[4]
                          ? this.state.actives[4].active
                            ? this.returnQuestion({ index: 4 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 5 */}
                      <p
                        onClick={this.openQuestion({ index: 5 })}
                        className={questionText}
                      >
                        Si recibo un pago, ¿En cuánto tiempo puedo usar el dinero?
                </p>
                      {
                        this.state.actives[5]
                          ? this.state.actives[5].active
                            ? this.returnQuestion({ index: 5 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 6 */}
                      <p
                        onClick={this.openQuestion({ index: 6 })}
                        className={questionText}
                      >
                        ¿Cómo puedo ver los detalles de las transacciones?
                </p>
                      {
                        this.state.actives[6]
                          ? this.state.actives[6].active
                            ? this.returnQuestion({ index: 6 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 7 */}
                      <p
                        onClick={this.openQuestion({ index: 7 })}
                        className={questionText}
                      >
                        ¿Qué tarifa cobra Gualy por recibir dinero?
                </p>
                      {
                        this.state.actives[7]
                          ? this.state.actives[7].active
                            ? this.returnQuestion({ index: 7 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 8 */}
                      <p
                        onClick={this.openQuestion({ index: 8 })}
                        className={questionText}
                      >
                        ¿Cómo se puede retirar el dinero?
                </p>
                      {
                        this.state.actives[8]
                          ? this.state.actives[8].active
                            ? this.returnQuestion({ index: 8 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 9 */}
                      <p
                        onClick={this.openQuestion({ index: 9 })}
                        className={questionText}
                      >
                        ¿Aceptan moneda extranjera?
                </p>
                      {
                        this.state.actives[9]
                          ? this.state.actives[9].active
                            ? this.returnQuestion({ index: 9 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 10 */}
                      <p
                        onClick={this.openQuestion({ index: 10 })}
                        className={questionText}
                      >
                        ¿Cómo puedo ver o editar la información de mi perfil?
                </p>
                      {
                        this.state.actives[10]
                          ? this.state.actives[10].active
                            ? this.returnQuestion({ index: 10 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 11 */}
                      <p
                        onClick={this.openQuestion({ index: 11 })}
                        className={questionText}
                      >
                        ¿Cómo puedo restablecer mi contraseña?
                </p>
                      {
                        this.state.actives[11]
                          ? this.state.actives[11].active
                            ? this.returnQuestion({ index: 11 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 12 */}
                      <p
                        onClick={this.openQuestion({ index: 12 })}
                        className={questionText}
                      >
                        ¿Cómo puedo solicitar ayuda o realizar un reclamo?
                </p>
                      {
                        this.state.actives[12]
                          ? this.state.actives[12].active
                            ? this.returnQuestion({ index: 12 })
                            : null
                          : null
                      }
                    </div>
                    <div className={questionContainer}>
                      {/* QUESTION 13 */}
                      <p
                        onClick={this.openQuestion({ index: 13 })}
                        className={questionText}
                      >
                        ¿Cómo verifico mi usuario?
                </p>
                      {
                        this.state.actives[13]
                          ? this.state.actives[13].active
                            ? this.returnQuestion({ index: 13 })
                            : null
                          : null
                      }
                    </div>
                  </div>
                </div>
                : null
            }
            {
              this.state.isSupportOpen ?
                <div className={sectionsContainer}>
                  <div className={`${section} ${supportMargin}`}>
                    <p>¿Tienes dudas sobre cómo usar Gualy o algún problema con tu cuenta?</p>
                    <p>Comunícate a nuestro Help Center</p>
                    <div
                      className={whatsappBussinesContainer}
                    >
                      <a
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://api.whatsapp.com/send?phone=584146853797"
                        alt="whatsapp"
                      >
                        <img
                          className={wsBussinesIconClass}
                          src={wsBussinesIcon}
                          alt=""
                        />
                      </a>
                      {/* <a
                          href="tel:+584146568315"
                          alt="call"
                          className={linkCallButton}
                        >
                          <img
                            alt=""
                            src={callIcon}
                            className={wsBussinesIconClass}
                          />
                        </a> */}
                      <img
                        alt=""
                        onClick={this.showNumber}
                        src={callIcon}
                        className={`${wsBussinesIconClass} ${linkCallButton}`}
                      />
                      {
                        this.state.numberIsShowed ?
                          <p>+582618085657</p>
                          : null
                      }
                    </div>
                  </div>
                  <div className={section}>
                    <div className={writeUsTextContainer}>
                      <p className={writeUsText}>
                        Escribenos a soporte@gualy.com
                  </p>
                      <p className={writeUsText}>
                        o por el siguiente formulario:
                  </p>
                    </div>
                    <form className={sendMessageForm} noValidate autoComplete='off' onSubmit={this.onSendMessage}>
                      <div css={`padding-bottom:5px;`} className='group mb-1'>
                        <input
                          placeholder='Nombre'
                          name='name'
                          disabled={true}
                          value={this.props.userInCollectionData.name}
                          required
                        />
                        <span className='highlight'></span>
                        <span className='bar'></span>
                      </div>
                      <div css={`padding-bottom:5px;`} className='group mb-1'>
                        <input
                          type='text'
                          disabled={true}
                          placeholder='Email'
                          name='email'
                          value={this.props.userInCollectionData.email}
                          required
                        />
                        <span className='highlight'></span>
                        <span className='bar'></span>
                      </div>
                      <div css={`padding-bottom:5px;`} className='group mb-1'>
                        <textarea
                          placeholder='Mensaje'
                          name='message'
                          className={registerTextArea}
                          onChange={this.handleInputChange}
                          value={this.state.message}
                          required
                        />
                        <span className='highlight'></span>
                        <span className='bar'></span>
                      </div>
                      <div
                        className={sendMessageButtonContainer}
                      >
                        <AcceptButton
                          type="submit"
                          loading={this.state.sendMessageLoading}
                          disabled={this.state.sendMessageLoading}
                          className={SendMessageButton}
                          content="ENVIAR"
                          width="198.4px"
                          height="38.8px"
                        />
                      </div>
                    </form>
                  </div>
                </div>
                : null
            }
            {
              this.state.IsReportProblemOpen ?
                <div className={reportProblemSectionsContainer}>
                  <form className={sendMessageForm} noValidate autoComplete='off' onSubmit={this.onReportProblem}>
                    <div css={`padding-bottom:5px; margin-top: 20px; margin-bottom: 40px`} className='group mb-1'>
                      <input
                        placeholder='Por favor, cuéntanos qué ocurrió.'
                        name='problemMessage'
                        onChange={this.handleInputChange}
                        value={this.state.problemMessage}
                        required
                      />
                      <span className='highlight'></span>
                      <span className='bar'></span>
                    </div>
                    <p className={reportProblemAttachText}>Adjunta una captura de pantalla*</p>
                    <div className={uploadBtnWrapper}>
                      <UploadBtn className={this.state.imageError ? specficError : ''}>
                        <img
                          src={this.state.problemImage === '' ? iconCamera : this.state.problemImage}
                          className={this.state.problemImage === '' ? iconCameraClass : imagePreview}
                          alt="uploadPicture"
                        />
                        <input
                          className={inputFileHidden}
                          value={this.state.problemPictureURI}
                          name='problemPictureURI'
                          type='file'
                          accept="image/*"
                          onChange={this.encodeImageFileAsURL}
                          required
                        />
                      </UploadBtn>
                    </div>
                    <p className={optionalText}>
                      * Opcional. Puedes añadir una captura de pantalla a tu reporte para complementar la información.
                    </p>
                    <div className={reportProblemButtonContainer}>
                      <AcceptButton
                        type="submit"
                        loading={this.state.reportProblemLoading}
                        disabled={this.state.reportProblemLoading}
                        className={SendMessageButton}
                        content="CONFIRMAR Y ENVIAR REPORTE"
                        width="293px"
                        height="38.8px"
                      />
                    </div>
                  </form>
                </div>
                : null
            }
            {
              this.state.IsSendMesaggeOpen ?
                <div className={reportProblemSectionsContainer}>
                  {
                    this.state.sendMesaggeFirstSectionIsOpen ?
                      <div
                        className={categoryButtonsContainer}
                      >
                        <p className={categoryTitle}>
                          Categorías:
                </p>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'SEND_PAYMENT', categoryName: 'ENVIAR PAGO' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={sendMoneyIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> ENVIAR PAGO </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'RECEIVE_MONEY', categoryName: 'SOLICITAR PAGO' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={requestPeymentIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> SOLICITAR PAGO </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'ADD_MONEY', categoryName: 'AÑADIR SALDO' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={addFoundsIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> AÑADIR SALDO </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'WITHDRAW', categoryName: 'RETIRAR SALDO' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={withdrawIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> RETIRAR SALDO </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'PAYMENT_METHODS', categoryName: 'MÉTODOS DE PAGO' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={paymentMethodIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> MÉTODOS DE PAGO </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'BANK_ACCOUNTS', categoryName: 'CUENTAS BANCARIAS' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={banksAccountsIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> CUENTAS BANCARIAS </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'MY_PROFILE', categoryName: 'MI PERFIL' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={myProfileIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> MI PERFIL </h1>
                          <div className={categoryDivisor} />
                        </div>
                        <div
                          onClick={this.goSecondSectionSendMessage({ categoryCode: 'COMMERCES', categoryName: 'COMERCIOS' })}
                          className={categoryContainer}
                        >
                          <img
                            className={sendMessageIconClass}
                            src={commercesIcon}
                            alt=""
                          />
                          <h1 className={categorySecondTitle}> COMERCIOS </h1>
                          <div className={categoryDivisor} />
                        </div>
                      </div>
                      :
                      <div>
                        <form className={sendMessageForm} noValidate autoComplete='off' onSubmit={this.onSendComment}>
                          <p className={categoryTitle}>
                            Categorías:
                </p>
                          <div
                            className={categoryContainerReadOnly}
                          >
                            {
                              this.state.selectedCategory === 'ENVIAR PAGO' ?
                                <img
                                  className={sendMessageIconClass}
                                  src={sendMoneyIcon}
                                  alt=""
                                />
                                : this.state.selectedCategory === 'AÑADIR SALDO' ?
                                  <img
                                    className={sendMessageIconClass}
                                    src={addFoundsIcon}
                                    alt=""
                                  />
                                  : this.state.selectedCategory === 'SOLICITAR PAGO' ?
                                    <img
                                      className={requestPeymentIcon}
                                      src={requestPeymentIcon}
                                      alt=""
                                    />
                                    : this.state.selectedCategory === 'MÉTODOS DE PAGO' ?
                                      <img
                                        className={paymentMethodIcon}
                                        src={paymentMethodIcon}
                                        alt=""
                                      />
                                      : this.state.selectedCategory === 'RETIRAR SALDO' ?
                                        <img
                                          className={paymentMethodIcon}
                                          src={withdrawIcon}
                                          alt=""
                                        />
                                        : this.state.selectedCategory === 'CUENTAS BANCARIAS' ?
                                          <img
                                            className={banksAccountsIcon}
                                            src={banksAccountsIcon}
                                            alt=""
                                          />
                                          : this.state.selectedCategory === 'MI PERFIL' ?
                                            <img
                                              className={banksAccountsIcon}
                                              src={myProfileIcon}
                                              alt=""
                                            />
                                            : this.state.selectedCategory === 'COMERCIOS' ?
                                              <img
                                                className={banksAccountsIcon}
                                                src={commercesIcon}
                                                alt=""
                                              />
                                              : null
                            }
                            <h1 className={categorySecondTitle}> {this.state.selectedCategory} </h1>
                            <div className={categoryDivisor} />
                          </div>
                          <div css={`padding-bottom:5px; margin-top: 20px; margin-bottom: 40px`} className='group mb-1'>
                            <input
                              placeholder='Por favor, danos tu opinión…'
                              name='opinion'
                              onChange={this.handleInputChange}
                              value={this.state.opinion}
                              required
                            />
                            <span className='highlight'></span>
                            <span className='bar'></span>
                          </div>
                          <p className={reportProblemAttachText}>Adjunta una captura de pantalla*</p>
                          <div className={uploadBtnWrapper}>
                            <UploadBtn className={this.state.imageError ? specficError : ''}>
                              <img
                                src={this.state.commentImage === '' ? iconCamera : this.state.commentImage}
                                className={this.state.commentImage === '' ? iconCameraClass : imagePreview}
                                alt="uploadPicture"
                              />
                              <input
                                className={inputFileHidden}
                                value={this.state.commentImageURI}
                                name='commentImageURI'
                                type='file'
                                accept="image/*"
                                onChange={this.encodeImageFileAsURLComments}
                                required
                              />
                            </UploadBtn>
                          </div>
                          <p className={optionalText}>
                            * Opcional. Puedes añadir una captura de pantalla a tu reporte para complementar la información.
                    </p>
                          <div className={reportProblemButtonContainer}>
                            <AcceptButton
                              type="submit"
                              loading={this.state.sendCommentLoading}
                              disabled={this.state.sendCommentLoading}
                              className={SendMessageButton}
                              content="CONFIRMAR Y ENVIAR"
                              width="215.3px"
                              height="38.8px"
                            />
                          </div>
                        </form>
                      </div>
                  }
                </div>
                : null
            }
          </div>
        </div>
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
        {
          this.redirect()
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    userFirebaseData: state.general.userFirebaseData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(History)