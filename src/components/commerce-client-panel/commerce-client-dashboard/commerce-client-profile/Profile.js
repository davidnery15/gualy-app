import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import firebase, { db, auth } from './../../../../firebase'
import CommerceHeader from './../../commerce-client-header/CommerceClientHeader'
import { css } from 'emotion'
import editIcon from '../../../../assets/icon-edit.svg'
// import Spinner from 'react-spinkit'
import OptionsModalWrapper from '../../../general/OptionsModalWrapper'
import styled from 'react-emotion'
import arrowIcon from './../../../../assets/blackArrow.svg'
import { ToastContainer, toast } from 'react-toastify'
import QRscan from './../../../../assets/blackQrCode.svg'
import printableIcon from './../../../../assets/prints/icon-button.svg'
import shareIcon from './../../../../assets/icon-share.svg'
import { QRCode } from 'react-qr-svg'
import userPicture from '../../../../assets/accountCircle.svg'
import iconVisibilityOff from './../../../../assets/baseline-visibility-off.svg'
import iconVisibilityOn from './../../../../assets/baseline-visibility-on.svg'
import { securityQuestionKeys } from './../../../../constants/securityQuestionKeys'
import { CardHeader } from 'reactstrap'
import { NavLink } from 'react-router-dom'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import AcceptButton from '../../../general/AcceptButton'
import { numbers, letters } from '../../../../constants/regex'
import { userLogout } from '../../../../redux/actions/general'
import AddBankAccountForm from '../../../general/AddBankAccountForm'
import { customConsoleLog } from '../../../../utils/customConsoleLog'
import R from '../../../../utils/R'
import CropImageModal from '../../../general/modals/CropImageModal'
const mainProfileContainer = css`
display: flex; 
flex-direction: row;
position: relative;
justify-content: center;
max-width: 1113px;
margin: auto;
@media(max-width: 1200px){
  flex-direction: column 
}
`
const myProfileContainer = css`
margin: 50px;
margin-bottom: 0px;
border-radius: 5px;
background-color: #2a2c6a;
-webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
padding-top: 21px;
display: -ms-flexbox;
display: flex;
flex-direction: column;
width: 420px;
min-height:420px;
height:auto;
position: relative;
max-width: 420px
@media(max-width: 500px){
  margin: 5%;
  width: 90%;
}
@media(max-width: 1200px){
  margin: auto;
  margin-top: 50px;
}
  `
const myProfileContainer2 = css`
margin-bottom: 0px;
border-radius: 5px;
background-color: #2a2c6a;
-webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
padding-top: 21px;
display: -ms-flexbox;
display: flex;
flex-direction: column;
width: 100%;
min-height:300px;
height:auto;
position: relative;
max-width: 420px
  `
// const payMethodsContainer = css`
// margin: 50px;
// margin-left: 0;
// margin-bottom: 0px;
// border-radius: 5px;
// background-color: #2a2c6a;
// -webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
// box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
// padding-top: 21px;
// display: -ms-flexbox;
// display: flex;
// width: 880px;
// height:420px;
// position: relative;
// max-width:880px 
// @media(max-width: 980px){
//   margin: 5%;
//   width: 90%;
//   height: 436px;
// }
//   `
// const configContainer = css`
// margin: 50px;
// margin-bottom: 0px;
// border-radius: 5px;
// background-color: #2a2c6a;
// -webkit-box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
// box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
// padding-top: 21px;
// display: -ms-flexbox;
// display: flex;
// height:420px;
// position: relative;
// max-width: 1353px;
// width: 90%;
// min-height: 436px
// height: auto;
// flex-direction: column;
// @media(max-width: 980px){
//   margin: 5%;
// }
//   `
const header = css`
  position: absolute;
  top: -6px;
  left: 2px;
    margin: 0px;
    display: flex;
    width: 100%;
    padding: 23px 25px 0 25px;
    justify-content: space-between;
    align-items: center;
    h1{
      margin: 0px;
      font-size: 13px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      letter-spacing: 1.1px;
      color: #f6f7fa;
      text-align: center
    }
    div{
      display: flex;
      align-items: center;
      cursor: pointer;
    }
  `
const editButtonContainer = css`
  display: flex;
  flex-direction: row;
  position: absolute;
  right: 13px;
  cursor: pointer;
  top: 14px;
  `
const editButtonContainer2 = css`
  display: flex;
  flex-direction: row;
  position: absolute;
  right: 13px;
  cursor: pointer;
  top: 0px;
  `
const editButtonText = css`
font-family: Montserrat;
font-size: 12.8px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
letter-spacing: normal;
text-align: right;
color: #95fad8;
margin-right: 6px;
margin-top: 14px
  `
const editIconStyle = css`
cursor: pointer
width: 16px;
height: 16px;
  `
const editIconStyleImages = css`
cursor: pointer
width: 16px;
height: 16px;
position: absolute;
top: -10px;
right: -8px;
  `
const editIconStyle2 = css`
cursor: pointer;
width: 16px;
height: 16px;
margin-top: 14px;
  `
const myProfileContentContainer = css`
display: flex;
flex-direction: row;
@media(max-width: 500px){
  flex-direction: column;
}
  `
const myProfileInfoContainer = css`
display: flex;
flex-direction: column;
justify-content: center;
width: 70%;
margin-top: 20px;
@media(max-width: 500px){
  width: 100%;
}
  `
const myProfileImageContainer = css`
display: flex;
flex-direction: column;
flex-wrap: wrap;
justify-content: center;
width: 30%;
margin-top: 20px;
@media(max-width: 500px){
  width: 100%;
}
  `
const myProfileInfoTextsContainer = css`
display: flex;
flex-direction: column;
flex-wrap: wrap;
justify-content: center;
margin-left: 20px;
margin-bottom: 8px;
@media(max-width: 500px){
  margin: auto;
}
  `
const myProfileInfoTextsContainer2 = css`
display: flex;
flex-direction: row;
flex-wrap: wrap;
justify-content: center;
margin-left: 20px;
margin-bottom: 8px;
@media(max-width: 500px){
  display-flex: column;
}
  `
const myProfileInfoTitle = css`
  font-family: Montserrat;
  font-size: 11px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  letter-spacing: normal;
  color: #7e848c;
  color: var(--steel-grey);
  height: 0;
  @media(max-width: 500px){
    text-align: center
  }
  `
const myProfileNames = css`
font-family: Montserrat;
font-size: 14px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
letter-spacing: normal;
color: #f6f7fa;
color: var(--pale-grey);
@media(max-width: 500px){
  text-align: center
}
  `
const myProfileNamesQuestion = css`
font-family: Montserrat;
font-size: 11px;
font-weight: 300;
font-style: normal;
font-stretch: normal;
letter-spacing: normal;
color: #f6f7fa;
color: var(--pale-grey);
@media(max-width: 500px){
  text-align: center
}
  `
const proileRoundImage = css`
position: relative;
width: 72px;
height: 72px;
border-radius: 50%;
margin-left: 20px;
margin-bottom: auto;
object-fit: cover;
box-shadow: 0 13px 26px 0 rgba(0,0,0,0.25);
@media(max-width: 500px){
  margin: auto;
}
  `
// const spinnerContainer = css`
//     width: 100%;
//     height: 100%;
//     display: flex;
//     justify-content: center;
//     align-items: center;
//     position: absolute;
//     top: 0;
//     left: 0;
//   `
const component = css`
    input {
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.41;
      letter-spacing: normal;
      text-align: center;
      // color: #f6f7fa;
      margin-top: 10px;
    }
  `
const deleteBankAccountModalContainer2 = css`
width: 100%;
height: auto;
padding: 10px;
border-radius: 4.2px;
box-shadow: 0 8px 8px 0 rgba(0, 0, 0, 0.15), 0 0 8px 0 rgba(0, 0, 0, 0.12);
border: solid 0.5px transparent;
background-origin: border-box;
background-clip: content-box, border-box;
text-align: center;
  `
const contentContainer = css`
width: 100%;
  display: flex;
  flex-direction: column;
  `
const addBankAccountModalInputsContainer = css`
  display: flex;
  flex-direction: column;
  margin: 5px;
  margin-left: 15px;
  margin-right: 15px;
  `
const CustomDropdown = styled('select')`
    height: 43.3px;
    width:100%;
    padding: 10px 0;
    background: transparent;
    border: none;
    cursor: pointer;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.68;
    letter-spacing: normal;
    text-align: left;
    color: #ffff;
    appearance: none; 
    outline: none !important;
    option{
      color: #ffff;
      background-color: #242657;
    }
  `
const dropDownDiv = css`
    position: relative;
    display: flex;
    align-items:center;
  `
const arrowContainer = css`
    width: 15px !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 0;
    top: 0;
    pointer-events: none;
  `
const arrow = css`
    width: 10.4px;
    height: 6.6px;
    object-fit: contain;
  `
const primaryButton = css`
    margin: 10px;
    margin-bottom: 20px;
    width: auto;
    height: 24px;
    border-radius: 5px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 12px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: left;
    color: #242656;
    cursor: pointer;
    outline: none !important;
    display: flex;
    padding-top: 4px;
    margin-left: 0;
    margin-top: -1px;
  `
const primaryButton2 = css`
    margin: auto;
    margin-bottom: 20px;
    width: 126px;
    height: 24px;
    border-radius: 5px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 12px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: left;
    color: #242656;
    cursor: pointer;
    outline: none !important;
    display: flex;
    padding-top: 4px;
    margin-left: 0;
    margin-top: -1px;
  `
const codeQrContainer = css`
  background-color: #fff;
  width: 200px;
  height: 200px;
  border: 12px solid #fff;
  border-radius: 5%;
  margin: auto
`
const btnVisibility = css`
    background: transparent;
    border: none;
    position: absolute;
    right: 5px;
    top: 10px;
  `
const passwordContainer = css`
    position: relative;
  `
const visibilityOnCSS = css` 
    width: 25px; 
    height: 25px; 
    object-fit: contain; 
    cursor: pointer;
  `
const passwordInfo = css`
font-size: 8px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.13;
letter-spacing: normal;
text-align: left;
color: #8078ff;
  `
const marginTop = css`
  margin-top: 20px !important;
`
const dropDownDivContainer = css`
border-bottom: 1px solid white;
width: 100%;
div{
  margin-top: 0;
}
`
// const displayRedBorders = css`
//     border-color:red;
// `
const dropdownQuestionPadding = css`
padding-right: 15px;
@media(max-width: 500px){
  option{
    font-size: 8px;
  }
}
`
const recoverTitleCard = css`
margin-top: 35px !important;
font-size: 24px;
font-weight: 900;
line-height: 1.2;
`
// const logInButton = css`
// cursor: pointer;
// position:relative;
// min-height: 48px;
// color: #242656 !important;
// margin-bottom: 15px;
// `
const passContainer = css`
display: flex;
@media(max-width: 500px){
justify-content: center
}
`
const copyTextContainer = css`
display: flex;
    justify-content: center;
    margin: 20px;
    flex-direction: column;
    textarea{
      background-color: #2a2c6a;
      resize: none;
      border: none;
      color: #95fad8
    }
`
const copyTextButton = css`
padding: 8px 18px;
    border-radius: 5px;
    border-radius: 100px;
    background-color: #95fad8;
    border: none;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.6px;
    text-align: center;
    color: #242656;
    cursor: pointer;
    outline: none !important;
`
// const shareButtonsContainer = css`
// position: absolute
// `
// const shareButtonsMainContainer = css`
// position: relative
// `
// const copyTextStyle = css`
// margin: 20px
// `
const copiedStyle = css`
text-align: center
`
// const uploadBtnWrapper = css`
// margin-left: 20px;
// margin-bottom: auto;
// @media(max-width: 500px){
//   margin: auto;
// }
//   `
const UploadBtn = styled('button')`
  padding: 0;
  width: 72px;
  height: 72px;
  border-radius: 50%;
  color: #242656;
  display: flex;
  justify-content: center;
  align-items: center;
  outline: none !important;
  cursor: pointer;
  background: transparent;
  border: none;
  box-shadow: 0 13px 26px 0 rgba(0, 0, 0, 0.25);
  position: relative;
  margin: auto;
`
const inputFileHidden = css`
    width: 100%;
    height: 100%;
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `
const iconCameraClass = css`
    width: 72px;
    height: 72px;
    border-radius: 50%;
    object-fit: cover;
    cursor: pointer;
  `
const MyQrModal = ({
  codeQR,
  onShareMyQrCodeClick,
  showCopy,
  copyText,
  onCopyTextChange,
  copied,
  onCopyClick
}) => (
    <div>
      <div className={myProfileContainer2}>
        <div className={header}>
          <h1>MI CÓDIGO QR</h1>
        </div>
        <div className={editButtonContainer}>
          <img
            onClick={onShareMyQrCodeClick}
            src={shareIcon}
            alt="EditIcon"
            className={editIconStyle}>
          </img>
        </div>
        <div className='horizontal-divisor'></div>
        <div className={codeQrContainer}>
          <QRCode
            bgColor="#FFFFFF"
            fgColor="#2a2c6a"
            level="L"
            style={{ width: "100%" }}
            value={codeQR}
          />
        </div>
        {
          showCopy ?
            <div className={copyTextContainer}>
              <textarea name="copyText" value={copyText} onChange={onCopyTextChange} cols={30} rows={6} />
              <CopyToClipboard text={copyText}
                onCopy={onCopyClick}>
                <button className={copyTextButton}>Copiar al portapapeles</button>
              </CopyToClipboard>
              {
                copied ?
                  <p className={copiedStyle}>Copiado!</p>
                  : null
              }
            </div>
            : null
        }
      </div>
    </div>
  )

const EditPassModal = ({
  onSubmit,
  passwordToAuthenticate,
  newPassword,
  passwordVisibility,
  passwordVisibility2,
  handleVisibility,
  handleVisibility2,
  handleInputChange,
  isLoading }) => (
    <div>
      {
        <form
          onSubmit={onSubmit}
          className={`${contentContainer} ${component}`}>
          <div className={deleteBankAccountModalContainer2}>
            <div className={addBankAccountModalInputsContainer}>
              <CardHeader className='text-center' css={`border-bottom: 0px solid transparent !important;`}>
                <span className={recoverTitleCard}>Cambia tu<br />contraseña</span>
              </CardHeader>
              <div css={`padding-bottom:5px;`} className={` ${passwordContainer}`}>
                <input
                  type={passwordVisibility ? 'text' : 'password'}
                  placeholder='Contraseña vieja'
                  name='passwordToAuthenticate'
                  onChange={handleInputChange}
                  value={passwordToAuthenticate}
                  required
                />
                <div
                  className={btnVisibility}
                  onClick={handleVisibility}>
                  <img
                    className={visibilityOnCSS}
                    src={passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
                    alt="visibilidad"
                  />
                </div>
                <span className='highlight'></span>
                <span className='bar'></span>
              </div>
              <div css={`padding-bottom:5px;`} className={` ${passwordContainer}`}>
                <input
                  type={passwordVisibility2 ? 'text' : 'password'}
                  placeholder='Contraseña nueva'
                  name='newPassword'
                  onChange={handleInputChange}
                  value={newPassword}
                  required
                />
                <div
                  className={btnVisibility}
                  onClick={handleVisibility2}>
                  <img
                    className={visibilityOnCSS}
                    src={passwordVisibility2 ? iconVisibilityOff : iconVisibilityOn}
                    alt="visibilidad"
                  />
                </div>
                <span className='highlight'></span>
                <span className='bar'></span>
              </div>
              <p className={passwordInfo}>Debe contener un número (0-9), una letra mayúscula. Entre 6 y 12 caracteres.</p>
            </div>
            <AcceptButton
              loading={isLoading}
              width="164px"
              height="39px"
              type="submit"
              content="CONFIRMAR"
            />
          </div>
        </form>
      }
    </div>
  )
const EditSecretAnswerModal = ({
  onSubmit,
  passwordToAuthenticate,
  newAnswer,
  passwordVisibility,
  handleVisibility,
  handleInputChange,
  isLoading }) => (
    <div>
      {
        <form
          onSubmit={onSubmit}
          className={`${contentContainer} ${component}`}>
          <div className={deleteBankAccountModalContainer2}>
            <div style={{ marginBottom: "20px" }} className={addBankAccountModalInputsContainer}>
              <CardHeader className='text-center' css={`border-bottom: 0px solid transparent !important;`}>
                <span className={recoverTitleCard}>Cambia tu<br />respuesta secreta</span>
              </CardHeader>
              <div css={`padding-bottom:5px;`} className={` ${passwordContainer}`}>
                <input
                  type={passwordVisibility ? 'text' : 'password'}
                  placeholder='Contraseña'
                  name='passwordToAuthenticate'
                  onChange={handleInputChange}
                  value={passwordToAuthenticate}
                  required
                />
                <button
                  className={btnVisibility}
                  onClick={handleVisibility}>
                  <img
                    className={visibilityOnCSS}
                    src={passwordVisibility ? iconVisibilityOff : iconVisibilityOn}
                    alt="visibilidad"
                  />
                </button>
                <span className='highlight'></span>
                <span className='bar'></span>
              </div>
              <input
                placeholder="Respuesta secreta"
                type="text"
                onChange={handleInputChange}
                value={newAnswer}
                name="newAnswer" />
            </div>
            <AcceptButton
              loading={isLoading}
              width="164px"
              height="39px"
              type="submit"
              content="CONFIRMAR"
            />
          </div>
        </form>
      }
    </div>
  )
const EditQuestionModal = ({
  onSubmit,
  newQuestion,
  oldQuestion,
  handleQuestionSelect,
  getOptions,
  isLoading }) => (
    <div>
      {
        <form
          onSubmit={onSubmit}
          className={`${contentContainer} ${component}`}>
          <div className={deleteBankAccountModalContainer2}>
            <div style={{ marginBottom: "20px" }} className={addBankAccountModalInputsContainer}>
              <CardHeader
                className='text-center'
                css={`border-bottom: 0px solid transparent !important;`}
              >
                <span className={recoverTitleCard}>Cambia tu<br />pregunta secreta</span>
              </CardHeader>
              <div className='group mb-1'>
                <div
                  css={`padding-bottom:5px;`}
                  className={`${marginTop} ${dropDownDivContainer}`}
                >
                  <div className={dropDownDiv}>
                    <CustomDropdown
                      className={dropdownQuestionPadding}
                      value={newQuestion}
                      name='newQuestion'
                      onChange={handleQuestionSelect}
                      required
                    >
                      {/* <option value="" disabled defaultValue>{question}</option> */}
                      {
                        getOptions(securityQuestionKeys)
                      }
                    </CustomDropdown>
                    <div className={arrowContainer}>
                      <img src={arrowIcon} className={arrow} alt="dropdown" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {
              newQuestion !== oldQuestion
                ? <AcceptButton
                  loading={isLoading}
                  width="164px"
                  height="39px"
                  type="submit"
                  content="CONFIRMAR"
                />
                : null
            }
          </div>
        </form>
      }
    </div>
  )
class Profile extends Component {
  state = {
    questionKey: '',
    question: null,
    newQuestion: '',
    oldQuestion: '',
    gualyBankAccount: null,
    idnumber: '',
    IdType: '',
    commerceName: '',
    isLoading: false,
    myQrModalShow: false,
    edit: false,
    editPass: false,
    editQuestion: false,
    editSecretAnswer: false,
    passwordToChange: '',
    secretQuestionToChange: '',
    editPassModalIsOpen: false,
    passwordToAuthenticate: '',
    newPassword: '',
    passwordVisibility: false,
    passwordVisibility2: false,
    editQuestionModalIsOpen: false,
    editSecretAnswerModalIsOpen: false,
    newAnswer: '',
    myQrShareModalIsOpen: false,
    showCopy: false,
    copied: false,
    copyText:
      `¡Hola! Soy ${this.props.userInCollectionData ? this.props.userInCollectionData.name : ""}, paga con Gualy http://deeplink.me/gualy.com colocando mi correo ${this.props.userInCollectionData ? this.props.userInCollectionData.email : ""}`,
    profileImgURI: '',
    image: '',
    profileImage: '',
    errorMessage: '',
    isUserBlocked: false,
    userInfo: "",
    cropImageModal: false,
  }
  componentDidMount = () => {
    if (this.props.userInCollectionData) {
      auth.onAuthStateChanged((user) => {
        if (user) {
          const adminUid = user.uid ? user.uid : null
          const usersRef = db.ref('users').orderByChild('userKey').equalTo(adminUid.toString())
          usersRef.on('value', async (snapshot) => {
            const data = R.values(snapshot.val())
            this.setState({
              gualyBankAccount: data ? R.values(data[0].bankAccount) : '',
              profileImage: data[0].profilePicture,
              userInfo: data[0],
            })
            if (
              (this.props.userInCollectionData.type === 'client' ||
                this.props.userInCollectionData.type === 'commerce') &&
              data
            ) {
              R.values(data[0].questions)
                .map((question) => {
                  return this.setState({
                    questionKey: question.questionKey,
                    newQuestion: question.questionKey,
                    oldQuestion: question.questionKey,
                    newAnswer: question.answer
                  })
                })
              if (this.state.questionKey) {
                const userQuestionRef = db.ref('questions').child(this.state.questionKey)
                userQuestionRef.on('value', (snapshot) => {
                  R.values(snapshot.val()).map((question) => {
                    return this.setState({ question, secretQuestionToChange: question })
                  })
                })
              }
            }
          })
        }
      })
      const isUserBlockRef = db.ref(`users/${this.props.userInCollectionData.userKey}/blocked`)
      isUserBlockRef.once('value', snapshot => {
        this.setState({
          isUserBlocked: snapshot.val(),
        })
      })
    }
  }
  encodeImageFileAsURL = (element) => {
    const file = element.target.files[0]
    if (file) {
      if (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') {
        let reader = new FileReader()
        //Read the contents of Image File.
        reader.readAsDataURL(file)
        reader.onload = (e) => {
          //Initiate the JavaScript Image object.
          let image = new Image()
          //Set the Base64 string return from FileReader as source.
          image.src = e.target.result
          image.onload = (e) => {
            //Validate the File Height and Width.
            let height = e.path[0].height
            let width = e.path[0].width
            if (height < 100 || width < 100 || height > 800 || width > 800) {
              toast.error("Altura y anchura de la imagen debe ser mínimo de 100px y un máximo de 800px y preferiblemente cuadrada. ej: 200x200");
              return false
            } else {
              this.setState({
                image: reader.result,
                imageError: false,
                cropImageModal: true
              })
            }
          }
        }
      } else {
        toast.error('Formato de imagen inválido.')
      }
    }
  }
  closeCropImageModal = () => {
    this.setState({ cropImageModal: false })
  }
  onShareMyQrCodeClick = async () => {
    const title = "Compartir mi codigo"
    const text = `¡Hola! Soy ${this.props.userInCollectionData.name}, paga con Gualy http://deeplink.me/gualy.com colocando mi correo ${this.props.userInCollectionData.email}`
    const url = "http://deeplink.me/gualy.com"
    if (navigator.share === undefined) {
      customConsoleLog('Error: Unsupported feature: navigator.share')
      return this.setState({ showCopy: true })
    }
    try {
      await navigator.share({ title, text, url });
    } catch (error) {
      customConsoleLog('Error sharing: ' + error);
      return;
    }
    customConsoleLog('Successfully sent share');
  }
  handleVisibility = (e) => {
    e.preventDefault()
    this.setState({ passwordVisibility: !this.state.passwordVisibility })
  }
  handleVisibility2 = (e) => {
    e.preventDefault()
    this.setState({ passwordVisibility2: !this.state.passwordVisibility2 })
  }
  edit = () => {
    this.setState({ edit: !this.state.edit })
  }
  editQuestion = () => {
    this.setState({ editQuestion: !this.state.editQuestion })
  }
  editSecretAnswer = () => {
    this.setState({ editSecretAnswer: !this.state.editSecretAnswer })
  }
  openEditPassModal = () => {
    this.setState({
      editPassModalIsOpen: !this.state.editPassModalIsOpen,
      passwordToAuthenticate: '',
      newPassword: ''
    })
  }
  openEditSecretAnswerModal = () => {
    this.setState({
      editSecretAnswerModalIsOpen: !this.state.editSecretAnswerModalIsOpen,
      passwordToAuthenticate: '',
      newAnswer: ''
    })
  }
  openEditQuestion = () => {
    this.setState({ editQuestionModalIsOpen: !this.state.editQuestionModalIsOpen })
  }
  handleQuestionSelect = ({ target: { value }, target }) => {
    this.setState({
      [target.name]: value
    })
  }
  getOptions = items => items.map(item => {
    return <option key={item.customId} value={item.questionKey}>{item.text}</option>
  })
  resetPass = (event) => {
    const newPass = this.state.newPassword.trim()
    const oldPass = this.state.passwordToAuthenticate.trim()
    event.preventDefault();
    if (
      this.state.passwordToAuthenticate
      && this.state.newPassword
      && this.state.newPassword.match(numbers)
      && this.state.newPassword.match(letters)
    ) {
      this.setState({ isLoading: true })
      let user = auth.currentUser;

      let credentials = firebase.auth.EmailAuthProvider.credential(
        user.email,
        this.state.passwordToAuthenticate
      );
      // Prompt the user to re-provide their sign-in credentials
      user.reauthenticateWithCredential(credentials).then(function () {
        // User re-authenticated.
        if (oldPass === newPass) {
          return toast.error('Por favor ingrese una contraseña diferente')
        } else {
          user.updatePassword(newPass).then(function () {
            toast.success('Contraseña cambiada con exito')
          }).catch(function (error) {
            // An error happened.
            customConsoleLog(error)
            return toast.error('Verifique su conexión y vuelva a intentarlo.')
          })
        }
      }).catch(function (error) {
        // An error happened.
        customConsoleLog(error)
        return toast.error('Su vieja contraseña es invalida')
      });
      this.setState({ isLoading: false, passwordToAuthenticate: '', newPassword: '' })
    } else {
      if (this.state.passwordToAuthenticate.trim() === '') {
        event.target.passwordToAuthenticate.focus()
        return toast.error('Debe colocar su vieja contraseña.')
      }
      if (this.state.newPassword.trim() === '') {
        event.target.newPassword.focus()
        return toast.update(this.toastLoaderSendId, {
          render: 'Debe colocar su nueva contraseña.',
          type: toast.TYPE.ERROR,
          autoClose: 5000,
        })
      }
      if (!this.state.newPassword.trim().match(numbers)) {
        event.target.newPassword.focus()
        return toast.error('La contraseña debe contener un número entre (0-9).')
      }
      if (!this.state.newPassword.trim().match(letters)) {
        event.target.newPassword.focus()
        return toast.error('La contraseña debe contener una letra mayúscula.')
      }
    }
  }
  changeSecretAnswer = (event) => {
    event.preventDefault();
    if (this.state.passwordToAuthenticate.trim() && this.state.newAnswer.trim()) {
      this.setState({ isLoading: true })
      let user = auth.currentUser;
      const userKey = this.props.userInCollectionData.userKey
      const questionKey = this.state.questionKey
      const newPass = this.state.newAnswer
      let credentials = firebase.auth.EmailAuthProvider.credential(
        user.email,
        this.state.passwordToAuthenticate.trim()
      );
      // Prompt the user to re-provide their sign-in credentials
      user.reauthenticateWithCredential(credentials).then(function () {
        // User re-authenticated.
        db.ref(`users/${userKey}`).update({
          questions: {
            [questionKey]: {
              questionKey: questionKey,
              answer: newPass
            }
          }
        })
        toast.success('Respuesta secreta cambiada con exito')
      }).catch(function (error) {
        // An error happened.
        customConsoleLog(error)
        toast.error('Su vieja contraseña es invalida')
      });
      this.setState({ isLoading: false, passwordToAuthenticate: '' })
    } else {
      if (this.state.passwordToAuthenticate.trim() === '') {
        event.target.passwordToAuthenticate.focus()
        return toast.error('Debe colocar su contraseña.')
      }
      if (this.state.newAnswer.trim() === '') {
        event.target.newAnswer.focus()
        return toast.error('Debe colocar su nueva respuesta secreta.')
      }
    }
  }
  toastLoaderSendId2 = null
  changeSecretQuestion = () => {
    this.toastLoaderSendId2 = toast('Cambiando pregunta secreta...', { autoClose: false })
    const newQuestion = this.state.newQuestion
    const userKey = this.props.userInCollectionData.userKey
    try {
      db.ref(`users/${userKey}`).update({
        questions: {
          [newQuestion]: {
            questionKey: newQuestion
          }
        }
      })
      toast.update(this.toastLoaderSendId2, {
        render: 'Pregunta secreta cambiada con exito.',
        autoClose: 5000,
      })
      this.setState({ editQuestionModalIsOpen: false })
    } catch (error) {
      console.error(error)
      return toast.update(this.toastLoaderSendId2, {
        render: 'Verifique su conexión y vuelva a intentarlo.',
        type: toast.TYPE.ERROR,
        autoClose: 5000,
      })
    }
  }
  onCopyClick = () => {
    this.setState({ copied: true })
  }
  redirect = () => {
    if (this.props.isLoggedIn) {
      if (this.state.isUserBlocked) {
        this.props.dispatch(userLogout())
      }
      if (this.props.userInCollectionData.type) {
        const redirect =
          (
            this.props.userInCollectionData.type === 'commerce'
            || this.props.userInCollectionData.type === 'client'
          )
            ?
            null
            :
            <Redirect to="/" />
        return redirect
      }
    } else {
      return <Redirect to="/" />
    }
  }
  //SHOW MY QR CODE MODAL
  showCodeQrModal = () => {
    this.setState({ myQrModalShow: !this.state.myQrModalShow, showCopy: false })
  }
  //HANDLE INPUTS
  handleInputChange = ({ target }) => {
    const value = target.name === 'accountNumber' && target.value.length > 20 ? target.value.slice(0, -1) : target.value
    this.setState({
      [target.name]: value
    })
  }
  render() {
    customConsoleLog("this.props.userInCollectionData: ", this.props.userInCollectionData)
    return (
      <div>
        <OptionsModalWrapper
          show={this.state.editPassModalIsOpen}
          onClose={this.openEditPassModal}
          width="310px"
        >
          <EditPassModal
            onSubmit={this.resetPass}
            passwordVisibility={this.state.passwordVisibility}
            passwordVisibility2={this.state.passwordVisibility2}
            newPassword={this.state.newPassword}
            handleInputChange={this.handleInputChange}
            passwordToAuthenticate={this.state.passwordToAuthenticate}
            isLoading={this.state.isLoading}
            handleVisibility={this.handleVisibility}
            handleVisibility2={this.handleVisibility2}
          />
        </OptionsModalWrapper>
        <OptionsModalWrapper
          show={this.state.editSecretAnswerModalIsOpen}
          onClose={this.openEditSecretAnswerModal}
          width="310px"
        >
          <EditSecretAnswerModal
            onSubmit={this.changeSecretAnswer}
            passwordVisibility={this.state.passwordVisibility}
            newAnswer={this.state.newAnswer}
            handleInputChange={this.handleInputChange}
            passwordToAuthenticate={this.state.passwordToAuthenticate}
            isLoading={this.state.isLoading}
            handleVisibility={this.handleVisibility}
          />
        </OptionsModalWrapper>
        <OptionsModalWrapper
          show={this.state.editQuestionModalIsOpen}
          onClose={this.openEditQuestion}
          width="310px"
        >
          <EditQuestionModal
            onSubmit={this.changeSecretQuestion}
            question={this.state.question}
            questionKey={this.state.questionKey}
            newQuestion={this.state.newQuestion}
            oldQuestion={this.state.oldQuestion}
            handleInputChange={this.handleInputChange}
            isLoading={this.state.isLoading}
            getOptions={this.getOptions}
            handleQuestionSelect={this.handleQuestionSelect}
          />
        </OptionsModalWrapper>
        <OptionsModalWrapper
          show={this.state.myQrModalShow}
          onClose={this.showCodeQrModal}
          width="310px"
        >
          <MyQrModal
            onShareMyQrCodeClick={this.onShareMyQrCodeClick}
            copied={this.state.copied}
            onCopyClick={this.onCopyClick}
            onCopyTextChange={this.handleInputChange}
            codeQR={`userQR-${this.props.userInCollectionData ? this.props.userInCollectionData.userKey : ""}`}
            showCopy={this.state.showCopy}
            copyText={this.state.copyText}
          />
        </OptionsModalWrapper>
        <CropImageModal
          show={this.state.cropImageModal}
          onClose={this.closeCropImageModal}
          image={this.state.image}
          userKey={this.props.userInCollectionData.userKey}
        />
        <CommerceHeader />
        <div className={mainProfileContainer}>
          <div className={myProfileContainer}>
            <div className={header}>
              <h1>
                {
                  this.props.userInCollectionData
                    ? this.props.userInCollectionData.type === 'client'
                      ? 'MI PERFIL'
                      : this.props.userInCollectionData.type === 'commerce'
                        ? 'ADMINISTRADOR'
                        : null
                    : null
                }
              </h1>
            </div>
            <div
              className={editButtonContainer2}
              onClick={this.edit}
            >
              <p className={editButtonText}>Editar</p>
              <img
                src={editIcon}
                alt="EditIcon"
                className={editIconStyle2}>
              </img>
            </div>
            <div className='horizontal-divisor'></div>
            {/* {
              this.state.question ? */}
                <div className={myProfileContentContainer}>
                  <div className={myProfileImageContainer}>
                    {
                      this.props.userInCollectionData
                        ? this.state.edit && this.props.userInCollectionData.type === 'client'
                          ? <div className={proileRoundImage}>
                            <UploadBtn>
                              <img
                                src={this.state.profileImage}
                                onError={(e) => { e.target.onerror = null; e.target.src = userPicture }}
                                className={iconCameraClass}
                                alt="uploadPicture"
                              />
                              <input
                                className={inputFileHidden}
                                value={this.state.profileImgURI}
                                name='profileImgURI'
                                type='file'
                                accept="image/*"
                                onChange={this.encodeImageFileAsURL}
                                required
                              />
                            </UploadBtn>
                            <img
                              src={editIcon}
                              alt="EditIcon"
                              className={editIconStyleImages}
                            />
                          </div>
                          : <img
                            src={
                              this.props.userInCollectionData.type === 'client'
                                ? this.state.profileImage
                                : userPicture
                            }
                            onError={(e) => { e.target.onerror = null; e.target.src = userPicture }}
                            className={proileRoundImage}
                            alt="profileRoundImage">
                          </img>
                        : null
                    }
                  </div>
                  <div className={myProfileInfoContainer}>
                    <div className={myProfileInfoTextsContainer}>
                      <p className={myProfileInfoTitle}>
                        Nombre
                    </p>
                      <p className={myProfileNames}>
                        {`${this.props.userInCollectionData.firstName} ${this.props.userInCollectionData.lastName}`}
                      </p>
                    </div>
                    <div className={myProfileInfoTextsContainer}>
                      <p className={myProfileInfoTitle}>
                        Email
                      </p>
                      <p className={myProfileNames}>
                        {this.props.userInCollectionData.email}
                      </p>
                    </div>
                    <div className={myProfileInfoTextsContainer}>
                      <p className={myProfileInfoTitle}>
                        Telefono
            </p>
                      <p className={myProfileNames}>
                        {this.props.userInCollectionData.phone}
                      </p>
                    </div>
                    <div className={myProfileInfoTextsContainer}>
                      <p className={myProfileInfoTitle}>
                        Contraseña
                    </p>
                      <div className={passContainer}>
                        <p className={myProfileNames}>
                          ********
                          </p>
                        {
                          this.state.edit ?
                            <img
                              onClick={this.openEditPassModal}
                              src={editIcon}
                              alt="EditIcon"
                              className={editIconStyle}>
                            </img>
                            : null
                        }
                      </div>
                    </div>
                    <div className={myProfileInfoTextsContainer}>
                      <div className={passContainer}>
                        <p className={myProfileNamesQuestion}>
                          {
                            this.state.questionKey
                            ? securityQuestionKeys.map((item) => {
                              if (item.questionKey === this.state.questionKey) {
                                return item.text
                              } else {
                                return ''
                              }
                            })
                            : "Sin pregunta secreta."
                          }
                        </p>
                        {
                          this.state.edit ?
                            <img
                              onClick={this.openEditQuestion}
                              src={editIcon}
                              alt="EditIcon"
                              className={editIconStyle}
                            />
                            : null
                        }
                      </div>
                      <div className={passContainer} >
                        <p className={myProfileNames}>
                          ********
                          </p>
                        {
                          this.state.edit ?
                            <img
                              onClick={this.openEditSecretAnswerModal}
                              src={editIcon}
                              alt="EditIcon"
                              className={editIconStyle}
                            />
                            : null
                        }
                      </div>
                    </div>
                    {
                      this.props.userInCollectionData.type === 'client'
                        ?
                        <div className={myProfileInfoTextsContainer2}>
                          <button
                            onClick={this.showCodeQrModal}
                            className={` ${primaryButton}`}
                          >MI CÓDIGO
                          <img css={`margin-bottom: 4px;margin-left: 8px;`}
                              src={QRscan}
                              alt="qr"
                            />
                          </button>
                          <NavLink to="/imprimibles">
                            <button
                              className={` ${primaryButton2}`}>
                              IMPRIMIBLES
                            <img css={`margin-bottom: 4px;margin-left: 8px`}
                                src={printableIcon}
                                alt="printable"
                              />
                            </button>
                          </NavLink>
                        </div>
                        : null
                    }
                  </div>
                </div>
               {/* :
                <div className={spinnerContainer}> <Spinner color='white' /> </div>
            } */}
          </div>
          {
            this.props.userInCollectionData
              ? this.props.userInCollectionData.type === 'client'
                ? <AddBankAccountForm
                  bankAccount={this.state.gualyBankAccount ? R.values(this.state.gualyBankAccount) : ''}
                  userInfo={this.state.userInfo}
                />
                : null
              : null
          }
          {
            this.props.userInCollectionData
              ? this.props.userInCollectionData.type === 'commerce'
                ? <div className={myProfileContainer}>
                  <div className={header}>
                    <h1> PERFIL DEL COMERCIO </h1>
                  </div>
                  {/* <div className={editButtonContainer}>
                  <p className={editButtonText}>Editar</p>
                  <img src={editIcon} alt="EditIcon" className={editIconStyle}></img>
                </div> */}
                  <div className='horizontal-divisor'></div>
                  {/* {
                    this.state.question
                      ?  */}
                      <div className={myProfileContentContainer}>
                        <div className={myProfileImageContainer}>
                          {
                            this.props.userInCollectionData
                              ? this.state.edit
                                ? <div className={proileRoundImage}>
                                  <UploadBtn>
                                    <img
                                      src={this.state.profileImage}
                                      onError={(e) => { e.target.onerror = null; e.target.src = userPicture }}
                                      className={iconCameraClass}
                                      alt="uploadPicture"
                                    />
                                    <input
                                      className={inputFileHidden}
                                      value={this.state.profileImgURI}
                                      name='profileImgURI'
                                      type='file'
                                      accept="image/*"
                                      onChange={this.encodeImageFileAsURL}
                                      required
                                    />
                                  </UploadBtn>
                                  <img
                                    src={editIcon}
                                    alt="EditIcon"
                                    className={editIconStyleImages}
                                  />
                                </div>
                                : <img
                                  src={this.state.profileImage}
                                  onError={(e) => { e.target.onerror = null; e.target.src = userPicture }}
                                  className={proileRoundImage}
                                  alt="profileRoundImage"
                                >
                                </img>
                              : null
                          }
                        </div>
                        <div className={myProfileInfoContainer}>
                          <div className={myProfileInfoTextsContainer}>
                            <p className={myProfileInfoTitle}>
                              Nombre
                      </p>
                            <p className={myProfileNames}>
                              {this.props.userInCollectionData.name}
                            </p>
                          </div>
                          <div className={myProfileInfoTextsContainer}>
                            <p className={myProfileInfoTitle}>
                              R.I.F.
                          </p>
                            <p className={myProfileNames}>
                              {this.props.userInCollectionData.dni.type + this.props.userInCollectionData.dni.id}
                            </p>
                          </div>
                          <div className={myProfileInfoTextsContainer}>
                            <p className={myProfileInfoTitle}>
                              Email
                          </p>
                            <p className={myProfileNames}>
                              {this.props.userInCollectionData.email}
                            </p>
                          </div>
                          <div className={myProfileInfoTextsContainer}>
                            <p className={myProfileInfoTitle}>
                              Telefono
                          </p>
                            <p className={myProfileNames}>
                              {this.props.userInCollectionData.phone}
                            </p>
                          </div>
                          <div className={myProfileInfoTextsContainer}>
                            <p className={myProfileInfoTitle}>
                              Dirección
                      </p>
                            <p className={myProfileNames}>
                              {this.props.userInCollectionData.address}
                            </p>
                          </div>
                          <div className={myProfileInfoTextsContainer2}>
                            <button
                              onClick={this.showCodeQrModal}
                              className={` ${primaryButton}`}>
                              <p>
                                CÓDIGO QR
                            </p>
                              <img
                                css={`margin-bottom: 4px;margin-left: 8px;`}
                                src={QRscan}
                                alt="qr"
                              />
                            </button>
                            <NavLink to="/imprimibles">
                              <button
                                className={` ${primaryButton2}`}>
                                IMPRIMIBLES
                            <img
                                  css={`margin-bottom: 4px;margin-left: 8px`}
                                  src={printableIcon}
                                  alt="printable"
                                />
                              </button>
                            </NavLink>
                          </div>
                        </div>
                      </div>
                       {/* : <div className={spinnerContainer}> <Spinner color='white' /> </div>
                   } */}
                </div>
                : null
              : null
          }
        </div>
        {
          // this.props.userInCollectionData.type === 'client' ?
          //   <div className={mainProfileContainer}>
          //     <div className={configContainer}>
          //       <div className={header}>
          //         <h1> CONFIGURACIÓN DE MI CUENTA </h1>
          //       </div>
          //       <div className='horizontal-divisor'></div>
          //     </div>
          //   </div>
          //   : null
        }
        {
          this.props.userInCollectionData
            ? this.props.userInCollectionData.type === 'commerce'
              ? <AddBankAccountForm
                bankAccount={this.state.gualyBankAccount ? R.values(this.state.gualyBankAccount) : ''}
                userInfo={this.state.userInfo}
                readyOnly={true}
              />
              : null
            : null
        }
        <div css={`margin-bottom: 50px;`}></div>
        {
          this.redirect()
        }
        <ToastContainer
          className={{
            fontSize: "15px"
          }} />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInCollectionData: state.general.userInCollectionData,
    isLoggedIn: state.general.isLoggedIn
  }
}
export default connect(mapStateToProps)(Profile)