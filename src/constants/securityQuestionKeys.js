export const securityQuestionKeys = [
  {
    questionKey: "-L7dVjrkLpwavHFYON77",
    text: "¿Cuál era el nombre de tu escuela primaria?",
    customId: 'nescuelaPrim'
  },
  {
    questionKey: "-L7dVrC9D6AvycYJ3Ahj",
    text: "¿En qué ciudad se conocieron tus padres?",
    customId: 'ciudadPadres'
  },
  {
    questionKey: "-L7dVuXSYFpO3pRIuzzd",
    text: "¿Quién fue tu héroe de la infancia?",
    customId: 'heroeInfancia'
  },
  {
    questionKey: "-L7dVyeqAcCU-L5-z-_r",
    text: "¿Cuál es el apellido de soltera de tu abuela materna?",
    customId: 'apellidoAbuela'
  },
  {
    questionKey: "-L7dW1WSCYiLp5QKsNq5",
    text: "¿Dónde conociste a tu pareja?",
    customId: 'conocistePareja'
  },
  {
    questionKey: "-L7dW69XVXXJuE-Z9vsm",
    text: "¿A qué hora nació tu primer hijo? (hh:mm)",
    customId: 'primerHijo'
  },
  {
    questionKey: "-L7dW9qvAWL9t9UX2ip9",
    text: "¿Cuál era el apellido de tu maestro/a de 3er grado?",
    customId: 'maestroGrado'
  },
  {
    questionKey: "-L7dWDlhRDqVKb-_F35B",
    text: "Cuando eras joven, ¿Qué querías ser de grande?",
    customId: 'jovenGrande'
  },
  {
    questionKey: "-L7dWH9WoO3vWXauDy7i",
    text: "¿Cuál es el nombre de tu autor favorito?",
    customId: 'autorFavorito'
  },
  {
    questionKey: "-L7dWKfvn6JVHubnNMkA",
    text: "¿Cuál el es apellido de tu primer jefe?",
    customId: 'apellidoJefe'
  },
]