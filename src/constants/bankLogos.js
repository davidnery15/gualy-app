// import bod from './../assets/bank-logos/bankLogo_bod.png'
// import banesco from './../assets/bank-logos/bankLogo_banesco.png'
// import provincial from './../assets/bank-logos/bankLogo_provincial.png'
// import cienBanco from './../assets/bank-logos/bankLogo_100banco.png'
// import bancaAmiga from './../assets/bank-logos/bankLogo_bancamiga.png'
// import bancoActivo from './../assets/bank-logos/bankLogo_bancoActivo.png'
// import bancoAgricola from './../assets/bank-logos/bankLogo_bancoAgricolaVenezuela.png'
// import bancoBicentenario from './../assets/bank-logos/bankLogo_bancoBicentenario.png'
// import bancoCaroni from './../assets/bank-logos/bankLogo_bancoCaroni.png'
// import bancoInternacionalDesarrollo from './../assets/bank-logos/bankLogo_bancoInternacionalDesarrollo.png'
// import bancoDeVenezuela from './../assets/bank-logos/bankLogo_bancoVenezuela.png'
// import bancoCaribe from './../assets/bank-logos/bankLogo_bancaribe.png'
// import bancoDelTesoro from './../assets/bank-logos/bankLogo_bancoTesore.png'
// import bancoExterior from './../assets/bank-logos/bankLogo_bancoExterior.png'
// import mercantil from './../assets/bank-logos/bankLogo_mercantil.png'
// import nacionalCredito from './../assets/bank-logos/bankLogo_bancoNacionalCredito.png'
// import plaza from './../assets/bank-logos/bankLogo_bancoPlaza.png'
// import venezolanoCredito from './../assets/bank-logos/bankLogo_venezolanoCredito.png'
// import banfanb from './../assets/bank-logos/bankLogo_banfanb.png'
// import bangente from './../assets/bank-logos/bankLogo_bangente.png'
// import banplus from './../assets/bank-logos/bankLogo_banplus.png'
// import citibank from './../assets/bank-logos/bankLogo_citibank.png'
// import delsur from './../assets/bank-logos/bankLogo_bancoDelsur.png'
// import fondoComun from './../assets/bank-logos/bankLogo_bfc.png'
// import mibanco from './../assets/bank-logos/bankLogo_miBanco.png'
// import sofitasa from './../assets/bank-logos/bankLogo_bancoSofitasa.png'
// import visa from './../assets/bank-logos/logo_visa.png'
// import master from './../assets/bank-logos/logo_master_card.png'

const bod = require('../assets/bank-logos/bankLogo_bod.png')
const banesco = require('../assets/bank-logos/bankLogo_banesco.png')
const provincial = require('../assets/bank-logos/bankLogo_provincial.png')
const cienBanco = require('../assets/bank-logos/bankLogo_100banco.png')
const bancaAmiga = require('../assets/bank-logos/bankLogo_bancamiga.png')
const bancoActivo = require('../assets/bank-logos/bankLogo_bancoActivo.png')
const bancoAgricola = require('../assets/bank-logos/bankLogo_bancoAgricolaVenezuela.png')
const bancoBicentenario = require('../assets/bank-logos/bankLogo_bancoBicentenario.png')
const bancoCaroni = require('../assets/bank-logos/bankLogo_bancoCaroni.png')
const bancoInternacionalDesarrollo = require('../assets/bank-logos/bankLogo_bancoInternacionalDesarrollo.png')
const bancoDeVenezuela = require('../assets/bank-logos/bankLogo_bancoVenezuela.png')
const bancoCaribe = require('../assets/bank-logos/bankLogo_bancaribe.png')
const bancoDelTesoro = require('../assets/bank-logos/bankLogo_bancoTesore.png')
const bancoExterior = require('../assets/bank-logos/bankLogo_bancoExterior.png')
const mercantil = require('../assets/bank-logos/bankLogo_mercantil.png')
const nacionalCredito = require('../assets/bank-logos/bankLogo_bancoNacionalCredito.png')
const plaza = require('../assets/bank-logos/bankLogo_bancoPlaza.png')
const venezolanoCredito = require('../assets/bank-logos/bankLogo_venezolanoCredito.png')
const banfanb = require('../assets/bank-logos/bankLogo_banfanb.png')
const bangente = require('../assets/bank-logos/bankLogo_bangente.png')
const banplus = require('../assets/bank-logos/bankLogo_banplus.png')
const citibank = require('../assets/bank-logos/bankLogo_citibank.png')
const delsur = require('../assets/bank-logos/bankLogo_bancoDelsur.png')
const fondoComun = require('../assets/bank-logos/bankLogo_bfc.png')
const mibanco = require('../assets/bank-logos/bankLogo_miBanco.png')
const sofitasa = require('../assets/bank-logos/bankLogo_bancoSofitasa.png')
// const visa = require('../assets/bank-logos/logo_visa.png')
// const master = require('../assets/bank-logos/logo_master_card.png')

export const banks = [
  {
    id: "0116",
    photo: bod,
  },
  {
    id: "0108",
    photo: provincial,
  },
  {
    id: "0134",
    photo: banesco
  },
  {
    id: "0156",
    photo: cienBanco
  },
  {
    id: "0172",
    photo: bancaAmiga
  },
  {
    id: "0171",
    photo: bancoActivo
  },
  {
    id: "0166",
    photo: bancoAgricola
  },
  {
    id: "0175",
    photo: bancoBicentenario
  },
  {
    id: "0128",
    photo: bancoCaroni
  },
  {
    id: "0102",
    photo: bancoDeVenezuela
  },
  {
    id: "0114",
    photo: bancoCaribe
  },
  {
    id: "0163",
    photo: bancoDelTesoro
  },
  {
    id: "0115",
    photo: bancoExterior
  },
  {
    id: "0173",
    photo: bancoInternacionalDesarrollo
  },
  {
    id: "0105",
    photo: mercantil
  },
  {
    id: "0191",
    photo: nacionalCredito
  },
  {
    id: "0138",
    photo: plaza
  },
  {
    id: "0104",
    photo: venezolanoCredito
  },
  {
    id: "0177",
    photo: banfanb
  },
  {
    id: "0146",
    photo: bangente
  },
  {
    id: "0174",
    photo: banplus
  },
  {
    id: "0190",
    photo: citibank
  },
  {
    id: "0157",
    photo: delsur
  },
  {
    id: "0151",
    photo: fondoComun
  },
  {
    id: "0169",
    photo: mibanco
  },
  {
    id: "0137",
    photo: sofitasa
  }
]

export const banksNames = {
  "0116": 'BOD',
  "0108": 'Provincial',
  "0134": 'Banesco',
  "0156": '100% Banco',
  "0172": 'Banca Amiga',
  "0171": 'Banco Activo',
  "0166": 'Banco Agricola',
  "0175": 'Banco Bicentenario',
  "0128": 'Banco Caroni',
  "0102": 'Banco De Venezuela',
  "0114": 'Banco Caribe',
  "0163": 'Banco Del Tesoro',
  "0115": 'Banco Exterior',
  "0173": 'Banco Internacional Desarrollo',
  "0105": 'Mercantil',
  "0191": 'Nacional de Crédito',
  "0138": 'Plaza',
  "0104": 'Venezolano de Crédito',
  "0177": 'Banfanb',
  "0146": 'Bangente',
  "0174": 'Banplus',
  "0190": 'Citibank',
  "0157": 'Delsur',
  "0151": 'Fondo Común',
  "0169": 'Mi Banco',
  "0137": 'Sofitasa'
}

export const getBankName = id => banksNames[id] || 'Banco no encontrado'
