import { PROD } from ".";

export const CLOUD_FUNCTIONS_URL = PROD
  ? 'https://us-central1-gualy-b39cb.cloudfunctions.net/gualyBack'
  : 'https://us-central1-gualy-dev.cloudfunctions.net/gualyBack'
// export const CLOUD_FUNCTIONS_URL = 'http://localhost:5000/gualy-b39cb/us-central1/gualyBack'