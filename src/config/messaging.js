import 'firebase/messaging'
import firebase from 'firebase/app'
import { publicVapidKey } from './index'
let messaging = null
if (firebase.messaging.isSupported()) {
messaging = firebase.messaging()
messaging.usePublicVapidKey(publicVapidKey)
}
export { messaging }