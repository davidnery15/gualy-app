import '@firebase/messaging'
import { toast } from 'react-toastify'
import { db, auth } from './firebase'
import {
  transactionHistorylimitIndex,
  moneyRequestTransactionslimitIndex,
} from './config/tablesIndexes'
import {
  getApprovedHistoryTransactions,
  getRejectedHistoryTransactions,
  getMoneyRequestTransactions,
} from './redux/actions/general'
import store from './redux/store'
import { customConsoleLog } from './utils/customConsoleLog'
import { messaging } from './config/messaging' // Retrieve Firebase Messaging object.

// Get Instance ID token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
export const requestPermission = () => {
  if (messaging){
    messaging.requestPermission().then(() => {
      customConsoleLog('Notification permission granted.')
      updateToken()
    }).catch(err => {
      customConsoleLog('Unable to get permission to notify.', err)
    })
  } else {
    customConsoleLog('This browser doesnt support the APIs required to use the firebase SDK. ')
  }
}
// Delete Instance ID token.
export const deleteToken = () => {
  if (messaging){
    messaging.getToken().then(currentToken => {
    messaging.deleteToken(currentToken).then(() => {
        customConsoleLog('Token deleted.')
        updateToken()
      }).catch(err => {
        customConsoleLog('Unable to delete token. ', err)
      })
    }).catch(err => {
      customConsoleLog('Error retrieving Instance ID token. ', err)
    })
  }
}

//Update FCMtoken from the user collection
const updateToken = () => {
  if (messaging){
    messaging.getToken().then(currentToken => {
    if (currentToken) {
        auth.onAuthStateChanged(user => {
          if (user) {
            const userKey = user.uid
            db.ref(`users/${userKey}`).update({
              FCMToken: currentToken
            })
          }
        })
        customConsoleLog("currentToken: ", currentToken)
      } else {
        customConsoleLog('No Instance ID token available. Request permission to generate one.')
      }
    }).catch(err => {
      customConsoleLog('An error occurred while retrieving token. ', err)
    })
  }
}
if(messaging){
  // Callback fired if Instance ID token is updated.
  messaging.onTokenRefresh(() => {
    messaging.getToken().then(refreshedToken => {
      customConsoleLog('Token refreshed: ', refreshedToken)
      auth.onAuthStateChanged(user => {
        if (user) {
          const userKey = user.uid
          db.ref(`users/${userKey}`).update({
            FCMToken: refreshedToken
          })
        }
      })
    }).catch(err => {
      customConsoleLog('Unable to retrieve refreshed token ', err)
    })
  })
  // Handle incoming messages. Called when:
  // - a message is received while the app has focus
  // - the user clicks on an app notification created by a service worker
  //   `messaging.setBackgroundMessageHandler` handler.
  messaging.onMessage(payload => {
    customConsoleLog('Message received. ', payload)
    auth.onAuthStateChanged(user => {
      if (user) {
        const userKey = user.uid
        //SHOW TOAST
        if (
          payload.data.type === "receiveMoney" ||
          payload.data.type === "processedWithdraw" ||
          payload.data.type === "processedAddMoney" ||
          payload.data.type === "request_payment"
        ) {
          toast(payload.notification.body)
        }
        //UPDATE TABLES
        if (
          payload.data.type === "receiveMoney" ||
          // payload.data.type === "sendMoney" ||
          payload.data.type === "processedWithdraw" ||
          payload.data.type === "processedAddMoney" ||
          payload.data.type === "rejectRequestPayment"
        ) {
          store.dispatch(getApprovedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
          store.dispatch(getRejectedHistoryTransactions({ userKey, transactionHistorylimitIndex }))
        }
        if (
          payload.data.type === "request_payment"
        ) {
          store.dispatch(getMoneyRequestTransactions({ userKey, moneyRequestTransactionslimitIndex }))
        }
      }
    })
  })
}

