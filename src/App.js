import React, { Component } from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import Idle from 'react-idle'
// GENERAL
import { userLogout } from './redux/actions/general'
import { connect } from 'react-redux'
import MessageModal from './components/general/modals/MessageModal'
import Login from './components/general/login/Login'
import Validate from './components/general/Validate'
import ResetCommercePassword from './components/general/ResetCommercePassword'
import ChangePassword from './components/general/ChangePassword'
import ChangeSecurityPassword from './components/general/ChangeSecurityPassword'
import ForgotPassword from './components/general/login/ForgotPassword'
import Register from './components/general/register/Register'
// import ConfirmEmail from './components/confirm-email/ConfirmEmail'
// ADMIN PANEL
import Dashboard from './components/admin-panel/dashboard/Dashboard'
import Commerce from './components/admin-panel/commerce/Commerce'
import Users from './components/admin-panel/users/Users'
import Transactions from './components/admin-panel/transactions/Transactions'
import Notifications from './components/admin-panel/notifications/Notifications'
import Issues from './components/admin-panel/issues/Issues'
import Reportes from './components/admin-panel/reportes'
import TopUsuarios from './components/admin-panel/reportes/TopUsuarios'
import TopComercios from './components/admin-panel/reportes/TopComercios'
import ListaNegraUsuarios from './components/admin-panel/reportes/ListaNegraUsuarios'
import ListaNegraTransf from './components/admin-panel/reportes/ListaNegraTransf'
import EditUser from './components/admin-panel/edit-user/EditUser'
// import HistoryComponent from './components/commerce-panel/commerce-dashboard/commerce-history/History.js'
import Profile from './components/commerce-client-panel/commerce-client-dashboard/commerce-client-profile/Profile.js'
import HelpSection from './components/commerce-client-panel/commerce-client-dashboard/help-section/HelpSection.js'
import Printable from './components/commerce-client-panel/commerce-client-dashboard/printable/Printable'
import CommercesDashboard from './components/commerce-client-panel/commerce-client-commerces/CommercesDashboard'
// import ForgotPassword from './components/login/ForgotPassword'
// import ChangePassword from './components/change-password/ChangePassword'
// import ConfirmEmail from './components/confirm-email/ConfirmEmail'
// COMMERCE PANEL
import CommerceDashboard from './components/commerce-client-panel/commerce-client-dashboard/CommerceClientDashboard'
import CommerceHistory from './components/commerce-client-panel/commerce-client-history/CommerceClientHistory'
import { db } from './firebase'

const history = createBrowserHistory()
class App extends Component {
  state = {
    visible: false,
    inactivityThreshold: "",
  }
  componentDidMount = () => {
    const systemSettingsRef = db.ref(`systemSettings/inactivityThreshold`)
    systemSettingsRef.on('value', snapshot => {
      this.setState({
        inactivityThreshold: snapshot.val(),
      })
    })
  }
  VisibleMessageModal = () => {
    this.setState({ visible: true })
  }
  InvisibleMessageModal = () => {
    this.setState({ visible: false })
  }
  render() {
    const { isLoggedIn } = this.props
    return (
      <Idle
        timeout={this.state.inactivityThreshold || 600000}
        // timeout={5000000}
        onChange={({ idle }) => {
          sessionStorage.setItem('idle', idle)
          if (isLoggedIn) {
            if (idle) {
              this.VisibleMessageModal()
              setTimeout(() => {
                if (sessionStorage.getItem('idle') === 'true') {
                  this.InvisibleMessageModal()
                  this.props.dispatch(userLogout())
                }
              }, 30000);
            }
          }
        }}
        render={() =>
          <div>
            <MessageModal
              title='INACTIVIDAD'
              message='Se ha detectado inactividad. En 30 segundos se cerrará su sesión'
              onClick={this.InvisibleMessageModal} visible={this.state.visible
              } />
            <Router history={history}>
              <Switch>
                {/* GENERAL */}
                <Route exact path='/' component={Login} />
                <Route path='/validacion' component={Validate} />
                <Route path='/nueva-contrasena' component={ResetCommercePassword} />
                <Route path='/olvido-contrasena' component={ForgotPassword} />
                <Route path='/reseteo-contrasena' component={ChangePassword} />
                <Route path='/reseteo-contrasena-especial' component={ChangeSecurityPassword} />
                {/* ADMIN */}
                <Route path='/dashboard-admin' component={Dashboard} />
                <Route path='/comercios' component={Commerce} />
                <Route path='/usuarios' component={Users} />
                <Route path='/transacciones' component={Transactions} />
                <Route path='/notificaciones' component={Notifications} />
                <Route path='/editar-usuario' component={EditUser} />
                {/* <Route path='/confirm-email/:id' component={ConfirmEmail} /> */}
                <Route path='/incidencias' component={Issues} />
                {/* COMERCIOS */}
                {/* <Route path='/dashboard-comercio' component={CommerceDashboard} /> */}
                <Route path='/comercio-historial-pagos' component={CommerceHistory} />
                <Route path='/reportes' component={Reportes} />
                <Route path='/balance' render={() =>
                  <div>Balance</div>
                } />
                <Route path='/top-usuarios' component={TopUsuarios} />
                <Route path='/top-comercios' component={TopComercios} />
                <Route path='/lista-negra-usuarios' component={ListaNegraUsuarios} />
                <Route path='/lista-negra-transferencias' component={ListaNegraTransf} />
                <Route path='/dashboard' component={CommerceDashboard} />
                <Route path='/historial' component={CommerceHistory} />
                <Route path='/ayuda' component={HelpSection} />
                <Route path='/directorio-comercios' component={CommercesDashboard} />
                <Route path='/perfil' component={Profile} />
                <Route path='/imprimibles' component={Printable} />
                <Route path='/registro' component={Register} />
                <Route path='/' component={Dashboard} />
              </Switch>
            </Router>
          </div>
        }
      />
    );
  }
}
const mapStateToProps = (state) => ({ isLoggedIn: state.general.isLoggedIn })
export default connect(mapStateToProps)(App)