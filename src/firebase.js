import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import { PROD } from './config'


const config = {
  apiKey: "AIzaSyCt5mxGdtaMEzPxHIzs675RvuUQaJs4mvo",
  authDomain: "gualy-b39cb.firebaseapp.com",
  databaseURL: "https://gualy-b39cb.firebaseio.com",
  projectId: "gualy-b39cb",
  storageBucket: "gualy-b39cb.appspot.com",
  messagingSenderId: "824355529309"
}

const devConfig = {
  apiKey: "AIzaSyB3dYWITHYydWTk-OLBvGNj_qOr5JFztqE",
  authDomain: "gualy-dev.firebaseapp.com",
  databaseURL: "https://gualy-dev.firebaseio.com",
  projectId: "gualy-dev",
  storageBucket: "gualy-dev.appspot.com",
  messagingSenderId: "990374764736"
}
firebase.initializeApp( PROD ? config : devConfig )

export default firebase
export const auth = firebase.auth()
export const db = firebase.database()
export const fireDatabase = firebase.database().ref()

